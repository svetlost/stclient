﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Threading;
using System.Xml.Linq;

namespace LogAlertHB
{
    public class Log
    {
        DispatcherTimer BackupLogTimer = new DispatcherTimer();
        static EventLog _eventLog;
        //string logName = "";
        string user = "";
        //string LogPath = "";
        string SaveBackupPath = "";
        static string detailedEntry = "";
        //long EventLogMaximumKilobytes;
        //FileInfo fi = null;

        #region old log 20140214
        //public Log(string logName, double BackupLogTimerIntervalMinutes, string user, long EventLogMaximumKilobytes, string LogPath, string SaveBeckupPath)
        //{
        //    _eventLog = new EventLog();
        //    this.logName = (logName == "" ? "no log name defined" : logName); ;
        //    this.user = (user == "" ? "no user defined" : user); // todo : ovde ifovi
        //    this.LogPath = (LogPath == "" ? "no path defined" : LogPath);
        //    this.EventLogMaximumKilobytes = EventLogMaximumKilobytes;
        //    this.SaveBeckupPath = SaveBeckupPath;

        //    if (!EventLog.SourceExists(this.user))
        //        EventLog.CreateEventSource(this.user, logName);

        //    if (!System.IO.Directory.Exists(SaveBeckupPath))
        //        System.IO.Directory.CreateDirectory(SaveBeckupPath);

        //    //EVENTLOG SET
        //    _eventLog.Source = this.user;
        //    _eventLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 1);
        //    _eventLog.MaximumKilobytes = EventLogMaximumKilobytes - (EventLogMaximumKilobytes % 64);

        //    //BACKUP TIMER SET
        //    BackupLogTimer.Interval = TimeSpan.FromMinutes(BackupLogTimerIntervalMinutes);
        //    BackupLogTimer.Tick += new EventHandler(BackupLog);
        //    //BackupLogTimer.Start();

        //    Write("Log started.");
        //    AlertLogic.Add("Log started.");
        //}

        //public static void Write(string entry)
        //{
        //    _eventLog.WriteEntry(entry);
        //}
        #endregion

        public Log(double BackupLogTimerIntervalMinutes, string user, string SaveBackupPath)
        {
            _eventLog = new EventLog();
            this.user = user;
            //this.logName = (logName == "" ? "no log name defined" : logName); ;
            //this.user = (user == "" ? "no user defined" : user); // todo : ovde ifovi
            //this.user = "ST_CLIENT";
            //this.LogPath = (LogPath == "" ? "no path defined" : LogPath);
            //this.EventLogMaximumKilobytes = EventLogMaximumKilobytes;
            this.SaveBackupPath = SaveBackupPath;

            //if (!EventLog.SourceExists(this.user))
            //    EventLog.CreateEventSource(this.user, logName);

            if (!System.IO.Directory.Exists(SaveBackupPath))
                System.IO.Directory.CreateDirectory(SaveBackupPath);

            //EVENTLOG SET
            _eventLog.Source = this.user;
            //_eventLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 1);
            //_eventLog.MaximumKilobytes = EventLogMaximumKilobytes - (EventLogMaximumKilobytes % 64);

            //BACKUP TIMER SET
            BackupLogTimer.Interval = TimeSpan.FromMinutes(BackupLogTimerIntervalMinutes);
            BackupLogTimer.Tick += new EventHandler(BackupLog);
            BackupLogTimer.Start();

            Write("Log started.", EventLogEntryType.Information);
            AlertLogic.Add("Log started.");
        }

        public static void Write(string entry, EventLogEntryType type)
        {
            //_eventLog.WriteEntry(entry);
            //_eventLog.WriteEntry(entry, EventLogEntryType.Information, 719);
            _eventLog.WriteEntry(entry, type);
        }

        public static void WriteDetailed(string entry)
        {
            detailedEntry += string.Format("{0:yyyyMMdd_HH:mm:ss.ffffff} \t {1} \n", DateTime.Now, entry);

        }


        System.Threading.Thread _logThread;
        void BackupLog(object sender, EventArgs e)
        {
            //BackupLog2();
            System.Threading.ThreadStart threadDelegate = new System.Threading.ThreadStart(BackupLog2);
            _logThread = new System.Threading.Thread(threadDelegate);
            _logThread.IsBackground = true;
            _logThread.Start();
            _logThread.Name = "stBkpThread";
        }


        void BackupLog2()
        {
            try
            {

                DirectoryInfo directoryInfo = new DirectoryInfo(SaveBackupPath);
                string s = DateTime.Now.ToString("yyyyMMdd") + ".zip";
                //string s = DateTime.Now.ToString("yyyyMMdd") + ".xml";
                //foreach (FileInfo fileinfo in directoryInfo.GetFiles("*.xml"))
                foreach (FileInfo fileinfo in directoryInfo.GetFiles("*.zip"))
                    if (fileinfo.Name == s) return;

                var query = from EventLogEntry entry in _eventLog.Entries
                            orderby entry.TimeGenerated descending
                            select entry;

                var xml = new XDocument(
                    new XElement("Log",
                        from EventLogEntry entry in _eventLog.Entries
                        orderby entry.TimeGenerated descending
                        select new XElement("Log",
                               new XElement("Message", entry.Message),
                               new XElement("TimeGenerated", entry.TimeGenerated),
                               new XElement("Source", entry.Source),
                               new XElement("Category", entry.Category),
                               new XElement("CategoryID", entry.CategoryNumber),
                               new XElement("EntryType", entry.EntryType.ToString())
                        )
                      )
                    );

                //xml.Save(SaveBackupPath + DateTime.Now.ToString("yyyyMMdd") + ".xml");

                Stream stream = new MemoryStream();
                xml.Save(stream);
                stream.Position = 0;

                string fullFIleName = SaveBackupPath + DateTime.Now.ToString("yyyyMMdd") + ".zip";

                using (FileStream zipToOpen = new FileStream(fullFIleName, FileMode.Create))
                {
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Create))
                    {
                        ZipArchiveEntry readmeEntry = archive.CreateEntry(DateTime.Now.ToString("yyyyMMdd") + ".xml");
                        xml.Save(readmeEntry.Open());
                    }
                }

                //_eventLog.Clear();
                Write("Log backup performed.", EventLogEntryType.Information);
            }

            catch (Exception ex)
            {
                Log.Write("Error writing log backup: " + ex.Message, EventLogEntryType.Information);
                //MessageBox.Show(SaveBackupPath + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml" + ": " + ex.Message);
            }
        }
    }
}

