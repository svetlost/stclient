﻿namespace LogAlertHB
{
    //celu klasu sklonio darko (mursob) 17 02 2011 zato sto treba natenane da se iztestira
    //public class Heartbeat
    //{

    //    public delegate void StopAll();

    //    IPEndPoint _localIpEp, _remoteIpEp;
    //    StopAll _onStopAll;
    //    Socket socketTx, socketRx;
    //    DispatcherTimer timerTx = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(50) };
    //    DispatcherTimer timerRx = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(300) };
    //    string _localName, _remoteName;
    //    public bool UDPInitialized = true;

    //    bool CurrentConnectedStatus = false;
    //    bool PreviousConnectedStatus = false;

    //    byte[] bufferRx = new byte[] { Convert.ToByte(false) };

    //    bool heartbeatStarted = false;
    //    bool HeartbeatStarted
    //    {
    //        get { return heartbeatStarted; }
    //        set
    //        {
    //            if (heartbeatStarted != value)
    //            {
    //                heartbeatStarted = value;
    //                AlertLogic.Add(_localName + (heartbeatStarted ? " heartbeat started" : " heartbeat stopped"));
    //            }
    //        }
    //    }

    //    public void HeartBeatStop()
    //    {
    //        timerTx.Stop();
    //        timerRx.Stop();
    //    }

    //    bool remoteIsPresent = false;
    //    bool RemoteIsPresent
    //    {
    //        get { return remoteIsPresent; }
    //        set
    //        {
    //            if (remoteIsPresent != value)
    //            {
    //                remoteIsPresent = value;
    //                AlertLogic.Add(_remoteName + (remoteIsPresent ? " present" : " not present"));
    //            }
    //        }
    //    }

    //    public Heartbeat(IPEndPoint localIpEp, string localName, IPEndPoint remoteIpEp, string remoteName, StopAll onStopAll)
    //    {
    //        _localIpEp = localIpEp;
    //        _remoteIpEp = remoteIpEp;
    //        _localName = localName;
    //        _remoteName = remoteName;
    //        _onStopAll = new StopAll(onStopAll);

    //        try
    //        {
    //            socketTx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    //            socketRx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    //        }
    //        catch (Exception ex)
    //        {
    //            AlertLogic.Add("Socket Exception :" + ex.Message);
    //            UDPInitialized = false;
    //        }

    //        try
    //        {
    //            socketRx.Bind((EndPoint)localIpEp);
    //            AlertLogic.Add("HeartBit Initialized, Server IP :" + localIpEp.Address + ", Port :" + localIpEp.Port);
    //            AlertLogic.Add("Client IP :" + _remoteIpEp.Address + ", Port :" + _remoteIpEp.Port);
    //        }
    //        catch (Exception ex)
    //        {
    //            AlertLogic.Add("UDP ERROR :" + ex.Message);
    //            UDPInitialized = false;
    //        }
    //    }

    //    public void StartServer()
    //    {
    //        heartbeatStarted = true;
    //        timerTx.Tick += new EventHandler(timerTx_Tick);
    //        timerTx.Start();

    //        timerRx.Tick += new EventHandler(timerRx_Tick);
    //        timerRx.Start();
    //    }

    //    public void StopServer()
    //    {
    //        heartbeatStarted = false;
    //    }

    //    void timerRx_Tick(object sender, EventArgs e)
    //    {
    //        bufferRx = new byte[1];

    //        try
    //        {
    //            if (socketRx.Available == 0)
    //            {
    //                remoteIsPresent = false;

    //                if (CurrentConnectedStatus == false)
    //                    return;    //all ok, do nothing
    //                else
    //                {
    //                    AlertLogic.Add("link error. stop all.");
    //                    _onStopAll();
    //                    CurrentConnectedStatus = false;
    //                    return;
    //                }
    //            }
    //            else
    //            {
    //                remoteIsPresent = false;

    //                while (socketRx.Available > 0)
    //                {
    //                    try
    //                    {
    //                        socketRx.BeginReceive(bufferRx, 0, bufferRx.Length, SocketFlags.None, new AsyncCallback(connTest), socketRx);

    //                        if (PreviousConnectedStatus == CurrentConnectedStatus)
    //                            continue;
    //                        else
    //                            if (CurrentConnectedStatus)
    //                                AlertLogic.Add("normal connect by " + _remoteName);
    //                            else
    //                                AlertLogic.Add("normal disconnect by " + _remoteName);

    //                        PreviousConnectedStatus = CurrentConnectedStatus;
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        MessageBox.Show(ex.Message);
    //                    }
    //                }
    //            }
    //        }
    //        catch (ArgumentException e1)
    //        {
    //            AlertLogic.Add("ArgumentException " + e1.Message);
    //        }
    //        catch (ObjectDisposedException e1)
    //        {
    //            AlertLogic.Add("ObjectDisposedException " + e1.Message);
    //        }
    //        catch (SocketException e1)
    //        {
    //            AlertLogic.Add("SocketException " + e1.Message);
    //        }
    //        catch (InvalidOperationException e1)
    //        {
    //            AlertLogic.Add("InvalidOperationException " + e1.Message);
    //        }
    //        catch (NullReferenceException e1)
    //        {
    //            AlertLogic.Add("NullReferenceException " + e1.Message);
    //        }
    //        catch (Exception e1)
    //        {
    //            AlertLogic.Add("Exception " + e1.Message);
    //        }
    //    }

    //    void connTest(IAsyncResult iar)
    //    {
    //        CurrentConnectedStatus = Convert.ToBoolean(bufferRx[0]);
    //        socketRx.EndReceive(iar);
    //    }

    //    void timerTx_Tick(object sender, EventArgs e)
    //    {
    //        try
    //        {
    //            socketTx.BeginSendTo(new byte[] { Convert.ToByte(heartbeatStarted) }, 0, 1, SocketFlags.None, _remoteIpEp, new AsyncCallback(SendToCB), null);
    //        }
    //        catch (Exception e1) { AlertLogic.Add("socket send error :" + e1.Message); }
    //    }
    //    void SendToCB(IAsyncResult ar)
    //    {
    //        try
    //        { socketTx.EndSendTo(ar); }
    //        catch (Exception ex)
    //        { MessageBox.Show(ex.Message); }
    //    }


    //}
}
