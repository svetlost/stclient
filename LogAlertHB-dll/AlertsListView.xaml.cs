﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace LogAlertHB
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class AlertsListView : UserControl, INotifyPropertyChanged
    {

        int CellingNumberOfMessages = 50;
        int NumberOfMessagesAfterCut;

        void LV_LayoutUpdated(object sender, EventArgs e)
        {
            try
            {
                if (AlertLogic.AlertsGet.Count > CellingNumberOfMessages)
                    for (int i = 0; i < NumberOfMessagesAfterCut; i++)
                        AlertLogic.AlertsGet.Remove(AlertLogic.AlertsGet[i]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);//20150813 sklonio, mislim da ponekad izbacuje exception. staviti ove operacija da idu kroz dispatcher
            }
        }

        void AlertsGet_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {


                if (!(LV.Items.IsEmpty))
                {
                    LV.ScrollIntoView(LV.Items[LV.Items.Count - 1]);
                }

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);//20150813 sklonio, mislim da ponekad izbacuje exception. staviti ove operacija da idu kroz dispatcher
            }
        }

        public AlertsListView(double height)
        {
            InitializeComponent();
            this.Height = height;

            NumberOfMessagesAfterCut = CellingNumberOfMessages - (CellingNumberOfMessages / 2);

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {

                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                {
                    LV.ItemsSource = AlertLogic.AlertsGet;
                    AlertLogic.AlertsGet.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(AlertsGet_CollectionChanged);
                    LV.LayoutUpdated += new EventHandler(LV_LayoutUpdated);
                });
            }

        }
        public AlertsListView()
        {
            InitializeComponent();
            this.VerticalAlignment = VerticalAlignment.Stretch;

            NumberOfMessagesAfterCut = CellingNumberOfMessages - (CellingNumberOfMessages / 2);

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {

                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                {
                    LV.ItemsSource = AlertLogic.AlertsGet;
                    AlertLogic.AlertsGet.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(AlertsGet_CollectionChanged);
                    LV.LayoutUpdated += new EventHandler(LV_LayoutUpdated);
                });
            }

        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion




        private void LV_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //MessageBoxResult result = MessageBox.Show("Clear Messages", "CLEAR", MessageBoxButton.YesNo);
            //switch (result)
            //{
            //    case MessageBoxResult.Yes:
            //        AlertLogic.AlertsGet.Clear();
            //        break;
            //    case MessageBoxResult.No:
            //        break;
            //}
        }
    }



}
