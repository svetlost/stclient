﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace StClient
{
    /// <summary>
    /// Interaction logic for CueListView.xaml
    /// </summary>
    public partial class CueListView : UserControl
    {

        PopupWindows popups = new PopupWindows();
        private static volatile CueListView instance;
        private static object syncRoot = new Object();

        DispatcherTimer timer = new DispatcherTimer();

        public static CueListView Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new CueListView();
                    }
                }

                return instance;
            }
        }

        private CueListView()
        {
            InitializeComponent();
            lv.SelectionChanged += new SelectionChangedEventHandler(CueLogic.UniqueInstance.lv_SelectionChanged);
            lv.DataContext = CueLogic.UniqueInstance._sortedcvs;

            timer.Tick += timer_Tick2;
            timer.Interval = TimeSpan.FromMilliseconds(300);
            timer.Start();

            //Binding b = new Binding();
            //b.Source = this;
            //b.Path = new PropertyPath("selectedRow");
            //lv.SetBinding(ListView.SelectedItemProperty, b);
        }

        void timer_Tick(object sender, EventArgs e)
        {

            //   todo 20131012    pravljenje da sarajuj boje razliciti stimunzi

            if (lv.Items.Count == 0) return;
            int rowCount = lv.Items.Count;

            int tempCueNum = (lv.Items[0] as CueItem).CueNo;
            Brush brush = Brushes.LightBlue;

            for (int i = 0; i < rowCount; i++)
            {
                if ((lv.Items[i] as CueItem).CueNo != tempCueNum)
                {
                    if (brush == Brushes.LightBlue) brush = Brushes.LightCyan;
                    else brush = Brushes.LightBlue;

                    tempCueNum = (lv.Items[i] as CueItem).CueNo;
                }

                //ListViewItem lvi = lv.Items[i] as ListViewItem;
                ListViewItem lvi = lv.ItemContainerGenerator.ContainerFromIndex(i) as ListViewItem;
                if (lvi == null) continue;
                lvi.Background = brush;
                //(lv.Items[i] as ListViewItem).Background = brush;

            }

        }
        void timer_Tick2(object sender, EventArgs e)
        {

            //   todo 20131012    pravljenje da sarajuj boje razliciti stimunzi

            if (lv.Items.Count == 0) return;
            int rowCount = lv.Items.Count;

            int tempCueNum = (lv.Items[0] as CueItem).CueNo;

            Brush brush = Brushes.LightBlue;

            for (int i = 0; i < rowCount; i++)
            {
                if ((lv.Items[i] as CueItem).CueNo != tempCueNum)
                {
                    if (brush == Brushes.LightBlue) brush = Brushes.LightCyan;
                    else brush = Brushes.LightBlue;
                }

                //if ((lv.Items[i] as CueItem).CueNo == CueLogic.UniqueInstance.currentCueNo)
                //{
                //    brush = Brushes.LightPink;
                //}
                tempCueNum = (lv.Items[i] as CueItem).CueNo;
                //ListViewItem lvi = lv.Items[i] as ListViewItem;
                ListViewItem lvi = lv.ItemContainerGenerator.ContainerFromIndex(i) as ListViewItem;
                if (lvi == null) continue;
                lvi.Background = brush;
                //(lv.Items[i] as ListViewItem).Background = brush;
            }

            for (int i = 0; i < rowCount; i++)
            {
                if ((lv.Items[i] as CueItem).CueNo != CueLogic.UniqueInstance.currentCueNo)
                    continue;

                brush = Brushes.LightPink;

                tempCueNum = (lv.Items[i] as CueItem).CueNo;
                ListViewItem lvi = lv.ItemContainerGenerator.ContainerFromIndex(i) as ListViewItem;
                if (lvi == null) continue;
                lvi.Background = brush;
            }


        }

        private void EditBoxSP_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popups.SP_Popup((lv.SelectedItem as CueItem).Motor.toList(), sender as TextBlock, true);
        }
        private void EditBoxSV_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popups.SVMV_Popup((lv.SelectedItem as CueItem).Motor.toList(), sender as TextBlock, true, ShowPercButtons.Show);
        }
        private void EditBoxDescription_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popups.PopupCommand(sender as TextBlock, true);
        }

        //void rowBackground()
        //{

        //}
    }
}
