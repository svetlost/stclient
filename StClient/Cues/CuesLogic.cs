﻿using LogAlertHB;
using StClient.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;


namespace StClient
{
    public class CueLogic : INotifyPropertyChanged
    {
        #region INIT
        public delegate void CallMotorLogic(int cueNumber, int motorID, CueDelegateStates CueDelegatStates);
        public event CallMotorLogic NotifyMlEventHandler;

        public ObservableCollection<CueItem> _lvitems = new ObservableCollection<CueItem>();
        public CollectionViewSource _sortedcvs = new CollectionViewSource();
        public List<MotorLogic> tempMotorsForCues = new List<MotorLogic>(); //motors with Cue ON but not yet Cued

        public List<CueItem> CurrentCueItemsList = new List<CueItem>(); //Current Cue Items List - ovo je manje od dva zla ili na ravno 34 mesta stalno racunati ili trpati u listu - konciznije je s listom 
        public List<MotorLogic> MovementMotorsList = new List<MotorLogic>();

        PopupWindows popups = new PopupWindows();

        //public string path = Environment.CurrentDirectory + @"\Cues\";
        public string _path = Settings.Default.PathCues;
        public string path { get { return _path; } set { if (_path != value) { _path = value; OnNotify("path"); } } }


        int _currentCueNo;
        public int currentCueNo { get { return _currentCueNo; } set { if (_currentCueNo != value) { _currentCueNo = value; OnNotify("currentCueNo"); } } }

        bool _AutoViewCued;
        public bool AutoViewCued { get { return _AutoViewCued; } set { if (_AutoViewCued != value) { _AutoViewCued = value; OnNotify("AutoViewCued"); } } }

        string _XmlFileName = "";
        public string XmlFileName { get { return _XmlFileName; } set { if (_XmlFileName != value) { _XmlFileName = value; OnNotify("XmlFileName"); } } }

        string _cuename = "";
        public string CueName { get { return _cuename; } set { if (_cuename != value) { _cuename = value; OnNotify("CueName"); } } }




        bool _viewCuedEnable;
        public bool ViewCuedEnable { get { return _viewCuedEnable; } set { if (_viewCuedEnable != value) { _viewCuedEnable = value; OnNotify("ViewCuedEnable"); } } }



        #region I row
        bool _insertCueEnabled;
        public bool InsertCueEnabled { get { return _insertCueEnabled; } set { if (_insertCueEnabled != value) { _insertCueEnabled = value; OnNotify("InsertCueEnabled"); } } }

        bool _updateCueEnabled;
        public bool UpdateCueEnabled { get { return _updateCueEnabled; } set { if (_updateCueEnabled != value) { _updateCueEnabled = value; OnNotify("UpdateCueEnabled"); } } }

        bool _updateMotorsEnabled;
        public bool UpdateMotorsEnabled { get { return _updateMotorsEnabled; } set { if (_updateMotorsEnabled != value) { _updateMotorsEnabled = value; OnNotify("UpdateMotorsEnabled"); } } }

        bool _copyEnabled;
        public bool CopyEnabled { get { return _copyEnabled; } set { if (_copyEnabled != value) { _copyEnabled = value; OnNotify("CopyEnabled"); } } }
        #endregion

        #region II row
        bool _clearCurrentEnabled;
        public bool ClearCurrentEnabled { get { return _clearCurrentEnabled; } set { if (_clearCurrentEnabled != value) { _clearCurrentEnabled = value; OnNotify("ClearCurrentEnabled"); } } }

        bool _deleteMotorEnabled;
        public bool DeleteMotorEnabled { get { return _deleteMotorEnabled; } set { if (_deleteMotorEnabled != value) { _deleteMotorEnabled = value; OnNotify("DeleteMotorEnabled"); } } }

        bool _deleteAllCuesEnabled;
        public bool DeleteAllCuesEnabled { get { return _deleteAllCuesEnabled; } set { if (_deleteAllCuesEnabled != value) { _deleteAllCuesEnabled = value; OnNotify("DeleteAllCuesEnabled"); } } }

        bool _copyCPEnabled;
        public bool CopyCPEnabled { get { return _copyCPEnabled; } set { if (_copyCPEnabled != value) { _copyCPEnabled = value; OnNotify("CopyCPEnabled"); } } }
        #endregion


        bool _Lock;
        public bool Lock { get { return _Lock; } set { if (_Lock != value) { _Lock = value; OnNotify("Lock"); } } }//safety lock - if motion is not safe block motion


        bool _trip, _moving, _brake, _sbcbit;
        public bool Trip { get { return _trip; } set { if (_trip != value) { _trip = value; OnNotify("Trip"); } } }
        public bool Moving { get { return _moving; } set { if (_moving != value) { _moving = value; OnNotify("Moving"); } } }
        public bool Brake { get { return _brake; } set { if (_brake != value) { _brake = value; OnNotify("Brake"); } } }

        public bool SbcBit { get { return _sbcbit; } set { if (_sbcbit != value) { _sbcbit = value; OnNotify("Brake"); } } }

        bool _stopEnabled;
        public bool stopEnabled { get { return _stopEnabled; } set { if (_stopEnabled != value) { _stopEnabled = value; OnNotify("stopEnabled"); } } }

        bool _autoEnabled;
        public bool autoEnabled { get { return _autoEnabled; } set { if (_autoEnabled != value) { _autoEnabled = value; OnNotify("autoEnabled"); } } }


        //bool _isync = false;
        //public bool IsSync { get { return _isync; } set { if (_isync != value) { _isync = value; OnNotify("IsSync"); } } }

        //double _syncvalue = 1;
        //public double SyncValue { get { return _syncvalue; } set { _syncvalue = value; } }

        CueLogic()
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            _sortedcvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("Cue", System.ComponentModel.ListSortDirection.Ascending));
            _sortedcvs.Source = _lvitems;




        }
        #endregion

        #region Button Events

        public void updateCue()
        {
            if (currentCueNo == -1 || tempMotorsForCues.Count == 0) return;

            //izbaci iz liste cuova u motoru i iz lvitemsa
            foreach (CueItem item in _lvitems.Where(l => l.CueNo == currentCueNo))
                NotifyMlEventHandler(currentCueNo, item.MotorID, CueDelegateStates.Remove);

            List<CueItem> templist = _lvitems.Where(qq => qq.CueNo == currentCueNo).ToList();

            foreach (MotorLogic item in tempMotorsForCues)
            {
                _lvitems.Add(new CueItem { CueNo = currentCueNo, MotorID = item.MotorID, MotorTitle = item.Title, Motor = item, SP = item.SP, SV = item.SV, CueDescription = "" });
                NotifyMlEventHandler(currentCueNo, item.MotorID, CueDelegateStates.Add);
            }
            for (int i = templist.Count - 1; i >= 0; i--) // promene 16 jun: da pravilno menja redoslede
                _lvitems.Remove(templist[i]);

            //for (int i = tempMotorsForCues.Count - 1; i >= 0; i--)
            //    tempMotorsForCues[i].Cued = false;
            SortLV();
            CueListView.Instance.lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //u selection changed ima recalc sveg
        }

        public void Copy()
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                          {
                              int number;
                              double d;
                              TextBlock tb = new TextBlock();
                              tb.Name = "Copy"; // promene 16 jun : obavestava popup ko je
                              popups.PopupCommand(tb, false);
                              if (double.TryParse(tb.Text, out d))
                                  number = Convert.ToInt32(d);
                              else
                                  return;

                              if (currentCueNo == -1 || number == currentCueNo || _lvitems.Count == 0) return; //promene 16 jun : dodat lvitems.count

                              List<CueItem> itemsToRemove = _lvitems.Where(i => i.CueNo == number).ToList(); //ako ima motora na tom broju obrisi ih
                              List<CueItem> itemsToAdd = _lvitems.Where(i => i.CueNo == currentCueNo).ToList();

                              if (itemsToRemove.Count != 0)
                                  foreach (CueItem item in itemsToRemove)
                                  {
                                      _lvitems.Remove(item);
                                      NotifyMlEventHandler(number, item.MotorID, CueDelegateStates.Remove);
                                  }

                              foreach (CueItem item in itemsToAdd)
                              {
                                  _lvitems.Add(new CueItem
                                  {
                                      CueNo = number,
                                      MotorID = item.MotorID,
                                      MotorTitle = item.MotorTitle,
                                      Motor = item.Motor,
                                      SP = item.SP,
                                      SV = item.SV,
                                      CueDescription = item.CueDescription
                                  });
                                  NotifyMlEventHandler(number, item.MotorID, CueDelegateStates.Add);
                                  CueListView.Instance.lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //u selection changed ima recalc sveg
                              }
                          });
        }

        public void insertCue()
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                           {
                               if (CurrentCueItemsList.Count == 0) return;

                               for (int i = 0; i < _lvitems.Count; i++)
                                   if (_lvitems[i].CueNo >= currentCueNo)
                                       _lvitems[i].CueNo++;

                               int tempCueNum = currentCueNo;
                               _lvitems.Add(new CueItem { CueNo = currentCueNo, MotorID = -1, Motor = new MotorLogic(), MotorTitle = "NO MOTOR", CueDescription = "Cue Inserted", SP = 0, SV = 0 });
                               SortLV();

                               if (CueListView.Instance.lv.SelectedItem != null)
                                   currentCueNo = ((CueItem)CueListView.Instance.lv.SelectedItem).CueNo;
                           });
        }

        public void UpdateMotors()
        {


            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                if (CurrentCueItemsList.Count == 0) return;
                if (currentCueNo == -1) return;

                if (CueListView.Instance.lv.SelectedItem == null) return;//todo da li sve ovo treba?? 20110219
                try
                {
                    
                    foreach (var m in Sys.StaticMotorList) m.Value.Cued = false;

                    foreach (CueItem item in CurrentCueItemsList)
                    {
                        if (item.MotorID == -1) continue;//ako je insertovan prazan cue i kaze se update bude eksplozija 20110224
                        MotorLogic temp = Sys.StaticMotorList.Values.FirstOrDefault(m => m.MotorID == item.MotorID);
                        temp.SP = item.SP;
                        temp.SV = item.SV;
                        temp.Cued = true;

                        
                    }

                    CueListView.Instance.lv.SelectedItem = _lvitems.FirstOrDefault(qq => qq.CueNo == currentCueNo); //u selection changed ima recalc sveg
                }
                catch (Exception ex)
                {
                    AlertLogic.Add(ex.Message);
                    Log.Write(ex.Message, EventLogEntryType.Error);
                }
            });
        }

        public void ClearCurrent()
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                         {
                             foreach (CueItem t in CurrentCueItemsList)
                             {
                                 _lvitems.Remove(t);
                                 NotifyMlEventHandler(currentCueNo, t.MotorID, CueDelegateStates.Remove);
                             }

                             //Remove
                             if (_lvitems.Count > 0)
                             {
                                 try { CueListView.Instance.lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); }
                                 catch { }
                             }
                         });

        }

        public void DeleteMotor()
        {

            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                         {
                             if (CueListView.Instance.lv.SelectedIndex == -1) return;

                             int tempIndex = CueListView.Instance.lv.SelectedIndex;
                             CueItem ml = CueListView.Instance.lv.SelectedItem as CueItem;

                             NotifyMlEventHandler(currentCueNo, ml.MotorID, CueDelegateStates.Remove);
                             _lvitems.Remove(CueListView.Instance.lv.SelectedItem as CueItem);

                             //Remove
                             if (_lvitems.Count > 0)
                                 CueListView.Instance.lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //nadji 
                         });

        }

        public void DeleteAllCues()
        {
            //if (CurrentCueItemsList.Count == 0) return;

            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                           {
                               //if (System.Windows.Forms.MessageBox.Show("Clear all cue data?", "Delete all cues", System.Windows.Forms.MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel) return;

                               PopUpWindowText pw = new PopUpWindowText(500, 500, "Delete all cues", "Clear all cue data?");

                               if (pw.DialogResult == true)
                               {

                                   foreach (int motorID in _lvitems.Select(qq => qq.MotorID).Distinct())
                                   {
                                       NotifyMlEventHandler(0, motorID, CueDelegateStates.Clear);
                                   }
                                   _lvitems.Clear();
                               }
                               //Remove
                           });
        }

        public void CopyCP()
        {
            if (CueListView.Instance.lv.SelectedIndex == -1) return;

            foreach (CueItem item in CurrentCueItemsList)
                item.SP = Sys.StaticMotorList.Values.FirstOrDefault(m => m.MotorID == item.MotorID).CP;
        }

        public void CueAll()
        {
            foreach (MotorLogic motor in Sys.StaticMotorList.Values.Where(t => t.GuiVisibility == System.Windows.Visibility.Visible))
                motor.Cued = true;
        }

        public void CueNone()
        {
            foreach (MotorLogic motor in Sys.StaticMotorList.Values.Where(t => t.GuiVisibility == System.Windows.Visibility.Visible))
                motor.Cued = false;
        }

        public void CueSP()
        {
            TextBlock tb = new TextBlock() { Name = "CueSP" };
            double min = CurrentCueItemsList.Min(qq => qq.Motor.LimNeg);
            double max = CurrentCueItemsList.Max(qq => qq.Motor.LimPos);
            PopUpWindow puw = new PopUpWindow(min, max, tb, 500, 500); //todo 20131016 oprati ovo sranje

            if (puw.DialogResult == true)
                foreach (CueItem item in CurrentCueItemsList)
                {
                    double temp = double.Parse(tb.Text);
                    temp = temp > item.Motor.LimPos ? item.Motor.LimPos : temp;
                    temp = temp < item.Motor.LimNeg ? item.Motor.LimNeg : temp;

                    item.SP = temp;
                }
        }

        public void CueSV()
        {
            TextBlock tb = new TextBlock() { Name = "CueSV" };
            //            double tempV = CurrentCueItemsList.Max(asd => asd.Motor.MaxV);
            PopUpWindow puw = new PopUpWindow(2, 100, tb, 500, 500); //todo 20131016 oprati ovo sranje

            if (puw.DialogResult == true)
                foreach (CueItem item in CurrentCueItemsList)
                {
                    //tempV = double.Parse(tb.Text);
                    //item.SV = tempV > item.Motor.MaxV ? item.Motor.MaxV : tempV;
                    item.SV = double.Parse(tb.Text);
                }
        }

        public void ViewCued()
        {
            VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();
        }

        public void ResetTrip()
        {
            //List<MotorLogic> lml = new List<MotorLogic>();
            string s = "";
            foreach (CueItem item in CurrentCueItemsList.Where(qq => qq.Motor.Trip))
            {
                //lml.Add(item.Motor);
                item.Motor.soloSM.tripReset();
                s += string.Format(" motor={0}, motID={1}, CP={2};", item.Motor.Title, item.Motor.MotorID, item.Motor.CP);
            }
            //commands.resetTrip(lml);
            Log.Write("(Cue Trip Reset)." + s, EventLogEntryType.Information);
        }

        public void ChangeCueNumber()
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                         {
                             int number;
                             double d;
                             TextBlock tb = new TextBlock();
                             tb.Name = "Cue"; // promene 16 jun : obavestava popup ko je
                             popups.PopupCommand(tb, false);
                             if (double.TryParse(tb.Text, out d))
                                 number = Convert.ToInt32(d);
                             else
                                 return;

                             currentCueNo = number;

                             if (_lvitems.Count > 0)
                             {
                                 try { CueListView.Instance.lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); }
                                 catch { }
                             }
                         });
        }

        #endregion


        #region SaveLoadPlayStopDeleteStopAll

        public void FileOpen()
        {
            if (XmlFileName == "") return;
            string fileName = path + this.XmlFileName;
            XmlUtil<CueItem> x = new XmlUtil<CueItem>();
            List<CueItem> tempList = x.ReadXml(fileName, CreateDefaultRow.DontCreate);
            if (tempList == null)
            {
                Log.Write("(Cue) File " + fileName + " open FAILED.", EventLogEntryType.Error);
                AlertLogic.Add("(Cue) File " + fileName + " open FAILED.");
                return;
            }

            Log.Write("(Cue) File " + fileName + " open.", EventLogEntryType.Information);
            AlertLogic.Add("(Cue) File " + fileName + " open.");

            _lvitems.Clear();
            foreach (CueItem c in tempList)
            {
                if (Sys.StaticMotorList.Values.First(m => m.MotorID == c.MotorID) != null)
                {
                    _lvitems.Add(new CueItem
                    {
                        CueNo = c.CueNo,
                        MotorID = c.MotorID,
                        MotorTitle = c.MotorTitle,
                        SP = c.SP,
                        SV = c.SV,
                        CueDescription = c.CueDescription

                    });
                    SortLV();
                    NotifyMlEventHandler(c.CueNo, c.MotorID, CueDelegateStates.Add);
                }
            }
            foreach (CueItem ci in _lvitems)
            {
                ci.Motor = Sys.StaticMotorList.Values.FirstOrDefault(qq => qq.MotorID == ci.MotorID);
                //CurrentCueItemsList.Add(ci);20110221

            }
            //CalculateTempCueItems(); CueGUIStates();
            try
            {
                CueListView.Instance.lv.SelectedItem = _lvitems.First(c => c.CueNo == _lvitems.Min(qq => qq.CueNo));
            }
            catch { }

        }

        public void ChangeFolder()
        {
            ExtendedFolderBrowseDialog.MyFolderBrowser mfb = new ExtendedFolderBrowseDialog.MyFolderBrowser() { Location = new System.Drawing.Point(500, 500) };
            if (mfb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.path = mfb.SelectedPath + @"\";
            }
        }

        public void FileSave()
        {

            if (this.XmlFileName == "")
            {
                AlertLogic.Add("Error: Please enter filename"); // promene 16 jun: 
                return;
            }


            string fileName = "";
            if (XmlFileName.EndsWith(".xml"))
                fileName = path + this.XmlFileName;
            else
                fileName = path + this.XmlFileName + ".xml";

            try
            {
                File.Delete(fileName);
            }
            catch
            {
                Log.Write("(Cue) File " + fileName + " not saved, file in use.", EventLogEntryType.Information);
                AlertLogic.Add("(Cue) File " + fileName + " not saved, file in use.");
                return;
            }

            XmlUtil<CueItem> x = new XmlUtil<CueItem>();

            if (x.WriteXml(fileName, _lvitems.ToList()))
            {
                Log.Write("(Cue) File " + fileName + " saved.", EventLogEntryType.Information);
                AlertLogic.Add("(Cue) File " + fileName + " saved.");
            }
            else
            {
                Log.Write("(Cue) File " + fileName + " save FAILED.", EventLogEntryType.Error);
                AlertLogic.Add("(Cue) File " + fileName + " save FAILED.");
            }
        }

        public void Play()
        {
            if (CurrentCueItemsList.Count == 0) return;

            //if (!CurrentCueItemsList.All(q => q.Motor.iSpSv.SP >= q.Motor.LimNeg && q.Motor.iSpSv.SP <= q.Motor.LimPos)) return;//20140413 todo proveriti dal ovo dobro radi (treba da izbaci ako je SP van domena)

            //if (this.Lock && Sys.LocksEnabled && (Sys.LocksOverrideEnabled && !Sys.lockMotors.OverrideActive)) return;

            MovementMotorsList.RemoveAll(m => m.Brake == true && m.SbcStatusBitIndicator == false);

            foreach (CueItem cueItem in CurrentCueItemsList)
                if (MovementMotorsList.Exists(m => m.MotorID == cueItem.MotorID)) return;//todo move this to enabled?

            foreach (CueItem cueItem in CurrentCueItemsList)
            {
                MovementMotorsList.Add(cueItem.Motor);
                cueItem.Motor.soloSM.cueAuto(cueItem);
                //commands.CueStarting(cueItem);
              // commands.AutoStarting(cueItem.Motor.toList(), cueItem);
            }
            Log.Write("(Cue) Start pressed.", EventLogEntryType.Information);
            //List<MotorLogic> motorList = new List<MotorLogic>();
            //foreach (CueItem cueItem in CurrentCueItemsList)
            //    motorList.Add(cueItem.Motor);

            //commands.AutoStarting(motorList, cueItem, CueLogic.UniqueInstance);
        }

        public void Stop()
        {


            //valjda ne treba invoke
            //foreach (MotorLogic motor in MovementMotorsList.Where(qq => qq.Brake == false))
            //    commands.Stop(motor.toList());

            //20140211 todo proveriti da li radi kako treba
            Log.Write("(Cue) Stop pressed.", EventLogEntryType.Information);
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                                    {

                                        if (CueListView.Instance.lv.SelectedIndex == -1) return;
                                        if (CurrentCueItemsList.Count == 0) return;//todo da li je ovo dobro?? 20110220

                                        foreach (CueItem item in CurrentCueItemsList.Where(t => t.Motor.Moving))
                                            item.Motor.soloSM.stopMotor();

                                        foreach (MotorLogic motor in MovementMotorsList.Where(q => q.Brake == false || q.SbcStatusBitIndicator == true))
                                            motor.soloSM.stopMotor();
                                    });
        }

        public void FileDelete()
        {
            if (this.XmlFileName == "")
            {
                AlertLogic.Add("Error: Please input name"); // promene 17 jun: 
                return;
            }
            if (File.Exists(path + this.XmlFileName)) // promene 17 jun:  
            {
                try
                {
                    File.Delete(path + this.XmlFileName);
                    CueName = "";
                    Log.Write("(Cue) File " + path + this.XmlFileName + " deleted.", EventLogEntryType.Information);
                    AlertLogic.Add("(Cue) File " + this.XmlFileName + " deleted.");
                }
                catch
                {
                    Log.Write("(Cue) File " + path + this.XmlFileName + " delete FAILED.", EventLogEntryType.Error);
                    AlertLogic.Add("(Cue) File " + this.XmlFileName + " delete FAILED.");
                }
            }
        }

        public void StopAll()
        {
            foreach (var obj in Sys.StaticMotorList)
            {
                MotorLogic motor = obj.Value;
                if (motor.Brake == false || motor.SbcStatusBitIndicator == true) { motor.soloSM.stopMotor(); }
              
               
            }
                
                
              

            Log.Write("(Cue) StopAll pressed.", EventLogEntryType.Information);
        }

        #endregion


        #region NextPreviousIncrementDecrement


        public void PreviousCue()
        {
            int _cueNo;
            int _prevCueNo = currentCueNo;
            try
            {
                if (_lvitems.Count > 0)
                {
                    _cueNo = _lvitems.Where(t => t.CueNo < currentCueNo).Max(t => t.CueNo);
                    MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                      {
                          CueListView.Instance.lv.SelectedItem = _lvitems.First(c => c.CueNo == _cueNo);
                      });

                    if (AutoViewCued) { UpdateCurrentCueItemsList(); ViewCued(); }

                    Log.Write(string.Format("(Cue) PreviousCue pressed. Current cue={0}, previous cue={1}.", currentCueNo, _prevCueNo), EventLogEntryType.Information);
                }
            }
            catch { }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }

        public void NextCue()
        {
            int _cueNo;
            int _prevCueNo = currentCueNo;
            try
            {
                if (_lvitems.Count > 0)
                {
                    _cueNo = _lvitems.Where(t => t.CueNo > currentCueNo).Min(t => t.CueNo);
                    MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                      {
                          CueListView.Instance.lv.SelectedItem = _lvitems.First(c => c.CueNo == _cueNo);
                      });

                    if (AutoViewCued) { UpdateCurrentCueItemsList(); ViewCued(); }

                    Log.Write(string.Format("(Cue) NextCue pressed. Current cue={0}, prev cue={1}.", currentCueNo, _prevCueNo), EventLogEntryType.Information);
                }
            }
            catch { }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }

        public void Increment()
        {
            Log.Write(string.Format("(Cue) Current cue={0}. IncrementCue pressed.", currentCueNo), EventLogEntryType.Information);
            currentCueNo++;

            try
            {
                if (CueListView.Instance.lv.SelectedItem != null)
                    CueListView.Instance.lv.SelectedItem = _lvitems.FirstOrDefault(l => l.CueNo == currentCueNo);
                else
                    CueListView.Instance.lv.SelectedItem = -1;
            }
            catch (Exception ex)
            {
                CueListView.Instance.lv.SelectedItem = -1;
                Debug.WriteLine(ex.Message + " " + ex.StackTrace);
            }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }

        public void Decrement()
        {
            Log.Write(string.Format("(Cue) Current cue={0}. DecrementCue pressed.", currentCueNo), EventLogEntryType.Information);
            if (currentCueNo > 0) currentCueNo--;

            try
            {
                if (CueListView.Instance.lv.SelectedItem != null)
                    CueListView.Instance.lv.SelectedItem = _lvitems.FirstOrDefault(l => l.CueNo == currentCueNo);
                else
                    CueListView.Instance.lv.SelectedItem = -1;
            }
            catch (Exception ex) {

                Debug.WriteLine(ex.Message + " " + ex.StackTrace);
                CueListView.Instance.lv.SelectedItem = -1;
            
            }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }

        #endregion


        # region Subscribe To Motors
        public void SubscribeMotorsToCue(MotorLogic subject)
        {
            subject.NotifyCueEventHandler += Update;
        }


        void Update(MotorLogic ml, CueUpdateAction state)
        {
            if (state == CueUpdateAction.Add)
                tempMotorsForCues.Add(ml);
            else
                tempMotorsForCues.Remove(ml);
        }
        #endregion


        #region Helpers


        void SortLV()
        {
            _sortedcvs.SortDescriptions.Clear();
            _sortedcvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("CueNo", System.ComponentModel.ListSortDirection.Ascending));
            _sortedcvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("MotorID", System.ComponentModel.ListSortDirection.Ascending));
        }



        //public List<MotorLogic> ListOfMotors() //interface specific
        //{
        //    return MovementMotorsList;
        //    //return null;
        //}

        public void lv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView lv = sender as ListView;
            if (lv.SelectedItem != null)
            {
                this.currentCueNo = (lv.SelectedItem as CueItem).CueNo;
                lv.ScrollIntoView(lv.SelectedItem);

                if (AutoViewCued) { UpdateCurrentCueItemsList(); ViewCued(); }

                Log.Write(string.Format("(Cue) ListViewMouseClick, SelectionChanged to cue {0}.", this.currentCueNo), EventLogEntryType.Information);
            }
        }

        public void UpdateCurrentCueItemsList()
        {//updated in timer
            CurrentCueItemsList = _lvitems.Where(qq => qq.CueNo == currentCueNo).ToList();
        }

        public void CalculateEnableds() // Play button,Trip i moving indikatori
        {
            //if (CurrentCueItemsList.Count != 0)
            //    SyncValue = CurrentCueItemsList.Min(qq => qq.Motor.MaxV);
            //else
            //    SyncValue = 1;

            //MovementMotorsList.Clear();
            //foreach (CueItem item in CurrentCueItemsList)
            //    MovementMotorsList.Add(Sys.StaticMotorList.Find(qq => qq.MotorID == item.MotorID));

            if (CurrentCueItemsList.Count > 0)
            {
                //this.Lock = CurrentCueItemsList.Any(q => q.Motor.Lock);
                Trip = CurrentCueItemsList.Exists(q => q.Motor.Trip);
                this.Moving = CurrentCueItemsList.Exists(q => q.Motor.Moving);
                Brake = CurrentCueItemsList.All(q => q.Motor.Brake);
                SbcBit = CurrentCueItemsList.All(q => q.Motor.SbcStatusBitIndicator);
                autoEnabled = !Trip && !Moving && Brake && !SbcBit && CurrentCueItemsList.All(q => q.Motor.RefOk && !q.Motor.Local && q.Motor.Net);

                stopEnabled = ClearCurrentEnabled = DeleteMotorEnabled = ViewCuedEnable = CopyEnabled = CopyCPEnabled = true;

                InsertCueEnabled = UpdateCueEnabled = UpdateMotorsEnabled = DeleteAllCuesEnabled = ClearCurrentEnabled = DeleteMotorEnabled = !(Moving);

                //if (this.Lock) autoEnabled = false;
                //if (this.Lock && Sys.LocksEnabled && (Sys.LocksOverrideEnabled && !Sys.lockMotors.OverrideActive))
                //{ autoEnabled = false; }
            }
            else
            {
                autoEnabled = Brake = SbcBit = stopEnabled = ClearCurrentEnabled = DeleteMotorEnabled = DeleteAllCuesEnabled = ViewCuedEnable = CopyEnabled = CopyCPEnabled = false;
                InsertCueEnabled = UpdateCueEnabled = true;
            }
        }

        #endregion


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion


        #region UniqueInstance
        class CueLogicCreator
        {
            static CueLogicCreator() { }
            internal static readonly CueLogic uniqueInstance = new CueLogic();
        }
        public static CueLogic UniqueInstance
        {
            get { return CueLogicCreator.uniqueInstance; }
        }

    }
    #endregion
}