﻿using LogAlertHB;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class CuesMotorGUI : UserControl
    {
        public MotorLogic motor;

        PopupWindows popups = new PopupWindows();
        //StateMachine statemachine = new StateMachine();


        public CuesMotorGUI(MotorLogic motor)
        {
            InitializeComponent();

            this.DataContext = motor;
            this.motor = motor;

        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            motor.soloSM.tripReset();
            //commands.resetTrip(motor.toList());
            Log.Write("cueMotorGUI: TRIP RST pressed", EventLogEntryType.Information);
        }

        //private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    commands.AutoStarting(motor.toList(), motor);
        //    Log.Write("cueMotorGUI: AUTO pressed", EventLogEntryType.Information);
        //}

        //private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    commands.Stopping(motor);
        //    Log.Write("cueMotorGUI: STOP pressed", EventLogEntryType.Information);
        //}

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.SP_Popup(motor.toList(), sender as TextBlock, false);
            Log.Write(string.Format("cueMotorGUI: SP changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.", motor.MotorID, motor.Title, motor.CP, motor.CV, motor.SP), EventLogEntryType.Information);
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.SVMV_Popup(motor.toList(), sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("cueMotorGUI: SV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.", motor.MotorID, motor.Title, motor.CP, motor.CV, motor.SV), EventLogEntryType.Information);
        }


    }

    public class GroupConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value != null)
            {
                if ((int)value == -1)
                    return string.Empty;
                else
                    return value;
            }

            return string.Empty;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }


}
