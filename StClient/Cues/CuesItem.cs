﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace StClient
{
    public class CueItem : INotifyPropertyChanged, ISpSv
    {
        int _cueNo = 1;
        int _motorID = 11;
        string _motorTitle = "motor11";
        string _cueDescription = "";
        double _sp = 300;
        double _sv = 10;
        double _CueSP;


        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }

        [XmlAttribute("MotorTitle")]
        public string MotorTitle { get { return _motorTitle; } set { _motorTitle = value; } }

        [XmlAttribute("CueDescription")]
        public string CueDescription { get { return _cueDescription; } set { if (_cueDescription != value) { _cueDescription = value; OnNotify("CueDescription"); } } }

        [XmlAttribute("CueNo")]
        public int CueNo { get { return _cueNo; } set { if (_cueNo != value) { _cueNo = value; OnNotify("CueNo"); } } }

        [XmlAttribute("SP")]
        public double SP
        {
            get { return _sp; }
            set { if (_sp != value) { _sp = value; OnNotify("SP"); } }
        }
        [XmlIgnore]
        public double CueSP
        {
            get { return _CueSP; }
            set { if (_CueSP != value) { _CueSP = value; OnNotify("CueSP"); } }
        }

        [XmlAttribute("SV")]
        public double SV { get { return _sv; } set { if (_sv != value) { _sv = value; OnNotify("SV"); } } }

        [XmlIgnore]
        public MotorLogic Motor;

        [XmlIgnore]
        public double LimitMin;
        [XmlIgnore]
        public double LimitMax;





        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion






    }
}
