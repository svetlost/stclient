﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;


namespace StClient
{
    /// <summary>
    /// Interaction logic for LV.xaml
    /// </summary>
    public partial class Cues : UserControl
    {

        DispatcherTimer CueListBackground = new DispatcherTimer();

        public Cues()
        {
            InitializeComponent();
            this.DataContext = CueLogic.UniqueInstance;
        }
        //    CueListBackground.Tick += CueListBackground_Tick;
        //    CueListBackground.Interval = TimeSpan.FromMilliseconds(500);
        //    CueListBackground.Start();
        //}

        //void CueListBackground_Tick(object sender, EventArgs e)
        //{

        //}     

        #region Button Events

        public void updateCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.updateCue();
        }

        public void Copy_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Copy();
        }

        public void insertCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.insertCue();
        }

        public void UpdateMotors_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.UpdateMotors();
        }

        public void ClearCurrent_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ClearCurrent();
        }

        public void DeleteMotor_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.DeleteMotor();
        }

        public void DeleteAllCues_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.DeleteAllCues();
        }

        public void CopyCP_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CopyCP();
        }

        public void CueAll_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CueAll();
        }

        public void CueNone_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CueNone();
        }

        public void ViewCued_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ViewCued();
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ResetTrip();
        }

        #endregion

        #region SaveLoadPlayStopDelete

        public void FileOpen_Click(object sender, RoutedEventArgs e)
        {
            PopUpWindowText pw = new PopUpWindowText(500, 500, "Open file", "Opening file will overwrite current data.\nOpen file?");
            if (pw.DialogResult == true)
                CueLogic.UniqueInstance.FileOpen();
        }

        public void FileSave_Click(object sender, RoutedEventArgs e)
        {
            //todo: indikator da je prazno
            if (CueName.Text != "")
            {
                PopUpWindowText pw = new PopUpWindowText(500, 500, "Save file", "Saving file will overwrite data in existing file.\nSave file?");
                if (pw.DialogResult == true)
                    CueLogic.UniqueInstance.FileSave();
            }
        }

        private void FileDelete_Click(object sender, RoutedEventArgs e)
        {
            //todo 2013 prebacitiu cuelogiuc
            //if (MessageBox.Show("Delete file?") == MessageBoxResult.OK)
            //if (MessageBox.Show("Delete file?", "Delete", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            PopUpWindowText pw = new PopUpWindowText(500, 500, "Delete file", "Delete file?");
            if (pw.DialogResult == true)
                CueLogic.UniqueInstance.FileDelete();
        }

        public void Play_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Play();
        }

        public void Stop_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Stop();
        }

        private void StopAll_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.StopAll();
        }

        #endregion

        private void ReadCueFilesInFolder(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(CueLogic.UniqueInstance.path);
            CueName.Items.Clear();
            foreach (FileInfo fileinfo in directoryInfo.GetFiles("*.xml"))
                CueName.Items.Add(fileinfo.Name.ToString());
        }


        #region NextPreviousIncrementDecrement
        private void goToPreviousCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.PreviousCue();
        }
        private void goToNextCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.NextCue();
        }
        private void CueIncrement_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Increment();
        }
        private void CueDecrement_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Decrement();
        }
        #endregion

        private void CueNumber_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CueLogic.UniqueInstance.ChangeCueNumber();
        }

        private void SP_All_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CueSP();
        }

        private void SV_All_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CueSV();
        }

        private void ChangeFolder_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ChangeFolder();
        }












    }


}