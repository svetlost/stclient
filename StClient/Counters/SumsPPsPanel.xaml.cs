﻿using System;
using System.Windows.Controls;

namespace StClient
{
    /// <summary>
    /// Interaction logic for SumsPPsPanel.xaml
    /// </summary>
    public partial class SumsPPsPanel : UserControl
    {
        private static volatile SumsPPsPanel instance;
        private static object syncRoot = new Object();

        SumsPPsPanel() { InitializeComponent(); }
        public static SumsPPsPanel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SumsPPsPanel();
                    }
                }

                return instance;
            }
        }

        public void AddPanel(CountersSumsPPS obj, int row)
        {
            Grid.SetColumn(obj, 0);
            Grid.SetRow(obj, row);
            Panelgrid.Children.Add(obj);
        }
    }


}
