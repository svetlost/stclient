﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace StClient
{

    public class GroupLogic : StandardCommands, IMotorIndicators, ISpSv
    {
        double _SP = 300, _SV = 20, _MV = 10;
        public List<MotorLogic> MotorsInGroup = new List<MotorLogic>();
        public List<byte> motorIds = new List<byte>();

        ObservableCollection<LVMotorsInGroup> listViewDataCollection = new ObservableCollection<LVMotorsInGroup>();

        public ObservableCollection<LVMotorsInGroup> ListviewMotorCollection { get { return listViewDataCollection; } }

        public DateTime ManTimeOut { get; set; }
        int _direction;
      


        Properties.Settings Sett = Properties.Settings.Default;
      


     

        MotorBrake _SecBrkControlLock = MotorBrake.Locked;

        public MotorBrake SbcLock
        {

            get { return _SecBrkControlLock; }
            set
            {
                if (_SecBrkControlLock != value)
                {

                    _SecBrkControlLock = value;
                    //SecondaryBreakControl sbc = Sys.SbcList.Find(qq => qq.CanChannel == CanChannel);
                    //Sys.printStack(this);
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("DIO", (ushort)value, motorIds.ToArray()); //DIO enable
                    // sbc.Output = SetBitInByteArray2(sbc.Output, SbcBitAddress, value);
                    OnNotify("GroupSbcLock");

                    //if (MotorID == 1)
                    //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} MOTOR: {1}, SBC getset:{2}", DateTime.Now, MotorID, SbcLock));

                }
            }
        }

        //public EdgeValue cmdAuto, cmdStop, cmdUp, cmdDown, cmdRstTrip, cmdJoyStart, cmdManStart;


        #region Group OBSERVABLE VALUES

        bool _grouphastrip, _grouphasunderload, _grouphasoverload, _allgroupismoving, _groupismoving, _groupisrefok, _groupisinh28ok, _grouphaslocal, _groupiscanok, _grpSpSvEnabled, _grpBrake, _sbcbit;
        //public bool GroupIsNotEmpty { get { return MotorsInGroup.Count > 0; } }


        public bool Trip
        {
            get
            {
                bool doesTripExisit = MotorsInGroup.Exists(q => q.Trip);
                if (_grouphastrip != doesTripExisit)
                    Trip = doesTripExisit;
                return _grouphastrip;
            }
            set
            {
                _grouphastrip = value;
                OnNotify("Trip");
                if (_grouphastrip == true)
                {
                    foreach (MotorLogic motor in MotorsInGroup.Where(qq => qq.soloSM.state != states.Stopped
                    && qq.soloSM.state != states.Stopping1 && qq.soloSM.state != states.Idle
                    && qq.soloSM.state == states.Error1))
                        motor.soloSM.state = states.Error1;
                }
            }
        }

        public bool GroupUnderLoad
        {
            get
            {
                bool isUnderload = MotorsInGroup.Exists(qq => qq.UnderLoadSignal == true);
                if (_grouphasunderload != isUnderload)
                    GroupUnderLoad = isUnderload;
                return _grouphasunderload;
            }
            set
            {
                _grouphasunderload = value;
                OnNotify("GroupUnderLoad");
                if (_grouphasunderload == true)
                {
                    foreach (MotorLogic motor in MotorsInGroup.Where(qq => qq.soloSM.state != states.Stopped
                    && qq.soloSM.state != states.Stopping1 && qq.soloSM.state != states.Idle
                    && qq.soloSM.state == states.Error1))
                        motor.soloSM.state = states.Error1;
                }
            }
        }

        public bool GroupOverLoad
        {
            get
            {
                bool isOverload = MotorsInGroup.Exists(qq => qq.OverLoadSignal == true);
                if (_grouphasoverload != isOverload)
                    GroupOverLoad = isOverload;
                return _grouphasoverload;
            }
            set
            {
                _grouphasoverload = value;
                OnNotify("GroupOverLoad");
                if (_grouphasoverload == true)
                {
                    foreach (MotorLogic motor in MotorsInGroup.Where(qq => qq.soloSM.state != states.Stopped
                    && qq.soloSM.state != states.Stopping1 && qq.soloSM.state != states.Idle
                    && qq.soloSM.state == states.Error1))
                        motor.soloSM.state = states.Error1;
                }
            }
        }

        public bool AllMoving
        {
            get
            {
                bool isMoving = MotorsInGroup.All(q => q.Moving);
                if (_allgroupismoving != isMoving)
                    AllMoving = isMoving;
                return _allgroupismoving;
            }
            set { _allgroupismoving = value; OnNotify("AllMoving"); }
        }

        public bool AnyMoving
        {
            get
            {
                bool isAnyMoving = MotorsInGroup.Exists(q => q.Moving);
                if (_groupismoving != isAnyMoving)
                    AnyMoving = isAnyMoving;
                return _groupismoving;
            }
            set { _groupismoving = value; OnNotify("AnyMoving"); }
        }

        public bool RefOk
        {
            get
            {
                bool isRefOKTrue = MotorsInGroup.All(q => q.RefOk);
                if (_groupisrefok != isRefOKTrue)
                    _groupisrefok = isRefOKTrue;
                return _groupisrefok;
            }
            set { _groupisrefok = value; OnNotify("GroupIsRefOk"); }
        }

        public bool Inh28
        {
            get
            {
                bool isInh28True = MotorsInGroup.All(q => q.Inh28);
                if (_groupisinh28ok != isInh28True)
                    _groupisinh28ok = isInh28True;
                return _groupisinh28ok;
            }
            set { _groupisinh28ok = value; OnNotify("GroupIsInh28Ok"); }
        }

        public bool Local
        {
            get
            {
                bool isLocal = MotorsInGroup.All(q => q.Local);
                if (_grouphaslocal != isLocal)
                    Local = isLocal;
                return _grouphaslocal;
            }
            set
            { _grouphaslocal = value; OnNotify("GroupHasLocal"); }
        }

        public bool Net
        {
            get
            {
                bool isNet = MotorsInGroup.All(q => q.Net);
                if (_groupiscanok != isNet)
                    Net = isNet;
                return _groupiscanok;
            }
            set
            {
                _groupiscanok = value;
                OnNotify("Net");

                if (_groupiscanok == false)
                {
                    foreach (MotorLogic motor in MotorsInGroup.Where(qq => qq.soloSM.state != states.Stopped
                    && qq.soloSM.state != states.Stopping1 && qq.soloSM.state != states.Idle))
                        motor.soloSM.stopMotor();
                    // motor.state = states.Stopping;
                    //commands.Stopping(motor);
                }
            }
        }

        public bool Brake { get { return _grpBrake; } set { _grpBrake = value; OnNotify("Brake"); } }
        public bool SbcBit { get { return _sbcbit; } set { _sbcbit = value; OnNotify("SbcBit"); } }

        //bool _cmdAuto, _cmdStop, _cmdUp, _cmdDown, _cmdRstTrip, _cmdJoyStart, _cmdManStart;
        //public bool cmdAuto
        //{
        //    get { return _cmdAuto; }
        //    set
        //    {
        //        if (value != _cmdAuto)
        //        {
        //            _cmdAuto = value;
        //            OnNotify("cmdAuto");
        //            if (value == true) TimedAction.ExecuteWithDelay(new Action(delegate
        //           {
        //               _cmdAuto = false;
        //           }), TimeSpan.FromSeconds(4));
        //        }
        //    }
        //}

        bool _Lock;
        [XmlIgnore]
        public bool Lock { get { return _Lock; } set { if (_Lock != value) { _Lock = value; OnNotify("Lock"); } } }//safety lock - if motion is not safe block motion

        bool _autoEnabled, _stopEnabled, _manEnabled, _grpResetEnabled, _manPosEnabled, _manNegEnabled;
        public bool autoEnabled { get { return _autoEnabled; } set { if (_autoEnabled != value) { _autoEnabled = value; OnNotify("AutoEnabled"); } } }
        public bool stopEnabled { get { return _stopEnabled; } set { if (_stopEnabled != value) { _stopEnabled = value; OnNotify("StopEnabled"); } } }
        public bool manEnabled { get { return _manEnabled; } set { if (_manEnabled != value) { _manEnabled = value; OnNotify("ManEnabled"); } } }
        // public bool manPosNegEnabled { get { return _manPosNegEnabled; } set { if (_manPosNegEnabled != value) { _manPosNegEnabled = value; OnNotify("ManPosNegEnabled"); } } }

        public bool manPosEnabled { get { return _manPosEnabled; } set { if (_manPosEnabled != value) { _manPosEnabled = value; OnNotify("manPosEnabled"); } } }
        [XmlIgnore]
        public bool manNegEnabled { get { return _manNegEnabled; } set { if (_manNegEnabled != value) { _manNegEnabled = value; OnNotify("manNegEnabled"); } } }
        #endregion

        public StJoystick _joy { get; set; }
        public double SP { get { return _SP; } set { if (_SP != value) { _SP = value; OnNotify("SP"); } } }
        public double SV { get { return _SV; } set { if (_SV != value) { _SV = value; OnNotify("SV"); } } }
        public double MV { get { return _MV; } set { if (_MV != value) { _MV = value; OnNotify("MV"); } } }


        bool _spenabled, _mvenabled;

        public bool SpSvEnabled { get { return _spenabled; } set { if (_spenabled != value) { _spenabled = value; OnNotify("SpSvEnabled"); } } }
        public bool MVEnabled { get { return _mvenabled; } set { if (_mvenabled != value) { _mvenabled = value; OnNotify("MVEnabled"); } } }

        public bool grpResetEnabled { get { return _grpResetEnabled; } set { if (_grpResetEnabled != value) { _grpResetEnabled = value; OnNotify("grpResetEnabled"); } } }
        public bool grpSpSvEnabled { get { return _grpSpSvEnabled; } set { if (_grpSpSvEnabled != value) { _grpSpSvEnabled = value; OnNotify("grpSpSvEnabled"); } } }

        public int groupNo { get; set; }
        public double SP_min { get; set; }
        public double SP_max { get; set; }

        public DateTime timeOutTime { get; set; }
        public void resetTimeOut() { timeOutTime = DateTime.Now + TimeSpan.FromSeconds(5); }



        Visibility _underloadVisibility = Visibility.Collapsed;

        public Visibility UnderLoadVisibility { get { return _underloadVisibility; } set { if (_underloadVisibility != value) { _underloadVisibility = value; OnNotify("UnderLoadVisibility"); } } }

        Visibility _overloadvisibility = Visibility.Collapsed;

        public Visibility OverLoadVisibility { get { return _overloadvisibility; } set { if (_overloadvisibility != value) { _overloadvisibility = value; OnNotify("OverLoadVisibility"); } } }


        //bool _isync = false;
        //public bool IsSync { get { return _isync; } set { if (_isync != value) { _isync = value; OnNotify("IsSync"); } } }

        //double _syncvalue = 1;
        //public double SyncValue { get { return _syncvalue; } set { _syncvalue = value; } }

        GroupStateMachine _groupSM;
        public GroupStateMachine groupSM
        {
            get
            {
                if (_groupSM == null) _groupSM = new GroupStateMachine(this);
                return _groupSM;
            }
        }


        public GroupLogic(int _groupNo)
        {
            groupNo = _groupNo;

            if (!manPosEnabled && !manNegEnabled && autoEnabled && manEnabled && stopEnabled)
                SpSvEnabled = MVEnabled = stopEnabled = true;
            else
                SpSvEnabled = MVEnabled = stopEnabled = false;
        }

        public void AddToGroup(MotorLogic item)
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                MotorsInGroup.Add(item);
                motorIds.Add((byte)item.MotorID);


                listViewDataCollection.Add(new LVMotorsInGroup { Title = item.Title });
            });
            //SyncValue = MotorsInGroup.Min(qq => qq.MaxV);
            // CalculateEnableds(); // ponovo izracunaj ako dodas motor u grupu
            //Brake = MotorsInGroup.All(q => q.Brake);


        }

        public void RemoveFromGroup(MotorLogic item)
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                MotorsInGroup.Remove(item);
                motorIds.Remove((byte)item.MotorID);

                listViewDataCollection.Remove(listViewDataCollection.First(qq => qq.Title == item.Title));
            });
            //if (MotorsInGroup.Count > 0)
            //{
            //    //SyncValue = MotorsInGroup.Min(qq => qq.MaxV);
            //    Brake = MotorsInGroup.All(q => q.Brake);
            //}
            // CalculateEnableds(); //ponovo izracujaj ako maknes motor iz grupe
            //if (MotorsInGroup.Count == 0)
            //{
            //    grpResetEnabled = Brake = false;
            //    //SyncValue = 0;
            //}
        }

        public void CalculateEnableds()
        {
            if (MotorsInGroup.Count == 0)
            {
                autoEnabled = manEnabled = manNegEnabled = manPosEnabled = grpResetEnabled = false;
                SpSvEnabled = MVEnabled = true;
                Lock = Brake = false;
            }
            else
            {
                //this.Lock = MotorsInGroup.Any(q => q.Lock);
                this.Brake = MotorsInGroup.All(q => q.Brake);

                this.SbcBit = MotorsInGroup.All(q => q.SbcStatusBitIndicator);

                bool loadCellUnderLoadEnabled = MotorsInGroup.Exists(qq => qq.loadCellUnderLoadEnabled);
                bool loadCellOverLoadEnabled = MotorsInGroup.Exists(qq => qq.loadCellOverLoadEnabled);

                if (loadCellUnderLoadEnabled) { UnderLoadVisibility = Visibility.Visible; } else { UnderLoadVisibility = Visibility.Collapsed; }
                if (loadCellOverLoadEnabled) { OverLoadVisibility = Visibility.Visible; } else { OverLoadVisibility = Visibility.Collapsed; }

                autoEnabled = (Net && RefOk && !AnyMoving && !Trip && !Local && !manPosEnabled && !manNegEnabled && Brake && !SbcBit
                    && !MotorsInGroup.All(qq => Math.Abs(SP - qq.CP) < 0.1)
                    && MotorsInGroup.All(qq => SP <= qq.LimPos && SP >= qq.LimNeg)); //20140406 todo 0.1 staviti u conf

                SpSvEnabled = MVEnabled = manEnabled = (Net && !AnyMoving && !Trip && !Local && !manPosEnabled && !manNegEnabled && Brake && !SbcBit);

                manPosEnabled = (_groupSM.state == states.ManStarted2 || _groupSM.state == states.ManStarted || _groupSM.state == states.ManPos || _groupSM.state == states.ManPos2); //&& !GroupUnderLoad
                manNegEnabled = (_groupSM.state == states.ManStarted2 || _groupSM.state == states.ManStarted || _groupSM.state == states.ManNeg || _groupSM.state == states.ManNeg2); //&& !GroupOverLoad
                //manPosNegEnabled = MotorsInGroup.Exists(q => (q.soloSM.state == states.ManStarted2
                //    && q.GetType() != typeof(StJoystick)));

                grpResetEnabled = (MotorsInGroup.All(qq => qq.soloSM.state == states.Idle));

                SP_min = this.MotorsInGroup.Max(t => t.LimNeg);
                SP_max = this.MotorsInGroup.Min(t => t.LimPos);

                if ( groupSM.state == states.Idle && MotorsInGroup.Count > 0)
                    grpResetEnabled = true;
                else
                    grpResetEnabled = false;

                //double tempV;
                //if (this.IsSync)
                //    tempV = MotorsInGroup.Min(asd => asd.MaxV);
                //else
                //tempV = MotorsInGroup.Max(asd => asd.MaxV);
                //if (this.SV > tempV) this.SV = tempV;20141018 speed is percentage, not cm/s
                //if (this.MV > tempV) this.MV = tempV;20141018 speed is percentage, not cm/s

            }

            stopEnabled = !Local;

            foreach (MotorLogic motor in MotorsInGroup)
            {
                motor.grpResetEnabled = motor.GroupChangeEnabled = Brake;

                //if (!Brake)
                //    motor.grpResetEnabled = motor.GroupChangeEnabled = false;
                //else
                //    motor.grpResetEnabled = motor.GroupChangeEnabled = true;
            }

            //if (this.Lock && Sys.LocksEnabled && (Sys.LocksOverrideEnabled && !Sys.lockMotors.OverrideActive))
            //{ autoEnabled = manEnabled = manPosNegEnabled = false; }

        }

        //void StateMachine() { }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion

    }

    public class LVMotorsInGroup : INotifyPropertyChanged
    {
        string title;
        // double cp;

        public string Title { get { return title; } set { if (title != value) { title = value; OnNotify("Title"); } } }
        // public double CP { get { return cp; } set { if (cp != value) { cp = value; OnNotify("CP"); } } }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
