﻿using LogAlertHB;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Groups.xaml
    /// 
    /// 
    /// </summary>

    public partial class GroupGUI : UserControl
    {
        public GroupLogic _group;

        PopupWindows popups = new PopupWindows();
        GrpCommands grpCmd = new GrpCommands();
        //EdgeValue cmdAuto, cmdStop, cmdUp, cmdDown, cmdRstTrip, cmdJoyStart, cmdManStart;

        public GroupGUI(GroupLogic group)
        {
            InitializeComponent();

            DataContext = group;
            MotorsInGroupListView.ItemsSource = group.ListviewMotorCollection;
            groupNumber.Text = "GROUP " + group.groupNo;
            _group = group;

            //if (_group.groupNo > 1) RelJoy.Visibility = Visibility.Hidden;//hide button for group 3
            //_group.cmdAuto = new EdgeValue(Auto);
            //_group.cmdStop = new EdgeValue(Stop);
            //_group.cmdUp = new EdgeValue(Up);
            //_group.cmdDown = new EdgeValue(Down);
            //_group.cmdRstTrip = new EdgeValue(Auto);
            //_group.cmdJoyStart = new EdgeValue(RelJoy);
            //_group.cmdManStart = new EdgeValue(resetTrip);
        }

        string getGroupState(GroupLogic grp)
        {
            if (grp.MotorsInGroup.Count == 0) return "";

            string s = "";
            foreach (var mot in grp.MotorsInGroup) s += string.Format("motor id={0} title={1} cp={2} cv={3} trip={4} mov={5} brk={6} sbc={7}\n",
                mot.MotorID, mot.Title, mot.CP, mot.CV, mot.Trip, mot.Moving, mot.Brake, mot.SbcStatusBitIndicator);

            return s;
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            grpCmd.ResetTrip(_group);
            //commands.resetTrip(_group.MotorsInGroup);
            //Log.Write("groupGUI: TRIP RST pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: TRIP RST pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            grpCmd.ResetGroup(_group);
            //commands.group_ResetGroup(this._group);
            //Log.Write("groupGUI: GRP RST pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: GRP RST pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        private void view_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowMotor(this._group.MotorsInGroup);
        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            grpCmd.ManRelease(_group);
            //commands.ManualStarting(_group.MotorsInGroup);
            //Log.Write("groupGUI: RELEASE BRK pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: RELEASE BRK pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }
        private void ReleaseJoy_Click(object sender, RoutedEventArgs e)
        {
            grpCmd.JoyRelease(_group);
            //if (_group.MotorsInGroup.Count == 0) return;
            //double _maxV = _group.MotorsInGroup.Max(er => er.MaxV);
            ////double _maxV = _group.IsSync ? _group.MotorsInGroup.Min(we => we.MaxV) : _group.MotorsInGroup.Max(er => er.MaxV);
            ////commands.ManualStarting(_group.MotorsInGroup, PhidgetsInterfaces.UniqueInstance.joystickList[_group.groupNo]);
            //Sys.joystickList[_group.groupNo].SetJoystickMaxV(_maxV);
            ////commands.JoyStart(_group.MotorsInGroup);
            //commands.JoyStart(_group);

            //Log.Write("groupGUI: RELEASE JOY pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: RELEASE JOY pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            grpCmd.ManPos_press(_group);
            //commands.Up_PreviewMouseLeftButtonDown(_group.MotorsInGroup);
            //Log.Write("groupGUI: MAN UP pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: MAN UP pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
            //Debug.WriteLine("{0:hh:mm:ss.fff} groupGUI: MAN UP pressed", DateTime.Now);
        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            grpCmd.ManPos_release(_group);
            //commands.UpDown_PreviewMouseLeftButtonUp(_group.MotorsInGroup);
            //Log.Write("groupGUI: MAN UP released", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: MAN UP released\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
            Debug.WriteLine("{0:hh:mm:ss.fff} groupGUI: MAN UP released", DateTime.Now);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            grpCmd.ManNeg_press(_group);
            //commands.Down_PreviewMouseLeftButtonDown(_group.MotorsInGroup);
            //Log.Write("groupGUI: MAN DOWN pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: MAN DOWN pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
            Debug.WriteLine("{0:hh:mm:ss.fff} groupGUI: MAN DOWN pressed", DateTime.Now);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            grpCmd.ManNeg_release(_group);
            //commands.UpDown_PreviewMouseLeftButtonUp(_group.MotorsInGroup);
            //Log.Write("groupGUI: MAN DOWN released", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: MAN DOWN released\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
            //Debug.WriteLine("{0:hh:mm:ss.fff} groupGUI: MAN DOWN released", DateTime.Now);
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)//20170903 list<>max prijavljivao exception
        {
            //MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
            //{
            //var _min = _group.MotorsInGroup.Max(t => t.LimNeg);//20171027 eksplodirao na GroupSP_Popup linq.max; zato je ovde izdvojeno da ne ide referenca na listu. zasto ne puca na SV???????
            //var _max = _group.MotorsInGroup.Min(t => t.LimPos);

            popups.GroupSP_Popup2(_group.SP_min, _group.SP_max, sender as TextBlock);
            //popups.GroupSP_Popup2(_min, _max, sender as TextBlock);
            //popups.GroupSP_Popup(_group.MotorsInGroup, sender as TextBlock);
            Log.Write(string.Format("groupGUI: SP changed to {1}, group={0}.", _group.groupNo, _group.SP), EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: SP changed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
            //});
        }
        //private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{

        //    popups.GroupSP_Popup(_group.MotorsInGroup, sender as TextBlock);
        //    //popups.SP_Popup(_group.MotorsInGroup, sender as TextBlock, false);
        //    Log.Write(string.Format("groupGUI: SP changed to {1}, group={0}.", _group.groupNo, _group.SP), EventLogEntryType.Information);
        //}

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.GroupSVMV_Popup(_group.MotorsInGroup, sender as TextBlock, ShowPercButtons.Show);
            //popups.SVMV_Popup(_group.MotorsInGroup, sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("groupGUI: SV changed to {1}, group={0}.", _group.groupNo, _group.SV), EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: SV changed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.GroupSVMV_Popup(_group.MotorsInGroup, sender as TextBlock, ShowPercButtons.Show);
            //popups.SVMV_Popup(_group.MotorsInGroup, sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("groupGUI: MV changed to {1}, group={0}.", _group.groupNo, _group.MV), EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: MV changed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            grpCmd.Auto(_group);
            //commands.AutoStarting(_group.MotorsInGroup, _group);
            //commands.GrpAuto(_group);
            //Log.Write("groupGUI: AUTO pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: AUTO pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            grpCmd.Stop(_group);
            //commands.Stop(_group.MotorsInGroup);
            //Log.Write("groupGUI: STOP pressed", EventLogEntryType.Information);
            Log.Write(string.Format("groupGUI {0}: STOP pressed\n{1}", _group.groupNo, getGroupState(_group)), EventLogEntryType.Information);
        }

        //private void SyncV_Checked(object sender, RoutedEventArgs e)
        //{
        //    if (!(bool)SyncV.IsChecked) return;

        //    var tempV = _group.MotorsInGroup.Min(asd => asd.MaxV);
        //    _group.JV = _group.JV > tempV ? tempV : _group.JV;
        //    _group.SV = _group.SV > tempV ? tempV : _group.SV;
        //}


    }
}


