﻿using System;
using System.Data.Linq.Mapping;

namespace StClient
{

    struct CodeWordWithValues

    {
        public string CodeWord { get; set; }
        public double value { get; set; }
        public byte numberOfBits { get; set; }
    }

    [Table(Name = "code_words")]
    public class CodeWords
    {
        private int id;
        [Column(Name = "id", DbType = "Int NOT NULL", IsPrimaryKey = true, IsDbGenerated = true)]
        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }

        }

        private string code;
        [Column(Name = "command_code")]
        public string CommandCode
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
            }
        }

        private string code_description;
        [Column(Name = "code_description")]
        public string CodeDescription
        {
            get
            {
                return this.code_description;
            }
            set
            {
                this.code_description = value;
            }
        }

        private byte number_of_bits;
        [Column(Name = "number_of_bits")]
        public byte NumberOfBits
        {
            get
            {
                return this.number_of_bits;
            }
            set
            {
                this.number_of_bits = value;
            }
        }

        private byte cyclic_read;
        [Column(Name = "cyclic_read")]
        public byte CyclicRead
        {
            get
            {
                return this.cyclic_read;
            }
            set
            {
                this.cyclic_read = value;
            }
        }

        private Nullable<Byte> code_order_in_regulator;
        [Column(Name = "code_order_in_regulator")]
        public Nullable<Byte> CodeOrderOnRegulator
        {
            get
            {
                return this.code_order_in_regulator;
            }
            set
            {
                this.code_order_in_regulator = value;
            }
        }

        private string par_name_in_app;
        [Column(Name = "par_name_in_app")]
        public string ParNameInApp
        {
            get
            {
                return this.par_name_in_app;
            }
            set
            {
                this.par_name_in_app = value;
            }
        }
    }
}

