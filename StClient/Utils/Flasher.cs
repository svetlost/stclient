﻿using System;
using System.Windows.Threading;

namespace StClient
{
    public class Flasher
    {
        static DispatcherTimer dt = new DispatcherTimer();

        public Flasher()
        {
            dt.Interval = TimeSpan.FromMilliseconds(100);
            dt.Tick += new EventHandler(dt_Tick);
            dt.Start();
        }

        static void dt_Tick(object sender, EventArgs e)
        {
            IncrementCounter();
        }

        public enum FlashType { VerySlow = 20, Slow = 15, Normal = 10, Fast = 6, VeryFast = 3 }
        static int counter;
        public static void IncrementCounter() { counter++; }

        public static bool Flash(bool SignalEnabled, FlashType type)
        {
            return (SignalEnabled == false ? false : counter % (int)type == 0);
        }
        public static bool FlashAlwaysOn(bool SignalEnabled, FlashType type)
        {
            return (SignalEnabled == false ? true : counter % (int)type == 0);
        }

    }
}
