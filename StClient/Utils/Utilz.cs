﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace StClient
{
    public class Utl
    {

        /// <summary>
        /// used for always sending 16 bytes as ip --> 10.10.10.10 = 192.168.100.100
        /// </summary>
        /// <param name="ipaddress"></param>
        /// <returns></returns>
        public static byte[] IpLenghtNormalization(string ipaddress)
        {
            byte[] bbb = new byte[16];
            byte[] bbb1 = Encoding.UTF8.GetBytes(ipaddress);
            if (bbb1.Length < 15)
            {
                Array.Copy(bbb1, bbb, bbb1.Length);
                return bbb;
            }
            else
            {
                return bbb1;
            }
        }
        //resize byte array and add source byte[] to destination byte[]
        public static byte[] appendBytes(byte[] source, byte[] destination)
        {
            int i = destination.Length;
            Array.Resize<byte>(ref destination, i + source.Length);
            source.CopyTo(destination, i);

            return destination;
        }
        /// <summary>
        /// gets sub array from main array
        /// </summary>
        /// <param name="data">received byte array</param>
        /// <param name="index">starting position</param>
        /// <param name="length">number of bytes</param>
        /// <param name="sender">help used in debugging, optional</param>
        /// <returns></returns>
        public static byte[] SubArray(byte[] data, int index, int length,string sender="")
        {
            byte[] result = new byte[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        /// <summary>
        /// creates a task that completes after specified number of milliseconds
        /// </summary>
        /// <param name="delay">number of milliseconds</param>
        /// <returns></returns>
        public static async Task PutTaskDelay(int delay)
        {
            await Task.Delay(delay).ConfigureAwait(true);
        }

        public static byte BoolanReturn(byte[] data, int index, int length)
        {
            var bits = new BitArray(data);

            byte[] result = new byte[length];
            Array.Copy(data, index, result, 0, length);
            return result[0];
        }
        public static bool GetBitFromByteArray(byte[] _in, int pos) { return ((_in[Convert.ToInt32(pos / 8)] & (byte)(1 << (pos % 8))) != 0); }



        public static bool getBitOfInt(int i, int bitNumber) { return (i & (1 << bitNumber)) != 0; }

        public static bool IsOn(int Value, byte Bit) { return (Value >> Bit & 1) == 1; }
        public static int SetBitOfInt(int Value, byte Bit) { return SetBitOfInt(Value, Bit, true); }
        public static int SetBitOfInt(int Value, byte Bit, bool On) { return On ? Value | (1 << Bit) : ClearBitOfInt(Value, Bit); }
        public static int ClearBitOfInt(int Value, byte Bit) { return Value & ~(1 << Bit); }
        //public static void printStack(MotorLogic ml)/////////skinuo2011
        public static void printStack(string desc)
        {
            //return;

            StackTrace st = new StackTrace();
            StackFrame[] sf = st.GetFrames();
            //Debug.WriteLine("*********");
            //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} stack=", DateTime.Now));
            string s = "";
            int max = Math.Min(6, sf.Length);
            //for (int i = 1; i < max; i++) Debug.Write(sf[i].GetMethod().Name + " ,");
            for (int i = 1; i < max; i++) s += sf[i].GetMethod().Name + " ,";

            Debug.WriteLine("{0:hh:mm:ss.fff} {1} stack={2}", DateTime.Now, desc, s);

            //Debug.WriteLine("{0:hh:mm:ss.fff}>{1,15} id={2,3} state={3,10} cp={4,7} cpold={5,7}  deltaCp={6,5} sbc={7} mov={8,6} stack={9}",
            //    DateTime.Now, ml.Title, ml.MotorID, ml.state, ml.CP, ml.previousCP, Math.Abs(ml.CP - ml.previousCP), ml.SbcLock, ml.Moving, BitConverter.ToInt32(ml.sbc.PDO_CanMsgID, 0), s);

            //Debug.WriteLine("*********");
        }

        public static byte[] SetBitInByteArray(byte[] _in, int pos, bool value)
        {
            byte[] ttt = new byte[8];
            Buffer.BlockCopy(_in, 0, ttt, 0, 8);
            if (value) //true
                ttt[Convert.ToInt32(pos / 8)] = (byte)(ttt[Convert.ToInt32(pos / 8)] | (byte)(1 << ((pos % 8))));//set
            else
                ttt[Convert.ToInt32(pos / 8)] = (byte)(ttt[Convert.ToInt32(pos / 8)] & (byte)~(1 << ((pos % 8))));//reset
            return ttt;
        }

        public static string PrintByteArray(byte[] bytes)
        {
            if (bytes.Length ==0) { return "{}"; }
            var sb = new StringBuilder("{");
            for (int i = 0; i < bytes.Length; i++)
            {
                // if (bytes.Length - 1 == i) { sb.Append(bytes[i]); }2
                sb.Append(bytes[i] + ", ");
            }
            sb.Append("}");
            return sb.ToString();
        }

        public static string PrintBitArray(BitArray bits)
        {
            var sb = new StringBuilder("{");
            for (int i = 0; i < bits.Length; i++)
            {
                // if (bytes.Length - 1 == i) { sb.Append(bytes[i]); }2
                sb.Append(bits[i] + ", ");
            }
            sb.Append("}");
            return sb.ToString();
        }




    }
}
