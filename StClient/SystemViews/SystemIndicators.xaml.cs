﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;
using static StClient.SoloStateMachine;

namespace StClient
{
    /// <summary>
    /// Interaction logic for SystemStates2.xaml
    /// </summary>
    public partial class SystemIndicators : UserControl
    {
        Properties.Settings Sett = Properties.Settings.Default;
      


        public SystemIndicators()
        {
            InitializeComponent();
            this.DataContext = VisibilityStatesLogic.UniqueInstance;
            //TimedAction.ExecuteWithDelay(new Action(delegate { sEnable.DataContext = sOverride.DataContext = Sys.lockMotors; }), TimeSpan.FromSeconds(4));
            BindingOperations.SetBinding(sEstop, Signal.valueProperty, new Binding("Estop_Pressed") { Source = ApplicationStateLogic.Instance });
            BindingOperations.SetBinding(sGelbauOK, Signal.valueProperty, new Binding("GelbauOk") { Source = ApplicationStateLogic.Instance });
            BindingOperations.SetBinding(InitButton, Button.BackgroundProperty, new Binding("PilzIsActivated") { Source = ApplicationStateLogic.Instance, Converter = new PilzToColorConverter() });
            dispt.Tick += dispt_Tick;
            dispt.Interval = TimeSpan.FromSeconds(1);
            dispt.Start();
        }

        public class PilzToColorConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (((bool)value) == true) return Brushes.GreenYellow;
                else return Brushes.Red;
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) { throw new NotImplementedException(); }

        }

        void dispt_Tick(object sender, EventArgs e)
        {
            ClockDisplay.Text = DateTime.Now.ToShortTimeString();
        }


        DispatcherTimer dispt = new DispatcherTimer();

        private void viewAll_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(true);
        }

        private void viewNone_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(false);
        }

        private void viewCued_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();

        }

        private void viewMoving_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowMovingMotors();
        }

        private void viewTripped_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowTrippedMotors();
        }

        private void LogOff_Click(object sender, RoutedEventArgs e)
        {

            ApplicationStateLogic.Instance.isAppEnabled = false;

            var MovingMotorsList = Sys.StaticMotorList.Values.Where(t => t.Net == true && t.Brake == false);

            foreach (MotorLogic motor in MovingMotorsList)
            {
                UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select 
                TimedAction.ExecuteWithDelay(new Action(delegate
                {
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 0, motor.MotorIdAsArray); //RUN disable
                    motor.SbcLock = MotorBrake.Locked;

                }), TimeSpan.FromMilliseconds(500));
            }
        }

        List<int> clusterList = new List<int>();


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }


        private void one_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(0, false);
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(1, false);
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(2, false);
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(4, false);
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(5, false);
        }
        private void six_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(6, false);
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(7, false);
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(8, false);
        }

        private void StopAll_Click(object sender, RoutedEventArgs e)
        {

            foreach (var obj in Sys.StaticMotorList.Where(m => m.Value.Brake == false || m.Value.SbcStatusBitIndicator == true))
                obj.Value.soloSM.stopMotor();
        }

        private void Button_ClickInit(object sender, RoutedEventArgs e)
        {

            UdpTx.UniqueInstance.ModBusSigleCommandWrite("SingleDIOCommandGroupedInit", (ushort)1, Sys.StaticMotorListIds.ToArray(), true);
            //  foreach (MotorLogic motor in Sys.StaticMotorList)
            //   {

            //       UdpTx.UniqueInstance.ModBusWrite("DIO", 1, motor.SbcIP, motor.SBCModul, motor.initSbcBit, true);
            //    }
        }
    }
}
