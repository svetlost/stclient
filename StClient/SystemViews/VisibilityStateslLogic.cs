﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace StClient
{


    public class VisibilityStatesLogic : INotifyPropertyChanged
    {
        static List<MotorLogic> SysMotorList { get { return Sys.StaticMotorList.Values.ToList(); } }
        PopupWindows popups = new PopupWindows();

        List<int> clusterList = new List<int>();
        public List<int> AllClusters = new List<int>();

        #region SYSTEM BUTTON AND SIGNAL STATES
        bool _trip, _moving, _brkany, _RefOkAll, _CanAll, _Inh28All, _LocalAny, _sbcany;
        public bool TripAny { get { return _trip; } set { if (_trip != value) { _trip = value; OnNotify("TripAny"); } } }
        public bool MoveAny { get { return _moving; } set { if (_moving != value) { _moving = value; OnNotify("MoveAny"); } } }
        public bool BrkAny { get { return _brkany; } set { if (_brkany != value) { _brkany = value; OnNotify("BrkAny"); } } }

        public bool SbcAny { get { return _sbcany; } set { if (_sbcany != value) { _sbcany = value; OnNotify("SbcAny"); } } }
        public bool LocalAny { get { return _LocalAny; } set { if (_LocalAny != value) { _LocalAny = value; OnNotify("LocalAny"); } } }
        public bool RefOkAll { get { return _RefOkAll; } set { if (_RefOkAll != value) { _RefOkAll = value; OnNotify("RefOkAll"); } } }
        public bool CanAll { get { return _CanAll; } set { if (_CanAll != value) { _CanAll = value; OnNotify("CanAll"); } } }
        public bool Inh28All { get { return _Inh28All; } set { if (_Inh28All != value) { _Inh28All = value; OnNotify("Inh28All"); } } }
        IEnumerable<MotorLogic> listOfMotors;

        bool _jedan = true, _dva = true, tri = true, cetiri = true, pet = true, sest = true, sedam = true;
        public bool Jedan { get { return _jedan; } set { if (_jedan != value) { _jedan = value; OnNotify("Jedan"); } } }
        public bool Dva { get { return _dva; } set { if (_dva != value) { _dva = value; OnNotify("Dva"); } } }
        public bool Tri { get { return tri; } set { if (tri != value) { tri = value; OnNotify("Tri"); } } }
        public bool Cetiri { get { return cetiri; } set { if (cetiri != value) { cetiri = value; OnNotify("Cetiri"); } } }
        public bool Pet { get { return pet; } set { if (pet != value) { pet = value; OnNotify("Pet"); } } }
        public bool Sest { get { return sest; } set { if (sest != value) { sest = value; OnNotify("Sest"); } } }
        public bool Sedam { get { return sedam; } set { if (sedam != value) { sedam = value; OnNotify("Sedam"); } } }

        bool all = false, none = true, cued = true, moving = true, trip = true;
        public bool All { get { return all; } set { if (all != value) { all = value; OnNotify("All"); } } }
        public bool None { get { return none; } set { if (none != value) { none = value; OnNotify("None"); } } }
        public bool Cued { get { return cued; } set { if (cued != value) { cued = value; OnNotify("Cued"); } } }
        public bool Moving { get { return moving; } set { if (moving != value) { moving = value; OnNotify("Moving"); } } }
        public bool Tripped { get { return trip; } set { if (trip != value) { trip = value; OnNotify("Tripped"); } } }

        #endregion

        public void FillOnStartup()
        {
            foreach (int i in AllClusters)
            {
                if (!clusterList.Contains(i))
                    clusterList.Add(i);
            }
        }

        public void ShowOnlyElectricClusteredMotors()
        {
            try
            {
                double d;
                int number;
                TextBlock tb = new TextBlock();

                tb.Name = "Electric"; // promene 16 jun: obavestava popup ko je
                popups.PopupCommand(tb, false);
                if (double.TryParse(tb.Text, out d))
                {
                    ShowNoMotors();
                    number = Convert.ToInt32(d);
                    foreach (MotorLogic motor in SysMotorList.FindAll(m => m.ClusterElectric == number))
                        motor.GuiVisibility = Visibility.Visible;
                }
                else
                    return;

            }
            catch (Exception e)
            {
                MainWindow.mainWindowDispacher.Invoke
                   (DispatcherPriority.Normal, (Action)delegate () { AlertLogic.Add("ERROR: Show Only Electric Clustered Motors: " + e.Message); });
                Log.Write("ERROR: Show Only Electric Clustered Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        public void ShowOnlyLogicClusteredMotors(List<int> numbers)
        {
            try
            {

                ShowNoMotors();

                var motors = from c in SysMotorList
                             from o in numbers
                             where c.ClusterLogical == o
                             select new { c };

                foreach (var motor in motors)
                    motor.c.GuiVisibility = Visibility.Visible;



            }
            catch { }
        }

        public void ClusterListAddRemove(int i, bool DaLiMenja)
        {
            try
            {

                //proveri dal se dodaje il oduzima iz liste
                if (clusterList.Contains(i))
                    clusterList.Remove(i);
                else
                    clusterList.Add(i);

                //prikazi tenutnu listu
                ShowOnlyLogicClusteredMotors(clusterList);
                if (DaLiMenja)
                    ClusterLogicSwicher(i);

            }
            catch (Exception e)
            {
                MainWindow.mainWindowDispacher.Invoke
                   (DispatcherPriority.Normal, (Action)delegate () { AlertLogic.Add("ERROR: Show Only Logical Clustered Motors: " + e.Message); });
                Log.Write("ERROR: Show Only Logical Clustered Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        //public void ShowNoMotors()
        //{

        //}

        public void ShowNoMotors()
        {
            try
            {
                MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                { foreach (MotorLogic motor in SysMotorList) motor.GuiVisibility = Visibility.Collapsed; });
            }
            catch (Exception e)
            {
                MainWindow.mainWindowDispacher.Invoke
                   (DispatcherPriority.Normal, (Action)delegate () { AlertLogic.Add("ERROR: Show No Motors: " + e.Message); });
                Log.Write("ERROR: Show No Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        public void ShowAllShowNone(bool Show)
        {
            try
            {
                if (Show)
                {
                    foreach (int i in AllClusters)
                    {
                        if (!clusterList.Contains(i))
                            clusterList.Add(i);
                    }
                    ShowOnlyLogicClusteredMotors(clusterList);

                    PreviewsSwicher(Properties.AIO.Default.PreviewAll, true);
                    //PhidgetsInterfaces.UniqueInstance.PreviewOutputViews(Properties.AIO.Default.PreviewAll);

                }
                else
                {
                    ShowNoMotors();
                    clusterList.Clear();
                    PreviewsSwicher(Properties.AIO.Default.PreviewNone, false);
                    //PhidgetsInterfaces.UniqueInstance.PreviewOutputViews(Properties.AIO.Default.PreviewNone);
                }
            }
            catch (Exception e)
            {
                MainWindow.mainWindowDispacher.Invoke
                    (DispatcherPriority.Normal, (Action)delegate () { AlertLogic.Add("ERROR: Show All/None Motors: " + e.Message); });
                Log.Write("ERROR: Show All/None Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        public void ShowTrippedMotors()
        {
            ShowNoMotors();
            try
            {
                foreach (MotorLogic motor in SysMotorList.Where(qq => qq.Trip))
                    motor.GuiVisibility = Visibility.Visible;

                //PhidgetsInterfaces.UniqueInstance.PreviewOutputViews(Properties.AIO.Default.PreviewTrip);
                PreviewsSwicher(Properties.AIO.Default.PreviewTrip, false);

            }
            catch (Exception e)
            {
                Log.Write("ERROR: Show Tripped Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        public void ShowMovingMotors()
        {
            ShowNoMotors();
            try
            {
                foreach (MotorLogic motor in SysMotorList.Where(qq => qq.Moving))
                    motor.GuiVisibility = Visibility.Visible;

                //PhidgetsInterfaces.UniqueInstance.PreviewOutputViews(Properties.AIO.Default.PreviewMoving);
                PreviewsSwicher(Properties.AIO.Default.PreviewMoving, false);


            }
            catch (Exception e)
            {
                MainWindow.mainWindowDispacher.Invoke
                    (DispatcherPriority.Normal, (Action)delegate () { AlertLogic.Add("ERROR: Show Moving Motors: " + e.Message); });
                Log.Write("ERROR: Show Moving Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        public void ShowCuedMotors()
        {
            ShowNoMotors();
            try
            {
                //if (CueLogic.UniqueInstance._lvitems.Count > 0)
                MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
               {
                   if (CueLogic.UniqueInstance.CurrentCueItemsList.Count > 0)
                       foreach (CueItem item in CueLogic.UniqueInstance.CurrentCueItemsList)
                           item.Motor.GuiVisibility = Visibility.Visible;
               });
                //PhidgetsInterfaces.UniqueInstance.PreviewOutputViews(Properties.AIO.Default.PreviewCued);
                PreviewsSwicher(Properties.AIO.Default.PreviewCued, false);

            }
            catch (Exception e)
            {
                Log.Write("ERROR: Show Cued Motors: " + e.Message, EventLogEntryType.Error);
            }
        }

        public void ShowMotor(List<MotorLogic> motors)
        {
            ShowNoMotors();
            try
            {
                MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                { foreach (MotorLogic motor in motors) motor.GuiVisibility = Visibility.Visible; });
            }
            catch (Exception e)
            {
                Log.Write("ERROR: Show List Of Motors: " + e.Message, EventLogEntryType.Error);
            }
        }



        public void CalculateSystemIndicators()
        {
            listOfMotors = Sys.StaticMotorList.Values.Where(q => q.Enabled && q.Net); //.FindAll(q => q.Enabled);//TODO puca ovde ako su svi enabled=false u config fajlu 18062010
            if (listOfMotors != null)
            {
                MoveAny = listOfMotors.Any(qq => qq.Moving);
                TripAny = listOfMotors.Any(qq => qq.Trip);
                BrkAny = listOfMotors.All(qq => qq.Brake);
                SbcAny = listOfMotors.All(qq => qq.SbcStatusBitIndicator);
                LocalAny = listOfMotors.Any(qq => qq.Local);
                RefOkAll = listOfMotors.All(qq => qq.RefOk);
                CanAll = listOfMotors.All(qq => qq.Net);
                Inh28All = listOfMotors.All(qq => qq.Inh28);
            }
        }

        void ClusterLogicAllStatesSwicher(bool state)
        {
            Jedan = Dva = Tri = Cetiri = Pet = Sest = Sedam = state;
        }

        public void ClusterLogicSwicher(int i)
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                switch (i)
                {
                    case 1:
                        Jedan = !Jedan;
                        break;
                    case 2:
                        Dva = !Dva;
                        break;
                    case 3:
                        Tri = !Tri;
                        break;
                    case 4:
                        Cetiri = !Cetiri;
                        break;
                    case 5:
                        Pet = !Pet;
                        break;
                    case 6:
                        Sest = !Sest;
                        break;
                    case 7:
                        Sedam = !Sedam;
                        break;
                }

            });
        }

        public void PreviewsSwicher(int i, bool clusterLogicButtonStates)
        {
            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                switch (i)
                {
                    case 10:
                        //All = false;
                        All = true;
                        None = Cued = Moving = Tripped = true;
                        break;
                    case 11:
                        None = false;
                        All = Cued = Moving = Tripped = true;
                        break;
                    case 12:
                        Cued = false;
                        All = None = Moving = Tripped = true;
                        break;
                    case 13:
                        Moving = false;
                        All = None = Cued = Tripped = true;
                        break;
                    case 14:
                        Tripped = false;
                        All = None = Cued = Moving = true;
                        break;
                }
                ClusterLogicAllStatesSwicher(clusterLogicButtonStates);

            });
        }


        #region UniqueInstance
        class MotorsVisibilityStatesCreator
        {
            static MotorsVisibilityStatesCreator() { }
            internal static readonly VisibilityStatesLogic uniqueInstance = new VisibilityStatesLogic();
        }
        public static VisibilityStatesLogic UniqueInstance
        {
            get { return MotorsVisibilityStatesCreator.uniqueInstance; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion

    }
}
