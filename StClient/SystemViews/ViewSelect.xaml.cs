﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using static StClient.SoloStateMachine;

namespace StClient
{
    /// <summary>
    /// Interaction logic for SystemStates2.xaml
    /// </summary>
    public partial class ViewSelect : UserControl
    {
        Properties.Settings Sett = Properties.Settings.Default;
      

        public ViewSelect()
        {
            InitializeComponent();
            this.DataContext = VisibilityStatesLogic.UniqueInstance;
        }



        private void viewAll_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(true);
        }

        private void viewNone_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(false);
        }

        private void viewCued_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();

        }

        private void viewMoving_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowMovingMotors();
        }

        private void viewTripped_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowTrippedMotors();
        }

        private void LogOff_Click(object sender, RoutedEventArgs e)
        {

            ApplicationStateLogic.Instance.isAppEnabled = false;           

            foreach (var obj in Sys.StaticMotorList)
            {
                MotorLogic motor = obj.Value;
                if (motor.Net && motor.Brake == false)
                {
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select 
                    TimedAction.ExecuteWithDelay(new Action(delegate
                    {
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 0, motor.MotorIdAsArray); //RUN disable
                    motor.SbcLock = MotorBrake.Locked;

                    }), TimeSpan.FromMilliseconds(500));
                }
            }
        }

        List<int> clusterList = new List<int>();


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }


        private void one_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(1, false);
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(2, false);
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(3, false);
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(4, false);
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(5, false);
        }
        private void six_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(6, false);
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(7, false);
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(8, false);
        }












    }
}
