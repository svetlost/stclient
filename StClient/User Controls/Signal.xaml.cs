﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Signal.xaml
    /// </summary>
    public partial class Signal : UserControl
    {
        public SolidColorBrush onColor { get { return (SolidColorBrush)GetValue(onColorProperty); } set { SetValue(onColorProperty, value); } }
        public static readonly DependencyProperty onColorProperty = DependencyProperty.Register("onColor", typeof(SolidColorBrush), typeof(Signal), new PropertyMetadata(Brushes.Red));

        public SolidColorBrush offColor { get { return (SolidColorBrush)GetValue(offColorProperty); } set { SetValue(offColorProperty, value); } }
        public static readonly DependencyProperty offColorProperty = DependencyProperty.Register("offColor", typeof(SolidColorBrush), typeof(Signal), new PropertyMetadata(Brushes.Black));

        public string text { get { return (string)textValue.Text; } set { textValue.Text = value; } }

        SolidColorBrush onForeground, offForeground;

        public bool value { get { return (bool)GetValue(valueProperty); } set { SetValue(valueProperty, value); } }
        public static readonly DependencyProperty valueProperty = DependencyProperty.Register("value", typeof(bool), typeof(Signal), new PropertyMetadata(false, new PropertyChangedCallback(OnValuePropertyChanged)));

        private static void OnValuePropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            Signal s = (Signal)dependencyObject;
            s.border.Background = (s.value ? s.onColor : s.offColor);
            s.textValue.Foreground = (s.value ? s.onForeground : s.offForeground);
        }

        public Signal()
        {
            InitializeComponent();

            onForeground = new SolidColorBrush(FindContrastColor(onColor.Color));
            offForeground = new SolidColorBrush(FindContrastColor(offColor.Color));

            onColor.Freeze();
            offColor.Freeze();
            onForeground.Freeze();
            offForeground.Freeze();

            border.Background = offColor;
            textValue.Foreground = offForeground;
        }


        Color FindContrastColor(Color backColor)
        {
            return new Color()
            {
                A = 0xFF,
                R = (byte)((255 - backColor.R) / 1.56789),
                G = (byte)(backColor.G / 0.46607),
                B = (byte)(backColor.R / 4.61681)
            };
        }

    }
}
