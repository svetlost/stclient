﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using LogAlertHB;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UdpBcastTst.xaml
    /// </summary>
    public partial class UdpBcastTst : UserControl
    {
        DispatcherTimer timer = new DispatcherTimer();

        public UdpBcastTst()
        {
            InitializeComponent();

            timer.Tick += timer_Tick4;
            timer.Interval = TimeSpan.FromMilliseconds(200);

            tbSvIn = tbSV_RotInner;
            tbSvOut = tbSV_RotOuter;

            DataContext = Sys.vagoniRxTx;

        }

        void timer_Tick4(object sender, EventArgs e)
        {
            PackBytes();
            s.SendTo(data2, iep1);
        }

        Socket s;
        //byte[] data0 = new byte[24] { 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0xda, 0x32, 0x00, 0x00, 0xcc, 0xcc, 0x00, 0x00 };
        byte[] data2 = new byte[100]{ 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x30, 0x00, 0x64, 0x00, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00    };
        byte[] data2allzero = new byte[100]{ 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x30, 0x00, 0x64, 0x00, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00    };
        IPEndPoint iep1 = new IPEndPoint(IPAddress.Broadcast, 1202);

        static TextBox tbSvIn, tbSvOut;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            timer.Start();
        }


        private void bNP_1_Checked(object sender, RoutedEventArgs e) { bNZ_1.IsChecked = !bNP_1.IsChecked; }
        private void bNZ_1_Checked(object sender, RoutedEventArgs e) { bNP_1.IsChecked = !bNZ_1.IsChecked; }
        private void bNP_2_Checked(object sender, RoutedEventArgs e) { bNZ_2.IsChecked = !bNP_2.IsChecked; }
        private void bNZ_2_Checked(object sender, RoutedEventArgs e) { bNP_2.IsChecked = !bNZ_2.IsChecked; }
        private void bNP_3_Checked(object sender, RoutedEventArgs e) { bNZ_3.IsChecked = !bNP_3.IsChecked; }
        private void bNZ_3_Checked(object sender, RoutedEventArgs e) { bNP_3.IsChecked = !bNZ_3.IsChecked; }
        private void bNP_4_Checked(object sender, RoutedEventArgs e) { bNZ_4.IsChecked = !bNP_4.IsChecked; }
        private void bNZ_4_Checked(object sender, RoutedEventArgs e) { bNP_4.IsChecked = !bNZ_4.IsChecked; }
        private void bNP_5_Checked(object sender, RoutedEventArgs e) { bNZ_5.IsChecked = !bNP_5.IsChecked; }
        private void bNZ_5_Checked(object sender, RoutedEventArgs e) { bNP_5.IsChecked = !bNZ_5.IsChecked; }
        private void bNP_6_Checked(object sender, RoutedEventArgs e) { bNZ_6.IsChecked = !bNP_6.IsChecked; }
        private void bNZ_6_Checked(object sender, RoutedEventArgs e) { bNP_6.IsChecked = !bNZ_6.IsChecked; }

        private void bSTOPALL_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { PackBytes(); data2[37] = 1; s.SendTo(data2, iep1); Log.Write("Wagon 1-6: STOP pressed.", EventLogEntryType.Information); }

        private void bSTOPALL_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[37] = 0; s.SendTo(data2allzero, iep1); }

        private void bAUTO_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PackBytes(); data2[38] = 1;
            s.SendTo(data2, iep1);
            string msg = "Wagon 1-6: AUTO pressed.";
            msg += string.Format("WAG1 FW={0}, BW={1}, GRP={2};", bNP_1.IsChecked, bNZ_1.IsChecked, bGRP_1.IsChecked);
            msg += string.Format("WAG2 FW={0}, BW={1}, GRP={2};", bNP_2.IsChecked, bNZ_2.IsChecked, bGRP_2.IsChecked);
            msg += string.Format("WAG3 FW={0}, BW={1}, GRP={2};", bNP_3.IsChecked, bNZ_3.IsChecked, bGRP_3.IsChecked);
            msg += string.Format("WAG4 FW={0}, BW={1}, GRP={2};", bNP_4.IsChecked, bNZ_4.IsChecked, bGRP_4.IsChecked);
            msg += string.Format("WAG5 FW={0}, BW={1}, GRP={2};", bNP_5.IsChecked, bNZ_5.IsChecked, bGRP_5.IsChecked);
            msg += string.Format("WAG6 FW={0}, BW={1}, GRP={2}.", bNP_6.IsChecked, bNZ_6.IsChecked, bGRP_6.IsChecked);
            Log.Write(msg, EventLogEntryType.Information);
        }

        private void bAUTO_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[38] = 0; s.SendTo(data2allzero, iep1); }

        void PackBytes()
        {
            data2[20] = (byte)((bool)bNP_1.IsChecked ? 1 : 0);
            data2[21] = (byte)((bool)bNZ_1.IsChecked ? 1 : 0);

            data2[23] = (byte)((bool)bNP_2.IsChecked ? 1 : 0);
            data2[24] = (byte)((bool)bNZ_2.IsChecked ? 1 : 0);

            data2[26] = (byte)((bool)bNP_3.IsChecked ? 1 : 0);
            data2[27] = (byte)((bool)bNZ_3.IsChecked ? 1 : 0);

            data2[29] = (byte)((bool)bNP_4.IsChecked ? 1 : 0);
            data2[30] = (byte)((bool)bNZ_4.IsChecked ? 1 : 0);

            data2[32] = (byte)((bool)bNP_5.IsChecked ? 1 : 0);
            data2[33] = (byte)((bool)bNZ_5.IsChecked ? 1 : 0);

            data2[35] = (byte)((bool)bNP_6.IsChecked ? 1 : 0);
            data2[36] = (byte)((bool)bNZ_6.IsChecked ? 1 : 0);

            if ((bool)!bGRP_1.IsChecked) data2[20] = data2[21] = 0;
            if ((bool)!bGRP_2.IsChecked) data2[23] = data2[24] = 0;
            if ((bool)!bGRP_3.IsChecked) data2[26] = data2[27] = 0;
            if ((bool)!bGRP_4.IsChecked) data2[29] = data2[30] = 0;
            if ((bool)!bGRP_5.IsChecked) data2[32] = data2[33] = 0;
            if ((bool)!bGRP_6.IsChecked) data2[35] = data2[36] = 0;


            data2[52] = data2allzero[52] = NumerizeTB(tbSV);

            //tbSV_RotOuter.Text = Sys.potSvRotOuter.ToString();
            //tbSV_RotInner.Text = Sys.potSvRotInner.ToString();
            //tbSV_RotOuter.Text = PhidgetsInterfaces.UniqueInstance.SvRotOuter.ToString();
            //tbSV_RotInner.Text = PhidgetsInterfaces.UniqueInstance.SvRotInner.ToString();


            data2[62] = data2allzero[62] = NumerizeTB(tbSV_RotOuter);
            data2[64] = data2allzero[64] = NumerizeTB(tbSV_RotInner);
            data2[66] = data2allzero[66] = NumerizeTB(tbSV_VagRot);

            //if ((bool)!bENABLE_ROT.IsChecked) data2[35] = data2[36] = 0;  ////////////////////todo!!!!!!!!!!!!!!!!  + stavvi togglebuttone

            data2[45] = (byte)(((bool)bRotInn_M.IsChecked || (bool)bRotCombi_M.IsChecked) && (bool)bENABLE_ROT.IsChecked ? 1 : 0);
            data2[49] = (byte)(((bool)bRotOut_M.IsChecked || (bool)bRotCombi_M.IsChecked) && (bool)bENABLE_ROT.IsChecked ? 1 : 0);

            data2[47] = (byte)((bool)bRotInn_MNEG.IsChecked || (bool)bRotCombi_MNEG.IsChecked || (bool)bRotCombi_MNEG2.IsChecked ? 1 : 0);
            data2[46] = (byte)((bool)bRotInn_MPOS.IsChecked || (bool)bRotCombi_MPOS.IsChecked || (bool)bRotCombi_MPOS2.IsChecked ? 1 : 0);
            data2[51] = (byte)((bool)bRotOut_MNEG.IsChecked || (bool)bRotCombi_MNEG.IsChecked || (bool)bRotCombi_MPOS2.IsChecked ? 1 : 0);
            data2[50] = (byte)((bool)bRotOut_MPOS.IsChecked || (bool)bRotCombi_MPOS.IsChecked || (bool)bRotCombi_MNEG2.IsChecked ? 1 : 0);
        }

        byte NumerizeTB(TextBox tbSV)
        {
            int i;
            try { i = int.Parse(tbSV.Text); }
            catch { i = 0; }

            if (i < 00 || i > 100) i = 0;
            //tbSV.Text = i.ToString();

            return (byte)i;
        }

        private void bSTOP_ROTVAG_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[39] = 1; Log.Write("Wagon ROT: STOP pressed.", EventLogEntryType.Information); }
        private void bSTOP_ROTVAG_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[39] = 0; }

        private void bNZ0_7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[40] = 1; data2[41] = 1; Log.Write("Wagon ROT: BW pressed.", EventLogEntryType.Information); }
        private void bNZ0_7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[40] = 0; data2[41] = 0; }
        private void bNP1_7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[40] = 1; data2[42] = 1; Log.Write("Wagon ROT: FW1 pressed.", EventLogEntryType.Information); }
        private void bNP1_7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[40] = 0; data2[42] = 0; }
        private void bNP2_7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[40] = 1; data2[43] = 1; Log.Write("Wagon ROT: FW2 pressed.", EventLogEntryType.Information); }
        private void bNP2_7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[40] = 0; data2[43] = 0; }

        private void bENABLE_ROT_Unchecked(object sender, RoutedEventArgs e)
        {
            bRotInn_M.IsChecked = bRotOut_M.IsChecked = false;
        }

        private void bRotInn_M_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTINNER_MAN changed state. New state={0}", bRotInn_M.IsChecked), EventLogEntryType.Information); }
        private void bRotInn_MPOS_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTINNER_CW changed state. New state={0}", bRotInn_MPOS.IsChecked), EventLogEntryType.Information); }
        private void bRotInn_MNEG_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTINNER_CCW changed state. New state={0}", bRotInn_MNEG.IsChecked), EventLogEntryType.Information); }

        private void bRotOut_M_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTOUTER_MAN changed state. New state={0}", bRotOut_M.IsChecked), EventLogEntryType.Information); }
        private void bRotOut_MPOS_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTOUTER_CW changed state. New state={0}", bRotOut_MPOS.IsChecked), EventLogEntryType.Information); }
        private void bRotOut_MNEG_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTOUTER_CCW changed state. New state={0}", bRotOut_MNEG.IsChecked), EventLogEntryType.Information); }

        private void bRotCombi_M_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI1_MAN changed state. New state={0}", bRotCombi_M.IsChecked), EventLogEntryType.Information); }
        private void bRotCombi_MPOS_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI1_CW changed state. New state={0}", bRotCombi_MPOS.IsChecked), EventLogEntryType.Information); }
        private void bRotCombi_MNEG_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI1_CCW changed state. New state={0}", bRotCombi_MNEG.IsChecked), EventLogEntryType.Information); }

        private void bRotCombi_MPOS2_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI2_CW changed state. New state={0}", bRotCombi_MPOS2.IsChecked), EventLogEntryType.Information); }
        private void bRotCombi_MNEG2_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI2_CCW changed state. New state={0}", bRotCombi_MNEG2.IsChecked), EventLogEntryType.Information); }

    }
}
