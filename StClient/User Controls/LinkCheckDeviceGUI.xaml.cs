﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace StClient
{
    /// <summary>
    /// Interaction logic for LinkCheckDevice.xaml
    /// </summary>
    public partial class LinkCheckDeviceGUI : UserControl
    {
        public LinkCheckDeviceGUI()
        {
            InitializeComponent();
        }
        public LinkCheckDeviceGUI(object dataContext)
        {
            InitializeComponent();

            this.DataContext = dataContext;
        }
    }

    public class LinkCheckDevice : INotifyPropertyChanged
    {
        string _Title;
        [XmlAttribute("Title")]
        public string Title { get { return _Title; } set { _Title = value; OnNotify("LinkIndicator"); } }

        string _CabinetTitle;
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; } }

        bool _LinkIndicator;
        [XmlIgnore]
        public bool LinkIndicator { get { return _LinkIndicator; } set { _LinkIndicator = value; OnNotify("LinkIndicator"); } }

        public LinkCheckDevice(string title, string cabinet) { _Title = title; _CabinetTitle = cabinet; }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }

    public interface ILinkCheck : INotifyPropertyChanged
    {
        bool LinkIndicator { get; set; }
        string Title { get; set; }
        string CabinetTitle { get; set; }
        void PingDevice();
        void ResponseReceived();
        void ResetIndicator();
    }

    public class Cabinet : INotifyPropertyChanged
    {
        string _CabinetTitle;
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; OnNotify("CabinetTitle"); } }

        public List<ILinkCheck> Devices = new List<ILinkCheck>();

        public Cabinet(List<ILinkCheck> list)
        {
            Devices.AddRange(list);
            if (list.Count > 0) CabinetTitle = list[0].CabinetTitle;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }

    public class Dio1000 : ILinkCheck
    {

        int _CanIpAddress;
        [XmlAttribute("CanIpAddress")]
        public int CanIpAddress { get { return _CanIpAddress; } set { _CanIpAddress = value; OnNotify("CanIpAddress"); } }

        byte _CanNetwork;
        [XmlAttribute("CanNetwork")]
        public byte CanNetwork { get { return _CanNetwork; } set { _CanNetwork = value; OnNotify("CanNetwork"); } }


        public Dio1000(int _canAdr, byte _canNet, string _title, string _cabinet)
        {
            CanIpAddress = _canAdr;
            CanNetwork = _canNet;
            Title = _title;
            CabinetTitle = _cabinet;
        }

        public Dio1000() { }

        public void PingDevice()
        {
           
        }

        public void ResponseReceived() { _LinkIndicator = true; }

        public void ResetIndicator() { _LinkIndicator = false; }

        bool _LinkIndicator = false;
        [XmlIgnore]
        public bool LinkIndicator { get { return _LinkIndicator; } set { _LinkIndicator = value; OnNotify("LinkIndicator"); } }


        string _Title;
        [XmlAttribute("Title")]
        public string Title { get { return _Title; } set { _Title = value; OnNotify("LinkIndicator"); } }

        string _CabinetTitle;
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; OnNotify("CabinetTitle"); } }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
    public class StServer : ILinkCheck
    {

        public StServer(string _title, string _cabinet, string _ipAdr, int _ipPort)
        {
            Title = _title;
            CabinetTitle = _cabinet;
            this.IpAdr = _ipAdr;
            this.IpPort = _ipPort;
        }

        public StServer() { }

        public void PingDevice() { }

        public void ResponseReceived() { _LinkIndicator = true; }

        public void ResetIndicator() { _LinkIndicator = false; }

        bool _LinkIndicator = false;
        [XmlIgnore]
        public bool LinkIndicator { get { return _LinkIndicator; } set { _LinkIndicator = value; OnNotify("LinkIndicator"); } }

        public string IpAdr { get; set; }
        public int IpPort { get; set; }

        string _Title;
        [XmlAttribute("Title")]
        public string Title { get { return _Title; } set { _Title = value; OnNotify("LinkIndicator"); } }

        string _CabinetTitle;
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; OnNotify("CabinetTitle"); } }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
    public class DrivePlc : ILinkCheck
    {

        int _CanIpAddress;
        [XmlAttribute("CanIpAddress")]
        public int CanIpAddress { get { return _CanIpAddress; } set { _CanIpAddress = value; OnNotify("CanIpAddress"); } }

        byte _CanNetwork;
        [XmlAttribute("CanNetwork")]
        public byte CanNetwork { get { return _CanNetwork; } set { _CanNetwork = value; OnNotify("CanNetwork"); } }


        public DrivePlc(int _canAdr, byte _canNet, string _title, string _cabinet)
        {
            CanIpAddress = _canAdr;
            CanNetwork = _canNet;
            Title = _title;
            CabinetTitle = _cabinet;
        }
        public DrivePlc() { }

        public void PingDevice() { }

        public void ResponseReceived() { _LinkIndicator = true; }

        public void ResetIndicator() { _LinkIndicator = false; }

        bool _LinkIndicator = false;
        [XmlIgnore]
        public bool LinkIndicator { get { return _LinkIndicator; } set { _LinkIndicator = value; OnNotify("LinkIndicator"); } }


        string _Title;
        [XmlAttribute("Title")]
        public string Title { get { return _Title; } set { _Title = value; OnNotify("LinkIndicator"); } }

        string _CabinetTitle;
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; OnNotify("CabinetTitle"); } }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}


