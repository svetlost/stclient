using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms;

namespace ExtendedFolderBrowseDialog
{
    public class Win32API
    {
        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SHBrowseForFolder([In] BROWSEINFO lpbi);

        [DllImport("shell32.dll")]
        public static extern int SHGetMalloc([Out, MarshalAs(UnmanagedType.LPArray)] IMalloc[] ppMalloc);

        [DllImport("shell32.dll")]
        public static extern int SHGetSpecialFolderLocation(IntPtr hwnd, int csidl, ref IntPtr ppidl);

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        public static extern bool SHGetPathFromIDList(IntPtr pidl, IntPtr pszPath);

        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage2(HandleRef hWnd, int msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [ComImport, Guid("00000002-0000-0000-c000-000000000046"), SuppressUnmanagedCodeSecurity, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IMalloc
        {
            IntPtr Alloc(int cb);
            void Free(IntPtr pv);
            IntPtr Realloc(IntPtr pv, int cb);
            int GetSize(IntPtr pv);
            int DidAlloc(IntPtr pv);
            void HeapMinimize();
        }

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        [DllImport("user32.dll")]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int cx, int cy, bool bRepaint);
        [DllImport("user32.dll")]
        public static extern IntPtr GetParent(IntPtr hWnd);

        public static uint SWP_ASYNCWINDOWPOS = 0x4000;

        public static uint SWP_DEFERERASE = 0x2000;

        public static uint SWP_DRAWFRAME = 0x0020;

        public static uint SWP_FRAMECHANGED = 0x0020;

        public static uint SWP_HIDEWINDOW = 0x0080;

        public static uint SWP_NOACTIVATE = 0x0010;

        public static uint SWP_NOCOPYBITS = 0x0100;

        public static uint SWP_NOMOVE = 0x0002;

        public static uint SWP_NOOWNERZORDER = 0x0200;

        public static uint SWP_NOREDRAW = 0x0008;

        public static uint SWP_NOREPOSITION = 0x0200;

        public static uint SWP_NOSENDCHANGING = 0x0400;

        public static uint SWP_NOSIZE = 0x0001;

        public static uint SWP_NOZORDER = 0x0004;

        public static uint SWP_SHOWWINDOW = 0x0040;


        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto), ComVisible(false)]
        public class BROWSEINFO
        {
            public IntPtr hwndOwner;
            public IntPtr pidlRoot;
            public IntPtr pszDisplayName;
            public string lpszTitle;
            public int ulFlags;
            public BrowseCallbackProc lpfn;
            public IntPtr lParam;
            public int iImage;
        }

        public const int BFFM_INITIALIZED = 1;
        public const int BFFM_SELCHANGED = 2;

        public delegate int BrowseCallbackProc(IntPtr hwnd, int msg, IntPtr lParam, IntPtr lpData);
    }

    public class MyFolderBrowser : CommonDialog
    {
        private Environment.SpecialFolder m_rootFolder;
        private string m_descriptionText = String.Empty;
        private string m_selectedPath = null;
        private Point m_location = new Point(0, 0);

        public Point Location { get { return m_location; } set { m_location = value; } }

        public string SelectedPath { get { return m_selectedPath; } }

        public string Description
        {
            get { return m_descriptionText; }
            set { m_descriptionText = (value == null) ? string.Empty : value; }
        }

        public override void Reset()
        {
            //m_rootFolder = Environment.SpecialFolder.Desktop;
            m_rootFolder = Environment.SpecialFolder.MyComputer;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Win32API.IMalloc GetSHMalloc()
        {
            Win32API.IMalloc[] mallocArray1 = new Win32API.IMalloc[1];
            Win32API.SHGetMalloc(mallocArray1);
            return mallocArray1[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hwndOwner"></param>
        /// <returns></returns>
        protected override bool RunDialog(System.IntPtr hwndOwner)
        {
            Reset();
            IntPtr ptr1 = IntPtr.Zero;
            bool flag1 = false;
            Win32API.SHGetSpecialFolderLocation(hwndOwner, (int)m_rootFolder, ref ptr1);
            if (ptr1 == IntPtr.Zero)
            {
                Win32API.SHGetSpecialFolderLocation(hwndOwner, 0, ref ptr1);
                if (ptr1 == IntPtr.Zero)
                {
                    throw new Exception("FolderBrowserDialogNoRootFolder");
                }
            }

            //Initialize the OLE to current thread.
            Application.OleRequired();
            IntPtr ptr2 = IntPtr.Zero;
            try
            {
                Win32API.BROWSEINFO browseinfo1 = new Win32API.BROWSEINFO();
                IntPtr ptr3 = Marshal.AllocHGlobal((int)(260 * Marshal.SystemDefaultCharSize));
                IntPtr ptr4 = Marshal.AllocHGlobal((int)(260 * Marshal.SystemDefaultCharSize));
                Win32API.BrowseCallbackProc proc1 = new Win32API.BrowseCallbackProc(this.FolderBrowserDialog_BrowseCallbackProc);
                browseinfo1.pidlRoot = ptr1;
                browseinfo1.hwndOwner = hwndOwner;
                browseinfo1.pszDisplayName = ptr3;
                browseinfo1.lpszTitle = m_descriptionText;
                browseinfo1.ulFlags = 0x40;
                browseinfo1.lpfn = proc1;
                browseinfo1.lParam = IntPtr.Zero;
                browseinfo1.iImage = 0;
                ptr2 = Win32API.SHBrowseForFolder(browseinfo1);

                if (ptr2 != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(ptr4);
                    Marshal.FreeHGlobal(ptr3);
                    flag1 = true;
                }
            }
            finally
            {
                //Win32API.IMalloc malloc1 = GetSHMalloc();
                //malloc1.Free(ptr1);
                //if (ptr2 != IntPtr.Zero)
                //{
                //    malloc1.Free(ptr2);
                //}
            }

            return flag1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="lParam"></param>
        /// <param name="lpData"></param>
        /// <returns></returns>
        private int FolderBrowserDialog_BrowseCallbackProc(IntPtr hwnd, int msg, IntPtr lParam, IntPtr lpData)
        {
            switch (msg)
            {
                case Win32API.BFFM_INITIALIZED:

                    IntPtr handle = Win32API.FindWindow(null, "Browse For Folder");
                    Win32API.SetWindowPos(handle, (IntPtr)0, this.m_location.X, this.m_location.Y, 0, 0, Win32API.SWP_NOSIZE | Win32API.SWP_NOZORDER | Win32API.SWP_DRAWFRAME);

                    break;
                case Win32API.BFFM_SELCHANGED: //Selction Changed
                    {
                        IntPtr ptr1 = lParam;
                        if (ptr1 != IntPtr.Zero)
                        {
                            IntPtr ptr2 = Marshal.AllocHGlobal((int)(260 * Marshal.SystemDefaultCharSize));
                            bool flag1 = Win32API.SHGetPathFromIDList(ptr1, ptr2);
                            m_selectedPath = Marshal.PtrToStringAuto(ptr2);

                            Marshal.FreeHGlobal(ptr2);
                            Win32API.SendMessage2(new HandleRef(null, hwnd), 0x465, 0, flag1 ? 1 : 0);
                        }

                        break;
                    }

            }
            return 0;
        }

    }
}
