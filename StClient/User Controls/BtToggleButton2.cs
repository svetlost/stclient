﻿using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace StClient
{
    class BtToggleButton2 : ToggleButton
    {
        public BtToggleButton2()
        {
            this.PreviewMouseLeftButtonDown += ToggleBB_PreviewMouseLeftButtonDown;
            this.MouseUp += ToggleBB_MouseUp;
        }
        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

    }
}
