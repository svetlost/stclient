﻿using System;
using System.ComponentModel;
using System.Windows.Controls;

namespace StClient
{
    /// <summary>
    /// Interaction logic for CountersSums.xaml
    /// </summary>
    public partial class Imot : UserControl
    {
        public Imot()
        {
            InitializeComponent();
            this.DataContext = ImotLogic.UniqueInstance;
        }
    }

    public class ImotLogic : INotifyPropertyChanged
    {
        ImotLogic() { }
        class ImotLogicCreator
        {
            static ImotLogicCreator() { }
            internal static readonly ImotLogic uniqueInstance = new ImotLogic();
        }
        public static ImotLogic UniqueInstance
        {
            get { return ImotLogicCreator.uniqueInstance; }
        }
        double _ImotTotal;
        public double ImotTotal { get { return _ImotTotal; } set { if (_ImotTotal != value) { _ImotTotal = value; OnNotify("ImotTotal"); } } }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }
}
