﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace StClient.User_Controls
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl, IHavePassword
    {
        public static TabItem loadcells;
        public static Popup pop;
        public LoginView(Popup _pop, TabItem _loadcells)
        {
            InitializeComponent();
            DataContext = new LoginViewModel();
            pop = _pop;
            loadcells = _loadcells;
        }
        public System.Security.SecureString Password
        {
            get
            {
                return UserPassword.SecurePassword;

            }
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {

            pop.IsOpen = false;

        }


    }
}
