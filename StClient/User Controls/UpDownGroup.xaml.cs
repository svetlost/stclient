﻿using LogAlertHB;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UpDownGroup.xaml
    /// </summary>
    public partial class UpDownGroup : UserControl
    {
        public UpDownGroup()
        {
            InitializeComponent();
        }

        private void SelAll_Click(object sender, RoutedEventArgs e) { foreach (var t in Sys.UpDownList) t.Group = true; }

        private void SelNone_Click(object sender, RoutedEventArgs e) { foreach (var t in Sys.UpDownList) t.Group = false; }

        private void _PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            foreach (var t in Sys.UpDownList)
                UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: false, CanAdr: t.canAdr);
            Log.Write(string.Format("(UP pressed HoistGroupGUI). {0}", simpleGroupMemebers()), EventLogEntryType.Information);
        }

        private void _down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var t in Sys.UpDownList.Where(q => q.Group == true))
                UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: true, CanAdr: t.canAdr);
            Log.Write(string.Format("(DOWN pressed HoistGroupGUI). {0}", simpleGroupMemebers()), EventLogEntryType.Information);
        }

        private void _up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var t in Sys.UpDownList.Where(q => q.Group == true))
                UdpTx.UniqueInstance.Send8200PDO(Up: true, Down: false, CanAdr: t.canAdr);
            Log.Write(string.Format("(UP/DOWN released HoistGroupGUI). {0}", simpleGroupMemebers()), EventLogEntryType.Information);
        }

        string simpleGroupMemebers()
        {
            string s = "GROUP MEMBERS:";
            foreach (var t in Sys.UpDownList.Where(q => q.Group == true)) s += (string.Format(" name={0} can={1},", t.Title, t.canAdr));

            return s;
        }
    }
}
