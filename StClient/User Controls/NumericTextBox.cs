﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace StClient
{
    public class NumericTextBox : TextBox
    {

        NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        string decimalSeparator;
        string negativeSign;

        public double LimitMin { get; set; }
        public double LimitMax { get; set; }


        private bool isStringType;
        public bool IsStringType { get { return isStringType; } set { isStringType = value; } }

        public NumericTextBox()
        {
            decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            negativeSign = numberFormatInfo.NegativeSign;
            this.TextChanged += new TextChangedEventHandler(NumericTextBox_TextChanged);

        }

        void NumericTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if (isStringType) return;

            //if (this.Text.Length > 0 && this.Text != negativeSign && this.Text != decimalSeparator && this.Text != decimalSeparator)
            //{
            //    if (this.Text == "-.")
            //    {
            //        this.Text = "-0.";
            //        return;
            //    }
            //    if (Convert.ToDouble(this.Text) < LimitMin)
            //    {
            //        this.Text = LimitMin.ToString();
            //        this.CaretIndex = LimitMin.ToString().Length;
            //    }
            //    if (Convert.ToDouble(this.Text) > LimitMax)
            //    {
            //        this.Text = LimitMax.ToString();
            //        this.CaretIndex = LimitMax.ToString().Length;
            //    }
            //}

            //if (this.CaretIndex == 0 && this.Text.Contains(negativeSign))
            //{
            //    e.Handled = true;
            //}
        }


        protected override void OnPreviewTextInput(System.Windows.Input.TextCompositionEventArgs e)
        {
            if (isStringType) return;

            //string groupSeparator = numberFormatInfo.NumberGroupSeparator;
            string keyInput = e.Text.ToString();
            foreach (Char Key in keyInput)
            {
                if (Char.IsDigit(Key))
                {
                    // Digits are OK
                    //this.Background = Brushes.Snow;
                }

                else if (keyInput.Equals(negativeSign))
                {
                    if (this.Text.Contains(negativeSign) || this.CaretIndex != 0)
                        e.Handled = true;
                }
                else if (keyInput.Equals(decimalSeparator))
                {
                    if (Text.Contains(decimalSeparator))
                    {
                        e.Handled = true;
                    }
                }
                else if (Key == '\b')
                {
                    // Backspace key is OK
                    // this.Background = Brushes.Snow;
                }
                else
                {
                    // Consume this invalid key and beep
                    e.Handled = true;
                }


            }

        }

    }
}
