﻿namespace StClient.User_Controls
{
    interface IHavePassword
    {
        System.Security.SecureString Password { get; }
    }
}
