﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for KompsGUI.xaml
    /// </summary>
    public partial class KompsGUI : UserControl
    {
        public KompsGUI()
        {
            InitializeComponent();

            Komp1_L1.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L1;
            Komp2_L2.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L2;
            Komp3_L3.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L3;
            Komp4_R1.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R1;
            Komp5_R2.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R2;
            Komp6_R3.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R3;
            Komp7_ROT.DataContext = KompenzacioneRxTx.UniqueInstance.KP_ROT;

            //Komp7_ROT.
        }
    }
}
