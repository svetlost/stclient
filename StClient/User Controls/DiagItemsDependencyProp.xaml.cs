﻿using System;
using System.ComponentModel;
using System.Windows.Controls;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Imap.xaml
    /// </summary>
    public partial class DiagItemUserControl : UserControl
    {
        public DiagItemUserControl(DiagItemValues ItemValues)
        {
            InitializeComponent();

            this.DataContext = ItemValues;
        }
    }

    public class DiagItemValues : INotifyPropertyChanged
    {

        double _value;
        public int LenzeIndex { get; set; }
        public byte LenzeSubIndex { get; set; }
        public byte Cluster { get; set; }
        public string Text { get; set; }
        public string Units { get; set; }
        public double Value { get { return _value; } set { if (_value != value) { _value = value; OnNotify("Value"); } } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
    }
}
