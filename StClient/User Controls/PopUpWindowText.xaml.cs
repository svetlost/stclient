﻿using System;
using System.Windows;

namespace StClient
{
    /// <summary>
    /// Interaction logic for PopUpWindowText.xaml
    /// </summary>
    public partial class PopUpWindowText : Window
    {
        bool reference  = false;
        MotorLogic motor;
        public PopUpWindowText(double X, double Y, string title, string msgTxt, MotorLogic _motor = null, bool _reference=false)
        {
            InitializeComponent();

            motor = _motor;
            tbTitle.Text = title;
            tbText.Text = msgTxt;
            this.Left = X;
            this.Top = Y;
            reference = _reference;
            if (_motor == null)
            {
                forReference.Height = new GridLength(0);
            }
            this.ShowDialog();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

            if (reference)
            {
                double parsedvalue;
                if (double.TryParse(popuptextbox.Text, out parsedvalue))
                {
                    motor.NewReferencePoint = parsedvalue;
                }

                else
                {
                    DialogResult = false;
                }
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            this.Topmost = false;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            this.Topmost = true;
        }

        
    }



}
