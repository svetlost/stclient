﻿using LogAlertHB;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UpDownControl.xaml
    /// </summary>
    public partial class RotControlALB : UserControl
    {
        public RotControlALB(UpDownLogicALB udl)
        {
            InitializeComponent();

            this.DataContext = t = udl;
        }
        UpDownLogicALB t;
        private void _up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, true);
            Log.Write(string.Format("(UP pressed ROT). {0}", hoistInfo()), EventLogEntryType.Information);
        }

        private void _PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, false);
            //UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, false);
            Log.Write(string.Format("(UP/DOWN released ROT). {0}", hoistInfo()), EventLogEntryType.Information);
        }

        private void _down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, true);
            Log.Write(string.Format("(DOWN pressed ROT). {0}", hoistInfo()), EventLogEntryType.Information);
        }
        string hoistInfo() { return string.Format("ROT: name={0} can={1},", t.Title, t.MotorID); }

        private void clickStopRot(object sender, MouseButtonEventArgs e)
        {
            _up.IsChecked = _down.IsChecked = false;
            UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, false);
            UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, false);
        }

        private void _up_Checked(object sender, RoutedEventArgs e)
        {
            UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, true);
            _down.IsChecked = false;
        }

        private void _up_Unchecked(object sender, RoutedEventArgs e)
        {
            UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, false);
        }

        private void _down_Unchecked(object sender, RoutedEventArgs e)
        {
            UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, false);
        }

        private void _down_Checked(object sender, RoutedEventArgs e)
        {
            UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, true);
            _up.IsChecked = false;
        }
    }



}
