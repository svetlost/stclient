﻿using LogAlertHB;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UpDownControl.xaml
    /// </summary>
    public partial class UpDownControlALB : UserControl
    {
        public UpDownControlALB(UpDownLogicALB udl)
        {
            InitializeComponent();

            this.DataContext = t = udl;

            if (t.MotorID == 26 || t.MotorID == 27 || t.MotorID == 28) //RC 1 - 3 na sceni nemaju indikatore ALB
                limHavSIG.Visibility = limNegSIG.Visibility = limPosSIG.Visibility = localSIG.Visibility = overCurrentSIG.Visibility = Visibility.Hidden;

            t.SetSpeed = 5;
        }
        UpDownLogicALB t;
        private void _up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, true);
            Log.Write(string.Format("(UP pressed HoistGUI). {0}", hoistInfo()), EventLogEntryType.Information);
        }

        private void _PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, false);
            UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, false);
            Log.Write(string.Format("(UP/DOWN released HoistGUI). {0}", hoistInfo()), EventLogEntryType.Information);
        }

        private void _down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, true);
            Log.Write(string.Format("(DOWN pressed HoistGUI). {0}", hoistInfo()), EventLogEntryType.Information);
        }
        string hoistInfo() { return string.Format("HOIST: name={0} can={1},", t.Title, t.MotorID); }

        private void InGroup_Checked(object sender, RoutedEventArgs e) { }

        private void InGroup_Unchecked(object sender, RoutedEventArgs e) { }
    }

    public class UpDownLogicALB : INotifyPropertyChanged
    {

        int _motorID = 301, _cluster = 0, _DIO_byte = 0, _DIO_cobID = 0, _CanNet;
        static int _cmdUp, _cmdDown;
        string _title = "simpleHoist";
        bool _group, _limHav, _limNeg, _limPos, _local, _overCurrent;

        [XmlIgnore]
        public static int cmdUp
        {
            get { return _cmdUp; }
            set
            {
                if (_cmdUp != value)
                {
                    UdpTx.UniqueInstance.SendKontakterALB_PDO(value, 768 + 55);
                    _cmdUp = value;
                }
            }
        }
        [XmlIgnore]
        public static int cmdDown
        {
            get { return _cmdDown; }
            set
            {
                if (_cmdDown != value)
                {
                    UdpTx.UniqueInstance.SendKontakterALB_PDO(value, 768 + 56);
                    _cmdDown = value;
                }
            }
        }
        [XmlIgnore]
        public bool Group { get { return _group; } set { _group = value; OnNotify("Group"); } }

        [XmlIgnore]
        public bool limHav { get { return _limHav; } set { _limHav = value; OnNotify("limHav"); } }
        [XmlIgnore]
        public bool limNeg { get { return _limNeg; } set { _limNeg = value; OnNotify("limNeg"); } }
        [XmlIgnore]
        public bool limPos { get { return _limPos; } set { _limPos = value; OnNotify("limPos"); } }
        [XmlIgnore]
        public bool overCurrent { get { return _overCurrent; } set { _overCurrent = value; OnNotify("overCurrent"); } }
        [XmlIgnore]
        public bool local { get { return _local; } set { _local = value; OnNotify("local"); } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }
        [XmlAttribute("Cluster")]
        public int Cluster { get { return _cluster; } set { _cluster = value; } }
        [XmlAttribute("CanNet")]
        public int CanNet { get { return _CanNet; } set { _CanNet = value; } }
        [XmlAttribute("DIO_byte")]
        public int DIO_byte { get { return _DIO_byte; } set { _DIO_byte = value; } }
        [XmlAttribute("DIO_cobID")]
        public int DIO_cobID { get { return _DIO_cobID; } set { _DIO_cobID = value; } }

        Visibility _GuiVisibility = Visibility.Visible;
        [XmlIgnore]
        public Visibility GuiVisibility { get { return _GuiVisibility; } set { if (_GuiVisibility != value) { _GuiVisibility = value; OnNotify("GuiVisibility"); } } }

        //alb rotacija, canadr=13, cannet=3
        bool _Brk, _Inh, _ZeroSpeed, _Trip;
        int _CurrentSpeed, _SetSpeed = 5;
        [XmlIgnore]
        public bool Brk { get { return _Brk; } set { if (_Brk != value) { _Brk = value; OnNotify("Brk"); } } }
        [XmlIgnore]
        public bool Inh { get { return _Inh; } set { if (_Inh != value) { _Inh = value; OnNotify("Inh"); } } }
        [XmlIgnore]
        public bool ZeroSpeed { get { return _ZeroSpeed; } set { if (_ZeroSpeed != value) { _ZeroSpeed = value; OnNotify("ZeroSpeed"); } } }
        [XmlIgnore]
        public bool Trip { get { return _Trip; } set { if (_Trip != value) { _Trip = value; OnNotify("Trip"); } } }
        [XmlIgnore]
        public int CurrentSpeed { get { return _CurrentSpeed; } set { if (_CurrentSpeed != value) { _CurrentSpeed = value; OnNotify("CurrentSpeed"); } } }
        [XmlIgnore]
        public int SetSpeed
        {
            get { return _SetSpeed; }
            set
            {
                if (_SetSpeed != value)
                {
                    _SetSpeed = value; OnNotify("SetSpeed");
                    byte[] temp = new byte[StClient.Properties.CanSettings.Default.CanOverUdpMessageLength];
                    temp[0] = BitConverter.GetBytes(value)[0];
                    temp[1] = BitConverter.GetBytes(value)[1];
                    temp[2] = BitConverter.GetBytes(value)[2];
                    temp[3] = BitConverter.GetBytes(value)[3];

                    UdpTx.UniqueInstance.SendPDORaw(768 + 52, 2, temp, 0, 8);//send speed to plc
                }
            }
        }



        public UpDownLogicALB() { }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }


}
