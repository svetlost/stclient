﻿using LogAlertHB;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UpDownGroup.xaml
    /// </summary>
    public partial class UpDownGroupALB : UserControl
    {
        public UpDownGroupALB()
        {
            InitializeComponent();

            //int controlMargin = 5;
            Thickness buttonMargin = new Thickness(0, 5, 0, 0);

            new KontGroupVisibility("MAIN STAGE", 1);
            new KontGroupVisibility("PROSC", 2);
            new KontGroupVisibility("BACK STAGE", 3);
            new KontGroupVisibility("LIGHTING", 4);
            new KontGroupVisibility("ROT", 5);

            int row = 7;

            var b = new Button() { Content = "ALL", Margin = buttonMargin };
            KontGroupGrid.Children.Add(b);
            Grid.SetRow(b, row++);
            b.Click += (sender, evArgs) =>
            {
                foreach (var d in Sys.UpDownListALB) d.GuiVisibility = Visibility.Visible;
                foreach (var h in KontGroupVisibility.KontClusterList) h.setVisable();
            };

            b = new Button() { Content = "GROUPED", Margin = buttonMargin };
            KontGroupGrid.Children.Add(b);
            Grid.SetRow(b, row++);
            b.Click += (sender, evArgs) =>
            {
                foreach (var d in Sys.UpDownListALB) d.GuiVisibility = (d.Group == true ? Visibility.Visible : Visibility.Collapsed);
            };

            b = new Button() { Content = "NONE", Margin = buttonMargin };
            KontGroupGrid.Children.Add(b);
            Grid.SetRow(b, row++);
            b.Click += (sender, evArgs) =>
            {
                foreach (var d in Sys.UpDownListALB) d.GuiVisibility = Visibility.Collapsed;
                foreach (var h in KontGroupVisibility.KontClusterList) h.setCollapsed();
            };

            b = new Button() { Content = "filler", Margin = buttonMargin, Visibility = Visibility.Hidden };
            KontGroupGrid.Children.Add(b);
            Grid.SetRow(b, row++);

            foreach (var s in KontGroupVisibility.KontClusterList)
            {
                b = new Button() { Content = s.GroupClusterName, Margin = buttonMargin };
                //var curID = s.GroupClusterID;
                KontGroupGrid.Children.Add(b);
                Grid.SetRow(b, row++);
                b.Click += (sender, evArgs) =>
                {
                    //var vis = s.toggleVisibility();
                    s.toggleVisibility();
                    foreach (var d in Sys.UpDownListALB.Where(f => f.Cluster == s.GroupClusterID)) d.GuiVisibility = s.ClusterVisibility;
                };
            }
        }

        private void SelAll_Click(object sender, RoutedEventArgs e) { foreach (var t in Sys.UpDownListALB.Where(v => v.GuiVisibility == Visibility.Visible)) t.Group = true; }

        private void SelNone_Click(object sender, RoutedEventArgs e) { foreach (var t in Sys.UpDownListALB.Where(v => v.GuiVisibility == Visibility.Visible)) t.Group = false; }

        private void _PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            foreach (var t in Sys.UpDownListALB)
            {
                UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, false);
                UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, false);
            }
            //                UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: false, CanAdr: t.canAdr);
            Log.Write(string.Format("(UP/DOWN released HoistGroupGUI). {0}", simpleGroupMemebers()), EventLogEntryType.Information);
        }

        private void _down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var t in Sys.UpDownListALB.Where(q => q.GuiVisibility == Visibility.Visible && q.Group == true))
                UpDownLogicALB.cmdDown = Utl.SetBitOfInt(UpDownLogicALB.cmdDown, (byte)t.MotorID, true);
            Log.Write(string.Format("(DOWN pressed HoistGroupGUI). {0}", simpleGroupMemebers()), EventLogEntryType.Information);
        }

        private void _up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var t in Sys.UpDownListALB.Where(q => q.GuiVisibility == Visibility.Visible && q.Group == true))
                UpDownLogicALB.cmdUp = Utl.SetBitOfInt(UpDownLogicALB.cmdUp, (byte)t.MotorID, true);
            Log.Write(string.Format("(UP pressed HoistGroupGUI). {0}", simpleGroupMemebers()), EventLogEntryType.Information);
        }

        string simpleGroupMemebers()
        {
            string s = "GROUP MEMBERS:";
            foreach (var t in Sys.UpDownListALB.Where(q => q.Group == true)) s += (string.Format(" name={0} can={1},", t.Title, t.MotorID));

            return s;
        }
    }

    public class KontGroupVisibility
    {
        public int GroupClusterID { get; }
        public string GroupClusterName { get; }
        Visibility _visibility = Visibility.Visible;
        public Visibility ClusterVisibility { get { return _visibility; } set { _visibility = value; } }
        public static List<KontGroupVisibility> KontClusterList = new List<KontGroupVisibility>();
        public KontGroupVisibility(string ClusterName, int ClusterID)
        {
            GroupClusterID = ClusterID;
            GroupClusterName = ClusterName;
            KontClusterList.Add(this);
        }

        public void toggleVisibility()
        {
            ClusterVisibility = (ClusterVisibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
            //return ClusterVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }
        public void setVisable() { ClusterVisibility = Visibility.Visible; }
        public void setCollapsed() { ClusterVisibility = Visibility.Collapsed; }
    }
}
