﻿using System.Windows.Controls;

namespace StClient
{
    /// <summary>
    /// Interaction logic for LinkCheckCabinet.xaml
    /// </summary>
    public partial class LinkCheckCabinetGUI : UserControl
    {
        public LinkCheckCabinetGUI(Cabinet cab)
        {
            InitializeComponent();

            DataContext = cab;

            foreach (var pp in cab.Devices) wrpPanel.Children.Add(new LinkCheckDeviceGUI() { DataContext = pp });
        }
        //public LinkCheckCabinetGUI(Cabinet cab)
        //{
        //    InitializeComponent();

        //    DataContext = cab;

        //    foreach (var pp in cab.Devices) wrpPanel.Children.Add(new LinkCheckDeviceGUI() { DataContext = pp });
        //}
    }
}
