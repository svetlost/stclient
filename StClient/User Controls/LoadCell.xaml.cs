﻿using LogAlertHB;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace StClient.Motors
{
    /// <summary>
    /// Interaction logic for LoadCell.xaml
    /// </summary>
    public partial class LoadCell : UserControl
    {


        public MotorLogic _motor;
        PopupWindows popups = new PopupWindows();
        public LoadCell(MotorLogic motor)
        {
            InitializeComponent();
            this.DataContext = motor;
            this._motor = motor;
        }

        private void ZeroLoad_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.SP_Popup(_motor, sender as TextBlock, false, true);
            Log.Write(string.Format("motorGUI: ZERO LOAD  motor ID={0}, title={1}, zero load={2} .",
                _motor.MotorID, _motor.Title, _motor.ZeroLoad), EventLogEntryType.Information);
        }

        private void RatedLoad_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.SP_Popup(_motor, sender as TextBlock, false, true);
            Log.Write(string.Format("motorGUI: RATED LOAD  motor ID={0}, title={1}, rated load={2} .",
                _motor.MotorID, _motor.Title, _motor.RatedLoad), EventLogEntryType.Information);
        }

        private void OverLoad_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.SP_Popup(_motor, sender as TextBlock, false, true);
            Log.Write(string.Format("motorGUI: OVER LOAD  motor ID={0}, title={1}, overload={2} .",
                _motor.MotorID, _motor.Title, _motor.OverLoadKg), EventLogEntryType.Information);
        }

        private void UnderLoad_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popups.SP_Popup(_motor, sender as TextBlock, false, true);
            Log.Write(string.Format("motorGUI: UNDER LOAD  motor ID={0}, title={1}, underload={2} .",
                _motor.MotorID, _motor.Title, _motor.UnderLoadKg), EventLogEntryType.Information);
        }

        private void SetZero_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MysqlUpdate("loadcell_zeroload", _motor.ZeroLoad.ToString(), _motor.IpAddress.ToString());
        }

        private void SetRated_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MysqlUpdate("loadcell_ratedload", _motor.RatedLoad.ToString(), _motor.IpAddress.ToString());
        }

        private void SetOver_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MysqlUpdate("loadcell_overload", _motor.OverLoadKg.ToString(), _motor.IpAddress.ToString());
        }

        private void SetUnder_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MysqlUpdate("loadcell_underload", _motor.UnderLoadKg.ToString(), _motor.IpAddress.ToString());
        }


        private void SetUnderEn_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            bool status = false;
            status = MysqlUpdate("loadcell_underload_enabled", _motor.loadCellUnderLoadEnabled.ToString(), _motor.IpAddress.ToString());
            if (status && _motor.loadCellUnderLoadEnabled == true) { _motor.UnderLoadVisibility = Visibility.Visible; }
            else { _motor.UnderLoadVisibility = Visibility.Collapsed; }
        }

        private void SetOvEn_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            bool status = false;
            status = MysqlUpdate("loadcell_overload_enabled", _motor.loadCellOverLoadEnabled.ToString(), _motor.IpAddress.ToString());
            if (status && _motor.loadCellOverLoadEnabled) { _motor.OverLoadVisibility = Visibility.Visible; }
            else { _motor.OverLoadVisibility = Visibility.Collapsed; }
        }


        public bool MysqlUpdate(string column, string value, string ip)
        {

            SqlConnection conn = new SqlConnection(@"Server=DESKTOP-QGRREN4;Database=serverMicrosoft;Trusted_Connection=Yes;");
            try
            {
               
                conn.Open();
                SqlCommand cmd = new SqlCommand("Update motor_list set " + column + "='" + value + "' WHERE  ip_address = '" + ip + "';", conn);

                int numRowsUpdated = cmd.ExecuteNonQuery();

                cmd.Dispose();

                TimedAction.ExecuteWithDelay(new Action(delegate
                {
                    // set value to 0 after a second
                    Description.Text = "";
                }), TimeSpan.FromSeconds(5));

                if (numRowsUpdated == 0) { Description.Text = column + " Not updated"; return false; }
                else
                { Description.Text = column + " Updated"; return true; }

            }
            catch (SqlException exSql)
            {
                Console.Error.WriteLine(exSql.StackTrace);
                return false;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.StackTrace);
                return false;
            }
            finally
            {
                if (conn != null) { conn.Close(); }
            }
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion

       
    }
}

