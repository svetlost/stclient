﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for PopUpWindow.xaml
    /// </summary>
    public partial class PopUpWindow : Window, INotifyPropertyChanged
    {
        public string ReturnValue { get; set; }
        public string CurrentValue { get; set; }
        double numberToProcess;
        private bool _spenabled, isStringType;
        public bool IsStringType { get { return isStringType; } set { isStringType = value; } }
        public bool SpSvEnabled { get { return _spenabled; } set { if (_spenabled != value) { _spenabled = value; OnNotify("SpSvEnabled"); } } }
        public double LimitMin { get; set; }
        public double LimitMax { get; set; }

        public string ValueDescription { get; set; }
        TextBlock tblock;

        //bool PopupAlreadyOpen() { return (App.Current.Windows.Count > 2); }

        public PopUpWindow(TextBlock _sender, bool isString, double X, double Y)
        {
            try
            {
                //if (App.Current.Windows.Count > 2) this.Close();
                InitializeComponent();

                this.DataContext = this;
                tb_TextChanged(popuptextbox, null);

                LimitMax = 100000;
                LimitMin = -100000;
                tblock = _sender;
                this.ValueDescription = tblock.Name; //promene 16 jun:
                this.CurrentValue = tblock.Text;
                this.Title = string.Format("Enter value for {0}.", ValueDescription);
                IsStringType = isString;
                Sys.receivedTxtBlock = popuptextbox;
                this.Left = X;
                this.Top = Y;

                this.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public PopUpWindow(double min, double max, TextBlock _sender, double X, double Y, ShowPercButtons showButtons)
        {
            try
            {
                InitializeComponent();

                this.DataContext = this;
                tb_TextChanged(popuptextbox, null);

                LimitMax = max;
                LimitMin = min;
                IsStringType = false;
                tblock = _sender;
                this.ValueDescription = tblock.Name;
                this.CurrentValue = tblock.Text;
                Sys.receivedTxtBlock = popuptextbox;
                this.Left = X;
                this.Top = Y;
                if (showButtons == ShowPercButtons.Show) btnOK_1.Visibility = btnOK_2.Visibility = btnOK_3.Visibility = btnOK_4.Visibility = btnOK_5.Visibility = Visibility.Visible;

                this.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public PopUpWindow(double min, double max, TextBlock _sender, double X, double Y) : this(min, max, _sender, X, Y, ShowPercButtons.NoShow) { }

        //public PopUpWindow(double min, double max, TextBlock _sender, double X, double Y)
        //{
        //    try
        //    {
        //        InitializeComponent();

        //        this.DataContext = this;
        //        tb_TextChanged(popuptextbox, null);

        //        LimitMax = max;
        //        LimitMin = min;
        //        IsStringType = false;
        //        tblock = _sender;
        //        this.ValueDescription = tblock.Name;
        //        this.CurrentValue = tblock.Text;
        //        Sys.receivedTxtBlock = popuptextbox;
        //        this.Left = X;
        //        this.Top = Y;
        //        this.ShowDialog();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //}

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            double t = 0;
            if (!IsStringType)
            {

                if (double.TryParse(popuptextbox.Text, out t))
                {
                    t = (t > LimitMax ? LimitMax : t);
                    t = (t < LimitMin ? LimitMin : t);
                }
            }

            if (popuptextbox.Text == "")
                tblock.Text = CurrentValue;
            else
                if (!IsStringType)
            {
                tblock.Text = string.Format("{0:0.0}", t);
            }
            else
                tblock.Text = popuptextbox.Text;

            DialogResult = true;
            this.Close();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            Numbers.Visibility = Visibility.Visible;

            switch (ValueDescription) //promene 16 jun: sve ovo je novo
            {
                case "Copy":
                    tbTitle.Text = string.Format("Copy current cue to desired location");
                    textBlock1.Text = "Copy To:";
                    break;
                case "Description":
                    tbTitle.Text = string.Format("Motor Description");
                    textBlock1.Text = "Description:";
                    Numbers.Visibility = Visibility.Hidden;
                    break;
                case "Electric":
                    tbTitle.Text = string.Format("Electric Cluster Number");
                    textBlock1.Text = "Cluster:";
                    break;
                case "Logic":
                    tbTitle.Text = string.Format("Logic Cluster Number");
                    textBlock1.Text = "Cluster:";
                    break;
                case "Cue":
                    LimitMin = 0;
                    LimitMax = 500;
                    tbTitle.Text = string.Format("Enter value for CUE NUMBER, min={0}, max={1}", LimitMin, LimitMax);
                    textBlock1.Text = "CUE No:";
                    break;
                case "Group":
                    LimitMin = -1;
                    LimitMax = Sys.StaticGroupList.Count - 1;
                    tbTitle.Text = string.Format("Enter value for GROUP NUMBER, min={0}, max={1} ; -1 Removes from group", LimitMin, LimitMax);
                    textBlock1.Text = "GROUP No:";
                    break;
                case "CueSP":
                    tbTitle.Text = string.Format("SP for all motors in cue, min={0:0.0}, max={1:0.0}", LimitMin, LimitMax);
                    textBlock1.Text = "Cue SP";
                    break;
                case "CueSV":
                    tbTitle.Text = string.Format("SV for all motors in cue, min={0:0.0}, max={1:0.0}", LimitMin, LimitMax);
                    textBlock1.Text = "Cue SV";
                    break;
                default:
                    tbTitle.Text = string.Format("Enter value for {0}, min={1:0.0}, max={2:0.0}", ValueDescription, LimitMin, LimitMax);
                    textBlock1.Text = ValueDescription;
                    break;
            }



            popuptextbox.Text = tblock.Text;
            popuptextbox.Focus();
            popuptextbox.SelectAll();
        }

        #region Provera
        //proveri svaki unos - dozvoljava brojeve
        private void tb_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (IsStringType) return;
            else if (e.Key == Key.Left || e.Key == Key.Home || e.Key == Key.Delete || e.Key == Key.Enter || e.Key == Key.Escape || e.Key == Key.Right || e.Key == Key.Back || ValidateTextInput(e.Key.ToString())) { return; }
            else if (e.Key == Key.OemMinus) { if (((TextBox)sender).Text.Contains("-") || ((TextBox)sender).CaretIndex != 0) { e.Handled = true; return; } }
            else if (e.Key == Key.Space) { e.Handled = true; return; }
            else if (e.Key == Key.Decimal || e.Key == Key.OemPeriod) { if (((TextBox)sender).Text.Contains(".")) { e.Handled = true; return; } }
            else if (e.Key == Key.Subtract) { if (((TextBox)sender).Text.Contains("-") || ((TextBox)sender).CaretIndex != 0) { e.Handled = true; return; } }
            else { e.Handled = true; }
        }

        private void tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsStringType) { SpSvEnabled = true; return; }
            if (!ValidateTextInput(((TextBox)sender).Text)) { SpSvEnabled = false; return; }
            if (((TextBox)sender).Text.Contains("-.")) { SpSvEnabled = false; return; }
            try
            {
                double.TryParse(((TextBox)sender).Text, out numberToProcess);
                if (numberToProcess < LimitMin) { SpSvEnabled = false; return; }
                if (numberToProcess > LimitMax) { SpSvEnabled = false; return; }
                else { SpSvEnabled = true; }
            }
            catch (Exception ex) { LogAlertHB.Log.Write(ex.Message, EventLogEntryType.Error); }
        }

        private bool ValidateTextInput(string aTextInput)
        {
            Match lInvalidMatch = Regex.Match(aTextInput, "[0-9]");
            return (lInvalidMatch.Success == true);
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion

        private void Button1_Click(object sender, RoutedEventArgs e) { processKey("1"); }
        private void Button2_Click(object sender, RoutedEventArgs e) { processKey("2"); }
        private void Button3_Click(object sender, RoutedEventArgs e) { processKey("3"); }
        private void Button4_Click(object sender, RoutedEventArgs e) { processKey("4"); }
        private void Button5_Click(object sender, RoutedEventArgs e) { processKey("5"); }
        private void Button6_Click(object sender, RoutedEventArgs e) { processKey("6"); }
        private void Button7_Click(object sender, RoutedEventArgs e) { processKey("7"); }
        private void Button8_Click(object sender, RoutedEventArgs e) { processKey("8"); }
        private void Button9_Click(object sender, RoutedEventArgs e) { processKey("9"); }
        private void Button0_Click(object sender, RoutedEventArgs e) { processKey("0"); }
        private void Button00_Click(object sender, RoutedEventArgs e) { processKey("00"); }
        private void ButtonDot_Click(object sender, RoutedEventArgs e) { processKey("."); }
        private void ButtonMinus_Click(object sender, RoutedEventArgs e) { processKey("-"); }
        private void Button1Del(object sender, RoutedEventArgs e) { processKey("del"); }

        public void processKey(string s)
        {
            int i = popuptextbox.SelectionStart;
            int k;

            if (popuptextbox.SelectedText.Length > 0) popuptextbox.SelectedText = "";

            if (s == "del")
            {
                if (i == 0) return;
                popuptextbox.Text = popuptextbox.Text.Remove(i - 1, 1); popuptextbox.SelectionStart = i - 1; popuptextbox.Focus();
            }
            else if (int.TryParse(s, out k))
            {
                popuptextbox.Text = popuptextbox.Text.Insert(i, s); popuptextbox.SelectionStart = i + 1; popuptextbox.Focus();
            }
            else if (s == "-" || s == ".")
            {
                if (popuptextbox.Text.Contains(s))
                {
                    popuptextbox.Text = popuptextbox.Text.Replace(s, ""); popuptextbox.SelectionStart = i; popuptextbox.Focus();
                }
                else
                {
                    if (s == "-")
                    {
                        popuptextbox.Text = popuptextbox.Text.Insert(0, "-"); popuptextbox.SelectionStart = i + 1; popuptextbox.Focus();
                    }
                    else
                    {
                        popuptextbox.Text = popuptextbox.Text.Insert(i, "."); popuptextbox.SelectionStart = i + 1; popuptextbox.Focus();
                    }
                }
            }
        }

        private void btnOK_Click_1(object sender, RoutedEventArgs e) { popuptextbox.Text = (.1 * LimitMax).ToString(); btnOK_Click("", new RoutedEventArgs()); }
        private void btnOK_Click_2(object sender, RoutedEventArgs e) { popuptextbox.Text = (.25 * LimitMax).ToString(); btnOK_Click("", new RoutedEventArgs()); }
        private void btnOK_Click_3(object sender, RoutedEventArgs e) { popuptextbox.Text = (.5 * LimitMax).ToString(); btnOK_Click("", new RoutedEventArgs()); }
        private void btnOK_Click_4(object sender, RoutedEventArgs e) { popuptextbox.Text = (.75 * LimitMax).ToString(); btnOK_Click("", new RoutedEventArgs()); }
        private void btnOK_Click_5(object sender, RoutedEventArgs e) { popuptextbox.Text = (1 * LimitMax).ToString(); btnOK_Click("", new RoutedEventArgs()); }

    }



}
