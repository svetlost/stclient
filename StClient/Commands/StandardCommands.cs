﻿namespace StClient
{
    public class StandardCommands
    {
        public bool cmdAuto { get; set; }
        public bool cmdStop { get; set; }
        public bool cmdUp { get; set; }
        public bool cmdDown { get; set; }
        public bool cmdRstTrip { get; set; }
        public bool cmdRstRef { get; set; }
        public bool cmdJoyStart { get; set; }
        public bool cmdManStart { get; set; }

    }
}
