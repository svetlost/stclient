﻿using LogAlertHB;
using System;
using System.Diagnostics;
using System.Windows;

namespace StClient
{
    class GrpCommands
    {
        static string Message;


        Properties.Settings Sett = Properties.Settings.Default;
       

        public void Auto(GroupLogic _group)
        {
            _group.cmdAuto = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdAuto = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void Stop(GroupLogic _group)
        {
            _group.groupSM.stopMotors();
            //_group.cmdStop = true;
            //TimedAction.ExecuteWithDelay(new Action(delegate
            //{ _group.cmdStop = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void JoyRelease(GroupLogic _group)
        {
            _group.cmdJoyStart = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdJoyStart = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void ManRelease(GroupLogic _group)
        {
            _group.cmdManStart = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdManStart = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void ManPos_press(GroupLogic _group) { _group.cmdUp = true; Debug.WriteLine("{0:hh:mm:ss.fff} ManPos_press: {1}, group: {2}", DateTime.Now, _group.cmdUp, _group.groupNo); }
        public void ManPos_release(GroupLogic _group) { _group.cmdUp = false; Debug.WriteLine("{0:hh:mm:ss.fff} ManPos_release: {1}, group: {2}", DateTime.Now, _group.cmdUp, _group.groupNo); }
        public void ManNeg_press(GroupLogic _group) { _group.cmdDown = true; Debug.WriteLine("{0:hh:mm:ss.fff} ManNeg_press: {1}, group: {2}", DateTime.Now, _group.cmdDown, _group.groupNo); }
        public void ManNeg_release(GroupLogic _group) { _group.cmdDown = false; Debug.WriteLine("{0:hh:mm:ss.fff} ManNeg_release: {1}, group: {2}", DateTime.Now, _group.cmdDown, _group.groupNo); }
        public void ResetTrip(GroupLogic _group)
        {
            _group.cmdRstTrip = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdRstTrip = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));
            //foreach (MotorLogic motor in _group.MotorsInGroup.Where(q => q.Trip == true))
            //{
            //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);
            //    Message += string.Format(" Motor={0}, MotID={1}, CP={2};", motor.Title, motor.MotorID, motor.CP);
            //}
            //if (_group.groupSM.state == states.Error2)
            //{
            //    _group.timeOutTime = DateTime.Now + TimeSpan.FromSeconds(1);
            //    _group.groupSM.state = states.Error3;
            //}
            Log.Write("(GROUP resetTrip)." + Message, EventLogEntryType.Information);
            Message = "";
        }

        public void ResetGroup(GroupLogic _group)
        {
            string s = "";
            try
            {
                for (int i = _group.MotorsInGroup.Count - 1; i >= 0; i--)
                {
                    s += string.Format(" Motor={0}, MotID={1}, CP={2};", _group.MotorsInGroup[i].Title,
                        _group.MotorsInGroup[i].MotorID, _group.MotorsInGroup[i].CP);
                    _group.MotorsInGroup[i].GroupNo = -1;
                }
                _group.motorIds.Clear();
                //if (_group.groupNo == 0) foreach (var hoist in Sys.UpDownList) hoist.Group = false;
                Log.Write(string.Format("(ResetGroup GroupGUI). " + s), EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
