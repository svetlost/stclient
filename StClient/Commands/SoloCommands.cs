﻿using LogAlertHB;
using System;
using System.Diagnostics;

namespace StClient
{
    public class SoloCommands
    {
        static string Message;
        //PopUpWindow popupWin;

        Properties.Settings Sett = Properties.Settings.Default;
      

        public void ResetGroup(MotorLogic _motor)
        {
            try
            {
                _motor.GroupNo = -1;
                Log.Write(string.Format("(ResetGroup MotorGUI). Motor={0}, MotID={1}, CP={2}", _motor.Title, _motor.MotorID,
                    _motor.CP), EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message, EventLogEntryType.Error);
            }
        }

        public void SoloAuto(MotorLogic motor)
        {
            if (motor.autoEnabled == false) return;
            if (motor.soloSM.state == states.Idle && Math.Abs(motor.TP - motor.CP * motor.SpCoeficient) > 0.01)
            {
                motor.soloSM.state = states.AutoStarting1;
                Message = string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};",
                            motor.Title, motor.MotorID, motor.TP, motor.TV, motor.CP);
                Log.Write("SM(SoloAuto). " + Message, EventLogEntryType.Information);
            }
        }

        public void Auto(MotorLogic motor)
        {
            motor.cmdAuto = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { motor.cmdAuto = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void Stop(MotorLogic motor)
        {
            motor.cmdStop = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { motor.cmdStop = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }

        public void ManRelease(MotorLogic motor)
        {
            motor.cmdManStart = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { motor.cmdManStart = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void ManPos_press(MotorLogic motor) { motor.cmdUp = true; }
        public void ManPos_release(MotorLogic motor) { motor.cmdUp = false; }
        public void ManNeg_press(MotorLogic motor) { motor.cmdDown = true; }
        public void ManNeg_release(MotorLogic motor) { motor.cmdDown = false; }
        public void ResetTrip(MotorLogic motor)
        {
            motor.cmdRstTrip = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
                { motor.cmdRstTrip = false; }),
                TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));
            Log.Write("(GROUP resetTrip)." + Message, EventLogEntryType.Information);
            Message = "";
        }
        public void ResetRef(MotorLogic motor)
        {
            motor.cmdRstRef = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
                {
                   
                    double newHome = (motor.referencepoint - (motor.CP - motor.NewReferencePoint))*motor.Coeficient;

                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("AmcOffset", newHome, motor.MotorIdAsArray); //amc ref select
                    motor.cmdRstRef = true;


                }),
                TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));
            Log.Write("(GROUP resetRef)." + Message, EventLogEntryType.Information);
            Message = "";
        }


    }
}
