﻿#pragma checksum "..\..\..\User Controls\UpDownControl.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "2C2517CCDFD68314E1DD2E62C0600FB8ACAC9DE302184E708BCC90831B462CD9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StClient {
    
    
    /// <summary>
    /// UpDownControl
    /// </summary>
    public partial class UpDownControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\..\User Controls\UpDownControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock _name;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\User Controls\UpDownControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton InGroup;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\User Controls\UpDownControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button _up;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\User Controls\UpDownControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button _down;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StClient;component/user%20controls/updowncontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\User Controls\UpDownControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this._name = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.InGroup = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 22 "..\..\..\User Controls\UpDownControl.xaml"
            this.InGroup.Checked += new System.Windows.RoutedEventHandler(this.InGroup_Checked);
            
            #line default
            #line hidden
            
            #line 22 "..\..\..\User Controls\UpDownControl.xaml"
            this.InGroup.Unchecked += new System.Windows.RoutedEventHandler(this.InGroup_Unchecked);
            
            #line default
            #line hidden
            return;
            case 3:
            this._up = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\User Controls\UpDownControl.xaml"
            this._up.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this._up_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 53 "..\..\..\User Controls\UpDownControl.xaml"
            this._up.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this._PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 4:
            this._down = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\..\User Controls\UpDownControl.xaml"
            this._down.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this._down_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 55 "..\..\..\User Controls\UpDownControl.xaml"
            this._down.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this._PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

