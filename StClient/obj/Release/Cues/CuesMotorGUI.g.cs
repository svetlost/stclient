﻿#pragma checksum "..\..\..\Cues\CuesMotorGUI.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "AD788073D26CA1E634D20011AF2159AB5582775BD9FD4D4C12DB18006DD59F0A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using StClient;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StClient {
    
    
    /// <summary>
    /// CuesMotorGUI
    /// </summary>
    public partial class CuesMotorGUI : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock title;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton Cue;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock GroupNumber;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CP;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CV;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SP;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SV;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Cues\CuesMotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetTrip;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StClient;component/cues/cuesmotorgui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Cues\CuesMotorGUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.title = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.Cue = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            return;
            case 3:
            this.GroupNumber = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.CP = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.CV = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.SP = ((System.Windows.Controls.TextBlock)(target));
            
            #line 50 "..\..\..\Cues\CuesMotorGUI.xaml"
            this.SP.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SP_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.SV = ((System.Windows.Controls.TextBlock)(target));
            
            #line 52 "..\..\..\Cues\CuesMotorGUI.xaml"
            this.SV.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SV_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 8:
            this.resetTrip = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\Cues\CuesMotorGUI.xaml"
            this.resetTrip.Click += new System.Windows.RoutedEventHandler(this.resetTrip_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

