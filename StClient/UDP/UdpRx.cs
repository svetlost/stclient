﻿using LogAlertHB;
using StClient.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

//using StClient.Wagons;

namespace StClient
{
    /// <summary>
    /// 
    /// CHECK if network is available --> SbcCanCheck_Tick
    /// receive message
    /// </summary>
    /// 

    public class UdpRx : IInitResult
    {

        DispatcherTimer SbcCanCheck = new DispatcherTimer();      
        ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;

        int RxSocketExceptionCounter;
        //PhidgetItem ph item { get { return Sys.phitem; } }

        Properties.Settings Sett = Properties.Settings.Default;


       
        static Socket RxSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);      
        static IPEndPoint ipepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.LocalIp), Properties.Settings.Default.LocalPort);       
        static EndPoint epClient = (EndPoint)ipepClient;       
        static byte[] ReceivedDatagram = new byte[Properties.Settings.Default.UdpMessageLength];       
        double Tollerance = Properties.Settings.Default.MovementDetection;

        ServerLink serverMain, serverBackup;
        string serverMainIp = Properties.Settings.Default.ServerMainIP;
        string serverBackupIp = Properties.Settings.Default.ServerBackupIP;

        //SUMS Panels
        CounterSumsLogic rXtotal = new CounterSumsLogic(), RXvagoni = new CounterSumsLogic(), unconfiguredRX = new CounterSumsLogic(), UdpRxError = new CounterSumsLogic();
     

        //public DispatcherTimer MessagesOverUDP = new DispatcherTimer();
        public Timer MessagesOverUDP;
        public int ModbusRXPollerInterval = Properties.Settings.Default.ModbusRXPollerInterval;//System.Threading.Timeout.Infinite;

        public UdpRx()
        {

            MessagesOverUDP = new Timer(new TimerCallback(ReceivingMessagesOverUDP_Tick), null, 0, System.Threading.Timeout.Infinite);

            SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(rXtotal), 0);
            SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(unconfiguredRX), 1);
            SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(UdpRxError), 2);
            UdpRxError.Names = "Udp Rx Error";
            rXtotal.Names = "RX Total";
            unconfiguredRX.Names = "Unconfigured PDO RX";

            try
            {
                RxSocket.Bind(ipepClient);               
                initResult += string.Format("RxSck bound to {0}:{1}", ipepClient.Address, ipepClient.Port);
            }
            catch (Exception e)
            {
                initResult += string.Format("RxSck bind to {0}:{1} FAILED", ipepClient.Address, ipepClient.Port) + e.Message;
                //Log.Write(initResult,EventLogEntryType.Error);
                //AlertLogic.Add(initResult);
            }

            serverMain = Sys.ServersList.Find(q => q.IpAddress == Properties.Settings.Default.ServerMainIP);
            serverBackup = Sys.ServersList.Find(q => q.IpAddress == Properties.Settings.Default.ServerBackupIP);

        }


        // FIONREAD is also available as the "Available" property.
        public const int FIONREAD = 0x4004667F;
        Dispatcher disp = Dispatcher.CurrentDispatcher;

        Stopwatch watch = new Stopwatch();
        int nothingReceivedCounter = 0;
      

        /// <summary>
        ///Receives udp messages from server,and update motor statuses
        ///<remark>udp datagrams are received in sequence, parallelism seems impossible </remark>
        /// </summary>           
        void ReceivingMessagesOverUDP_Tick(object state)
        {

            try
            {
                var watch = Stopwatch.StartNew();

                if (RxSocket.Available == 0)
                {
                    nothingReceivedCounter++;
                    // stop timer until all messages processed
                    MessagesOverUDP.Change(0, Timeout.Infinite);
                    if (nothingReceivedCounter > 20 && Sys.StaticMotorList != null)
                    {
                        foreach (var m in Sys.StaticMotorList)
                        {
                            m.Value.Net = false;
                        }
                    }
                }               

                //UDP receiving
                while (RxSocket.Available > 0)
                {
                    Debug.WriteLine("\nVRH\n");
                    MessagesOverUDP.Change(Timeout.Infinite, Timeout.Infinite);
                    nothingReceivedCounter = 0;
                    if (RxSocket.Available > Properties.Settings.Default.UDPbuffer)
                    {
                          Log.Write("UDP RX Buffer Full", EventLogEntryType.Error);
                        //  AlertLogic.Add("UDP RX Buffer Full - Please Try Restart");


                        foreach (var ml in Sys.StaticMotorList) //&&(m.Brake==false) dodao darko 08082010. testirati.SKINUO DARKO
                        {
                            //  Log.Write("ERROR: UDP RX Buffer Full - Motor" + ml.Value.IpAddress.ToString() + " STOPPED", EventLogEntryType.Error);
                            // AlertLogic.Add("ERROR: UDP RX Buffer Full - Motor" + ml.Value.IpAddress.ToString() + " STOPPED");
                        }
                        // break;
                    }

                    // reading all from buffer
                    int receivedResultsLength = RxSocket.ReceiveFrom(ReceivedDatagram, ref epClient);
                    
                    // resize data to actual size of received datagram
                    Array.Resize(ref ReceivedDatagram, receivedResultsLength);

                    //  Trace.WriteLine("RX: "+ stopwatch.ElapsedMilliseconds.ToString() + " ms per tick === server has: " + bytesAvailable.ToString() + " bytes pending. Available property says: " + RxSocket.Available.ToString() + ". bytes:   " + xyz);

                    // regulator dio normal response
                    if (ReceivedDatagram[0] == 0){ continue; }
                    
                    //starting position for reading received message
                    int ReadBytePosition = 0;

                    List<MotorLogic> Motors = new List<MotorLogic>();
                    List<byte> messages = new List<byte>();

                    //firs byte is number of regulators data was send to
                    int ReceivedMotorsCount = ReceivedDatagram[0];
                    ReadBytePosition++; 

                    //populate list of receiving motors
                    for (int i = 0; i < ReceivedMotorsCount; i++)
                    {
                      //  Debug.WriteLine("i: " + i + "  packetOffset: " + data[packetOffset]);
                        if (!Sys.StaticMotorList.ContainsKey(ReceivedDatagram[ReadBytePosition]))
                        {
                            Debug.WriteLine("motorlogic items ovo nema data value :" + ReceivedDatagram[ReadBytePosition] + " packetOffset: " + ReadBytePosition);
                        }
                        Motors.Add(Sys.StaticMotorList[ReceivedDatagram[ReadBytePosition]]);
                        // Debug.WriteLine("i: " + i  + "  packetOffset: " + data[packetOffset]);
                        ReadBytePosition++;
                    }

                    //length of messages
                    for (int i = 0; i < ReceivedMotorsCount; i++)
                    {
                        //depending on length, determine error or full message
                        Motors[i].RxMessageLength = ReceivedDatagram[ReadBytePosition];                       
                        ReadBytePosition++;
                    }

                    // populate each motor from motor list with new message
                    for (int i = 0; i < ReceivedMotorsCount; i++)
                    {
                        //determine if message is same as last one, saves processing time
                        byte[] receivedMessage = Utl.SubArray(ReceivedDatagram, ReadBytePosition, Motors[i].RxMessageLength, "rx");
                        if (Motors[i].RxMessage == null)
                        {
                            Motors[i].RxMessage = receivedMessage;
                            Motors[i].RxMessageSame = false;
                        }
                        else if (!receivedMessage.SequenceEqual(Motors[i].RxMessage))
                        {
                            Motors[i].RxMessage = receivedMessage;
                            Motors[i].RxMessageSame = false;
                           // Debug.WriteLine("i:" + i + " MotorID: " + Motors[i].MotorID + " message length :" + Motors[i].RxMessageLength + "  packetOffset: " + packetOffset + " rxLenght: " + receivedResultsLength + " package : " + Utl.PrintByteArray(Motors[i].RxMessage));
                        }
                        else
                        {
                            Motors[i].RxMessageSame = true;
                           // Debug.WriteLine("i:" + i + " MotorID: " + Motors[i].MotorID + " message length :" + Motors[i].RxMessageLength + "  packetOffset: " + packetOffset + " rxLenght: " + receivedResultsLength + " package : {}");
                        }

                        //move up the array list index
                        ReadBytePosition += Motors[i].RxMessageLength;
                    }

                    //parallel processing for motors in list with RxMessageSame == false
                    Parallel.ForEach(Motors, worker => { if (worker.RxMessageSame == false && worker.RxMessageLength > 0) worker.MotorStatuses(); });

                    if (ReadBytePosition >= receivedResultsLength)
                    { ReadBytePosition = 0; Motors.Clear(); messages.Clear(); }

                }
                watch.Stop();

                //wait for interval - check sync
               // if (watch.ElapsedMilliseconds < ModbusRXPollerInterval)
               // { 
               //     await Utl.PutTaskDelay(ModbusRXPollerInterval - (int)watch.ElapsedMilliseconds).ConfigureAwait(true);                    
               // }

                    //restart
                    MessagesOverUDP.Change(ModbusRXPollerInterval, Timeout.Infinite);              


                //SYSTEM PANEL RECALCULATE
                VisibilityStatesLogic.UniqueInstance.CalculateSystemIndicators();

                //RX TOTAL COUNT                        
                rXtotal.Count++;

                //read request for all motor states 
                //UdpTx.UniqueInstance.ModbusSync();

            }
            catch (SocketException sockEx)
            {
                RxSocketExceptionCounter++;
                if (RxSocketExceptionCounter % 100 == 2)
                {
                    string str = "Message send failed :" + sockEx.ErrorCode.ToString() + "\nMessage UDP send failed :" + sockEx.Message + "\nMessage UDP send failed :" + sockEx.StackTrace;
                    //AlertLogic.Add(str);
                    //Log.Write(str, EventLogEntryType.Error);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, ex.StackTrace);
            }

        }

        public string initResult { get; set; }

    }
}