﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Threading;

namespace StClient
{
    [Serializable]
    struct CanOverUdp
    {
        public DateTime timestamp;
        public byte[] Data;
    }

    class UdpTx : IInitResult
    {
        List<CanOverUdp> delaySendList = new List<CanOverUdp>();
        static IPEndPoint ipepServer = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerMainIP), Properties.Settings.Default.ServerPort);
        static Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        CanOverUdp tempMsg = new CanOverUdp();
        DispatcherTimer UdpDelayedSendTimer = new DispatcherTimer();

        Properties.Settings Sett = Properties.Settings.Default;
       
        public string initResult { get; set; }


        //SUMS Panels
        CounterSumsLogic TXtotal = new CounterSumsLogic(), UdpTxError = new CounterSumsLogic();

        UdpTx()
        {
            // UdpDelayedSendTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.UdpSendInterval);
            //  UdpDelayedSendTimer.Tick += new EventHandler(UdpDelayedSend);
            //  UdpDelayedSendTimer.Start();           

            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                          {
                              SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(TXtotal), 4);
                              SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(UdpTxError), 3);
                              UdpTxError.Names = "Udp Tx Error";
                              TXtotal.Names = "TX Total";
                          });
            initResult += string.Format("TxSck set to {0}:{1}", ipepServer.Address, ipepServer.Port);
        }

        class UdpTxCreator
        {
            static UdpTxCreator() { }
            internal static readonly UdpTx uniqueInstance = new UdpTx();
        }
        public static UdpTx UniqueInstance
        {
            get { return UdpTxCreator.uniqueInstance; }
        }
        /// <summary>
        /// send udp
        /// </summary>
        /// <param name="msg">byte array for sending</param>
        void SendUDP(byte[] msg)
        {
           
            try
            {
                server.SendTo(msg, msg.Length, SocketFlags.None, ipepServer);

                TXtotal.Count++;
            }
            catch (Exception ex)
            {
                UdpTxError.Count++;
                Log.Write("UDPTX-SendUDP ErrorCount= " + UdpTxError.Count + ", ErrorMessage=" + ex.Message, EventLogEntryType.Error);
            }
        }

        #region MODBUS
        /// <summary>
        ///Single command is send to ,one or many  DIOs or Regulators
        /// </summary>
        /// <remark>command type is hard coded in message, 8 for DIO, regulator is 16 or 32, depends on writing in one or two registers</remark>
        /// <param name="command">string command, for dio or SingleDIOCommandGroupedInit commands use word DIO</param>
        /// <param name="value">value to send</param>       
        /// <param name="motors">motorlogic.MotorIdAsArray, grp.motorIds.ToArray()</param>        
        /// <param name="initAll">for drive initialization,optional</param>
        public void ModBusSigleCommandWrite(string command, double value, byte[] motors, bool initAll = false)
        {

            int counter = 0;
            List<byte> result = new List<byte>();

            //array of motors
            result.Add((byte)motors.Length); // number of motors
            result.AddRange(motors);   //motor ids from db

            if (command == "DIO")
            {
                //command type!!!
                result.Add((byte)8);

                foreach (byte motor in motors)
                {
                    MotorLogic tm = null;
                    bool exists = Sys.StaticMotorList.TryGetValue(motor,out tm);

                    //DIO BIT
                    result.Add(tm.SbcBitAddress);

                }

                //BYTE
                result.AddRange(BitConverter.GetBytes((byte)value));

            }
            else if (command == "SingleDIOCommandGroupedInit")
            {
                //command type!!!
                result.Add((byte)8);

                foreach (byte motor in motors)
                {
                    MotorLogic tm = null;
                    bool exists = Sys.StaticMotorList.TryGetValue(motor, out tm);

                    //INIT DIO BIT
                    result.Add(tm.initSbcBit);
                }

                //BYTE
                result.AddRange(BitConverter.GetBytes((byte)value));
                counter = result.Count;
            }

            //REGULATOR
            else
            {
                //message type from codewords NumberOfBits
                CodeWords cdw = Sys.codewords.Find(q => q.CodeDescription.Equals(command));

                // to do if(cdw == null) {  }

                //NumberOfBits, 16 or 32 
                result.Add((byte)cdw.NumberOfBits);

                //length of command
                result.AddRange(BitConverter.GetBytes(cdw.CommandCode.Length)); // we need to send the length of the string as offset to start reading from correct position

                //command
                byte[] commandArray = Encoding.UTF8.GetBytes(cdw.CommandCode);

                // add to byte array
                result.AddRange(commandArray);

                // DOUBLE
                result.AddRange(BitConverter.GetBytes(value));
            }


            SendUDP(result.ToArray());

            Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, parameter={1}, value={2}, motors={3} ModBusSigleCommandWrite", DateTime.Now, command, value, Utl.PrintByteArray(motors)));


            // DIO COMMAND init all, delayed send
            if (initAll)
            {
                TimedAction.ExecuteWithDelay(new Action(delegate
                {
                    // set value to 0 after a second
                    result[result.Count - 2] = 0;
                    SendUDP(result.ToArray());

                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, parameter={1}, value={2}, motors={3} ExecuteWithDelay", DateTime.Now, command, 0, Utl.PrintByteArray(motors)));
                }), TimeSpan.FromMilliseconds(200));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command">string command, for dio commands use word DIO</param>
        /// <param name="value">value to send</param>       
        /// <param name="motors">grp.motorIds.ToArray() array of motors</param>    
        public void ModbusGroupWrite(string[] commands, double[] values, byte[] motors)
        {
            List<byte> result = new List<byte>();

            //motors
            result.Add((byte)motors.Length);
            result.AddRange(motors);   

            //command type!!!
            result.Add(64); // designation for group of motors, multiple commands

            result.Add((byte)commands.Length); //array of commands length

            for (int i = 0; i < commands.Length; i++)
            {
                CodeWords cdw = Sys.codewords.Find(q => q.CodeDescription.Equals(commands[i]));

                result.Add((byte)cdw.NumberOfBits);

                //length of command
                result.AddRange(BitConverter.GetBytes(cdw.CommandCode.Length)); // length of the string as offset to start reading from correct position

                byte[] commandArray = Encoding.UTF8.GetBytes(cdw.CommandCode); // command

                result.AddRange(commandArray); // add byte array                
            }

            result.Add((byte)values.Length);

            foreach (double value in values)
            {
                result.AddRange(BitConverter.GetBytes(value));
            }

            SendUDP(result.ToArray());

            Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, commands=[{1}], values={2} ModbusGroupWrite", DateTime.Now, string.Join(", ", commands), values));
        }

        #endregion
        #region CAN      
      
        public void Send8200PDO(bool Up, bool Down, int CanAdr)
        {

            byte[] bb = BitConverter.GetBytes(CanAdr + 512);

            byte[] temp = new byte[1000];
            //temp[0] = BitConverter.GetBytes(data)[0];
            if (Up == true) temp[0] = 4;
            else if (Down == true) temp[0] = 2;
            else temp[0] = 0;
            temp[1] = temp[2] = temp[3] = temp[4] = temp[5] = temp[6] = temp[7] = 0;
            temp[8] = 2; //can channel
            temp[9] = 8; //dlc
            temp[10] = bb[0];
            temp[11] = bb[1];

            if (temp.Length == 58) { Trace.WriteLine("stop Send8200PDO"); }
            SendUDP(temp);

        }
       
        #endregion



    }
}
