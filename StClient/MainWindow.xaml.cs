﻿using LogAlertHB;
using Microsoft.Win32;
using StClient.Motors;
using StClient.User_Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;  //needed for the event handling classes
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using static StClient.SoloStateMachine;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// MainLoop starts state machine, loops every 100ms , checks motor states
    /// </summary>
    public partial class MainWindow : Window
    {



        public Sys sys;
        //CREATE SENDER RECEIVER
        UdpRx _UdpRx;
        UdpTx _UdpTx;


        public DispatcherTimer MainLoop = new DispatcherTimer();
        public static string password = "";
        public static bool ispasscorect = false;

        VisibilityStatesLogic mvstates = new VisibilityStatesLogic();
        ListBox lb;
        public static Dispatcher mainWindowDispacher;
        // public static int XXX;
        ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;
        //DispatcherTimer MainLoop = new DispatcherTimer();

        Properties.Settings Sett = Properties.Settings.Default;
        

        //UpDownControl ZavServis;
        public static SynchronizationContext uiContext;
        public MainWindow()
        {
            ConfigurationManager.RefreshSection("connectionStrings");
            InitializeComponent();

            Popup codePopup = new Popup();
            TextBlock popupText = new TextBlock();
            popupText.Text = "Pop up Text";
            popupText.Background = Brushes.LightBlue;
            popupText.Foreground = Brushes.Blue;
            codePopup.Child = popupText;

            this.DataContext = mainWindowEnabled;
            mainWindowDispacher = this.Dispatcher;
            uiContext = SynchronizationContext.Current;

            Microsoft.Win32.SystemEvents.PowerModeChanged += OnPowerChange;

            sys = new Sys();

            //CREATE GROUPS
            foreach (GroupLogic gl in Sys.StaticGroupList)
            {
                GroupGUI gGUI = new GroupGUI(gl);
                lbOfGroups.Items.Add(gGUI);
            }

            //CREATE DRIVES
            lbOfMotors.Items.Clear();
            lbOfCuesMotorGUI.Items.Clear();
            if (Sys.StaticMotorList != null)
            {
                foreach (MotorLogic motor in Sys.StaticMotorList.Values)
                {
                    if (motor.Enabled)
                    {
                        //var tempMotGui = new MotorGUI(motor);
                        //if(motor.MotorID == 61 || motor.MotorID == 62)
                        lbOfMotors.Items.Add(new MotorGUI(motor));
                        lbOfCuesMotorGUI.Items.Add(new CuesMotorGUI(motor));
                        DiagLeft.Children.Add(new DiagGUI(motor));
                        GraphLeft.Children.Add(new Graphic(motor));
                        LoadCells.Children.Add(new LoadCell(motor));
                        PassWord.Children.Add(new LoginView(Pass, LoadCellsTab));
                        ////TC only, TC=can17..can24, cluster=3
                        //if (motor.ClusterLogical==3)
                        //    MiscLeft.Children.Add(new TC(motor));
                        // CueGraphLeft.Children.Add(new GraphGui2(motor));
                    }
                }
            }
            //MiscLeft.Children.Add(new Test2());
            CueListBox.Children.Add(CueListView.Instance);

            //UdpBcastTst ut = new UdpBcastTst();
            //MiscLeft.Children.Add(new UdpBcastTst());
            //vagonGrid.Children.Add(new UdpBcastTst());//20140119
            //todo dodato 2013 zvagone


            //HOIST
            //foreach (UpDownLogic ud in Sys.UpDownList)
            //{
            //    UpDownControl tmp = new UpDownControl(udl: ud);
            //    _hoistsGrid.Children.Add(tmp);
            //    Grid.SetColumn(tmp, ud.Column);
            //    Grid.SetRow(tmp, ud.Row);

            //    if (ud.MotorID == 321)
            //    {
            //        ZavServis = tmp;
            //        ZavServis.Visibility = Visibility.Hidden;
            //    }
            //}

          
           


            //foreach (StartecLogic starteclogic in Sys.StaticStartecList)
            //{
            //    Startec startec = new Startec(starteclogic);
            //    RcAndHoist.Children.Add(startec);
            //    Grid.SetColumn(startec, starteclogic.Column);
            //    Grid.SetRow(startec, starteclogic.Row);
            //}


            //listBoxes
            Sys.MotorListbox = lbOfMotors;
            Sys.GroupListBox = lbOfGroups;
            Sys.CuesMotorGUIListBox = lbOfCuesMotorGUI;

           
            //start receiving data from server
            _UdpRx = new UdpRx();

            _UdpTx = UdpTx.UniqueInstance;
            string s1 = ((_UdpRx as IInitResult).initResult) + "\n" + ((_UdpTx as IInitResult).initResult);
            Log.Write(s1, EventLogEntryType.Information);
            AlertLogic.Add(s1);

            //CLUSTER ELECTRIC
            DiagRight.Children.Add(SumsPPsPanel.Instance);
            // if (Sys.render3D) d3.Children.Add(new view3d()); //20140211 cekaju se bolja vremena da se stavi 3d model todo
            //Tab3D.Visibility = Visibility.Collapsed;
            //TabVagon.Visibility = Visibility.Collapsed;

            //ALERT  LIST VIEW
            //AlertsListView alview = new AlertsListView();
            //Grid3dLog.Children.Add(alview);
            //Grid.SetColumn(alview, 1);


            //AlertsListView alview1 = new AlertsListView();
            //CuesRight.Children.Add(alview1);
            //Grid.SetRow(alview1, 2);

            AlertsListView alview2 = new AlertsListView();
            ttt.Children.Add(alview2);
            //Grid.SetRow(alview2, 1);


            myBrowser.Navigate(new Uri("file:///C:/ProgramData/Control%20Techniques/Database%20Server/M700/en-GB/ParamRefGuide/ParamRef/RFC_A/trips.html"));

            ////diag2
            //todo cleanup
            Signal sig1 = new Signal() { text = "LP1", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig1, Signal.valueProperty, new Binding("LP1") { Source = ApplicationStateLogic.Instance });
            Signal sig2 = new Signal() { text = "LP2", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig2, Signal.valueProperty, new Binding("LP2") { Source = ApplicationStateLogic.Instance });
            Signal sig3 = new Signal() { text = "ESTOP", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig3, Signal.valueProperty, new Binding("Estop_Pressed") { Source = ApplicationStateLogic.Instance });
            Signal sig4 = new Signal() { text = "PULT_PK1", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig4, Signal.valueProperty, new Binding("PULT_PK1") { Source = ApplicationStateLogic.Instance });
            Signal sig5 = new Signal() { text = "PULT_PK2", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig5, Signal.valueProperty, new Binding("PULT_PK2") { Source = ApplicationStateLogic.Instance });
            Signal sig6 = new Signal() { text = "PCP_PK1", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig6, Signal.valueProperty, new Binding("PCP_PK1") { Source = ApplicationStateLogic.Instance });
            Signal sig7 = new Signal() { text = "PCP_PK2", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig7, Signal.valueProperty, new Binding("PCP_PK2") { Source = ApplicationStateLogic.Instance });
            //Diag2Left.Children.Add(sig1);
            wrapSysDiag.Children.Add(sig3);
            wrapSysDiag.Children.Add(new TextBlock());
            //Diag2Left.Children.Add(sig2);
            wrapSysDiag.Children.Add(sig7);
            wrapSysDiag.Children.Add(sig6);
            wrapSysDiag.Children.Add(sig5);
            wrapSysDiag.Children.Add(sig4);
            wrapSysDiag.Children.Add(sig2);
            wrapSysDiag.Children.Add(sig1);
            wrapSysDiag.Children.Add(new TextBlock() { Text = "xxx", Foreground = Brushes.Transparent });
            //  wrapSysDiag.Children.Add(new GelbauGUI());

            //AlertsListView alview4 = new AlertsListView();
            //MiscRightGrid.Children.Add(alview4);
            //Grid.SetRow(alview4, 1);


            //StartecGroup stl = new StartecGroup(Sys.StartecGroupLogic);
            //RCHoistRight.Children.Add(stl);
            //Grid.SetRow(stl, 0);

            //AlertsListView alview3 = new AlertsListView();
            //AlertsListView alview3 = new AlertsListView(850);
            //alview3.ve
            //RCHoistRight2.Children.Add(alview3);
            //Grid.SetRow(alview3, 1);

            Sys.TabControl = MainWindowTabControl;
            if (Properties.Settings.Default.JoystickAttached)
            {
                PhidgetsInterfaces.UniqueInstance.initPhidget();
            }
             MainLoop.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval);
              MainLoop.Tick += new EventHandler(MainLoop_Tick);
              MainLoop.Start();
            //MainLoopTimer = new Timer(new TimerCallback(MainLoop_Tick), null, 0, Properties.Settings.Default.SyncInterval);

            //  TimedAction.ExecuteWithDelay(new Action(delegate
            //  {
            //       foreach (MotorLogic motor in Sys.StaticMotorList.FindAll(t => t.Net)) motor.SP = motor.CP;
            //   }), TimeSpan.FromSeconds(4));

            SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(mainloopTimeCounter), 5);
        }
        CounterSumsLogic mainloopTimeCounter = new CounterSumsLogic() { Names = "mainloopTime us" };
        //CounterSumsLogic rXtotal = new CounterSumsLogic()
        DateTime loopStart;


        //treba da nestane!!! tajmer moze da bude nesinhronizovan sa primanjem podataka
        void MainLoop_Tick(object sender, EventArgs e)
        //void MainLoop_Tick(object state)
        {
            if (Sys.StaticMotorList == null) { return; }
            Task.Run(() => { 
            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                loopStart = DateTime.Now;
                CueLogic.UniqueInstance.UpdateCurrentCueItemsList();

                CueLogic.UniqueInstance.CalculateEnableds();
                VisibilityStatesLogic.UniqueInstance.CalculateSystemIndicators();


                //group states check // posle rx paralele
                foreach (GroupLogic gl in Sys.StaticGroupList)
                {
                    gl.CalculateEnableds();
                    gl.groupSM.ProcessGroupStateMachine();
                }

                // process each motor states every 300ms (Properties.Settings.Default.SyncInterval)
                foreach (var obj in Sys.StaticMotorList)
                {
                    MotorLogic ml = obj.Value;
                    ml.CalculateEnableds(); // prebaciti u motor logic nakon preimanja podataka rx paralelno izvrsavanje

                    //  if (ml.GroupNo == -1)
                    ml.soloSM.ProcessStateMachine(); // odmah posle

                    if (ml.GroupNo == -1)
                        ml.grpResetEnabled = false;
                    else
                    {
                        GroupLogic gl = Sys.StaticGroupList.Find(g => g.groupNo == ml.GroupNo); //rx posle paralel
                        ml.grpResetEnabled = (!CueLogic.UniqueInstance.Moving && !gl.AnyMoving);
                    }
                }
                mainloopTimeCounter.Count = Convert.ToInt32((DateTime.Now - loopStart).Ticks) / 10;//1s=10 000 000ticks, 1us=10tick

            }));
            });

        }

        public void ShowTab(int index)
        {
            try { MainWindowTabControl.SelectedIndex = index; }
            catch { }
        }

        private void OnPowerChange(object s, PowerModeChangedEventArgs e)
        {
            if (e.Mode == PowerModes.Suspend) App.Current.Shutdown();
            if (e.Mode == PowerModes.Resume) App.Current.Shutdown();
            //switch (e.Mode)
            //{
            //    case PowerModes.Resume: break;
            //    case PowerModes.Suspend: break;
            //}
        }

        public void Window1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) > 0)
            {
                switch (e.Key)
                {
                    case (Key.F1):
                        ShowTab(0);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(0);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Motors);
                        break;
                    case (Key.F2):
                        ShowTab(1);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(1);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Cues);
                        break;
                    case (Key.F3):
                        ShowTab(2);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(2);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Graph);
                        break;
                    case (Key.F4):
                        ShowTab(3);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(3);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Diag);
                        break;
                    case (Key.F5):
                        //MainGraphicTabControl.SelectedIndex = 0;
                        break;
                    case (Key.F6):
                        //MainGraphicTabControl.SelectedIndex = 1;
                        break;
                }
            }
            else if ((Keyboard.Modifiers & ModifierKeys.Shift) > 0)
            {
                switch (e.Key)
                {
                    case (Key.F1):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(1, true);
                        break;
                    case (Key.F2):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(2, true);
                        break;
                    case (Key.F3):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(3, true);
                        break;
                    case (Key.F4):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(4, true);
                        break;
                    case (Key.F5):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(5, true);
                        break;
                    case (Key.F6):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(6, true);
                        break;
                    case (Key.F7):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(7, true);
                        break;
                    case (Key.F8):
                        VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(true);
                        break;
                    case (Key.F9):
                        VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(false);
                        break;
                    case (Key.F10):
                        VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();
                        break;
                    case (Key.F11):
                        VisibilityStatesLogic.UniqueInstance.ShowMovingMotors();
                        break;
                    case (Key.F12):
                        VisibilityStatesLogic.UniqueInstance.ShowTrippedMotors();
                        break;
                }
            }
            else
            {
                switch (e.Key)
                {
                    case (Key.F1):
                        if (Sys.StaticGroupList.Count > 0 && Sys.StaticGroupList[0].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[0].MotorsInGroup);
                        break;
                    case (Key.F2):
                        if (Sys.StaticGroupList.Count > 1 && Sys.StaticGroupList[1].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[1].MotorsInGroup);
                        break;
                    case (Key.F3):
                        if (Sys.StaticGroupList.Count > 2 && Sys.StaticGroupList[2].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[2].MotorsInGroup);
                        break;
                    case (Key.F4):
                        if (Sys.StaticGroupList.Count > 3 && Sys.StaticGroupList[3].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[3].MotorsInGroup);
                        break;
                    case (Key.F5):
                        if (Sys.StaticGroupList.Count > 4 && Sys.StaticGroupList[4].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[4].MotorsInGroup);
                        break;
                    case (Key.F6):
                        if (Sys.StaticGroupList.Count > 5 && Sys.StaticGroupList[5].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[5].MotorsInGroup);
                        break;
                    case (Key.F7):
                        VisibilityStatesLogic.UniqueInstance.ShowOnlyElectricClusteredMotors();
                        break;

                }
            }
        }


        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.KeyDown += new System.Windows.Input.KeyEventHandler(Window1_KeyDown);

            // used for removing close minimize and maximize buttons
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

        protected override void OnClosed(EventArgs e)
        {
            // PhidgetsInterfaces.UniqueInstance.AllOutsOff();
        }

        DispatcherTimer CloseAppTimer = new DispatcherTimer();
        bool wait = false;

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {

            try
            {
                CloseAppTimer.Tick += new EventHandler(CloseApp_Tick);
                CloseAppTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.WaitItervalForMotorsStopOnExit);

                //Sys.vagoniRxTx.StopRotOnExit_Phase1();
                //Sys.vagoniRxTx.StopRotOnExit();



                //NkkControl.ClearAll();

                foreach (MotorLogic motor in Sys.StaticMotorList.Values)
                {
                    if (motor.Net && !motor.Brake && motor.SbcStatusBitIndicator)
                    {
                        if (!wait)
                        {
                            string _allMotors = "";

                            _allMotors += string.Format(" mot={0} motID={1} cp={2} cv={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);

                            MessageBoxResult result = MessageBox.Show("Motors " + _allMotors + "are still running. Stop motors?", "Stop motors", MessageBoxButton.YesNo);

                            switch (result)
                            {
                                case MessageBoxResult.Yes:
                                    e.Cancel = true;
                                    wait = false;
                                    CloseAppTimer.Start();


                                    if (motor.Moving)
                                    {
                                       
                                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select
                                        TimedAction.ExecuteWithDelay(new Action(delegate
                                        {
                                            UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 0, motor.MotorIdAsArray); //RUN disable
                                                motor.SbcLock = MotorBrake.Locked;

                                        }), TimeSpan.FromMilliseconds(1000));
                                    }
                                    else
                                    {
                                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 0, motor.MotorIdAsArray); //RUN disable 
                                        motor.SbcLock = MotorBrake.Locked;
                                    }


                                    CloseAppTimer.Start();

                                    Log.Write("On application exit user stopped motors: " + _allMotors, EventLogEntryType.Information);
                                    break;

                                case MessageBoxResult.No:
                                    Log.Write("On application exit user did not stop motors: " + _allMotors, EventLogEntryType.Warning);
                                    break;
                            }
                        }
                        else
                            base.OnClosing(e);
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void CloseApp_Tick(object sender, EventArgs e)
        {
            foreach (MotorLogic motor in Sys.StaticMotorList.Values)
            {
                if (motor.Net && !motor.Brake && motor.SbcStatusBitIndicator)
                {
                    if (wait)
                    {
                        CloseAppTimer.Stop();
                        Application.Current.Shutdown();
                    }
                }
                else
                {
                    wait = true;
                }
            }

        }
        //try-Catch
        private void lbOfMotors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {


                lb = sender as ListBox;

                if (lb.SelectedItem != null)
                {
                    Sys.SelectedMotor = lb.SelectedItem as MotorGUI;
                    Sys.SelectedGroup = null;
                    Sys.SelectedCuesMotorGUIMotor = null;
                    lbOfGroups.SelectedItem = lbOfCuesMotorGUI.SelectedItem = null;
                }



            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
            }
        }
        //try-Catch
        private void lbOfGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

                lb = sender as ListBox;
                if (lb.SelectedItem != null)
                {
                    Sys.SelectedGroup = lb.SelectedItem as GroupGUI;
                    Sys.SelectedMotor = null;
                    Sys.SelectedCuesMotorGUIMotor = null;
                    lbOfMotors.SelectedItem = lbOfCuesMotorGUI.SelectedItem = null;


                }

            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
            }
        }

        private void lbOfCuesMotorGUI_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (Sys.StaticMotorList.Values.All(qq => qq.Brake && qq.SbcStatusBitIndicator == false))
                {
                    lb = sender as ListBox;
                    if (lb.SelectedItem != null)
                    {
                        Sys.SelectedGroup = null;
                        Sys.SelectedMotor = null;
                        Sys.SelectedCuesMotorGUIMotor = lb.SelectedItem as CuesMotorGUI;
                        lbOfMotors.SelectedItem = lbOfGroups.SelectedItem = null;


                    }
                }
                else
                    AlertLogic.Add("Selection Change Not Allowed: Brakes Released ");
            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
            }
        }

        private void MainWindowTabControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            if (e.Source is TabItem)  //do not handle clicks on TabItem content but on TabItem itself

            {
                if (e.Source == LoadCellsTab)  //User leaving the tab
                {
                    if (ispasscorect)
                    {
                        LoadCellsTab.IsSelected = true;
                    }
                    else
                    {
                        e.Handled = true;
                        Pass.IsOpen = true;
                    }
                }
            }
        }

    }

    public static class WebBrowserUtility
    {
        public static readonly DependencyProperty BindableSourceProperty =
            DependencyProperty.RegisterAttached("BindableSource", typeof(string), typeof(WebBrowserUtility), new UIPropertyMetadata(null, BindableSourcePropertyChanged));

        public static string GetBindableSource(DependencyObject obj)
        {
            return (string)obj.GetValue(BindableSourceProperty);
        }

        public static void SetBindableSource(DependencyObject obj, string value)
        {
            obj.SetValue(BindableSourceProperty, value);
        }

        public static void BindableSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            WebBrowser browser = o as WebBrowser;
            if (browser != null)
            {
                string uri = e.NewValue as string;
                browser.Source = !String.IsNullOrEmpty(uri) ? new Uri(uri) : null;
            }
        }

    }




}
