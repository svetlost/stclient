﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MotorGUI : UserControl
    {
        public MotorLogic _motor;
        //List<MotorLogic> motors;

        SoloCommands soloCmd = new SoloCommands();
        PopupWindows popups = new PopupWindows();

        public MotorGUI(MotorLogic motor)
        {
            InitializeComponent();
            this.DataContext = motor;
            _motor = motor;
            //motors = _motor.toList();

            //mora za svaki motorlogic da se napravi unos u dictionary (=mnogo CPU)
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloAuto, Auto);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloStop, Stop);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloManRelease, Release);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloManPos, Up);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloManNeg, Down);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloResTrip, resetTrip);

            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
                if (!((_motor.MotorID == 61 || _motor.MotorID == 62) && (i == 0 || i == 1))) combo.Items.Add(i.ToString());

        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            soloCmd.ManRelease(_motor);
            //commands.ManualStarting(motors);
            //Log.Write(string.Format("motorGUI id={0} title={1}: RELEASE BRK pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: RELEASE BRK pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
    _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);

        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManPos_press(_motor);
            //commands.Up_PreviewMouseLeftButtonDown(motors);
            Log.Write(string.Format("motorGUI: MAN UP pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
    _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
            //Log.Write(string.Format("motorGUI id={0} title={1}: MAN UP pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);

        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManPos_release(_motor);
            //commands.UpDown_PreviewMouseLeftButtonUp(motors);
            //Log.Write(string.Format("motorGUI: MAN UP released", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: MAN UP released, motor ID={0}, title={1}, cp={2}, cv={3}.",
  _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManNeg_press(_motor);
            //commands.Down_PreviewMouseLeftButtonDown(motors);
            //Log.Write(string.Format("motorGUI: MAN DOWN pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: MAN DOWN pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManNeg_release(_motor);
            //commands.UpDown_PreviewMouseLeftButtonUp(motors);
            //Log.Write(string.Format("motorGUI: MAN DOWN released", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: MAN DOWN pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            //UdpTx.UniqueInstance.SendSDOWriteRQ(_motor, 2, 0, 43, TimeSpan.FromMilliseconds(0));
            //UdpTx.UniqueInstance.SendSDOWriteRQ(_motor, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43, TimeSpan.FromMilliseconds(0));
            soloCmd.ResetTrip(_motor);
            Log.Write(string.Format("motorGUI: TRIP RST pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.Auto(_motor);
            //commands.AutoStarting(motors, _motor);
            //commands.SoloAuto(_motor);
            //Log.Write(string.Format("motorGUI: AUTO pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: AUTO pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.Stop(_motor);
            //commands.Stop(motors);
            //Log.Write(string.Format("motorGUI: STOP pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: STOP pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
    _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);

        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            soloCmd.ResetGroup(_motor);
            //commands.ResetGroup(this);
            //Log.Write(string.Format("motorGUI: GRP RST pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: GRP RST pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
 _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            popups.SP_Popup(_motor, sender as TextBlock, false);
            Log.Write(string.Format("motorGUI: SP changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.",
                _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.SP), EventLogEntryType.Information);
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            popups.SVMV_Popup(_motor, sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("motorGUI: SV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.",
                _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.SV), EventLogEntryType.Information);
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            popups.SVMV_Popup(_motor, sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("motorGUI: MV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.",
                _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.MV), EventLogEntryType.Information);
        }

        private void ResetMinMax_Click(object sender, RoutedEventArgs e)
        {
            _motor.minLC = 100000;
            _motor.maxLC = 0;
        }

        private void exportValues_Click(object sender, RoutedEventArgs e)
        {


            List<string> combined = new List<string>();
            combined.Add(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}", "vreme", "pravac", "pozicije", "brzina", "milliamps", "Time Diff", "Poss Diff", "Ubrzanje", "kg - nikola", "kg Static", "kg - move", "dynamici kilogrami", "kile Sile", "tip Kretanja")); //timestempovi.Substring(timestempovi.Length-7)
            int count = _motor.LoadCellList.Count >= _motor.CPList.Count ? _motor.LoadCellList.Count : _motor.CPList.Count;
            for (int i = 0; i < count; i++)
            {
                string miliamps = _motor.LoadCellList.Count <= i ? "" : _motor.LoadCellList[i].ToString();
                string kgmirovanje = _motor.KgMirovanjeList.Count <= i ? "" : _motor.KgMirovanjeList[i].ToString();
                string kgstvarnimirovanje = _motor.StvarniKgMirovanjeList.Count <= i ? "" : _motor.StvarniKgMirovanjeList[i].ToString();

                string pozicije = _motor.CPList.Count <= i ? "" : _motor.CPList[i].ToString();
                string proslaPozicije = _motor.PreviousCPList.Count <= i ? "" : _motor.PreviousCPList[i].ToString();
                string proslabrzina = _motor.PreviousCVList.Count <= i ? "" : _motor.PreviousCVList[i].ToString();
                string pravacLista = _motor.pravacList.Count <= i ? "" : _motor.pravacList[i].ToString();
                //string timestempovi = _motor.timestempList.Count <= i ? "" : _motor.timestempList[i].ToString();
                string brzina = _motor.brzinaList.Count <= i ? "" : _motor.brzinaList[i].ToString();
                string vreme = _motor.vremeList.Count <= i ? "" : _motor.vremeList[i];
                string kilogrami = _motor.kilogramiList.Count <= i ? "" : _motor.kilogramiList[i].ToString();
                double dynamickilogrami = _motor.dynamicKilogramiList.Count <= i ? 0 : _motor.dynamicKilogramiList[i];
                dynamickilogrami = Math.Round(dynamickilogrami, 4);
                string Tick = _motor.tickList.Count <= i ? "" : _motor.tickList[i].ToString();
                string previousTick = _motor.previusTickList.Count <= i ? "" : _motor.previusTickList[i].ToString();
                string vrstaKretanja = _motor.VrstaKretanjaList.Count <= i ? "" : _motor.VrstaKretanjaList[i];
                double TimeDiff = (_motor.tickList[i] - _motor.previusTickList[i]);
                TimeDiff = Math.Round(TimeDiff * 0.001, 4);
                double PossDiff = (_motor.CPList[i] - _motor.PreviousCPList[i]);
                double speedDiff = (_motor.brzinaList[i] - _motor.PreviousCVList[i]) * 0.01;
                double Ubrzanje = Math.Round(speedDiff / TimeDiff, 4);
                string kile = _motor.ocekivanaKilaza.Count <= i ? "" : _motor.ocekivanaKilaza[i].ToString();

                //Ubrzanje = Math.Round(Ubrzanje, 4);

                combined.Add(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}", vreme, pravacLista, pozicije, brzina, miliamps, TimeDiff.ToString(), PossDiff.ToString(), Ubrzanje, kgmirovanje, kgstvarnimirovanje, kilogrami, dynamickilogrami, kile, vrstaKretanja)); //timestempovi.Substring(timestempovi.Length-7)
            }
            DateTime a = DateTime.UtcNow;

            var Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            string fileName = "MyTest" + Timestamp.ToString();
            File.WriteAllLines(@"c:\test\" + fileName + ".csv", combined);
            _motor.LoadCellList.Clear();
            _motor.CPList.Clear();
            _motor.pravacList.Clear();
            _motor.timestempList.Clear();
            _motor.brzinaList.Clear();
            _motor.dynamicKilogramiList.Clear();
            _motor.tickList.Clear();
            _motor.previusTickList.Clear();
            _motor.PreviousCVList.Clear();
            _motor.KgMirovanjeList.Clear();
            _motor.PreviousCPList.Clear();
            _motor.vremeList.Clear();
            _motor.kilogramiList.Clear();
            _motor.VrstaKretanjaList.Clear();
            _motor.ocekivanaKilaza.Clear();
            _motor.StvarniKgMirovanjeList.Clear();

        }
    }

}