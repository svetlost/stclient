﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;


namespace StClient
{
    public partial class DiagGUI : UserControl
    {
        public MotorLogic _motor;

        PopupWindows popups = new PopupWindows();
        SoloCommands soloCmd = new SoloCommands();

        public DiagGUI(MotorLogic motor)
        {
            InitializeComponent();

            //  lbTripHistory.SelectionChanged += new SelectionChangedEventHandler(lbTripHistory_SelectionChanged);//9400 nema opis greske
            this.DataContext = motor;
            _motor = motor;

            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
                combo.Items.Add(i.ToString());

            foreach (DiagItemValues diags in _motor.DiagItemsList)
                DiagInfo.Children.Add(new DiagItemUserControl(diags));



            Binding b = new Binding();
            b.Source = motor;
            b.Path = new PropertyPath("TripHistoryGet");
            lbTripHistory.SetBinding(ListView.ItemsSourceProperty, b);

        }

        void lbTripHistory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TripHistoryItem a = lbTripHistory.SelectedItem as TripHistoryItem;
            try
            {
                if (a != null)//TODO pucalo ovde!!!!!!!!??????????????????                  
                    //MessageBox.Show(a.ErrorCauseRemedy);
                    popups.ShowTripInfo(a.ErrorCauseRemedy);
            }
            catch { };
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            soloCmd.ResetTrip(_motor);
            //commands.resetTrip(_motor.toList());
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.Stop(_motor);
            //commands.Stop(_motor.toList());
        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            _motor.GroupNo = -1;
        }

        private void refreshHistory_Click(object sender, RoutedEventArgs e)
        {
            _motor.TripHistoryList.Clear();
          

            //TripHistoryList.Clear();
           

        }



        private void resetRef_Click(object sender, RoutedEventArgs e)
        {
            popups.ResetReference(_motor);
            //soloCmd.ResetRef(_motor);
            //commands.ResetReference(_motor);
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;
            var button = sender as Hyperlink;
            var code = button.Tag;
            mainWindowEnabled.webaddress = new Uri("about:blank");
            mainWindowEnabled.webaddress = new Uri(String.Format("file:///C:/ProgramData/Control%20Techniques/Database%20Server/M700/en-GB/ParamRefGuide/ParamRef/RFC_A/trips.html#{0}", code.ToString()), UriKind.Absolute); ;
        }
    }
}
