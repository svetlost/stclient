﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace StClient
{


    [Table(Name = "motor_list")]
    public class MotorLogic : StandardCommands, IMotorIndicators, ISpSv, ICpCv, INotifyPropertyChanged, ILinkCheck
    {

        Properties.Settings Sett = Properties.Settings.Default;



        #region test

        public List<double> LoadCellList = new List<double>();

        public List<double> CPList = new List<double>();

        public List<string> VrstaKretanjaList = new List<string>();

        public List<double> KgMirovanjeList = new List<double>();

        public List<double> StvarniKgMirovanjeList = new List<double>();

        public List<double> PreviousCPList = new List<double>();

        public List<double> PreviousCVList = new List<double>();

        public List<int> pravacList = new List<int>();

        public List<double> timestempList = new List<double>();

        public List<string> vremeList = new List<string>();

        public List<double> brzinaList = new List<double>();

        public List<double> kilogramiList = new List<double>();

        public List<double> dynamicKilogramiList = new List<double>();

        public List<double> previusTickList = new List<double>();

        public List<double> tickList = new List<double>();

        public List<double> ocekivanaKilaza = new List<double>();

        public double izmerenikilogramistatic;
        public double stvarnikgstatic;
        #endregion

        Dispatcher disp = Dispatcher.CurrentDispatcher;
        public delegate void CallCues(MotorLogic motor, CueUpdateAction Add);
        public event CallCues NotifyCueEventHandler;

        public int NetResponseCount = 0;
        public double preiousTV = -100000;

        #region Visibility

        Visibility _visibility = Visibility.Collapsed;

        public Visibility NetOffVisibility { get { return _visibility; } set { if (_visibility != value) { _visibility = value; OnNotify("NetOffVisibility"); } } }

        Visibility _underloadVisibility = Visibility.Collapsed;

        public Visibility UnderLoadVisibility { get { return _underloadVisibility; } set { if (_underloadVisibility != value) { _underloadVisibility = value; OnNotify("UnderLoadVisibility"); } } }


        Visibility _overloadvisibility = Visibility.Collapsed;

        public Visibility OverLoadVisibility { get { return _overloadvisibility; } set { if (_overloadvisibility != value) { _overloadvisibility = value; OnNotify("OverLoadVisibility"); } } }

        #endregion
        
        /// <summary>
        /// determine if message is ok or is an error message
        /// </summary>
        public int RxMessageLength = 0;
        /// <summary>
        /// received message in bytes
        /// </summary>
        public byte[] RxMessage;
        /// <summary>
        /// if message is same as last one don't process it
        /// </summary>
        public bool RxMessageSame = false;



        public ObservableCollection<TripHistoryItem> TripHistoryList = new ObservableCollection<TripHistoryItem>();

        public ObservableCollection<TripHistoryItem> TripHistoryGet
        { get { return TripHistoryList; } }

        public List<int> ListOfCueNumbers = new List<int>();

        public DateTime ManTimeOut { get; set; }

        bool? _loadcellenabled;
        double _sp = 300, _CueSP, _tp, _sv = 100, _tv, _cp, _cv, _loadcell, _mv = 100, _limNeg = -300, _newreferencepoint, _limcoeficient = 1, _limPos = 1000, _coeficient = 60467.8848, _MaxV = 10, _DefaultMV = 20, _Imot, angular_speed = 0, _previouscv = 0, _previouscp = -100000000;
        bool _modbus = false, _enabled = true, _inverted = false, _cued = false, _canRecived, _trip, _refOk, _inh28, _moving, _brake, _sbcbit, _local, _can, _limPosReached, _limNegReached, _loadcelloverloadEnabled = false, _loadcellunderloadEnabled = false;
        bool _underloadsignal, _overloadsignal, _manPosEnabled, _manNegEnabled;
        int _motorID = 1, _group = -1, _sdoChannel = 1, _SdoCanMsgID;
        string _ipaddress = "", _secbrakecontrolip = "", _title = "QWERTYU", _description = "", _triperrorname, _triperrorlink;


        byte _ClusterLogical, _ClusterElectric, _canChannel = 1, _diomodul = 0, _screen_order_number = 0;
        double _zeroload, _ratedload, _overload, _underload, _loadcellkg, _loadcellmovingkg, _maxloadcell = 0, _minloadcell = 10000000, _spcoeficient = 1, _mvsvcoeficient = 1, _cvcoeficient = 1;
        Visibility _GuiVisibility = Visibility.Visible;


        [Column(Name = "sp_coefficient")]
        public double SpCoeficient { get { return _spcoeficient; } set { _spcoeficient = value; } }
        [Column(Name = "mv_sv_coefficient")]
        public double MvSvCoeficient { get { return _mvsvcoeficient; } set { _mvsvcoeficient = value; } }
        [Column(Name = "cv_coefficient")]
        public double CvCoeficient { get { return _cvcoeficient; } set { _cvcoeficient = value; } }


        /// <summary>
        /// this is set in MotorID set part, hack!
        /// </summary>
        public byte[] MotorIdAsArray = new byte[1];
     
        [Column(Name = "id")]
        public int MotorID { get { return _motorID; } set { _motorID = value; MotorIdAsArray[0] = (byte)value; } }
        [Column(Name = "screen_order_number")]
        public byte ScreenOrderNumber { get { return _screen_order_number; } set { _screen_order_number = value; } }
        [Column(Name = "ip_address")]
        public string IpAddress { get { return _ipaddress; } set { _ipaddress = value; } }

        [Column(Name = "title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        string _CabinetTitle;
        [Column(Name = "cabinet_title")]
        public string CabinetTitle { get { return _CabinetTitle; } set { if (_CabinetTitle != value) { _CabinetTitle = value; OnNotify("CabinetTitle"); } } }       


        public bool Cued
        {
            get { return _cued; }
            set
            {
                if (_cued != value)
                {
                    _cued = value;

                    if (value == true)
                        NotifyCueEventHandler(this, CueUpdateAction.Add);
                    //NotifyCueEventHandler(this, true);
                    else
                        NotifyCueEventHandler(this, CueUpdateAction.Remove);
                    //NotifyCueEventHandler(this, false);

                    OnNotify("Cued");
                }
            }
        }


        public int GroupNo
        {
            ///obavezno je dva puta 
            ///_group = value;
            /// OnNotify("GroupNo");
            /// zbog glupog update buga sa listboxom


            get { return _group; }
            set
            {
                if (_group != value)
                {
                    if (_group != -1) //trenutna vrednost
                    {
                        Sys.StaticGroupList[_group].RemoveFromGroup(this);
                    }
                    if (value != -1)//mot ubacen u grp
                    {
                        Sys.StaticGroupList[value].AddToGroup(this);

                    }

                    if (value == -1) //mot izbacen iz grp
                    {
                        soloSM.state = states.Idle;
                    }

                    _group = value;
                    OnNotify("GroupNo");
                    //CalculateEnableds(); // potrebno da bi se disablovali dugmici kad je u grupi i enablovali kad izadje //sklonjeno 20140406 - vec se izvrsava u tajmeru

                }
            }
        }
        bool _groupnochange;

        public bool GroupChangeEnabled { get { return _groupnochange; } set { if (_groupnochange != value) { _groupnochange = value; OnNotify("GroupChangeEnabled"); } } }


        public double SP { get { return _sp; } set { if (_sp != value) { _sp = value; OnNotify("SP"); } } }

        public double TP { get { return _tp; } set { if (_tp != value) { _tp = value; OnNotify("TP"); } } }//target position

        public double CueSP { get { return _CueSP; } set { if (_CueSP != value) { _CueSP = value; OnNotify("CueSP"); } } }


        public double TV { get { return _tv; } set { if (_tv != value) { _tv = value; OnNotify("TV"); } } }//target velocity

        public double SV { get { return _sv; } set { if (_sv != value) { _sv = value; OnNotify("SV"); } } }


        public double CP
        {
            get { return _cp; }
            set
            {
                if (_cp != value)
                {
                    //if (Sys.render3D) AnimateMotor.StartAnimation(MotorID, _cp, value);//u getsetu se okida animacija ako se promeni CP
                    //animateMotor.startAnimation(MotorID,  value);//u getsetu se okida animacija ako se promeni CP
                    //Debug.WriteLine("{0:hh mm ss.fff} cp={1}", DateTime.Now, _cp);
                    _cp = value;
                    OnNotify("CP");

                    ////motor is a parent
                    //if (ChildMotors.Count > 0)
                    //    foreach (var m in ChildMotors) m.UpdateCpAbsolute();
                    ////motor is a child
                    //else if (ParentMotor != null)
                    //    OnNotify("CP_Absolute");

                    //if(MotorID==40) Sys.printStack(this);
                }
            }
        }
        //public void UpdateCpAbsolute() { OnNotify("CP_Absolute"); }


        public double previousCP { get { return _previouscp; } set { _previouscp = value; } }

        public double previousCV { get { return _previouscv; } set { _previouscv = value; } }

        public double angularSpeed { get { return angular_speed; } set { angular_speed = value; } }

        #region LOAD CELL


        public double LC
        {
            get { return _loadcell; }
            set
            {
                if (_loadcell != value)
                {

                    _loadcell = value;
                    OnNotify("LC");

                    ////motor is a parent

                }
            }
        }


        public double minLC
        {
            get { return _minloadcell; }
            set
            {
                if (_minloadcell != value)
                {

                    _minloadcell = value;
                    OnNotify("minLC");

                    ////motor is a parent

                }
            }
        }


        public double maxLC
        {
            get { return _maxloadcell; }
            set
            {
                if (_maxloadcell != value)
                {

                    _maxloadcell = value;
                    OnNotify("maxLC");

                    ////motor is a parent

                }
            }
        }

        [Column(Name = "loadcell_enabled", CanBeNull = true)]
        // do we check for overload 
        public bool? loadCellEnabled { get { return _loadcellenabled; } set { if (_loadcellenabled != value) { _loadcellenabled = value; LogAndNotify("loadCellEnabled", value); } } }
        [Column(Name = "loadcell_overload_enabled")]
        // do we check for overload 
        public bool loadCellOverLoadEnabled { get { return _loadcelloverloadEnabled; } set { if (_loadcelloverloadEnabled != value) { _loadcelloverloadEnabled = value; LogAndNotify("loadCellOverLoadEnabled", value); } } }
        [Column(Name = "loadcell_underload_enabled")]
        // do we check for undeload
        public bool loadCellUnderLoadEnabled { get { return _loadcellunderloadEnabled; } set { if (_loadcellunderloadEnabled != value) { _loadcellunderloadEnabled = value; LogAndNotify("loadCellUnderLoadEnabled", value); } } }


        // is underload detected
        public bool UnderLoadSignal { get { return _underloadsignal; } set { if (_underloadsignal != value) { _underloadsignal = value; LogAndNotify("UnderLoadSignal", value); } } }

        // is overload detected
        public bool OverLoadSignal { get { return _overloadsignal; } set { if (_overloadsignal != value) { _overloadsignal = value; LogAndNotify("OverLoadSignal", value); } } }

        [Column(Name = "loadcell_zeroload")]
        public double ZeroLoad { get { return _zeroload; } set { if (_zeroload != value) { _zeroload = value; OnNotify("ZeroLoad"); } } }
        [Column(Name = "loadcell_ratedload")]
        public double RatedLoad { get { return _ratedload; } set { if (_ratedload != value) { _ratedload = value; OnNotify("RatedLoad"); } } }

        [Column(Name = "loadcell_overload")]
        public double OverLoadKg { get { return _overload; } set { if (_overload != value) { _overload = value; OnNotify("OverLoadKg"); } } }
        [Column(Name = "loadcell_underload")]
        public double UnderLoadKg { get { return _underload; } set { if (_underload != value) { _underload = value; OnNotify("UnderLoadKg"); } } }


        public double LCkg
        {
            get { return _loadcellkg; }
            set
            {
                if (_loadcellkg != value)
                {
                    _loadcellkg = value;
                    OnNotify("LCkg");
                }
            }
        }

        public double LcMovingKg
        {
            get { return _loadcellmovingkg; }
            set
            {
                if (_loadcellmovingkg != value)
                {
                    _loadcellmovingkg = value;
                    OnNotify("LcMovingKg");
                }
            }
        }
        #endregion



        public string prevousTripErrorName = "";


        public string TripErrorLink
        {
            get { return _triperrorlink; }
            set { if (_triperrorlink != value) { _triperrorlink = value; OnNotify("TripErrorLink"); } }
        }

        public double referencepoint;

        public double NewReferencePoint
        {
            get { return _newreferencepoint; }
            set { if (_newreferencepoint != value) { _newreferencepoint = value; OnNotify("NewReferencePoint"); } }

        }



        public double CV { get { return _cv; } set { if (_cv != value) { _cv = value; OnNotify("CV"); } } } //absCV = Math.Abs(value); - cemu l

        public double MV { get { return _mv; } set { if (_mv != value) { _mv = value; OnNotify("MV"); } } }//20131227 todo kandidat za brisanje, ne koristi se

        [Column(Name = "max_v")]
        public double MaxV { get { return _MaxV; } set { if (_MaxV != value) { _MaxV = value; OnNotify("MaxV"); } } }

        double _BigTicks = double.NaN;
        [Column(Name = "big_ticks")]
        public double BigTicks { get { return _BigTicks; } set { _BigTicks = value; } }

        [Column(Name = "default_mv")]
        public double DefaultMV { get { return _DefaultMV; } set { if (_DefaultMV != value) { _DefaultMV = value; OnNotify("DefaultMV"); } } }


        #region limits

        [Column(Name = "lim_neg")]
        public double LimNeg { get { return _limNeg; } set { if (_limNeg != value) { _limNeg = value; OnNotify("LimNeg"); } } }
        [Column(Name = "lin_pos")]
        public double LimPos { get { return _limPos; } set { if (_limPos != value) { _limPos = value; OnNotify("LimPos"); } } }
        [Column(Name = "limit_coefficient")]
        public double LimitCoeficient { get { return _limcoeficient; } set { if (_limcoeficient != value) { _limcoeficient = value; OnNotify("LimitCoeficient"); } } }

        public bool LimPosReached { get { return _limPosReached; } set { if (_limPosReached != value) { _limPosReached = value; LogAndNotify("LimPosReached", value); } } }
        public bool LimNegReached { get { return _limNegReached; } set { if (_limNegReached != value) { _limNegReached = value; LogAndNotify("LimNegReached", value); } } }

        #endregion


        [Column(Name = "enabled")]
        public bool Enabled { get { return _enabled; } set { _enabled = value; } }
        [Column(Name = "inverted")]
        public bool Inverted { get { return _inverted; } set { _inverted = value; } }


        [Column(Name = "coefficient")]
        public double Coeficient { get { return _coeficient; } set { _coeficient = value; } }

        [Column(Name = "cluster_logical")]//logical group of drives such as stage platforms, sidestage hoists ...
        public byte ClusterLogical { get { return _ClusterLogical; } set { _ClusterLogical = value; } }
        [Column(Name = "cluster_electric")]//electrical circuit that supplies the drive. used for power consumption calc
        public byte ClusterElectric { get { return _ClusterElectric; } set { _ClusterElectric = value; } }

        double _GraphMin, _GraphMax;
        [Column(Name = "graph_min")]
        public double GraphMin { get { return _GraphMin; } set { _GraphMin = value; } }
        [Column(Name = "graph_max")]
        public double GraphMax { get { return _GraphMax; } set { _GraphMax = value; } }
        int _name3d;
        [Column(Name = "name_3d")]
        public int Name3D { get { return _name3d; } set { _name3d = value; } }

        // decide whitch  modbus or can commands are beeng used 
        SoloStateMachine _soloSM;
        public SoloStateMachine soloSM { get { if (_soloSM == null) { _soloSM = new SoloStateMachine(this); } return _soloSM; } }

        public Visibility GuiVisibility { get { return _GuiVisibility; } set { if (_GuiVisibility != value) { _GuiVisibility = value; OnNotify("GuiVisibility"); } } }

        public bool previousTrip = false;

        public bool Trip { get { return _trip; } set { if (_trip != value) { _trip = value; if (value) { soloSM.tripOccured(); } LogAndNotify("Trip", value); } } }

        public bool RefOk { get { return _refOk; } set { if (_refOk != value) { _refOk = value; LogAndNotify("RefOk", value); } } }

        public bool Inh28 { get { return _inh28; } set { if (_inh28 != value) { _inh28 = value; LogAndNotify("Inh28", value); } } }

        public bool Moving { get { return _moving; } set { if (_moving != value) { _moving = value; LogAndNotify("Moving", value); } } }


        public bool Brake { get { return _brake; } set { if (_brake != value) { _brake = value; LogAndNotify("Brake", value); } } }

        public bool SbcStatusBitIndicator { get { return _sbcbit; } set { if (_sbcbit != value) { _sbcbit = value; LogAndNotify("SbcBit", value); } } }

        public bool Local { get { return _local; } set { if (_local != value) { _local = value; LogAndNotify("Local", value); } } }

        public bool MainContactor { get { return _MainContactor; } set { if (_MainContactor != value) { _MainContactor = value; LogAndNotify("MainContactor", value); } } }

        bool _MainContactor;

        public ChangeMonitor2 CpChanging = new ChangeMonitor2();

        public void SubscribeToCueLogic(CueLogic subject)
        {
            subject.NotifyMlEventHandler += UpdateFromCue;
        }

        void UpdateFromCue(int cueNumber, int MotorID, CueDelegateStates state)
        {
            if (this.MotorID == MotorID)
            {
                switch (state)
                {
                    case CueDelegateStates.Add:
                        ListOfCueNumbers.Add(cueNumber);
                        break;
                    case CueDelegateStates.Remove:
                        ListOfCueNumbers.Remove(cueNumber);
                        break;
                    case CueDelegateStates.Clear:
                        ListOfCueNumbers.Clear();
                        break;
                }
            }
        }

        #region   VALUES TO OBSERVE

        bool _autoEnabled, _stopEnabled, _manEnabled, _grpResetEnabled, _tripEnabled;//_autoEnabledCues

        // 
        //  public bool manPosNegEnabled { get { return _manPosNegEnabled; } set { if (_manPosNegEnabled != value) { _manPosNegEnabled = value; OnNotify("manPosNegEnabled"); } } }


        public bool manPosEnabled { get { return _manPosEnabled; } set { if (_manPosEnabled != value) { _manPosEnabled = value; OnNotify("manPosEnabled"); } } }

        public bool manNegEnabled { get { return _manNegEnabled; } set { if (_manNegEnabled != value) { _manNegEnabled = value; OnNotify("manNegEnabled"); } } }

        public bool autoEnabled { get { return _autoEnabled; } set { if (_autoEnabled != value) { _autoEnabled = value; OnNotify("autoEnabled"); } } }

        public bool stopEnabled { get { return _stopEnabled; } set { if (_stopEnabled != value) { _stopEnabled = value; OnNotify("stopEnabled"); } } }

        public bool manEnabled { get { return _manEnabled; } set { if (_manEnabled != value) { _manEnabled = value; OnNotify("manEnabled"); } } }

        public bool tripEnabled { get { return _tripEnabled; } set { if (_tripEnabled != value) { _tripEnabled = value; OnNotify("tripEnabled"); } } }






        public bool Net { get { return _can; } set { if (_can != value) { _can = value; OnNotify("Net"); OnNotify("LinkIndicator"); } } }
        #endregion


        // [Column(Name = "DiagItems")]
        //   public string DiagItems { get { return _DiagItems; } set { if (_DiagItems != value) { _DiagItems = value; OnNotify("DiagItems"); } } }

        bool _spenabled, _mvenabled;

        public bool SpSvEnabled { get { return _spenabled; } set { if (_spenabled != value) { _spenabled = value; OnNotify("SpSvEnabled"); } } }

        public bool MVEnabled { get { return _mvenabled; } set { if (_mvenabled != value) { _mvenabled = value; OnNotify("MVEnabled"); } } }

        public bool grpResetEnabled { get { return _grpResetEnabled; } set { if (_grpResetEnabled != value) { _grpResetEnabled = value; OnNotify("grpResetEnabled"); } } }


        public double PosDirection { get; set; }

        public double NegDirection { get; set; }


        public BrkMonitor _brkMonitor = new BrkMonitor();

        #region SBC


        byte _SecBrkControlBit = 0, _initSecBrkControlBit = 0;
        MotorBrake _SecBrkControlLock = MotorBrake.Locked;

        /// <summary>
        /// address on DIO, red from database
        /// </summary>
        [Column(Name = "init_sbc_bit")]
        public byte initSbcBit { get { return _initSecBrkControlBit; } set { if (_initSecBrkControlBit != value) { _initSecBrkControlBit = value; OnNotify("initSecBrkControlBit"); } } }

        /// <summary>
        /// address on DIO
        /// </summary>
        [Column(Name = "sbc_bit")]
        public byte SbcBitAddress { get { return _SecBrkControlBit; } set { if (_SecBrkControlBit != value) { _SecBrkControlBit = value; OnNotify("SecBrkControlBit"); } } }
        /// <summary>
        /// enables moving 
        /// </summary>
        /// <remarks>dio state locked or unlocked</remarks>
        public MotorBrake SbcLock { get { return _SecBrkControlLock; } set { if (_SecBrkControlLock != value) { _SecBrkControlLock = value; UdpTx.UniqueInstance.ModBusSigleCommandWrite("DIO", (double)value, MotorIdAsArray); OnNotify("SbcLock"); } } }

        #endregion


        public MotorLogic()
        {

        }

        public void SetMotorDirection()
        {
            this.PosDirection = (this.Inverted ? LimNeg : LimPos); // (this.Inverted ? CanSett.CanCommandManNegative : CanSett.CanCommandManPositive);
            this.NegDirection = (this.Inverted ? LimPos : LimNeg); // (this.Inverted ? CanSett.CanCommandManPositive : CanSett.CanCommandManNegative);
        }


        public List<DiagItemValues> DiagItemsList = new List<DiagItemValues>();


        /// <summary>
        /// convert this motor to work as list
        /// helper
        /// </summary>
        /// <returns></returns>
        public List<MotorLogic> toList()
        {
            List<MotorLogic> a = new List<MotorLogic>();
            a.Add(this);
            return a;
        }

        //public void CalculateButtonsAndIndicatorsIsEnabled(MotorLogic motor1)
        public void CalculateEnableds()
        {
            autoEnabled = manEnabled = manPosEnabled = manNegEnabled = MVEnabled = SpSvEnabled = tripEnabled = true;
            GroupChangeEnabled = true;
            NetOffVisibility = Visibility.Visible;

            //darko promenio murska sobota 20101201 (cim stavis motor u grupu on pokaze CANOFF)
            if (!Net)
                NetOffVisibility = Visibility.Visible;
            else
                NetOffVisibility = Visibility.Collapsed;

            if (loadCellUnderLoadEnabled) { UnderLoadVisibility = Visibility.Visible; } else { UnderLoadVisibility = Visibility.Collapsed; }

            if (loadCellOverLoadEnabled) { OverLoadVisibility = Visibility.Visible; } else { OverLoadVisibility = Visibility.Collapsed; }


            if (GroupNo == -1)
            {
                autoEnabled = (Net && RefOk && !Moving && !Trip && Brake && !SbcStatusBitIndicator && !Local && manEnabled);
                stopEnabled = !Local;
                GroupChangeEnabled = (!Local && Brake && !SbcStatusBitIndicator);
                SpSvEnabled = MVEnabled = manEnabled = (Net && !Moving && !Trip && !Local && Brake && !SbcStatusBitIndicator);

                //manPosEnabled = (soloSM.state == states.ManStarted2 || soloSM.state == states.ManStarted || soloSM.state == states.ManPos || soloSM.state == states.ManNeg) && !UnderLoadSignal;
                //manNegEnabled = (soloSM.state == states.ManStarted2 || soloSM.state == states.ManStarted || soloSM.state == states.ManPos || soloSM.state == states.ManNeg) && !OverLoadSignal;
                manPosEnabled = (soloSM.state == states.ManStarted2 || soloSM.state == states.ManStarted || soloSM.state == states.ManPos || soloSM.state == states.ManPos2 ||
                    soloSM.state == states.ManNeg || soloSM.state == states.ManNeg2) && !UnderLoadSignal;
                manNegEnabled = (soloSM.state == states.ManStarted2 || soloSM.state == states.ManStarted || soloSM.state == states.ManPos || soloSM.state == states.ManPos2 ||
                    soloSM.state == states.ManNeg || soloSM.state == states.ManNeg2) && !OverLoadSignal;
            }
            else
                autoEnabled = manEnabled = manPosEnabled = manNegEnabled = MVEnabled = SpSvEnabled = tripEnabled = false;


        }

        bool cpChanged = false;
        int pravac;


        public double previous_angular_speed = 0;
        public double previous_speed = 0;
        public long previusTick = 0;
        double speed = 0;
        double ugaono_ubrzanje = 0;
        double sila = 0;

        public void MotorStatuses()
        {

            byte[] chunkAll = RxMessage;
            SbcStatusBitIndicator = Convert.ToBoolean(chunkAll[0]);
            byte[] chunk = chunkAll.Skip(1).ToArray(); // skipping dio

            //Array.Reverse(chunk, 0, chunk.Length);
            // message received length is the end of processing received datagrams
            if (chunk.Length > RxMessageLength - 1)
            {
                Debug.WriteLine(chunk.Length + "  " + RxMessageLength);
            }
            byte[] bytes = new byte[] { };
            // +4 is for 32bit register read
            for (var i = 0; i < chunk.Length - 1; i += 4)
            {

                bytes = Utl.SubArray(chunk, i, 4, "chunk");

                switch (i)
                {

                    //0-3 -- AMC ENABLE
                    case 0:
                        break;

                    //CV ---> 4-7 -- AMC SPEED -- 33.005
                    case 4:
                        Array.Reverse(bytes, 0, bytes.Length);
                        CV = BitConverter.ToInt32(bytes, 0) / CvCoeficient;
                        break;

                    //CP ---> 8-11 -- AMC POSITION -- 33.004 
                    case 8:
                        Array.Reverse(bytes, 0, bytes.Length);
                        CP = BitConverter.ToInt32(bytes, 0) / Coeficient;

                        // start position
                        if (previousCP != -100000000)
                        {
                            cpChanged = Math.Abs(previousCP - CP) >= MaxV * Properties.Settings.Default.MovingCoeficient;
                            if (Math.Abs(previousCP - CP) > 0)
                            {
                                // Trace.WriteLine("difference cp  :" + Math.Abs(previousCP - CP).ToString());
                                //  Trace.WriteLine("max v * coefficient :" + (MaxV * Properties.Settings.Default.MovingCoeficient).ToString());
                            }
                        }
                        else
                        {
                            cpChanged = false;
                        }
                        Moving = cpChanged;
                        break;
                    //12-15 -- AMC STATUS
                    case 12:
                        // gimnastika da bi statusna rec bila ista kao i na regulatoru
                        // posto se citaju dva byta [0,1] pa [2,3]
                        //
                        byte[] amcStatus = Utl.SubArray(chunk, i + 2, 2, "chunk12");

                        var amcStatusBits = new BitArray(amcStatus);

                        LimPosReached = !Convert.ToBoolean(amcStatusBits[10]); // only lover two bytes, because its 16 bit word
                        LimNegReached = !Convert.ToBoolean(amcStatusBits[11]); // only lover two bytes, because its 16 bit word
                        break;
                    //TRIP ---> 16-19 --- Drive Healthy
                    case 16:
                        Trip = !Convert.ToBoolean(bytes[3]);
                        break;
                    // 20-23 --- LastTrip
                    case 20:
                        TripHistoryItem thi = new TripHistoryItem();
                        Array.Reverse(bytes, 0, bytes.Length);
                        thi.Error = BitConverter.ToInt32(bytes, 0).ToString();
                        if (!thi.Error.Equals(prevousTripErrorName) || (Trip != previousTrip && Trip == true))
                        {
                            thi.ErrorLink = new Uri(String.Format("file:///C:/ProgramData/Control%20Techniques/Database%20Server/M700/en-GB/ParamRefGuide/ParamRef/RFC_A/trips.html#{0}", thi.Error), UriKind.Absolute); //todo

                            thi.ErrorText = DateTime.Now.ToString();

                            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                            {
                                // :todo make file out of old trips 
                                TripHistoryList.Add(thi);
                            });
                           
                        }

                        prevousTripErrorName = thi.Error;
                        previousTrip = Trip;
                        break;
                    //24-27 --- Amc Select 
                    case 24:
                        break;
                    //28-31 --- Brake Control
                    case 28:
                        Brake = !Convert.ToBoolean(bytes[3]);
                        break;
                    //32-35 --- LOCAL
                    case 32:
                        Local = Convert.ToBoolean(bytes[3]); // 00.068 --> LOCAL
                        break;
                    //36-39 --  INHIBIT
                    case 36:
                        Inh28 = !Convert.ToBoolean(bytes[3]); // 00.069 --> INHIBIT       
                        break;
                    //40-43 -- SPEED REF -- rpm
                    case 40:
                        Array.Reverse(bytes, 0, bytes.Length);
                        angularSpeed = (2 * 3.14 * BitConverter.ToInt32(bytes, 0)) / 60;
                        if (angularSpeed > 0)
                        {
                            // Trace.WriteLine("angularSpeed: " + angularSpeed + "rpm");
                        }
                        break;
                    //44-47 -- LOAD CELL -- milliamp  %
                    case 44:

                        //cams as % from Emerson regulator
                        // to convert to ma from %
                        // from Emerson table formula ---> 4-20mA ((Input Current - 4mA) / 16mA) x 100.00% 
                        Array.Reverse(bytes, 0, bytes.Length);
                        var mapercent = (double)BitConverter.ToUInt16(bytes, 0) / 100;

                        //this is in Milli amps
                        // LC = (double)((ma percent * 16 / 100) + 4);

                        LC = mapercent; //(double)(mapercent * 10 / 100) - 0.705;

                        //converting amps to kN
                        // double newton from amp = LC * 0.83333 - 3.3333;
                        double newtonfromamp = LC * 1.02 - 0.2;
                        break;



                        #region test
                        if (Moving)
                        {
                            // Trace.WriteLine("vrsta kretanja :" + ubrzanje);
                            long tickNow = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                            if (CV > previousCV)
                            {
                                pravac = 1;
                            }
                            else { pravac = -1; }
                            if (CP > previousCP)
                            {
                                pravacList.Add(1);
                            }
                            else { pravacList.Add(-1); }

                            if (Math.Abs(CV) + 4 >= SV)
                            {

                                VrstaKretanjaList.Add("ravnomerno");

                            }
                            else
                            {
                                if (Math.Abs(CV) > Math.Abs(previousCV))
                                {
                                    VrstaKretanjaList.Add("ubrzanje");
                                }
                                else
                                {
                                    VrstaKretanjaList.Add("usporenje");
                                }
                            }
                            // -s = mg + ma
                            if (previusTick > 0)
                            {
                                double kgnow = (newtonfromamp * 101.97); //29.36 * mapercent - 87.8033;
                                                                         // LcMovingKg = (izmerenikilogramistatic * 9.8066500286389 + izmerenikilogramistatic * ((CP - previousCP) * 0.01) / ((tickNow - previusTick) * 0.001)) * 0.10197;

                                ugaono_ubrzanje = Math.Round((angularSpeed - previous_angular_speed) / (tickNow - previusTick) * 0.001, 4);
                                // Trace.WriteLine("ugaono_ubrzanje: " + ugaono_ubrzanje);

                                double moment_inercije = (izmerenikilogramistatic * 0.360 * 0.360);
                                //  Trace.WriteLine("moment_inercije: " + moment_inercije);

                                double moment_sile = Math.Round(moment_inercije * ugaono_ubrzanje, 4);
                                // Trace.WriteLine("moment_sile: " + moment_sile);

                                sila = Math.Round(moment_sile / 0.360, 4);
                                //  Trace.WriteLine("sila: " + sila);


                                // maxLC = Math.Round(sila * 0.10197);

                                LcMovingKg = Math.Round(((kgnow * 9.8066500286389 + pravac * kgnow * ((CV - previousCV) * 0.01 / ((tickNow - previusTick) * 0.001)) * 0.10197) / 10) - 8.5, 2);

                            }

                            dynamicKilogramiList.Add(LcMovingKg);
                            KgMirovanjeList.Add(izmerenikilogramistatic);
                            StvarniKgMirovanjeList.Add(stvarnikgstatic);

                            CPList.Add(CP);
                            PreviousCPList.Add(previousCP);
                            PreviousCVList.Add(previousCV);
                            LoadCellList.Add(LC);
                            //timestempList.Add(new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds());
                            DateTime dt = DateTime.Now;
                            vremeList.Add(dt.ToLongTimeString() + "." + dt.Millisecond.ToString());
                            brzinaList.Add(CV);
                            kilogramiList.Add(LCkg);
                            tickList.Add(tickNow);
                            previusTickList.Add(previusTick);
                            ocekivanaKilaza.Add(sila * 0.10197 * 1000);


                            if (minLC > LcMovingKg && LcMovingKg != 0) { minLC = LcMovingKg; }
                            if (maxLC < LcMovingKg) { maxLC = LcMovingKg; }
                            previusTick = tickNow;
                            previous_speed = speed;
                            previous_angular_speed = angularSpeed;
                        }
                        else
                        {
                            izmerenikilogramistatic = (29.36 * mapercent - 87.8033) - 8.5;
                            stvarnikgstatic = (newtonfromamp * 101.97) - 8.5;

                        }
                        #endregion

                        // checking overload and underload status 
                        if (loadCellOverLoadEnabled && LC < UnderLoadKg) { UnderLoadSignal = true; } else { UnderLoadSignal = false; }
                        if (loadCellUnderLoadEnabled && LC > OverLoadKg) { OverLoadSignal = true; } else { OverLoadSignal = false; }

                        if (UnderLoadSignal)
                        {
                            if (CP - previousCP > 0)
                            {
                                soloSM.state = states.Stopping1;
                            }
                            if (!Moving) { soloSM.state = states.Error1; }
                        }
                        if (OverLoadSignal)
                        {
                            if (CP - previousCP > 0)
                            {
                                soloSM.state = states.Stopping1;
                            }
                            if (!Moving) { soloSM.state = states.Error1; }
                        }

                        //   LCkg = (a + b* LC) / (1 + ca * LC + d * LC * LC);

                        break;
                    //48-51 --- Movement detection 
                    case 48:
                        Moving = !Convert.ToBoolean(bytes[3]); ;
                        break;

                    case 52:
                        Array.Reverse(bytes, 0, bytes.Length);
                        referencepoint = BitConverter.ToInt32(bytes, 0) / Coeficient;
                        break;
                }
            }

            RefOk = true;
            Net = true;
            NetResponseCount++;

            MainContactor = true;//Utl.GetBitFromByteArray(data, 6);

            //CV = Convert.ToInt32(100 * BitConverter.ToInt16(data, 2) / CanSett.PDO_WordDenominator); // 9300
            //record states 
            previousCP = CP;
            previousCV = CV;


            //   previousBrake = Brake;

            var c = GetType().GetProperty("CV");


        }



        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        private void LogAndNotify(String parameter, bool? value) // proveriti
        {
            //Log.Write("Motor " + MotorID.ToString() + ": " + parameter + " changed to " + value.ToString());
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }


        #endregion




        #region ILinkCheck
        public bool LinkIndicator { get { if (Sett.Modbus) { return true; } else { return this.Net; } } set { } }//this.can radi notify 

        public void PingDevice() { }//plc pinguje

        public void ResponseReceived() { }//can rx radi

        public void ResetIndicator() { }//can rx radi


        #endregion
    }


}
