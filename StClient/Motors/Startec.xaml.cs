﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.ComponentModel;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Startec.xaml
    /// </summary>
    public partial class Startec : UserControl
    {
        StartecLogic startec;
        public Startec(StartecLogic startec)
        {
            InitializeComponent();
            this.startec = startec;
            DataContext = startec;


        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(startec.Up);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(startec.Down);
        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(0);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(0);
        }



        //private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    UdpTx.UniqueInstance.SendHoistPDO((int)Math.Pow(2, e.NewValue));
        //}

        private void CHKGroup_Checked(object sender, RoutedEventArgs e)
        {
            Sys.StartecGroupLogic.AddToGroup(startec);
        }

        private void CHKGroup_Unchecked(object sender, RoutedEventArgs e)
        {
            Sys.StartecGroupLogic.RemoveFromGroup(startec);
        }
    }

    public class StartecLogic : INotifyPropertyChanged
    {

        int _motorID = 10, _row = 0, _column = 0;
        //, _SecBrkControlAddress = 1, _SecBrkControlBit=1;
        short _encodermotorid = 10;
        string _title = "hoist";
        double _cp = 0;
        double _coef = 1;
        long _up = 0, _down = 0;
        //MotorBrake _SecBrkControlLock = MotorBrake.Locked;

        [XmlAttribute("UP")]
        public long Up { get { return _up; } set { _up = value; } }
        [XmlAttribute("DOWN")]
        public long Down { get { return _down; } set { _down = value; } }
        [XmlAttribute("EncoderMotorID")]
        public short EncoderMotorID { get { return _encodermotorid; } set { _encodermotorid = value; } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }
        [XmlAttribute("ROW")]
        public int Row { get { return _row; } set { _row = value; } }
        [XmlAttribute("COLUMN")]
        public int Column { get { return _column; } set { _column = value; } }
        [XmlAttribute("COEFICIENT")]
        public double Coeficient { get { return _coef; } set { _coef = value; } }
        /*
        [XmlAttribute("SecBrkControlAddress")]
        public int SecBrkControlAddress { get { return _SecBrkControlAddress; } set { _SecBrkControlAddress = value; } }
        [XmlAttribute("SecBrkControlBit")]
        public int SecBrkControlBit { get { return _SecBrkControlBit; } set { _SecBrkControlBit = value; } }
        

        [XmlIgnore]
        public MotorBrake SecBrkControlLock
        {
            get { return _SecBrkControlLock; }
            set
            {
                if (_SecBrkControlLock != value)
                {
                    MessageBox.Show("zavrsi!!!!!!");
                    _SecBrkControlLock = value;
                    //SecondaryBreakControl sbc = Sys.SecundaryBreakControlList.Find(qq => qq.CanChannel == CanChannel);
                    //sbc.Output = MotorLogic.SetBitInByteArray2(sbc.Output, SecBrkControlBit, value);
                    OnNotify("SecBrkControlLock");
                }
            }
        }
        */
        bool ischkchecked = false;
        [XmlIgnore]
        public bool isCHKchecked { get { return ischkchecked; } set { if (ischkchecked != value) { ischkchecked = value; OnNotify("isCHKchecked"); } } }

        [XmlIgnore]
        public double CP { get { return _cp; } set { if (_cp != value) { _cp = value / Coeficient; OnNotify("CP"); } } }

        public StartecLogic()
        {
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
