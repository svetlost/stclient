﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StClient.Wagons;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Vagoni003.xaml
    /// </summary>
    public partial class VagonGoGui : UserControl
    {
        Commands commands = new Commands();
        //VagonLogic003 vl;

        //public VagonGoGui(VagonLogic003 vagon)
        //{
        //    InitializeComponent();

        //    this.DataContext = vl = vagon;

        //}
        public VagonGoGui()
        {
            InitializeComponent();
            TimedAction.ExecuteWithDelay(new Action(delegate { this.DataContext = Sys.vagoniRxTx; this.SV.Text = "100"; }), TimeSpan.FromSeconds(3));
            //this.DataContext = Sys.vagoniRxTx;
            bGo.Visibility = Visibility.Hidden;


        }

        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ToggleButton t = (ToggleButton)sender;
            t.IsChecked = true; if (t.Name == "bEnable") bGo.Visibility = Visibility.Visible;
        }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SV_Vagon_Popup((TextBlock)sender);
        }

        private void bEnable_Click(object sender, RoutedEventArgs e)
        {
            bGo.Visibility = (bool)bEnable.IsChecked ? Visibility.Visible : Visibility.Hidden;
        }

    }
}
