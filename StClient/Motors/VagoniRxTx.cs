﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using StClient.Wagons;
using System.Threading;

namespace StClient
{
    public class VagoniRxTx : INotifyPropertyChanged
    {
        #region UniqueInstance
        //class VagoniRxTxCreator
        //{
        //    static VagoniRxTxCreator() { }
        //    internal static readonly VagoniRxTx uniqueInstance = new VagoniRxTx();
        //}
        //public static VagoniRxTx UniqueInstance
        //{
        //    get { return VagoniRxTxCreator.uniqueInstance; }
        //}
        #endregion
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion

        int a;
        public int A { get { return a; } set { if (a != value) { a = value; OnNotify("A"); } } }


        //int _SvRotInner = 5, _SvRotOuter = 10, _SvWagons = 30, _SvWagonRot = 15, _CvRotInner, _CvRotOuter;
        int _SvRotInner, _SvRotOuter, _SvWagons, _SvWagonRot, _CvRotInner, _CvRotOuter;
        int _SpRotInner, _SpRotOuter;
        public int SvRotInner { get { return _SvRotInner; } set { if (_SvRotInner != value) { RotInn.SV = _SvRotInner = TrimSv(value); OnNotify("SvRotInner"); } } }
        public int SvRotOuter { get { return _SvRotOuter; } set { if (_SvRotOuter != value) { RotOut.SV = _SvRotOuter = TrimSv(value); OnNotify("SvRotOuter"); } } }
        public int SpRotInner { get { return _SpRotInner; } set { if (_SpRotInner != value) { _SpRotInner = (value); OnNotify("SpRotInner"); } } }
        public int SpRotOuter { get { return _SpRotOuter; } set { if (_SpRotOuter != value) { _SpRotOuter = (value); OnNotify("SpRotOuter"); } } }
        public void UpdateRotSP() { OnNotify("SpRotInner"); OnNotify("SpRotOuter"); }
        public int CvRotInner { get { return _CvRotInner; } set { if (_CvRotInner != value) { _CvRotInner = TrimSv(value); OnNotify("CvRotInner"); } } }
        public int CvRotOuter { get { return _CvRotOuter; } set { if (_CvRotOuter != value) { _CvRotOuter = TrimSv(value); OnNotify("CvRotOuter"); } } }
        public int SvWagons { get { return _SvWagons; } set { if (_SvWagons != value) { _SvWagons = TrimSv(value); OnNotify("SvWagons"); } } }
        public int SvWagonRot { get { return _SvWagonRot; } set { if (_SvWagonRot != value) { _SvWagonRot = TrimSv(value); OnNotify("SvWagonRot"); } } }

        public int TrimSv(int i) { return (i < 0 || i > 100 ? 0 : i); }

        bool _RotInnTrip, _RotInnMoving, _RotInnCan, _RotInnBrake, _RotInnInh28;
        bool _RotOutTrip, _RotOutMoving, _RotOutCan, _RotOutBrake, _RotOutInh28;
        public bool RotInnTrip { get { return _RotInnTrip; } set { if (_RotInnTrip != value) { _RotInnTrip = value; OnNotify("RotInnTrip"); } } }
        public bool RotInnMoving { get { return _RotInnMoving; } set { if (_RotInnMoving != value) { _RotInnMoving = value; OnNotify("RotInnMoving"); } } }
        public bool RotInnCan { get { return _RotInnCan; } set { if (_RotInnCan != value) { _RotInnCan = value; OnNotify("RotInnCan"); } } }
        public bool RotInnBrake { get { return _RotInnBrake; } set { if (_RotInnBrake != value) { _RotInnBrake = value; OnNotify("RotInnBrake"); } } }
        public bool RotInnInh28 { get { return _RotInnInh28; } set { if (_RotInnInh28 != value) { _RotInnInh28 = value; OnNotify("RotInnInh28"); } } }

        public bool RotOutTrip { get { return _RotOutTrip; } set { if (_RotOutTrip != value) { _RotOutTrip = value; OnNotify("RotOutTrip"); } } }
        public bool RotOutMoving { get { return _RotOutMoving; } set { if (_RotOutMoving != value) { _RotOutMoving = value; OnNotify("RotOutMoving"); } } }
        public bool RotOutCan { get { return _RotOutCan; } set { if (_RotOutCan != value) { _RotOutCan = value; OnNotify("RotOutCan"); } } }
        public bool RotOutBrake { get { return _RotOutBrake; } set { if (_RotOutBrake != value) { _RotOutBrake = value; OnNotify("RotOutBrake"); } } }
        public bool RotOutInh28 { get { return _RotOutInh28; } set { if (_RotOutInh28 != value) { _RotOutInh28 = value; OnNotify("RotOutInh28"); } } }

        //double _SV;
        //public double SV { get { return _SV; } set { if (_SV != value) { _SV = value; OnNotify("SV"); } } }
        //string _Title;
        //public string Title { get { return _Title; } set { if (_Title != value) { _Title = value; OnNotify("Title"); } } }
        //bool _Enable;
        //public bool Enable { get { return _Enable; } set { if (_Enable != value) { _Enable = value; OnNotify("Enable"); } } }
        //public VoidNoArg GoDelegate, StopDelegate;

        public static List<VagonLogic003> VagoniList = new List<VagonLogic003>();
        DispatcherTimer timer = new DispatcherTimer();
        //DispatcherTimer rotStopTimer = new DispatcherTimer();
        //DispatcherTimer enabledsTimer = new DispatcherTimer();
        IPEndPoint ipepClient;
        EndPoint epClient;
        Socket sockTx, sockRx;
        IPEndPoint iep1 = new IPEndPoint(IPAddress.Broadcast, 1202);//todo20131230 staviti u config,l ovo i ostalo

        byte[] data2 = new byte[100]{ 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x30, 0x00, 0x64, 0x00, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00    };
        byte[] data2allzero = new byte[100]{ 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x30, 0x00, 0x64, 0x00, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00    };

        public VagonLogic003 wL1 { get; set; }
        public VagonLogic003 wL2 { get; set; }
        public VagonLogic003 wL3 { get; set; }
        public VagonLogic003 wR1 { get; set; }
        public VagonLogic003 wR2 { get; set; }
        public VagonLogic003 wR3 { get; set; }
        public VagonLogic003 wROT { get; set; }
        public VagonLogic003 RotInn { get; set; }
        public VagonLogic003 RotOut { get; set; }

        Properties.Settings Sett = Properties.Settings.Default;

        public VagoniRxTx()
        {
            VagoniList.Add(wL1 = new VagonLogic003(46, "WAG LEFT  1"));
            VagoniList.Add(wL2 = new VagonLogic003(48, "WAG LEFT  2"));
            VagoniList.Add(wL3 = new VagonLogic003(50, "WAG LEFT  3"));
            VagoniList.Add(wR1 = new VagonLogic003(52, "WAG RIGHT 1"));
            VagoniList.Add(wR2 = new VagonLogic003(54, "WAG RIGHT 2"));
            VagoniList.Add(wR3 = new VagonLogic003(56, "WAG RIGHT 3"));
            VagoniList.Add(wROT = new VagonLogic003(58, "WAG ROT"));
            VagoniList.Add(RotInn = new VagonLogic003(60, "ROT INN"));
            VagoniList.Add(RotOut = new VagonLogic003(62, "ROT OUT"));

            timer.Tick += timer_Tick4;
            timer.Interval = TimeSpan.FromMilliseconds(200);
            sockRx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sockTx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sockTx.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);

            //enabledsTimer.Tick += enabledsTimer_Tick;
            //enabledsTimer.Interval = TimeSpan.FromMilliseconds(600);

            //ipepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.LocalIp), 3510);
            //ipepClient = new IPEndPoint(IPAddress.Parse("10.4.4.40"), 13555);
            ipepClient = new IPEndPoint(IPAddress.Parse(Sett.LocalIp), Sett.WagonRxPort);
            epClient = (EndPoint)ipepClient;

            //ipepPlc = new IPEndPoint(IPAddress.Parse("10.4.4.54"), 4500);


            try { sockRx.Bind(ipepClient); }
            catch { }

            timer.Start();
            //enabledsTimer.Start();

            //rotStopTimer.Interval = TimeSpan.FromSeconds(6);
            //rotStopTimer.Tick += rotStopTimer_Tick;
        }

        public void TotalStop()
        {
            bStopAllSideWagons = bStopRotWagon = true;
            RotInn.FW = RotInn.BW = RotOut.FW = RotOut.BW = false;
            bRotCombi_MNEG = bRotCombi_MNEG2 = bRotCombi_MPOS = bRotCombi_MPOS2 = false;
            bRotInn_MNEG = bRotInn_MPOS = bRotOut_MNEG = bRotOut_MPOS = false;
        }

        public void StopRotOnExit()
        {

            bRotInn_M = bRotOut_M = true;
            timer_Tick4(null, new EventArgs());//send immediately

            Thread.Sleep(500);

            bStopAllSideWagons = bStopRotWagon = true;
            RotInn.FW = RotInn.BW = RotOut.FW = RotOut.BW = false;
            bRotCombi_MNEG = bRotCombi_MNEG2 = bRotCombi_MPOS = bRotCombi_MPOS2 = false;
            bRotInn_MNEG = bRotInn_MPOS = bRotOut_MNEG = bRotOut_MPOS = false;

            bENABLE_ROT = false;

            timer_Tick4(null, new EventArgs());//send immediately
            Thread.Sleep(500);
        }
        public void StopRotOnExit_Phase1()
        {

            bRotInn_M = bRotOut_M = true;
            timer_Tick4(null, new EventArgs());//send immediately

        }
        public void StopRotOnExit_Phase2()
        {
            Thread.Sleep(300);

            bStopAllSideWagons = bStopRotWagon = true;
            RotInn.FW = RotInn.BW = RotOut.FW = RotOut.BW = false;
            bRotCombi_MNEG = bRotCombi_MNEG2 = bRotCombi_MPOS = bRotCombi_MPOS2 = false;
            bRotInn_MNEG = bRotInn_MPOS = bRotOut_MNEG = bRotOut_MPOS = false;

            bENABLE_ROT = false;

            timer_Tick4(null, new EventArgs());//send immediately
            Thread.Sleep(300);
        }

        //void enabledsTimer_Tick(object sender, EventArgs e)
        //{
        //    foreach (var w in VagoniList.Where(q => q.Addr < 60))
        //    {
        //        if (w.Lock && Sys.LocksEnabled && (Sys.LocksOverrideEnabled && !Sys.lockMotors.OverrideActive))
        //        {
        //            w.autoManEnabled = w.GRP = false;
        //            if (w.Addr == 58) w.FW = w.BW = false;
        //        }
        //    }
        //}

        public void closeSocket() { sockRx.Close(); }

        //void rotStopTimer_Tick(object sender, EventArgs e) { rotStopTimer.Stop(); }

        void timer_Tick4(object sender, EventArgs e)
        {
            PackBytes();

            //try
            //{
                sockTx.SendTo(data2, iep1);
            //}
            //catch (Exception exc)
            //{
            //    //MessageBox.Show(this, "Socket error. Please restart the application.");
            //    App.Current.MainWindow.Width = 50;
            //    App.Current.MainWindow.Height = 50;
            //    MessageBox.Show("Socket error. Please restart the application.");//20150813 this should never happen. Event detects hibernation and shuts down app.
            //    Log.Write(string.Format("timer_Tick4 socket.send error (OS hibernated while app was alive?). Error={0}", exc.Message), EventLogEntryType.Error);
            //    App.Current.Shutdown();
            //}

            ProcessRx();
        }

        public const int msgLength = 32;
        //byte[] data = new byte[msgLength];
        byte[] dataTx = new byte[msgLength];
        byte[] dataReceived = new byte[msgLength];
        ushort _tempAddress;
        VagonLogic003 _tmpVag;
        int newCounter;
        //bool bRotCombi_StopTimer = false;

        void ProcessRx()
        {
            try
            {
                while (sockRx.Available > 0)
                {
                    sockRx.ReceiveFrom(dataReceived, ref epClient);

                    _tempAddress = (ushort)(dataReceived[0] + 256 * dataReceived[1]);
                    _tmpVag = VagoniList.Find(q => q.Addr == _tempAddress);
                    if (_tmpVag == null) continue;

                    _tmpVag.status1 = (ushort)(dataReceived[2] + 256 * dataReceived[3]);
                    _tmpVag.status2 = (ushort)(dataReceived[4] + 256 * dataReceived[5]);
                    _tmpVag.state = (ushort)(dataReceived[6] + 256 * dataReceived[7]);
                    //CP = (ushort)(data[8] + 256 * data[9]);
                    //CV = (ushort)(data[10] + 256 * data[11]);
                    _tmpVag.CP = BitConverter.ToInt16(new byte[] { dataReceived[8], dataReceived[9] }, 0);
                    _tmpVag.CV = BitConverter.ToInt16(new byte[] { dataReceived[10], dataReceived[11] }, 0);
                    //CV = (ushort)(data[10] + 256 * data[11]);
                    newCounter = (ushort)(dataReceived[30] + 256 * dataReceived[31]);
                    //_tmpVag.Can = (newCounter != _tmpVag.Counter);//20130116 koristi se drugaciji metod, CAN racuna timer u UdpRx
                    _tmpVag.Counter = newCounter;
                    _tmpVag.Trip = Sys.getBitOfInt(_tmpVag.status1, 2);
                    _tmpVag.Brake = !Sys.getBitOfInt(_tmpVag.status1, 3);
                    _tmpVag.Inh28 = Sys.getBitOfInt(_tmpVag.status1, 4);
                    _tmpVag.RefOk = Sys.getBitOfInt(_tmpVag.status1, 5);
                    _tmpVag.LimNeg = Sys.getBitOfInt(_tmpVag.status1, 6);
                    _tmpVag.LimPos = Sys.getBitOfInt(_tmpVag.status1, 7);
                    _tmpVag.Local = Sys.getBitOfInt(_tmpVag.status1, 8);
                    _tmpVag.Moving = Math.Abs(_tmpVag.CV) > 0.5;
                    _tmpVag.CanResponseCount++;
                    //Debug.WriteLine(string.Format("adress={0} counter={1:D6} cp={2,4} cv={3,3} status1={4:D5} state={5} trip={6} brk={7} inh={8} limneg={9} limpos={10} can={11}",
                    //    _tempAddress, _tmpVag.Counter, _tmpVag.CP, _tmpVag.CV, _tmpVag.status1, _tmpVag.state, _tmpVag.Trip, _tmpVag.Brake, _tmpVag.Inh28, _tmpVag.LimNeg, _tmpVag.LimPos, _tmpVag.Can));
                }
            }
            catch { };
        }

        void PackBytes()
        {
            data2[20] = (byte)(wL1.FW ? 1 : 0);
            data2[21] = (byte)(wL1.BW ? 1 : 0);

            data2[23] = (byte)(wL2.FW ? 1 : 0);
            data2[24] = (byte)(wL2.BW ? 1 : 0);

            data2[26] = (byte)(wL3.FW ? 1 : 0);
            data2[27] = (byte)(wL3.BW ? 1 : 0);

            data2[29] = (byte)(wR1.FW ? 1 : 0);
            data2[30] = (byte)(wR1.BW ? 1 : 0);

            data2[32] = (byte)(wR2.FW ? 1 : 0);
            data2[33] = (byte)(wR2.BW ? 1 : 0);

            data2[35] = (byte)(wR3.FW ? 1 : 0);
            data2[36] = (byte)(wR3.BW ? 1 : 0);

            if (!wL1.GRP) data2[20] = data2[21] = 0;
            if (!wL2.GRP) data2[23] = data2[24] = 0;
            if (!wL3.GRP) data2[26] = data2[27] = 0;
            if (!wR1.GRP) data2[29] = data2[30] = 0;
            if (!wR2.GRP) data2[32] = data2[33] = 0;
            if (!wR3.GRP) data2[35] = data2[36] = 0;

            data2[22] = (byte)(_bRotInn_TripReset ? 1 : 0);
            data2[25] = (byte)(_bRotOut_TripReset ? 1 : 0);
            data2[28] = (byte)(_bRotInn_RefReset ? 1 : 0);
            data2[31] = (byte)(_bRotOut_RefReset ? 1 : 0);

            data2[52] = data2allzero[52] = (byte)SvWagons;

            //tbSV_RotOuter.Text = Sys.potSvRotOuter.ToString();
            //tbSV_RotInner.Text = Sys.potSvRotInner.ToString();
            //BindingOperations.SetBinding(s, Slider.ValueProperty, new Binding("CP") { Source = motor });

            //data2[60] = data2allzero[60] = (byte)SpRotInner;
            //data2[58] = data2allzero[58] = (byte)SpRotOuter;
            //data2[60] = data2allzero[60] = (byte)RotInn.SP;
            //data2[60] = data2allzero[60] = (byte)(RotInn.SP % 256);
            //data2[61] = data2allzero[61] = (byte)((int)RotInn.SP / 256);
            byte[] b1 = BitConverter.GetBytes((int)RotInn.SP);
            data2[60] = data2allzero[60] = b1[0];
            data2[61] = data2allzero[61] = b1[1];

            byte[] b2 = BitConverter.GetBytes((int)RotOut.SP);
            //data2[58] = data2allzero[58] = (byte)RotOut.SP;
            data2[58] = data2allzero[58] = b2[0];
            data2[59] = data2allzero[59] = b2[1];

            data2[56] = data2allzero[56] = (byte)SvRotInner;//sv
            data2[54] = data2allzero[54] = (byte)SvRotOuter;

            data2[44] = (byte)((bRotInn_Auto || bRotCombi_Auto) && _bENABLE_ROT ? 1 : 0);
            data2[48] = (byte)((bRotOut_Auto || bRotCombi_Auto) && _bENABLE_ROT ? 1 : 0);


            data2[62] = data2allzero[62] = (byte)SvRotOuter;
            data2[64] = data2allzero[64] = (byte)SvRotInner;
            data2[66] = data2allzero[66] = (byte)SvWagonRot;

            //if ((bool)!bENABLE_ROT.IsChecked) data2[35] = data2[36] = 0;  ////////////////////todo!!!!!!!!!!!!!!!!  + stavvi togglebuttone

            //data2[45] = (byte)((_bRotInn_M || _bRotCombi_M || bRotCombi_Stop) && _bENABLE_ROT ? 1 : 0);
            //data2[49] = (byte)((_bRotOut_M || _bRotCombi_M || bRotCombi_Stop) && _bENABLE_ROT ? 1 : 0);
            data2[45] = (byte)((_bRotInn_M || _bRotCombi_M || rotStopTimer.IsEnabled) && _bENABLE_ROT ? 1 : 0);
            data2[49] = (byte)((_bRotOut_M || _bRotCombi_M || rotStopTimer.IsEnabled) && _bENABLE_ROT ? 1 : 0);

            data2[47] = (byte)(_bRotInn_MNEG || _bRotCombi_MNEG || _bRotCombi_MNEG2 ? 1 : 0);
            data2[46] = (byte)(_bRotInn_MPOS || _bRotCombi_MPOS || _bRotCombi_MPOS2 ? 1 : 0);
            data2[51] = (byte)(_bRotOut_MNEG || _bRotCombi_MNEG || _bRotCombi_MPOS2 ? 1 : 0);
            data2[50] = (byte)(_bRotOut_MPOS || _bRotCombi_MPOS || _bRotCombi_MNEG2 ? 1 : 0);

            data2[37] = (byte)(_bStopAllSideWagons ? 1 : 0);
            data2[38] = (byte)(_bAutoSideWagons ? 1 : 0);
            data2[39] = (byte)(_bStopRotWagon ? 1 : 0);
            data2[40] = (byte)(_bAutoRotWagon ? 1 : 0);
            data2[41] = (byte)(wROT.BW ? 1 : 0);
            data2[42] = (byte)(wROT.FW ? 1 : 0);
            //data2[41] = (byte)(_bBwRotWagon ? 1 : 0);
            //data2[42] = (byte)(_bFw1RotWagon ? 1 : 0);
            data2[43] = (byte)(_bFw2RotWagon ? 1 : 0);
        }
        bool _bRotInn_Auto = false, _bRotOut_Auto = false, _bRotCombi_Auto = false, _bRotCombi_Stop = false;
        public bool bRotInn_Auto { get { return _bRotInn_Auto; } set { if (_bRotInn_Auto != value) { _bRotInn_Auto = value; OnNotify("bRotInn_Auto"); } } }
        public bool bRotOut_Auto { get { return _bRotOut_Auto; } set { if (_bRotOut_Auto != value) { _bRotOut_Auto = value; OnNotify("bRotOut_Auto"); } } }
        public bool bRotCombi_Auto { get { return _bRotCombi_Auto; } set { if (_bRotCombi_Auto != value) { _bRotCombi_Auto = value; OnNotify("bRotCombi_Auto"); } } }
        public bool bRotCombi_Stop { get { return _bRotCombi_Stop; } set { if (_bRotCombi_Stop != value) { _bRotCombi_Stop = value; OnNotify("bRotCombi_Stop"); } } }
        bool _bRotInn_M = false, _bRotOut_M = false, _bRotCombi_M = false, _bENABLE_ROT = false;
        public bool bRotInn_M { get { return _bRotInn_M; } set { if (_bRotInn_M != value) { _bRotInn_M = value; OnNotify("bRotInn_M"); } } }
        public bool bRotOut_M { get { return _bRotOut_M; } set { if (_bRotOut_M != value) { _bRotOut_M = value; OnNotify("bRotOut_M"); } } }
        public bool bRotCombi_M { get { return _bRotCombi_M; } set { if (_bRotCombi_M != value) { _bRotCombi_M = value; OnNotify("bRotCombi_M"); } } }
        public bool bENABLE_ROT { get { return _bENABLE_ROT; } set { if (_bENABLE_ROT != value) { _bENABLE_ROT = value; OnNotify("bENABLE_ROT"); } } }
        bool _bRotCombi_MNEG = false, _bRotCombi_MPOS = false;
        public bool bRotCombi_MNEG { get { return _bRotCombi_MNEG; } set { if (_bRotCombi_MNEG != value) { _bRotCombi_MNEG = value; OnNotify("bRotCombi_MNEG"); } } }
        public bool bRotCombi_MPOS { get { return _bRotCombi_MPOS; } set { if (_bRotCombi_MPOS != value) { _bRotCombi_MPOS = value; OnNotify("bRotCombi_MPOS"); } } }
        bool _bRotCombi_MNEG2 = false, _bRotCombi_MPOS2 = false;
        public bool bRotCombi_MNEG2 { get { return _bRotCombi_MNEG2; } set { if (_bRotCombi_MNEG2 != value) { _bRotCombi_MNEG2 = value; OnNotify("bRotCombi_MNEG2"); } } }
        public bool bRotCombi_MPOS2 { get { return _bRotCombi_MPOS2; } set { if (_bRotCombi_MPOS2 != value) { _bRotCombi_MPOS2 = value; OnNotify("bRotCombi_MPOS2"); } } }
        bool _bRotInn_MNEG = false, _bRotInn_MPOS = false;
        public bool bRotInn_MNEG { get { return _bRotInn_MNEG; } set { if (_bRotInn_MNEG != value) { _bRotInn_MNEG = value; OnNotify("bRotInn_MNEG"); } } }
        public bool bRotInn_MPOS { get { return _bRotInn_MPOS; } set { if (_bRotInn_MPOS != value) { _bRotInn_MPOS = value; OnNotify("bRotInn_MPOS"); } } }
        bool _bRotOut_MNEG = false, _bRotOut_MPOS = false;
        public bool bRotOut_MNEG { get { return _bRotOut_MNEG; } set { if (_bRotOut_MNEG != value) { _bRotOut_MNEG = value; OnNotify("bRotOut_MNEG"); } } }
        public bool bRotOut_MPOS { get { return _bRotOut_MPOS; } set { if (_bRotOut_MPOS != value) { _bRotOut_MPOS = value; OnNotify("bRotOut_MPOS"); } } }
        bool _bRotOut_TripReset = false, _bRotInn_TripReset = false;
        public bool bRotOut_TripReset { get { return _bRotOut_TripReset; } set { if (_bRotOut_TripReset != value) { _bRotOut_TripReset = value; OnNotify("bRotOut_TripReset"); } } }
        public bool bRotInn_TripReset { get { return _bRotInn_TripReset; } set { if (_bRotInn_TripReset != value) { _bRotInn_TripReset = value; OnNotify("bRotInn_TripReset"); } } }
        bool _bRotOut_RefReset = false, _bRotInn_RefReset = false;
        public bool bRotOut_RefReset { get { return _bRotOut_RefReset; } set { if (_bRotOut_RefReset != value) { _bRotOut_RefReset = value; OnNotify("bRotOut_RefReset"); } } }
        public bool bRotInn_RefReset { get { return _bRotInn_RefReset; } set { if (_bRotInn_RefReset != value) { _bRotInn_RefReset = value; OnNotify("bRotInn_RefReset"); } } }
        bool _bRotOut_Trip = false, _bRotInn_Trip = false;
        public bool bRotOut_Trip { get { return _bRotOut_Trip; } set { if (_bRotOut_Trip != value) { _bRotOut_Trip = value; OnNotify("bRotOut_Trip"); } } }
        public bool bRotInn_Trip { get { return _bRotInn_Trip; } set { if (_bRotInn_Trip != value) { _bRotInn_Trip = value; OnNotify("bRotInn_Trip"); } } }

        public void rotStop() { rotStopTimer.Start(); }


        bool _bStopAllSideWagons = false, _bAutoSideWagons = false;
        public bool bStopAllSideWagons
        {
            get { return _bStopAllSideWagons; }
            set
            {
                if (_bStopAllSideWagons != value)
                {
                    _bStopAllSideWagons = value;
                    OnNotify("bStopAllSideWagons");
                    if (value) Log.Write("Wagon 1-6: STOPALL pressed.", EventLogEntryType.Information);
                }
            }
        }
        public bool bAutoSideWagons
        {
            get { return _bAutoSideWagons; }
            set
            {
                if (_bAutoSideWagons != value)
                {
                    _bAutoSideWagons = value;
                    OnNotify("bAutoSideWagons");
                    if (value)
                    {
                        string msg = "Wagon 1-6: AUTO pressed.";
                        msg += string.Format("\nWAG1L1 FW={0}, BW={1}, GRP={2};", wL1.FW, wL1.BW, wL1.GRP);
                        msg += string.Format("\nWAG2L2 FW={0}, BW={1}, GRP={2};", wL2.FW, wL2.BW, wL2.GRP);
                        msg += string.Format("\nWAG3L3 FW={0}, BW={1}, GRP={2};", wL3.FW, wL3.BW, wL3.GRP);
                        msg += string.Format("\nWAG4R1 FW={0}, BW={1}, GRP={2};", wR1.FW, wR1.BW, wR1.GRP);
                        msg += string.Format("\nWAG5R2 FW={0}, BW={1}, GRP={2};", wR2.FW, wR2.BW, wR2.GRP);
                        msg += string.Format("\nWAG6R3 FW={0}, BW={1}, GRP={2}.", wR3.FW, wR3.BW, wR3.GRP);
                        Log.Write(msg, EventLogEntryType.Information);
                    }
                }
            }
        }
        bool _bStopRotWagon = false, _bAutoRotWagon = false, _bFw1RotWagon = false, _bFw2RotWagon = false, _bBwRotWagon = false;
        public bool bStopRotWagon
        {
            get { return _bStopRotWagon; }
            set
            {
                if (_bStopRotWagon != value)
                {
                    _bStopRotWagon = value;
                    OnNotify("bStopRotWagon");
                    if (value) Log.Write("Wagon ROT: STOP pressed.", EventLogEntryType.Information);
                }
            }
        }
        public bool bAutoRotWagon { get { return _bAutoRotWagon; } set { if (_bAutoRotWagon != value) { _bAutoRotWagon = value; OnNotify("bAutoRotWagon"); } } }
        public bool bFw1RotWagon
        {
            get { return _bFw1RotWagon; }
            set
            {
                if (_bFw1RotWagon != value)
                {
                    _bFw1RotWagon = value; OnNotify("bFw1RotWagon");
                    bAutoRotWagon = value;
                    if (value) Log.Write("Wagon ROT: FW1 pressed.", EventLogEntryType.Information);
                }
            }
        }
        public bool bFw2RotWagon
        {
            get { return _bFw2RotWagon; }
            set
            {
                if (_bFw2RotWagon != value)
                {
                    _bFw2RotWagon = value; OnNotify("bFw2RotWagon");
                    bAutoRotWagon = value;
                    if (value) Log.Write("Wagon ROT: FW2 pressed.", EventLogEntryType.Information);
                }
            }
        }
        public bool bBwRotWagon
        {
            get { return _bBwRotWagon; }
            set
            {
                if (_bBwRotWagon != value)
                {
                    _bBwRotWagon = value; OnNotify("bBwRotWagon");
                    bAutoRotWagon = value;
                    if (value) Log.Write("Wagon ROT: BW pressed.", EventLogEntryType.Information);
                }
            }
        }

        //private void bSTOPALL_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { PackBytes(); data2[37] = 1; sockTx.SendTo(data2, iep1); Log.Write("Wagon 1-6: STOPALL pressed."); }
        //private void bSTOPALL_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[37] = 0; sockTx.SendTo(data2allzero, iep1); }
        //private void bAUTO_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    PackBytes(); data2[38] = 1;
        //    sockTx.SendTo(data2, iep1);
        //    string msg = "Wagon 1-6: AUTO pressed.";
        //    msg += string.Format("\nWAG1L1 FW={0}, BW={1}, GRP={2};", wL1.FW, wL1.BW, wL1.Grp);
        //    msg += string.Format("\nWAG2L2 FW={0}, BW={1}, GRP={2};", wL2.FW, wL2.BW, wL2.Grp);
        //    msg += string.Format("\nWAG3L3 FW={0}, BW={1}, GRP={2};", wL3.FW, wL3.BW, wL3.Grp);
        //    msg += string.Format("\nWAG4R1 FW={0}, BW={1}, GRP={2};", wR1.FW, wR1.BW, wR1.Grp);
        //    msg += string.Format("\nWAG5R2 FW={0}, BW={1}, GRP={2};", wR2.FW, wR2.BW, wR2.Grp);
        //    msg += string.Format("\nWAG6R3 FW={0}, BW={1}, GRP={2}.", wR3.FW, wR3.BW, wR3.Grp);
        //    Log.Write(msg);
        //}
        //private void bSTOP_ROTVAG_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[39] = 1; Log.Write("Wagon ROT: STOP pressed."); }
        //private void bSTOP_ROTVAG_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[39] = 0; }
        //private void bNZ0_7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[40] = 1; data2[41] = 1; Log.Write("Wagon ROT: BW pressed."); }
        //private void bNZ0_7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[40] = 0; data2[41] = 0; }
        //private void bNP1_7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[40] = 1; data2[42] = 1; Log.Write("Wagon ROT: FW1 pressed."); }
        //private void bNP1_7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[40] = 0; data2[42] = 0; }
        //private void bNP2_7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { data2[40] = 1; data2[43] = 1; Log.Write("Wagon ROT: FW2 pressed."); }
        //private void bNP2_7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[40] = 0; data2[43] = 0; }

        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }


        private void bENABLE_ROT_Unchecked(object sender, RoutedEventArgs e) { bRotInn_M = bRotOut_M = false; }

        private void bRotInn_M_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTINNER_MAN changed state. New state={0}", bRotInn_M), EventLogEntryType.Information); }
        private void bRotInn_MPOS_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTINNER_CW changed state. New state={0}", bRotInn_MPOS), EventLogEntryType.Information); }
        private void bRotInn_MNEG_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTINNER_CCW changed state. New state={0}", bRotInn_MNEG), EventLogEntryType.Information); }

        private void bRotOut_M_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTOUTER_MAN changed state. New state={0}", bRotOut_M), EventLogEntryType.Information); }
        private void bRotOut_MPOS_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTOUTER_CW changed state. New state={0}", bRotOut_MPOS), EventLogEntryType.Information); }
        private void bRotOut_MNEG_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTOUTER_CCW changed state. New state={0}", bRotOut_MNEG), EventLogEntryType.Information); }

        private void bRotCombi_M_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI1_MAN changed state. New state={0}", bRotCombi_M), EventLogEntryType.Information); }
        private void bRotCombi_MPOS_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI1_CW changed state. New state={0}", bRotCombi_MPOS), EventLogEntryType.Information); }
        private void bRotCombi_MNEG_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI1_CCW changed state. New state={0}", bRotCombi_MNEG), EventLogEntryType.Information); }

        private void bRotCombi_MPOS2_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI2_CW changed state. New state={0}", bRotCombi_MPOS2), EventLogEntryType.Information); }
        private void bRotCombi_MNEG2_Click(object sender, RoutedEventArgs e) { Log.Write(string.Format("ROTCOMBI2_CCW changed state. New state={0}", bRotCombi_MNEG2), EventLogEntryType.Information); }

    }
}
