﻿using System;

namespace StClient
{
    /// <summary>
    /// closes the brakes if the brakes are open for too long without any motion
    /// </summary>
    public class BrkMonitor
    {
        bool _brk, _mov;
        enum _states { idle, brkOpen, brkOpenMov, timeout, stopping }
        _states state;
        DateTime timeoutTime { get; set; }
        public double timeoutTime_sec = 10;
        public bool timeout { get; private set; }

        void ResetTimeout(double timeSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromSeconds(timeSeconds); }

        public void getValues(bool Brk, bool Mov)
        {
            _brk = Brk;
            _mov = Mov;
            process();
        }

        void process()
        {
            switch (state)
            {
                case _states.idle:
                    timeout = false;
                    if (_brk == false) state = _states.idle;
                    else
                    {
                        ResetTimeout(timeoutTime_sec);
                        state = _states.brkOpen;
                    }
                    break;
                case _states.brkOpen:
                    if (timeoutTime < DateTime.Now) state = _states.timeout;
                    if (_brk == false) state = _states.idle;
                    else if (_mov == true)
                    {
                        ResetTimeout(timeoutTime_sec);
                        state = _states.brkOpen;
                    }
                    else state = _states.brkOpen;
                    break;
                case _states.timeout:
                    timeout = true;
                    state = _states.stopping;
                    break;
                case _states.stopping:
                    timeout = false;
                    if (_brk == true) state = _states.idle;
                    else state = _states.stopping;
                    break;
            }
        }
    }
}
