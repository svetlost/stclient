﻿using System;
using System.ComponentModel;

namespace StClient
{
    public enum states
    {
        Idle, Error1, Error2, Error3, AutoStarting1, AutoStarting2, DelayAutoStart, Started, Stopping1, Stopping2, Stopping3, Stopped, WaitForBrake,
        ResRef1, ResRef2, ResRef3, ResRef4, ResRef5,
        JoyStarting, JoyStarting2,  JoyEnded, JoyButtPressed, JoyButtPressed1, JoyButtReleased, JoyPos, JoyNeg,
        ManPos, ManPos2, ManNeg, ManNeg2, ManResetTimer, ManStarting, ManStarted, ManStarted2, ManButtReleased
    }//,AutoStarting,


    public enum CueDelegateStates { Add, Remove, Clear }
    public enum SdoPriority { lo, med, hi }
    public enum SdoTimeOut { lo = 5000, med = 800, hi = 600 }
    public enum SdoType { read, write, write2byte };
    public enum MotorBrake { Locked = 0, Unlocked = 1 } //0,1
    // public enum Movement { SingleOrGroup, Cues, NoMovement }
    //enum SBCstates { OK, Erroring, Error }
    public enum CreateDefaultRow { Create, DontCreate };
    public enum CueUpdateAction { Add, Remove };
    //public enum SM_State { SM_Idle, SM_Up, SM_Down, SM_Stop }
    //public enum SyncMode { Sync, NoSync }
    public enum ShowPercButtons { Show, NoShow }
    //public enum LogEntryType { Info = 719, Warning = 721, Init = 761, Action = 765, Error = 767, Critical = 779 }

    public interface IMotorIndicators : INotifyPropertyChanged
    {
        bool RefOk { get; set; }
        bool Local { get; set; }
        bool Net { get; set; }
        bool Inh28 { get; set; }
        //  bool manPosNegEnabled { get; set; }
        bool manPosEnabled { get; set; }
        bool manNegEnabled { get; set; }

        bool SpSvEnabled { get; set; }
        bool manEnabled { get; set; }
        DateTime ManTimeOut { get; set; }
        void CalculateEnableds();
    }
    public interface IInitResult
    {
        string initResult { get; set; }
    }

    //public interface ISyncIMotorsList
    //{
    //    bool Trip { get; set; }
    //    bool Moving { get; set; }
    //    bool Brake { get; set; }
    //    bool autoEnabled { get; set; }
    //    bool stopEnabled { get; set; }
    //    //bool IsSync { get; set; }
    //    //double SyncValue { get; set; }
    //    List<MotorLogic> ListOfMotors();
    //}

    //public interface IMv  //for Single , Group  or joystick
    //{
    //    double JV { get; set; }
    //    int Direction { get; set; }
    //}

    public interface ISpSv // for single,Group or CueItem
    {
        double SP { get; set; }
        double SV { get; set; }
    }
    //public interface IStopGo // for single,Group or CueItem
    //{
    //    double SV { get; set; }
    //    string Title { get; set; }
    //    bool Enable { get; set; }
    //    bool Stop { get; set; }
    //    bool Go { get; set; }
    //    //VoidNoArg StopDelegate;
    //    //VoidNoArg GoDelegate;
    //}
    public interface ICpCv // for single,Group or CueItem
    {
        double CP { get; set; }
        //double CP_Absolute { get; }
        double CV { get; set; }
    }
    //public interface ILock // for single,Group or CueItem
    //{
    //    bool Lock { get; set; }
    //}






}
