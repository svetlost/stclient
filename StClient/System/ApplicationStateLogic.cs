﻿using LogAlertHB;
using System;
using System.ComponentModel;

namespace StClient
{
    class ApplicationStateLogic : INotifyPropertyChanged
    {
        private static volatile ApplicationStateLogic instance;
        private static object syncRoot = new Object();


        bool _isappenabled = true;
        //bool _isappenabled = true;  //todo vrati na false test 20130929
        public bool isAppEnabled
        {
            get { return _isappenabled; }
            set
            {
                if (_isappenabled != value) { _isappenabled = value; OnNotify("isAppEnabled"); }

            }
        }

        private ApplicationStateLogic()
        { }

        public static ApplicationStateLogic Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ApplicationStateLogic();
                    }
                }

                return instance;
            }
        }

        bool _PilzIsActivated;
        public bool PilzIsActivated
        {
            get { return _PilzIsActivated; }
            set
            {
                if (_PilzIsActivated != value)
                {
                    _PilzIsActivated = value;
                    OnNotify("PilzIsActivated");
                    Log.WriteDetailed(string.Format("PilzIsActivated changed to: ", value));
                }
            }
        }

        bool _LP1 = false;
        public bool LP1
        {
            get { return _LP1; }
            set
            {
                if (_LP1 != value)
                {
                    _LP1 = value;
                    OnNotify("LP1"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("LP1={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }

        bool _LP2 = false;
        public bool LP2
        {
            get { return _LP2; }
            set
            {
                if (_LP2 != value)
                {
                    _LP2 = value;
                    OnNotify("LP2"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("LP2={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }

        bool _PULT_PK1 = false;
        public bool PULT_PK1
        {
            get { return _PULT_PK1; }
            set
            {
                if (_PULT_PK1 != value)
                {
                    _PULT_PK1 = value;
                    OnNotify("PULT_PK1"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("PULT_PK1={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }
        bool _PULT_PK2 = false;
        public bool PULT_PK2
        {
            get { return _PULT_PK2; }
            set
            {
                if (_PULT_PK2 != value)
                {
                    _PULT_PK2 = value;
                    OnNotify("PULT_PK2"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("PULT_PK2={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }
        bool _PCP_PK1 = false;
        public bool PCP_PK1
        {
            get { return _PCP_PK1; }
            set
            {
                if (_PCP_PK1 != value)
                {
                    _PCP_PK1 = value;
                    OnNotify("PCP_PK1"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("PCP_PK1={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }
        bool _PCP_PK2 = false;
        public bool PCP_PK2
        {
            get { return _PCP_PK2; }
            set
            {
                if (_PCP_PK2 != value)
                {
                    _PULT_PK2 = value;
                    OnNotify("PCP_PK2"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("PCP_PK2={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }

        bool _GelbauOk = false;
        public bool GelbauOk
        {
            get { return _GelbauOk; }
            set
            {
                if (_GelbauOk != value)
                {
                    _GelbauOk = value;
                    OnNotify("GelbauOk");
                    Log.WriteDetailed(string.Format("GelbauActivated={0}", value)); WriteAxesCurrentData();
                }
            }
        }
        private Uri _webaddress;
        public Uri webaddress
        {
            get { return _webaddress; }
            set
            {
                if (_webaddress != value)
                { _webaddress = value; OnNotify("webaddress"); }
            }
        }

        public bool Estop_Pressed { get { return _LP1 || _LP2 || _PULT_PK1 || _PULT_PK2 || _PCP_PK1 || _PCP_PK2; } }
        void WriteAxesCurrentData()
        {
            foreach (var obj  in Sys.StaticMotorList){
                MotorLogic ml = obj.Value;
                Log.WriteDetailed(string.Format("\t Axis id={0,5} name={1,10} cp={2,10} cv={3,10}  ky3={4,5}",
                        ml.MotorID, ml.Title, ml.CP, ml.CV, ml.SbcLock));
            } 
        }




        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion
    }
}
