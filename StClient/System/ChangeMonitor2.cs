﻿using System;

namespace StClient
{
    /// <summary>
    /// used to filter MOV signal. if a few scans show POSITION is not changing (ie when pushing joy from up to down so speed is 0 at one point)
    /// </summary>
    public class ChangeMonitor2
    {
        enum CmStates { stNotChanging, stStartChanging, stChanging, stEndChanging }
        public double value //has to be called each cycle TODO: change this!!!!!!
        {
            get { return _value; }
            set
            {
                _value = value;
                processSM();
                //if (nextCompareTime < DateTime.Now)
                //{
                _oldvalue = value;
                //    nextCompareTime = DateTime.Now + compareCycleTime;
                //}
            }
        }
        double _value, _oldvalue;
        CmStates state = CmStates.stNotChanging;
        //public ChangeMonitor(double value) { }
        //public bool Changing { get { return state != CmStates.stNotChanging; } }
        //public bool Changing { get { return state == CmStates.stChanging || state == CmStates.stEndChanging || state == CmStates.stStartChanging; } }
        public bool Changing { get { return state == CmStates.stChanging || state == CmStates.stEndChanging; } }
        public DateTime timeoutTime { get; set; }
        public double changeTime_sec = 0.3;
        public double startChangeTime_sec = 1;
        int changingCount, notChangingCount;
        //TimeSpan compareCycleTime = TimeSpan.FromMilliseconds(300);
        //DateTime nextCompareTime = DateTime.Now;

        void ResetTimeout(double timeSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromSeconds(timeSeconds); }

        void processSM()
        {

            switch (state)
            {
                case CmStates.stNotChanging:
                    if (_oldvalue != _value)
                    {
                        ResetTimeout(startChangeTime_sec);
                        state = CmStates.stStartChanging;
                    }
                    else state = CmStates.stNotChanging;//(_oldvalue == _value)
                    changingCount = notChangingCount = 0;
                    break;
                case CmStates.stStartChanging:
                    if (_oldvalue != _value) changingCount++;
                    else changingCount = 0;// (_oldvalue == _value)

                    if (timeoutTime < DateTime.Now) state = CmStates.stNotChanging;
                    else if (changingCount > 3) state = CmStates.stChanging;
                    else state = CmStates.stStartChanging;
                    break;
                case CmStates.stChanging:
                    if (_oldvalue == _value)
                    {
                        changingCount = notChangingCount = 0;
                        state = CmStates.stEndChanging;
                    }
                    else state = CmStates.stChanging;
                    break;

                case CmStates.stEndChanging:
                    if (_oldvalue == _value) notChangingCount++;

                    if (_oldvalue != _value) state = CmStates.stChanging;
                    else if (notChangingCount > 2) state = CmStates.stNotChanging;
                    else state = CmStates.stEndChanging;
                    break;
            }
        }
    }
}
