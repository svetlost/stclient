﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace StClient
{
    public class PopupWindows
    {
        PopUpWindow popupWin;

        public void SP_Popup(MotorLogic motor, TextBlock sender, bool isSenderCue, bool isLoadCell=false)
        {
            SP_Popup(new List<MotorLogic>() { motor }, sender, isSenderCue, isLoadCell);
        }
        public void SP_Popup(List<MotorLogic> motorsList, TextBlock sender, bool isSenderCue,bool isLoadCell= false)
        {
            if (PopupAlreadyOpen()) return;
            if (isLoadCell)
            {
                popupWin = new PopUpWindow(0, 5000, sender, 500, 500);
            }
           else if (!isSenderCue && !isLoadCell)
            {
                //if (motorsList.Count > 0 && motorsList.All(qq => qq.iMotorIndicators.manEnabled))
                if (motorsList.Count > 0 && motorsList.All(qq => qq.manEnabled))
                    popupWin = new PopUpWindow(motorsList.Min(t => t.LimNeg), motorsList.Max(t => t.LimPos), sender, 500, 500);
            }
            else
                popupWin = new PopUpWindow(motorsList.Max(t => t.LimNeg), motorsList.Min(t => t.LimPos), sender, 500, 500);
        }

        public void SVMV_Popup(MotorLogic motor, TextBlock sender, bool isSenderCue, ShowPercButtons showPercButtons)
        { SVMV_Popup(new List<MotorLogic>() { motor }, sender, isSenderCue, showPercButtons); }

        public void SVMV_Popup(List<MotorLogic> motorsList, TextBlock sender, bool isSenderCue, ShowPercButtons showPercButtons)
        {
            if (PopupAlreadyOpen()) return;
            if (motorsList.Count < 1) return;

            //double _maxSV = motorsList.Max(q => q.MaxV);
            double _maxSV = motorsList.Min(q => q.MaxV);
            double _minSV = _maxSV * 0.05;

            //if (_syncMode == SyncMode.NoSync) _maxSV = motorsList.Max(q => q.MaxV);
            //else _maxSV = motorsList.Min(q => q.MaxV);

            if (isSenderCue)
                //popupWin = new PopUpWindow(_maxSV * .01, _maxSV, sender, 500, 500, ShowPercButtons.Show); 
                popupWin = new PopUpWindow(_minSV, _maxSV, sender, 500, 500, ShowPercButtons.Show);//20141018
            else
                if (motorsList.All(qq => qq.manEnabled))
                popupWin = new PopUpWindow(_minSV, _maxSV, sender, 500, 500, ShowPercButtons.Show);//20141018
                                                                                                   //popupWin = new PopUpWindow(_maxSV * .01, _maxSV, sender, 500, 500, ShowPercButtons.Show);

        }
        public void GroupSP_Popup(List<MotorLogic> motorsList, TextBlock sender)
        {
            if (PopupAlreadyOpen()) return;

            popupWin = new PopUpWindow(motorsList.Max(t => t.LimNeg), motorsList.Min(t => t.LimPos), sender, 500, 500);
        }
        public void GroupSP_Popup2(double grpMin, double grpMax, TextBlock sender)
        {
            if (PopupAlreadyOpen()) return;

            popupWin = new PopUpWindow(grpMin, grpMax, sender, 500, 500);
        }

        public void GroupSVMV_Popup(List<MotorLogic> motorsList, TextBlock sender, ShowPercButtons showPercButtons)
        {
            if (PopupAlreadyOpen()) return;
            if (motorsList.Count < 1) return;
            double tempV = motorsList.Min(m => m.MaxV);
            popupWin = new PopUpWindow(1, tempV, sender, 500, 500, ShowPercButtons.Show);
            //popupWin = new PopUpWindow(1, 50, sender, 500, 500, ShowPercButtons.Show);

        }
        public void ResetReference(MotorLogic motor)
        {
            PopUpWindowText pwin = new PopUpWindowText(500, 500, "Reset reference", "Do you want to reset reference?\n\nFor authorized personall only!\nWARNING!\nIncorrect reference can lead to unit malfunction or damage.", motor);
            //if (pwin.DialogResult == true && motor.soloSM.state == states.Idle)
            //    motor.soloSM.state = states.ResRef1;
            SoloCommands cmd = new SoloCommands();
            if (pwin.DialogResult == true && motor.soloSM.state == states.Idle)
                cmd.ResetRef(motor);
        }


        bool PopupAlreadyOpen()
        {
            return (App.Current.Windows.Count > 2);//20190301 PROVERITI TODO
        }
        public void PopupCommand(TextBlock sender, bool isString)
        {
            //transform = sender.TransformToAncestor(App.Current.MainWindow);
            //rootPoint = transform.Transform(new Point(0, 0));
            if (!PopupAlreadyOpen())
                popupWin = new PopUpWindow(sender, isString, 500, 500);
        }
        public void ShowTripInfo(string tripTxt)
        {
            if (PopupAlreadyOpen()) return;
            PopUpWindowText win = new PopUpWindowText(500, 500, "Trip info", tripTxt);
        }

    }
}
