﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for LockMotorsGui.xaml
    /// </summary>
    public partial class LockMotorsGui : UserControl
    {
        public LockMotorsGui()
        {
            InitializeComponent();
            //this.DataContext = Sys.lockMotors;
            TimedAction.ExecuteWithDelay(new Action(delegate { this.DataContext = Sys.lockMotors; }), TimeSpan.FromSeconds(4));
        }

        private void time5m_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.SetTime(TimeSpan.FromMinutes(5)); }

        private void time15m_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.SetTime(TimeSpan.FromMinutes(15)); }

        private void time30m_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.SetTime(TimeSpan.FromMinutes(30)); }

        private void time1h_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.SetTime(TimeSpan.FromMinutes(60)); }

        private void time2h_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.SetTime(TimeSpan.FromMinutes(120)); }

        private void time4h_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.SetTime(TimeSpan.FromMinutes(240)); }

        private void StartOverride_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.StartOverride(); }

        private void StopOverride_Click(object sender, RoutedEventArgs e) { Sys.lockMotors.StopOverride(); }
    }
}
