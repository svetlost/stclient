﻿using System;

namespace StClient
{
    /// <summary>
    /// used to filter MOV signal. if a few scans show POSITION is not changing (ie when pushing joy from up to down so speed is 0 at one point)
    /// </summary>
    public class ChangeMonitor
    {
        enum CmStates { stNotChanging, stStartChanging, stChanging, stEndChanging }
        public double value //has to be called each cycle TODO: change this!!!!!!
        {
            get { return _value; }
            set
            {
                _value = value;
                processSM();
                _oldvalue = value;
            }
        }
        double _value, _oldvalue;
        CmStates state = CmStates.stNotChanging;
        //public ChangeMonitor(double value) { }
        public bool Changing { get { return state == CmStates.stChanging || state == CmStates.stEndChanging; } }
        public DateTime timeoutTime { get; set; }
        public double changeTime_sec = 0.3;//todo take value from properties
        public double changeValue_units = 0.2;//minimum delta required to register change. todo take value from properties

        void ResetTimeout(double timeSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromSeconds(timeSeconds); }

        void processSM()
        {
            switch (state)
            {
                case CmStates.stNotChanging:
                    if (_oldvalue != _value)
                    {
                        ResetTimeout(changeTime_sec);
                        state = CmStates.stStartChanging;
                    }
                    else state = CmStates.stNotChanging;
                    break;
                case CmStates.stStartChanging:
                    if ((_oldvalue != _value) && timeoutTime < DateTime.Now) state = CmStates.stChanging;
                    else if (_oldvalue == _value)
                    {
                        ResetTimeout(changeTime_sec);
                        state = CmStates.stEndChanging;
                    }
                    else state = CmStates.stStartChanging;
                    break;
                case CmStates.stChanging:
                    if (_oldvalue == _value)
                    {
                        ResetTimeout(changeTime_sec);
                        state = CmStates.stEndChanging;
                    }
                    else state = CmStates.stChanging;
                    break;

                case CmStates.stEndChanging:
                    if (_oldvalue != _value)
                    {
                        ResetTimeout(changeTime_sec);
                        state = CmStates.stChanging;
                    }
                    else if ((_oldvalue == _value) && timeoutTime < DateTime.Now) state = CmStates.stNotChanging;
                    else state = CmStates.stEndChanging;
                    break;
            }
        }
    }
}
