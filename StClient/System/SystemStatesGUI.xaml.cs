﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


namespace StClient
{
    /// <summary>
    /// Interaction logic for TripMoveUserControl.xaml
    /// </summary>
    public partial class SystemPanel : UserControl
    {
       

        public SystemPanel()
        {          

                InitializeComponent();
                DataContext = SystemPanelLogic.UniqueInstance;           
        }

        private void viewAll_Click(object sender, RoutedEventArgs e)
        {
            MotorsVisibilityStates.ShowAllMotors();
        }

        private void viewNone_Click(object sender, RoutedEventArgs e)
        {
            MotorsVisibilityStates.ShowNoMotors();
        }

        private void viewTripped_Click(object sender, RoutedEventArgs e)
        {
            MotorsVisibilityStates.ShowTrippedMotors();           
        }

        private void LogOff_Click(object sender, RoutedEventArgs e)
        {

            ApplicationStateLogic.Instance.isAppEnabled = false;
            List<MotorLogic> MovingMotorsList = Sys.StaticMotorList.FindAll(t => t.Can == true && t.Brake == false);

            foreach (MotorLogic motor in MovingMotorsList)
            {
                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanStructure.CanCommandStop, 0, 8);
                //  UdpTx.UniqueInstance.SendPdoSbc((byte)motor.CanChannel, (byte)motor.SecBrkControlAddress, 0);
                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanStructure.CanCommandIdle, 50, 2);
            }
        }

        private void GroupAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (MotorLogic motor in Sys.StaticMotorList)
            {
                motor.GroupNo = 0;
            }
        }

        private void GroupNone_Click(object sender, RoutedEventArgs e)
        {
            foreach (MotorLogic motor in Sys.StaticMotorList)
            {
                motor.GroupNo = -1;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

      
    }
}
