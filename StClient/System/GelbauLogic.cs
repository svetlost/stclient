﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace StClient
{
    class GelbauLogic : INotifyPropertyChanged
    {
        static int _gelbauCount;
        public static int gelbauCount
        {
            get { return _gelbauCount; }
            set
            {
                if (_gelbauCount == value) return;
                _gelbauList.Clear();
                for (int i = 1; i <= value; i++) _gelbauList.Add(new GelbauLogic() { ID = i });
                _gelbauCount = value;
            }
        }
        static List<GelbauLogic> _gelbauList = new List<GelbauLogic>();
        public static List<GelbauLogic> gelbauList { get { return _gelbauList; } }
        public int ID { get; private set; }
        public int bitNo { get { return ID - 1; } }

        bool _Trip;
        public bool Trip { get { return _Trip; } set { if (_Trip != value) { _Trip = value; OnNotify("Trip"); } } }

        public string Name { get { return string.Format("EDGE{0}", ID); } }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion
    }
}

