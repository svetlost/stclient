﻿using LogAlertHB;
using StClient.Properties;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
//using StClient.Wagons;
using System.Xml.Serialization;

namespace StClient
{
    public class Sys
    {
        //Unique Instances

        public static TabControl TabControl;
        public static MotorGUI SelectedMotor;
        public static GroupGUI SelectedGroup;
        public static CuesMotorGUI SelectedCuesMotorGUIMotor;
        public static ListBox MotorListbox, GroupListBox, CuesMotorGUIListBox;
        
        public static TextBox receivedTxtBlock;

        //static LISTS
        public static ConcurrentDictionary<int, MotorLogic> StaticMotorList = new ConcurrentDictionary<int, MotorLogic>();
        public static List<byte> StaticMotorListIds = new List<byte>();



        public static List<GroupLogic> StaticGroupList = new List<GroupLogic>();
      
        public static List<DiagItemValues> StaticClusterElectricList = new List<DiagItemValues>();
        public static List<TripXML> StaticListOfTripsLenze9300 = new List<TripXML>();
        public static List<UpDownLogic> UpDownList = new List<UpDownLogic>();
       
       
        public static List<SecondaryBreakControl> SbcList = new List<SecondaryBreakControl>();
        public static List<ServerLink> ServersList = new List<ServerLink>();
        public static List<StJoystick> joystickList;
        public static List<RfidItem> rfidList;
       
        public static List<ILinkCheck> linkCheckDeviceList = new List<ILinkCheck>();
        public static List<Cabinet> cabinetList = new List<Cabinet>();
        public static List<Dio1000> DioList = new List<Dio1000>();
        public static List<DrivePlc> DrivePlcList = new List<DrivePlc>();
        public static List<StServer> StServerList = new List<StServer>();
       
        public static StServer stServer;
        public static JoyManager joyManager;
        public static List<CodeWords> codewords;
       
        static string _pathConfig = Environment.CurrentDirectory + @"\Config\";
        public static string pathConfig { get { return _pathConfig; } set { if (_pathConfig != value) { _pathConfig = value; } } }

       
        
        public static SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQLServerConnectionString);

        //Class Specific
        GroupLogic _tempGroupLogic;
        
        DispatcherTimer LinkIndicator_SendPing = new DispatcherTimer();
        DispatcherTimer LinkIndicator_ResetIndicator = new DispatcherTimer();        
        public DispatcherTimer HoistCPUpdateTimer = new DispatcherTimer();

        //init
        
        XmlUtil<UpDownLogic> xmlUpDown = new XmlUtil<UpDownLogic>();
       
        XmlUtil<DrivePlc> DrivePlcXml = new XmlUtil<DrivePlc>();

        Properties.Settings Sett = Properties.Settings.Default;
        

        public static bool render3D = Properties.Settings.Default.Render3D;
       

        public static string initResult { get; set; }
        public static void AddInitResult(string s) { initResult += s + "\n"; }

        public static string ParseResult(string filename, object list, int itemsImported)
        {
            if (list == null) return "Parsing " + filename + " failed, no records imported.";

            return "Parsing " + filename + " OK, imported " + itemsImported.ToString() + " items";
        }
        public static string ParseResult2<T>(string filename, List<T> list)
        {
            if (list == null) return "Parsing " + filename + " FAILED, no items imported.";

            return "Parsing " + filename + " OK, imported " + list.Count + " items";
        }

        /// <summary>
        /// read configurations from data base
        /// </summary>
        /// <param name="conn"></param>
        public void DBread(SqlConnection conn)
        {
            
            using (conn)
            {

                try
                {
                    conn.Open();

                    //MOTORS
                    using (DataContext db = new DataContext(conn))
                    {

                        Table<MotorLogic> MotorLogic = db.GetTable<MotorLogic>();

                        // Attach the log to show generated SQL.
                        //db.Log = Console.Out;


                        // Query for CodeWords in menu 0
                        IQueryable<MotorLogic> custQuery =
                            from motor in MotorLogic

                            select motor;


                        List<MotorLogic> mll = custQuery.ToList<MotorLogic>().FindAll(q => q.Enabled);
                        mll.ConvertAll(x => (byte)x.MotorID);  /// max 255 motors!!!
                        for (int i = 0; i < mll.Count; i++)
                        {
                            StaticMotorList.TryAdd(mll[i].MotorID, mll[i]);
                            StaticMotorListIds.Add((byte)mll[i].MotorID);
                        }  
                    }

                    //RFID
                    using (DataContext db = new DataContext(conn))
                    {

                        Table<RfidItem> RfidItem = db.GetTable<RfidItem>();

                        // Attach the log to show generated SQL.
                        //db.Log = Console.Out;


                        // Query for CodeWords in menu 0
                        IQueryable<RfidItem> custQuery =
                            from rfid in RfidItem
                            select rfid;

                        rfidList = custQuery.ToList<RfidItem>();
                    }

                    //HOIST
                    using (DataContext db = new DataContext(conn))
                    {

                        Table<UpDownLogic> hoists = db.GetTable<UpDownLogic>();


                        // Attach the log to show generated SQL.
                        db.Log = Console.Out;
                        Console.WriteLine("\n\n");

                        // Query for CodeWords in menu 0
                        IQueryable<UpDownLogic> custQuery =
                            from hoist in hoists

                            select hoist;
                        
                        UpDownList = custQuery.ToList<UpDownLogic>();
                    }


                    //JOYSTICK
                    using (DataContext db = new DataContext(conn))
                    {

                        Table<StJoystick> joysticks = db.GetTable<StJoystick>();


                        // Attach the log to show generated SQL.
                        db.Log = Console.Out;
                        Console.WriteLine("\n\n");

                        // Query for CodeWords in menu 0
                        IQueryable<StJoystick> custQuery =
                            from joy in joysticks

                            select joy;

                        joystickList = custQuery.ToList<StJoystick>();
                        if (joystickList.Count > 0)
                        {
                            joyManager = new JoyManager(joystickList, Settings.Default.JoystickType);
                        }

                    }

                                   
                    //CODE WORDS
                    using (DataContext db = new DataContext(conn))
                    {

                        Table<CodeWords> codeWords = db.GetTable<CodeWords>();



                        // Query for CodeWords in menu 0
                        IQueryable<CodeWords> custQuery =
                            from code in codeWords
                            orderby code.CodeOrderOnRegulator ascending
                            select code;

                        codewords = custQuery.ToList();
                    }

                    //PASSWORD
                    using (SqlCommand command = new SqlCommand("SELECT * FROM password", conn))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {                                    
                                    MainWindow.password = reader.GetString(1);
                                }
                            }
                            else
                            {
                                Trace.WriteLine("No rows found.");
                            }
                        }
                    }

                }
                
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    
                }
                finally
                {
                    if (conn.State != ConnectionState.Closed) { conn.Close(); }

                }
            }
        }

        public Sys()
        {
            if (!Directory.Exists(pathConfig))
                Directory.CreateDirectory(pathConfig);

            //CREATE GROUPS
            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
            {
                _tempGroupLogic = new GroupLogic(i);
                StaticGroupList.Add(_tempGroupLogic);
            }

            StaticGroupList.Add(new GroupLogic(1000));

           


            //ALL CONFIGS FROM DB, motors,hoist,code words, joystick, rfid...
            DBread(conn);


            //set sp to cp
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                if (Sys.StaticMotorList != null)
                {
                    foreach (var obj in Sys.StaticMotorList)
                    {
                        if (obj.Value.Net) { obj.Value.SP = obj.Value.CP; }
                    }

                   
                }
            }), TimeSpan.FromSeconds(4));


            ServersList.Add(new ServerLink("Server MAIN", Properties.Settings.Default.ServerMainIP));

            if (StaticMotorList != null && StaticMotorList.Count > 0)
            {

                AddInitResult("Motors parsed:");
                foreach (var mm in StaticMotorList) AddInitResult(string.Format("Motor, {0}, id={1}, lim-={2} lim+={3} adr={4}, maxV={5}", mm.Value.Title, mm.Value.MotorID, mm.Value.LimNeg, mm.Value.LimPos, mm.Value.IpAddress, mm.Value.MaxV));

                foreach (var motor in StaticMotorList)
                {
                    motor.Value.SetMotorDirection();
                    CueLogic.UniqueInstance.SubscribeMotorsToCue(motor.Value);
                    motor.Value.SubscribeToCueLogic(CueLogic.UniqueInstance);

                   

                    int CalculatedCanAddress = 0;
                    if (motor.Value.IpAddress.Contains("."))
                    {
                        CalculatedCanAddress = Int32.Parse(motor.Value.IpAddress.Split('.').Last());
                    }
                    else
                    {
                        CalculatedCanAddress = Int32.Parse(motor.Value.IpAddress);
                    }



                    if (StClient.Properties.Settings.Default.Modeleven % 11 == 0) ApplicationStateLogic.Instance.isAppEnabled = true;



                    if (!VisibilityStatesLogic.UniqueInstance.AllClusters.Contains(motor.Value.ClusterLogical))
                        VisibilityStatesLogic.UniqueInstance.AllClusters.Add(motor.Value.ClusterLogical);
                }

                VisibilityStatesLogic.UniqueInstance.FillOnStartup();

                //CLUSTER ELECTRIC
                foreach (byte _ClusterID in StaticMotorList.Select(q => q.Value.ClusterElectric).Distinct())
                {
                    DiagItemValues item = new DiagItemValues
                    {
                        Cluster = _ClusterID,
                        Text = "Cabinet " + _ClusterID.ToString(),
                        Units = "A"
                    };
                    StaticClusterElectricList.Add(item);
                }

                // TimedAction.ExecuteWithDelay(new Action(delegate { foreach (var m in Sys.StaticMotorList)m.MV = m.SV = 0.5 * m.MaxV; }), TimeSpan.FromSeconds(3));
                TimedAction.ExecuteWithDelay(new Action(delegate { foreach (var m in Sys.StaticMotorList) m.Value.MV = m.Value.SV = m.Value.DefaultMV; }), TimeSpan.FromSeconds(3));


                //at init end write log string
                Log.Write(initResult, EventLogEntryType.Information);
            }
        }

       
    }
   
    public class TripXML
    {
        public int ID { get; set; }
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
        public string CauseRemedy { get; set; }
    }

    public class ServerLink : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
        int counter = 0;
        bool _LinkOk = false;
        public bool LinkOk { get { return _LinkOk; } set { if (_LinkOk != value) { _LinkOk = value; OnNotify("LinkOk"); } } }
        public void IncrementCounter() { counter++; }
        public void CheckLink() { LinkOk = counter > 0; counter = 0; }

        public ServerLink(string _name, string _IpAdress)
        {
            Name = _name;
            IpAddress = _IpAdress;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }

}



