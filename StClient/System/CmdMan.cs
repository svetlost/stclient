﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls.Primitives;

namespace StClient
{
    /// <summary>
    /// ova klasa treba da detektuje promenu stanja komandnog dugmeta. problem je sto moze da se koristi samo za jedno dugme,
    /// a ista komanda moze da se poziva sa vise mesta
    /// </summary>
    public class CmdMan
    {
        List<ButtonBase> buttonCmdInstancesList = new List<ButtonBase>();
        static Dictionary<cmdType, CmdMan> _cmdTypesList = new Dictionary<cmdType, CmdMan>();

        static CmdMan()
        {
            //foreach (var t in Enum.GetValues(typeof(cmdType)))
            //    _cmdTypesList.Add((cmdType)t, new CmdMan());
            //_timer.Interval = TimeSpan.FromMilliseconds(100);
            //_timer.Tick += _timer_Tick;
            //_timer.Start();
        }

        //public EdgeValue(ref bool boolValue)
        public enum cmdType
        {
            soloAuto, soloStop, soloManRelease, soloManPos, soloManNeg, soloResRef, soloResTrip,
            grpAuto, grpStop, grpManRelease, grpJoyRelease, grpManPos, grpManNeg, grpResTrip,
            cueAuto, cueStop,
            stopAll
        }
        //public EdgeValue(cmdType cmd)
        //{
        //    _cmdType = cmd;
        //    _list.Add(this);
        //}
        //public cmdType _cmdType { get; private set; }
        //public EdgeValue(ButtonBase _button)
        //{
        //    //if (_list == null) _list = new List<EdgeValue>();
        //    //if (_list == null) _list.Add(this);
        //    button = _button;
        //    _list.Add(this);
        //}
        public static void AddButtonInstance(cmdType _cmdType, ButtonBase _button)
        {
            if (_cmdTypesList.ContainsKey(_cmdType) == false) _cmdTypesList.Add(_cmdType, new CmdMan());

            if (_cmdTypesList[_cmdType].buttonCmdInstancesList.Exists(p => p == _button)) return;
            _cmdTypesList[_cmdType].buttonCmdInstancesList.Add(_button);
        }

        public bool _in { get; private set; }
        public bool _out { get; private set; }
        public bool _rEdge { get; private set; }
        public bool _fEdge { get; private set; }
        public bool _anyEdge { get; private set; }
        bool previousIn;
        public static void _process()
        {
            foreach (var t in _cmdTypesList.Where(q => q.Value.buttonCmdInstancesList.Count > 0))
            {
                var temp = t.Value;

                temp._in = temp.buttonCmdInstancesList.Any(w => w.IsPressed == true);
                temp._out = temp._in;
                temp._rEdge = (temp.previousIn != temp._in && temp._in == true);
                temp._fEdge = (temp.previousIn != temp._in && temp._in == false);
                temp._anyEdge = temp._rEdge || temp._fEdge;
                temp.previousIn = temp._in;
            }
        }
        //static DispatcherTimer _timer = new DispatcherTimer();
        //ButtonBase button;
        //private static void _timer_Tick(object sender, EventArgs e)
        //this has to be processed once in main loop
    }
    //public class Wrapper<T>
    //{
    //    public T Value { get; set; }

    //    public Wrapper(T value)
    //    {
    //        Value = value;
    //    }
    //}
}
