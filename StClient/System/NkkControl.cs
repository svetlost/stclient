﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Threading;
using LogAlertHB;

namespace StClient
{
    public class NkkControl
    {

        //public int NkkSwtichCount = Properties.Settings.Default.NkkSwitchCount;
        public static Dispatcher windowDispacher { get; set; }

        private static SerialPort _serialPort = new SerialPort();
        private int _baudRate = 9600;
        private int _dataBits = 8;
        private Handshake _handshake = Handshake.None;
        private Parity _parity = Parity.None;
        private string _portName = "COM1";
        private StopBits _stopBits = StopBits.One;

        public NkkControl(Dispatcher _windowDispacher, string _COM_Port)
        {
            windowDispacher = _windowDispacher;
            _portName = _COM_Port;
            if (OpenComPort() == false)
            {
                AlertLogic.Add(string.Format("Nkk ComPort {0} Busy", _COM_Port));
                Log.Write(string.Format("Nkk ComPort {0} Busy", _COM_Port), EventLogEntryType.Error);
            }
            else
                CommInitCheck();

            //for (int i = 0; i < NkkSwtichCount; i++) NkkButtons[i] = new NkkButton();
        }

        System.Threading.Thread thread;
        void TestLedAndText()
        {
            for (int i = 0; i < 7; i++)
            {
                windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { Write_4N_4N_Chars(i, i.ToString()); });
                Thread.Sleep(30);
            }

            foreach (NkkColors col in (NkkColors[])Enum.GetValues(typeof(NkkColors)))
            {
                for (int i = 0; i < 7; i++)
                {
                    windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { SetColorToButton(col, i); });
                    Thread.Sleep(30);
                }
                Thread.Sleep(300);
            }
            for (int i = 0; i < 7; i++)
            {
                windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate()
                {
                    Write_4N_4N_Chars(i, "");
                    SetColorToButton(NkkColors.black, i);
                });
                Thread.Sleep(30);
            }
        }

        public void TestTextAndColor()
        {
            thread = new System.Threading.Thread(new System.Threading.ThreadStart(TestLedAndText));
            thread.IsBackground = true;
            thread.Start();
        }

        static void send(string s)
        {
            if (!_serialPort.IsOpen) return;
            byte[] bb = Encoding.ASCII.GetBytes(s);
            _serialPort.Write(bb, 0, bb.Length);
        }

        static void send(byte[] b)
        {
            if (!_serialPort.IsOpen) return;
            _serialPort.Write(b, 0, b.Length);
        }

        public static bool CommCheckOk { get; private set; }

        public int BaudRate { get { return _baudRate; } set { _baudRate = value; } }
        public int DataBits { get { return _dataBits; } set { _dataBits = value; } }
        public Handshake Handshake { get { return _handshake; } set { _handshake = value; } }
        public Parity Parity { get { return _parity; } set { _parity = value; } }
        public string PortName { get { return _portName; } set { _portName = value; } }
        public bool OpenComPort()
        {
            try
            {
                _serialPort.BaudRate = _baudRate;
                _serialPort.DataBits = _dataBits;
                _serialPort.Handshake = _handshake;
                _serialPort.Parity = _parity;
                _serialPort.PortName = _portName;
                _serialPort.StopBits = _stopBits;
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                _serialPort.Open();
            }
            catch { return false; }
            return true;
        }

        //public static byte[] intToAsciiOLD(int i)
        //{
        //    string hex = i.ToString("X");
        //    if (hex.Length < 2) hex = "0" + hex;
        //    return Encoding.ASCII.GetBytes(hex);
        //}

        public static byte[] intToAscii(int i) { return Encoding.ASCII.GetBytes(i.ToString("X2")); }

        public static void BrightnessIncr() { send(new byte[] { 0x26, 0x49, 0x58 }); }
        public static void BrightnessDecr() { send(new byte[] { 0x26, 0x4A, 0x58 }); }

        public enum NkkColors { red = 17, blue = 68, green = 34, cyan = 102, magenta = 85, white = 119, yellow = 51, black = 0 }

        public static void Write_6N_6N_6N_Chars(int buttonNo, string txt) { WriteChars(buttonNo, txt, 0x38, 18); }

        public static void Write_4N_4N_Chars(int buttonNo, string txt) { WriteChars(buttonNo, txt, 0x34, 8); }

        public static void Write_4I_4N_Chars(int buttonNo, string txt) { WriteChars(buttonNo, txt, 0x36, 8); }

        static void WriteChars(int buttonNo, string txt, byte _byCommand, byte _length)
        {
            byte[] byAddress = intToAscii(buttonNo);
            byte[] byTxt = Encoding.ASCII.GetBytes(normalizeToLength(txt, _length));
            byte[] byCommand = new byte[] { 0x27, byAddress[0], byAddress[1], 0x30, _byCommand };
            byte[] byToSend = Combine(byCommand, byTxt);
            //Debug.WriteLine(Encoding.Default.GetString(byToSend));
            send(byToSend);
        }

        public static void SetColorToButton(NkkColors color, int buttonNo)
        {
            byte[] arg1, arg2;
            arg1 = intToAscii(buttonNo);
            arg2 = intToAscii((int)color);
            byte[] ba = new byte[] { 0x2f, 0x30, arg1[0], arg1[1], arg2[0], arg2[1] };
            //Debug.WriteLine("{0}{1}{2}{3}{4}{5}", ba[0], ba[1], ba[2], ba[3], ba[4], ba[5]);
            send(ba);
        }

        public static void loadImageToButton(int imageAddress, int buttonNo)
        {
            byte[] arg1, arg2;
            arg1 = intToAscii(buttonNo);
            arg2 = intToAscii(imageAddress);
            byte[] ba = new byte[] { 0x2e, 0x30, arg1[0], arg1[1], arg2[0], arg2[1] };
            //Debug.WriteLine("{0:X}{1:X}{2:X}{3:X}{4:X}{5:X}", ba[0], ba[1], ba[2], ba[3], ba[4], ba[5]);
            //Debug.WriteLine(Encoding.Default.GetString(ba));
            send(ba);
        }

        public static void CommInitCheck() { CommCheckOk = false; send(new byte[] { 0x01 }); }
        //public static void CommInitCheck()
        //{
        //    CommCheckOk = false; 
        //    send(new byte[] { 0x01 });

        //    System.Threading.Thread threadComCheck;
        //    threadComCheck = new System.Threading.Thread(new System.Threading.ThreadStart((Action)delegate() { }));
        //    threadComCheck.IsBackground = true;
        //    threadComCheck.Start();


        //}

        static string normalizeToLength(string s, int length) { return s.Length > length ? s.Substring(0, length) : s.PadRight(length); }
        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
        StringBuilder receiveBuffer = new StringBuilder();

        void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            bool clearBuffer = false;
            int bufSize = 20;
            Byte[] dataBuffer = new Byte[bufSize];
            int count = _serialPort.Read(dataBuffer, 0, bufSize);
            for (int i = 0; i < count; i++) receiveBuffer.Append(String.Format("{0:X}", Convert.ToInt32(dataBuffer[i])));
            //Debug.WriteLine(receiveBuffer.ToString());

            var regex = new Regex(@"8[1-6]|B[1-6]|61");
            Match match;
            do
            {
                match = regex.Match(receiveBuffer.ToString());
                if (match.Success)
                {
                    //Console.WriteLine(match.Captures[0].Value);
                    receiveBuffer.Remove(match.Captures[0].Index, match.Captures[0].Length);
                    int _button = Convert.ToInt32(match.Captures[0].Value[1]) - 48;
                    //int _button = Convert.ToInt32(match.Captures[0].Value[1]) - 49;
                    NkkButton temp = NkkButtons.Find(w => w.ButtonNo == _button);
                    if (temp == null) continue;

                    if (match.Captures[0].Value[0] == 56 && ApplicationStateLogic.Instance.isAppEnabled)
                    {
                        //Debug.WriteLine(string.Format("button {0} pressed", _button));
                        Log.Write(String.Format("Nkk: button {0} pressed ({1})", _button,temp.Text), EventLogEntryType.Information);
                        if (temp.OnSwitchPress != null)
                            windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { temp.OnSwitchPress(); });
                    }
                    else if (match.Captures[0].Value[0] == 66 && ApplicationStateLogic.Instance.isAppEnabled)
                    {
                        //Debug.WriteLine(string.Format("button {0} released", _button));
                        Log.Write(String.Format("Nkk: button {0} released ({1})", _button, temp.Text), EventLogEntryType.Information);
                        if (temp.OnSwitchRelease != null)
                            windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { temp.OnSwitchRelease(); });
                    }
                    else if (match.Captures[0].Value[0] == 54 && match.Captures[0].Value[1] == 49) CommCheckOk = true;
                    else Debug.WriteLine("???");
                    clearBuffer = true;
                }
            } while (match.Success);

            if (clearBuffer) receiveBuffer.Clear();

        }

        public class NkkButton
        {
            public Action OnSwitchPress { get; set; }
            public Action OnSwitchRelease { get; set; }
            public string Text { get; set; }
            public NkkColors Color { get; set; }
            public int ButtonNo { get; set; }
        }

        public static List<NkkButton> NkkButtons = new List<NkkButton>();

        public static void Init_4N_4N()
        {
            foreach (var n in NkkButtons)
            {
                NkkControl.Write_4N_4N_Chars(n.ButtonNo, n.Text);
                NkkControl.SetColorToButton(n.Color, n.ButtonNo);
            }
        }

        public static void ClearAll()
        {
            foreach (var n in NkkButtons)
            {
                NkkControl.Write_4N_4N_Chars(n.ButtonNo, " ");
                NkkControl.SetColorToButton(NkkColors.black, n.ButtonNo);
                n.OnSwitchPress = n.OnSwitchRelease = null;
            }
            //for (int i = 0; i <= Properties.Settings.Default.NkkSwitchCount; i++)
            //{
            //    if (NkkButtons[i] == null) continue;
            //    NkkControl.Write_4N_4N_Chars(i, " ");
            //    NkkControl.SetColorToButton(NkkColors.black, i);
            //    NkkButtons[i].OnSwitchPress = NkkButtons[i].OnSwitchRelease = null;
            //}
        }

        #region visak
        #endregion


    }

}
