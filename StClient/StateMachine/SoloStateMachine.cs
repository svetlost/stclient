﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;

namespace StClient
{
    /// <summary>
    /// imamo dve vrste manuelnog upravljanja joystik ciju brzinu posle zavrsetka kretanja treba da postavimo na 0 i mv od grupe ili singla gde se to ne radi
    /// 
    /// </summary>
    public class SoloStateMachine
    {
        Stopwatch stopwatch = new Stopwatch();
        MotorLogic motor;


        public enum AmcRef { stop0 = 0, PositionAbs1 = 1, Speed2 = 2, Home5 = 5 }
        string[] commands;
        List<double> values = new List<double>();
        public SoloStateMachine(MotorLogic _mot)
        {
            motor = _mot;


        }
        

        states _state, _prevState;
        public states state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _prevState = _state;
                    _state = value;
                }
            }
        }

        Properties.Settings Sett = Properties.Settings.Default;
        
        bool mouseIsDown = false;
        public void ProcessStateMachine()
        {
           
            // Trace.WriteLine(string.Format("{0:hh:mm:ss.fff} --- SOLO STATE :{1}, IP ADDRESS: {2}", DateTime.Now, state, motor.IpAddress.ToString()));

            switch (state)
            {
                // Basic state of all motors, reacts to events AUTO, MANual reset Reference and TRIP
                case states.Idle:

                    stopwatch.Restart();    

                    motor.SbcLock = MotorBrake.Locked;

                    //  AUTO is clicked on GUI, set temporary position to desired position (SP)
                    //set temporary velocity to desired velocity (SV)
                    // SP and SV set on motor GUI                    
                    if (motor.cmdAuto)
                    {
                        motor.TP = motor.SP * motor.SpCoeficient;
                        motor.TV = motor.SV * motor.MvSvCoeficient;
                        // if current position and temporary positions are different
                        if (Math.Abs(motor.TP - motor.CP * motor.SpCoeficient) > 0.01)
                            state = states.AutoStarting1;
                    }
                    else if (motor.cmdRstRef) state = states.ResRef1;
                    else if (motor.cmdManStart)
                    {
                        motor.TV = motor.MV * motor.MvSvCoeficient;
                        state = states.ManStarted;
                    }
                    else if (motor.Trip) state = states.Error1;
                    break;

                #region MANUAL

                case states.ManStarted:
                    AddTimeout(5);
                    motor.SbcLock = MotorBrake.Unlocked;

                    List<double> values = new List<double>();
                    commands = new string[] { "Run", "Set Position", "Amc Selected Reference", "DriveEnable", "Temp Velocity", "amc absolute MAX speed" };

                    values.Add((double)1);
                    values.Add((double)motor.CP);
                    values.Add((double)0);
                    values.Add((double)1);
                    values.Add((double)motor.TV);
                    values.Add((double)Math.Abs(motor.TV));

                    UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), motor.MotorIdAsArray); ;
                   
                    /*
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 1, motor.MotorIdAsArray); //RUN enable
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Set Position", motor.CP, motor.MotorIdAsArray); //set position sending current because we will go to upper or lower limit
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", 0, motor.MotorIdAsArray); //amc ref select = stop         
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("DriveEnable", 1, motor.MotorIdAsArray); //drive enable     
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Temp Velocity", motor.TV, motor.MotorIdAsArray); //amc sv mv
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("amc absolute MAX speed", Math.Abs(motor.TV), motor.MotorIdAsArray); //amc sv,mv MAX speed
                    //Trace.WriteLine($"ManStarted: {stopwatch.ElapsedMilliseconds.ToString()} ms --- CP: {motor.CP} --- CV: {motor.CV} ");
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, man started" + " ms ---  MOTOR IP:" + motor.IpAddress, DateTime.Now));
                   
                    */
                    state = states.ManStarted2;


                    break;

                case states.ManStarted2:

                    //Trace.WriteLine("ManStarted2: " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                    if (motor.Moving)
                    {
                        //motor sometimes moves 1, with 0 as CV, it prevents it from stopping
                        if (Math.Abs(motor.CP - motor.previousCP) < 2 && motor.previousCV == 0 && motor.CV == 0)
                        {
                            Trace.WriteLine("---- !!!~!~ jog moving detected !!!! ----");
                        }
                    }

                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, manstarted2" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));

                    //Trace.WriteLine("ManStarted2: " + stopwatch.ElapsedMilliseconds.ToString() + " ms  --- timeout:  " + timeoutTime.ToString() + "  now: " + DateTime.Now.ToString());
                    //Trace.WriteLine("ManStarted2  timeoutTime: " + timeoutTime.ToString() + "  NOW:  " + DateTime.Now.ToString());
                    if (timeoutTime < DateTime.Now) state = states.Stopping2;
                    else if (motor.cmdDown) state = states.ManNeg;
                    else if (motor.cmdUp) state = states.ManPos;
                    else state = states.ManStarted2;
                    break;



                case states.ManPos:
                   
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, ManPos" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));

                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Set Position", motor.LimPos * motor.LimitCoeficient, motor.MotorIdAsArray); //set position
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.PositionAbs1, motor.MotorIdAsArray); //amc ref select 
                    state = states.ManPos2;
                    break;

                case states.ManPos2:
                  
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, ManPos2" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));
                    mouseIsDown = System.Windows.Input.Mouse.LeftButton == MouseButtonState.Pressed;
                    if (motor.cmdUp == false || mouseIsDown == false)
                    {
                        Trace.WriteLine("!!!! BBBBBBB  ManPos - pusten: " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                        state = states.ManStarted2;
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray);
                      
                        AddTimeout(5);
                    }
                    break;

                case states.ManNeg:
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, ManNeg" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));

                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Set Position", motor.LimNeg * motor.LimitCoeficient, motor.MotorIdAsArray); //set position
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.PositionAbs1, motor.MotorIdAsArray); //amc ref select
                    state = states.ManNeg2;
                    break;

                case states.ManNeg2:
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, ManNeg2" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));
                    mouseIsDown = System.Windows.Input.Mouse.LeftButton == MouseButtonState.Pressed;
                    if (motor.cmdDown == false || mouseIsDown == false)
                    {
                        Trace.WriteLine(" !!!! BBBBBBB ManNeg - pusten : " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                        state = states.ManStarted2;
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray);
                      
                        AddTimeout(5);
                    }
                    break;
                #endregion

                #region AUTO

                case states.AutoStarting1:
                    Trace.WriteLine("AutoStarting1: " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                    motor.SbcLock = MotorBrake.Unlocked;

                    commands = new string[] { "DriveEnable", "Temp Velocity", "amc absolute MAX speed", "Set Position", "AmcEnable", "Run" };
                    values = new List<double>();

                    values.Add(1);
                    values.Add(motor.TV);
                    values.Add(Math.Abs(motor.TV));
                    values.Add(motor.TP);
                    values.Add(1);
                    values.Add(1);

                    UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), motor.MotorIdAsArray);
                    /*
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("DriveEnable", 1, motor.MotorIdAsArray); //drive enable                   
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Temp Velocity", motor.TV, motor.MotorIdAsArray); //amc sv mv
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("amc absolute MAX speed", Math.Abs(motor.TV), motor.MotorIdAsArray); //amc sv,mv MAX speed
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Set Position", motor.TP, motor.MotorIdAsArray); //set position
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("AmcEnable", 1, motor.MotorIdAsArray); //amc enable
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 1, motor.MotorIdAsArray); //RUN enable
                    */
                    AddTimeout(4);//if not started in 3s => timeout=>err
                    state = states.DelayAutoStart;
                    break;
                // todo: namestiti

                case states.DelayAutoStart:
                    Trace.WriteLine("DelayAutoStart: " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                    TimedAction.ExecuteWithDelay(new Action(delegate
                    {
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.PositionAbs1, motor.MotorIdAsArray); //amc ref select
                        state = states.AutoStarting2;
                    }), TimeSpan.FromMilliseconds(500));

                    //state = states.AutoStarting2;
                    break;



                case states.AutoStarting2:
                    Trace.WriteLine("AutoStarting2: " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                    if (timeoutTime < DateTime.Now)
                    {
                        AddTimeout(2);
                        state = states.Error1;
                    }
                    else if (motor.Moving == true) state = states.Started;//todo:dodati timeout?
                    else state = states.AutoStarting2;

                    break;



                case states.Started:
                    Trace.WriteLine("Started: " + stopwatch.ElapsedMilliseconds.ToString() + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV);
                    motor.SbcLock = MotorBrake.Unlocked; //???

                    if (motor.Moving)//todo 20140405 prepravljeno zato sto je zaustavljao stimung pre vremena (tj odma na pocetku)
                    {
                        state = states.Started;
                        //motor.StoppingCounter = 0;
                    }
                    else state = states.Stopping1;

                    break;

                #endregion

                #region STOP
                case states.Stopping1:
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, Stopping1" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));
                    motor.SbcLock = MotorBrake.Unlocked; // this is for man or something ???                 
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select 
                    state = states.Stopping2;
                    break;


                case states.Stopping2:
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, Stopping2" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));
                    if (motor.Moving == false || (timeoutTime < DateTime.Now))
                    {
                        state = states.Stopped;
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 0, motor.MotorIdAsArray); //RUN disable
                        AddTimeout(1);
                    }
                    else state = states.Stopping2;
                    break;


                case states.Stopped:
                    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, Stopped" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));

                    if (timeoutTime < DateTime.Now)
                    {
                        motor.SbcLock = MotorBrake.Locked;
                        state = states.Idle;
                        AddTimeout(1); //????
                    }
                    else
                    {
                        state = states.Stopped;
                    }                                                                            //    state = states.ResRef3;

                    break;


                #endregion

                #region RESET REFERENCE

                case states.ResRef1:
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Save", 1001, motor.MotorIdAsArray); //amc ref select
                    state = states.ResRef2;
                    break;
                case states.ResRef2:
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Drive Reset", 1, motor.MotorIdAsArray); //amc ref select
                    state = states.ResRef3;

                    break;
                case states.ResRef3:
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Drive Reset", 0, motor.MotorIdAsArray); //amc ref select
                    state = states.Idle;
                    motor.cmdRstRef = false;
                    break;

                #endregion

                #region ERROR
                case states.Error1:

                    Trace.WriteLine("brake:" + motor.Brake);
                    if (motor.Brake == false) state = states.Error3;
                    else
                    {
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select 
                        state = states.Error2;
                    }
                    break;
                case states.Error2:
                    if ((motor.Brake == false) || (timeoutTime < DateTime.Now))
                    {
                        // canModbus.Error2();//??
                        state = states.Error3;
                    }
                    else state = states.Error2;
                    break;
                case states.Error3:
                    if (motor.SbcLock != MotorBrake.Locked)
                        motor.SbcLock = MotorBrake.Locked;

                    if (motor.cmdRstTrip) tripReset();
                    //UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(motor, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43);

                    if (motor.Trip == false) state = states.Idle;
                    else state = states.Error3;
                    break;

                    #endregion

            }

            //if (motor.MotorID == 1)
            //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} MOTOR: {1}, STATE: {2}, SBC:{3}", DateTime.Now, motor.MotorID, state, motor.SbcLock));

            if (motor.cmdStop) stopMotor();


        }
        public void cueAuto(CueItem cueItem)
        {
            string Message = "";
            motor.TP = cueItem.SP * motor.SpCoeficient;
            motor.TV = cueItem.SV * motor.MvSvCoeficient; 
            if ((Math.Abs(motor.TP - motor.CP * motor.SpCoeficient) > Properties.Settings.Default.PositionTollerance) &&
                motor.soloSM.state == states.Idle)
            {
                motor.soloSM.state = states.AutoStarting1;
                Message += string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};",
                    motor.Title, motor.MotorID, motor.TP, motor.TV, motor.CP);
            }
            Log.Write("SM(CueStart). " + Message, EventLogEntryType.Information);
        }
        public void tripOccured()
        {
            //canModbus.TripOccuredCommands();
            if (motor.GroupNo == -1 && !(state == states.Error1 || state == states.Error2 || state == states.Error3))
              state = states.Error1;
        }
        public void tripReset()
        {

            //for manual trips in over speed
            motor.cmdUp = false;
            motor.cmdDown = false;

            UdpTx.UniqueInstance.ModBusSigleCommandWrite("User Trip", 100, motor.MotorIdAsArray); //amc home switch       

           // UdpTx.UniqueInstance.ModBusSigleCommandWrite("Drive Reset", 1, motor.MotorIdAsArray); //amc ref select

         //   UdpTx.UniqueInstance.ModBusSigleCommandWrite("Drive Reset", 0, motor.MotorIdAsArray); //amc ref select
        }
        public void stopMotor()
        {
            if (!(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Stopping1;
        }
        public DateTime timeoutTime { get; set; }

        void AddTimeout(double timeSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromSeconds(timeSeconds); }


    }



}
