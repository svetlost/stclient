﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient.StateMachine
{
   public  interface ISoloStates
    {
       
       
        void ManStarted();
        void ManPos();
        void ManNeg();
        void ManButtReleased();
        void Autostarting1();
        void Stopping1();
        void Stopped();
        void ResRef1();
        void Error1();
        void Error2();
        void tripReset();

        void TripOccuredCommands();

        states ManNegState();
        states ManPosState();
    }
}
