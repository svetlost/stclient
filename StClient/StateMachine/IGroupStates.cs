﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient.StateMachine
{
    public interface IGroupStates
    {
         void JoyStarting(MotorLogic motor);

        void JoyButtReleased(MotorLogic motor);

        void JoyButtPressed(MotorLogic motor, bool but1Pressed);
    }
}
