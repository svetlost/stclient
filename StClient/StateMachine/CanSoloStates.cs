﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient.StateMachine
{
    class CanSoloStates :ISoloStates
    {

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;
        MotorLogic motor;

       public CanSoloStates(MotorLogic _mot)
        {
            motor = _mot;
            
        }

        public void ManStarted()
        {
            UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.MvIndex, CanSett.MvSubIndex, motor.TV, TimeSpan.FromMilliseconds(0));           
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        }

        public void ManPos()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.PosDirection, 0, 8);
        }

        public void ManNeg()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.NegDirection, 0, 8);
        }

        public void ManButtReleased()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        }

        public void Autostarting1()
        {
            UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.SpIndex, CanSett.SpSubIndex, motor.TP, TimeSpan.FromMilliseconds(0));
            UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.SvIndex, CanSett.SvSubIndex, motor.TV, TimeSpan.FromMilliseconds(CanSett.SdoProcTime));

            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandAuto, CanSett.SdoProcTime * 5, 8);
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, CanSett.SdoProcTime * 22, 8);
        }

        public void Stopping1()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        }

        public void Stopped()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, CanSett.SdoProcTime, 8);
        }

        public void ResRef1()
        {
            UdpTx.UniqueInstance.SendPDO_ResRef(motor.MotorID, 64, 1000, 8);//20170903 res ref terazije. canin2 bit8=256??? a ne bit6=64
            UdpTx.UniqueInstance.SendPDO_ResRef(motor.MotorID, CanSett.CanCommandIdle, 4000, 8);//4000
        }

        public void Error1()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        }
        public void Error2()
        {
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 0, 8);
        }
        public void tripReset()
        {
            UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(motor, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43);
        }

        public void TripOccuredCommands()
        {
            UdpTx.UniqueInstance.SendSDOReadRQ(motor, 168, 0, TimeSpan.FromMilliseconds(0));
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);//send stop and idle (reset 9300)
            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 500, 8);//send stop and idle (reset 9300)
            //UdpTx.UniqueInstance.SendPDO_ResRef(MotorID, CanSett.CanCommandStop, 0, 2);//send stop and idle (reset 9300)
            //UdpTx.UniqueInstance.SendPDO_ResRef(MotorID, CanSett.CanCommandIdle, 100, 2);//send stop and idle (reset 9300)
            //UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 168, 1, SdoPriority.lo);
        }

        public states ManNegState() {return  states.ManStarted; }
        public states ManPosState() { return states.ManStarted; }



    }
}
