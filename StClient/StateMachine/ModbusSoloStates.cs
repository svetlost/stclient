﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient.StateMachine
{
    class ModbusSoloStates : ISoloStates
    {
        MotorLogic motor;
        public string address;
       
        public enum AmcRef { stop0 = 0, PositionAbs1 = 1, Speed2 = 2, Home5 = 5 }

        public ModbusSoloStates(MotorLogic _mot)
        {
            motor = _mot;
            address = motor.IpAddress;
        }
        
        public void ManStarted() {           
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("06.034", 1, motor.MotorIdAsArray); //RUN enable
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.007", 0, motor.MotorIdAsArray); //amc ref select = stop         
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("06.015", 1, motor.MotorIdAsArray); //drive enable     
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.006", motor.MV,motor.MotorIdAsArray); //amc sv mv
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("38.003", Math.Abs(motor.MV), motor.MotorIdAsArray); //amc sv,mv MAX speed
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("41.001", 1, motor.MotorIdAsArray); //amc enable
        }
        public void ManPos() {           
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34_003", motor.LimPos * motor.LimitCoeficient, motor.MotorIdAsArray); //set position
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.007", (ushort)AmcRef.PositionAbs1,motor.MotorIdAsArray); //amc ref select 
        }
        public void ManNeg() {            
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34_003", motor.LimNeg*motor.LimitCoeficient, motor.MotorIdAsArray); //set position
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.007", (ushort)AmcRef.PositionAbs1,motor.MotorIdAsArray); //amc ref select
        }
        public void ManButtReleased() {
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.006", 0, motor.MotorIdAsArray); //amc sv mv
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("38.003", 0, motor.MotorIdAsArray); //amc sv,mv MAX speed
        }
        public void Autostarting1() {
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("06.015", 1, motor.MotorIdAsArray); //drive enable       
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.007", (ushort)AmcRef.PositionAbs1, motor.MotorIdAsArray); //amc ref select 
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.006", motor.TP, motor.MotorIdAsArray); //amc sv mv
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("38.003", Math.Abs(motor.TP), motor.MotorIdAsArray); //amc sv,mv MAX speed
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34_003", motor.TP, motor.MotorIdAsArray); //set position
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("41.001", 1, motor.MotorIdAsArray); //amc enable
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("06.034", 1, motor.MotorIdAsArray); //RUN enable
        }
        public void Stopping1() {
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("34.007", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select 
        }
        public void Stopped() {
            UdpTx.UniqueInstance.ModBusSigleCommandWrite("06.034", 0, motor.MotorIdAsArray); //RUN disable
        }
        public void ResRef1() { //UdpTx.UniqueInstance.SendToModbus(8);
        }
        public void Error1() {// UdpTx.UniqueInstance.SendToModbus(9); 
        }
        public void Error2() { //UdpTx.UniqueInstance.SendToModbus(10); 
        }
        public void tripReset() { //UdpTx.UniqueInstance.SendToModbus(11); 
        }

        public void TripOccuredCommands() { //UdpTx.UniqueInstance.SendToModbus(12); 
        }

        public states ManNegState() { return states.Stopping1;
        }
        public states ManPosState() { return  states.Stopping1; 
        }
    }
}
