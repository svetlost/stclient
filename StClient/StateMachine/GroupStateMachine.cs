﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using static StClient.SoloStateMachine;

namespace StClient
{
    public class GroupStateMachine
    {

        string Message = "";
        Properties.Settings Sett = Properties.Settings.Default;
       


        public GroupStateMachine(GroupLogic _grp)
        {
            grp = _grp;

        }
        GroupLogic grp;
        states _state, _prevState;
        //public states state { get; set; }
        public states state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _prevState = _state;
                    _state = value;
                    Log.Write(string.Format("Grp {0} state changed from {1} to {2}. ",
                       grp.groupNo, _prevState.ToString(), _state.ToString()), EventLogEntryType.Information);
                }
            }
        }

        public DateTime timeoutTime { get; set; }
        bool mouseIsDown = false, deleyAutoStartFired = false;
        public states lastState;

        string[] commands;
        // this is done in infinite loop called MainLoop_Tick
        public void ProcessGroupStateMachine()
        {
            if (grp.MotorsInGroup.Count == 0) return;

            if (state != states.Idle)
            {
                Trace.WriteLine(string.Format("\r{0:hh:mm:ss.fff} --- STATE :{1}, MOVING:{2}, CP:{3}", DateTime.Now, state, grp.AnyMoving, grp.MotorsInGroup[0].CP));
            }

            switch (state)
            {
                case states.Idle:
                    //state = states.Idle;
                    if (grp.cmdAuto)
                    {
                        grp.SbcLock = MotorBrake.Unlocked;
                        state = states.AutoStarting1;
                        deleyAutoStartFired = false;
                    }
                    else if (grp.cmdJoyStart) state = states.JoyStarting;
                    else if (grp.cmdManStart)
                    {
                        grp.SbcLock = MotorBrake.Unlocked;
                        state = states.ManStarted;
                    }
                    else
                    {
                        state = states.Idle;
                    }
                    break;


                #region MANUAL

                // waiting foe up or down click for TimeoutMan_ms amount of time
                case states.ManStarted:
                    AddTimeout(Properties.Settings.Default.TimeoutMan_ms);

                    List<double> values = new List<double>();
                    commands = new string[] { "Run", "Set Position", "Amc Selected Reference", "DriveEnable", "Temp Velocity", "amc absolute MAX speed" };

                    foreach (var motor in grp.MotorsInGroup)
                    {

                        motor.TV = grp.MV * motor.MvSvCoeficient;

                        values.Add((double)1);
                        values.Add((double)motor.CP);
                        values.Add((double)0);
                        values.Add((double)1);
                        values.Add((double)motor.TV);
                        values.Add((double)Math.Abs(motor.TV));
                    }

                    UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), grp.motorIds.ToArray());

                    state = states.ManStarted2;
                    break;

                case states.ManStarted2:

                    mouseIsDown = System.Windows.Input.Mouse.LeftButton == MouseButtonState.Pressed; // bug in the system prevents detection ov buton release, so we check
                    if (grp.AnyMoving) { AddTimeout(Properties.Settings.Default.TimeoutMan_ms); }
                    if (timeoutTime < DateTime.Now) { state = states.Stopping2; }


                    else if (grp.cmdDown && mouseIsDown)
                    {
                        state = states.ManNeg;
                    }
                    else if (grp.cmdUp && mouseIsDown)
                    {

                        state = states.ManPos;
                    }
                    else
                    {
                        state = states.ManStarted2;
                    }
                    break;


                case states.ManPos:

                    values = new List<double>();
                    commands = new string[] { "Set Position", "Amc Selected Reference" };

                    foreach (var motor in grp.MotorsInGroup)
                    {
                        // motor.SbcLock = MotorBrake.Unlocked;

                        values.Add(motor.LimPos * motor.LimitCoeficient);
                        values.Add((ushort)AmcRef.PositionAbs1);
                    }

                    UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), grp.motorIds.ToArray());
                    state = states.ManPos2;
                    break;

                case states.ManPos2:
                    mouseIsDown = System.Windows.Input.Mouse.LeftButton == MouseButtonState.Pressed;
                    if (grp.cmdUp == false || mouseIsDown == false)
                    {

                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, grp.motorIds.ToArray());
                        state = states.ManStarted2;

                        AddTimeout(5);
                    }
                    break;

                case states.ManNeg:

                    values = new List<double>();
                    commands = new string[] { "Set Position", "Amc Selected Reference" };

                    foreach (var motor in grp.MotorsInGroup)
                    {
                        // motor.SbcLock = MotorBrake.Unlocked;

                        values.Add(motor.LimNeg * motor.LimitCoeficient);
                        values.Add((ushort)AmcRef.PositionAbs1);
                    }

                    UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), grp.motorIds.ToArray());

                    state = states.ManNeg2;
                    break;

                case states.ManNeg2:

                    mouseIsDown = System.Windows.Input.Mouse.LeftButton == MouseButtonState.Pressed;
                    if (grp.cmdDown == false || mouseIsDown == false)
                    {

                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, grp.motorIds.ToArray());
                        state = states.ManStarted2;
                        AddTimeout(5);
                    }
                    break;

                #endregion

                #region AUTO

                case states.AutoStarting1:


                    Message = "";
                    values = new List<double>();


                    // todo: redosled komandi 
                    commands = new string[] { "Run", "Temp Velocity", "amc absolute MAX speed", "Set Position", "AmcEnable", "DriveEnable" };

                    foreach (var motor in grp.MotorsInGroup)
                    {
                        motor.TP = grp.SP * motor.SpCoeficient;
                        motor.TV = grp.SV * motor.MvSvCoeficient;
                        if ((Math.Abs(motor.TP - motor.CP * motor.SpCoeficient) > Properties.Settings.Default.PositionTollerance) &&
                            motor.soloSM.state == states.Idle)
                        {
                            values.Add(1);
                            values.Add(grp.SV * motor.MvSvCoeficient);
                            values.Add(Math.Abs(grp.SV * motor.MvSvCoeficient));
                            values.Add(grp.SP * motor.SpCoeficient);
                            values.Add(1);
                            values.Add(1);

                            Message += string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};", motor.Title, motor.MotorID, motor.TP, motor.TV, motor.CP);
                        }


                    }

                    Trace.WriteLine("AutoStarting1: no of motors " + grp.motorIds.Count);
                    UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), grp.motorIds.ToArray());

                    if (Message != "") Log.Write("SM(GrpAuto). " + Message, EventLogEntryType.Information);
                    state = states.DelayAutoStart;
                    break;



                case states.DelayAutoStart:

                    TimedAction.ExecuteWithDelay(new Action(delegate
                    {
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.PositionAbs1, grp.motorIds.ToArray()); //amc ref select
                        state = states.AutoStarting2;
                    }), TimeSpan.FromMilliseconds(500));

                    /*   if (deleyAutoStartFired)
                       {

                           state = states.AutoStarting2;
                           deleyAutoStartFired = true;
                           UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.PositionAbs1, grp.motorIds.ToArray());
                           AddTimeout(1000);
                       }
                       else
                       {
                           state = states.DelayAutoStart; deleyAutoStartFired = true;

                       }
                      */
                    break;



                case states.AutoStarting2:

                    if (grp.AnyMoving) state = states.Started;
                    else if (timeoutTime < DateTime.Now)
                    {

                        state = states.Error1;
                    }

                    else state = states.AutoStarting2;
                    break;


                case states.Started:

                    if (grp.AnyMoving)//todo 20140405 prepravljeno zato sto je zaustavljao stimung pre vremena (tj odma na pocetku)
                    {
                        state = states.Started;
                        //motor.StoppingCounter = 0;
                    }
                    else state = states.Stopping1;
                    //if (grp.Brake == true) state = states.Stopped;
                    //else state = states.Started;
                    break;
                #endregion

                #region JOYSTICK

                case states.JoyStarting:

                   
                    if (grp._joy != null)
                    {
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", 1, grp.motorIds.ToArray());
                        grp.SbcLock = MotorBrake.Unlocked;
                        state = states.JoyButtReleased;
                        AddTimeout(Properties.Settings.Default.TimeoutJoy_ms);
                    }
                    else
                    {
                        state = states.Idle;
                    }
                    
                    break;

                case states.JoyButtReleased:
                    //Trace.WriteLine(string.Format("{0:hh:mm:ss.fff} {4:hh:mm:ss.fff} raw={1} butt={2} state={3}", DateTime.Now, grp._joy.JV_Raw, grp._joy.Butt1Pressed, state, timeoutTime));

                    foreach (var motor in grp.MotorsInGroup)
                    {
                        Trace.WriteLine("motor.CV :" + motor.CV * 2.3);
                        if (Math.Abs(motor.CV) * 2.3 > motor.MaxV)
                        {
                            state = states.Stopping3;
                        }

                    }


                    if (grp._joy.Butt1Pressed) { state = states.JoyButtPressed; }

                    else if (timeoutTime < DateTime.Now)
                    {

                        state = states.Stopping2;
                    }
                    else
                    {
                        if (lastState == states.JoyButtReleased) { break; }
                        //  Trace.WriteLine(string.Format("{0:hh:mm:ss.fff} OPALILO ZAUSTAVLJANJE ", DateTime.Now));
                        values = new List<double>();

                        commands = new string[] { "amc absolute MAX speed", "Temp Velocity", "Amc Selected Reference" };

                        foreach (var motor in grp.MotorsInGroup)
                        {
                            //reset TV values
                            motor.preiousTV = -100000;

                            values.Add(0);
                            values.Add(0);
                            values.Add((ushort)AmcRef.stop0);

                        }
                        UdpTx.UniqueInstance.ModbusGroupWrite(commands, values.ToArray(), grp.motorIds.ToArray());

                        state = states.JoyButtReleased;

                    }
                    lastState = states.JoyButtReleased;
                    break;

                case states.JoyButtPressed:
                    //    Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, JoyButtPressed", DateTime.Now));
                    if (grp._joy.Butt1Pressed)
                    {

                        foreach (var motor in grp.MotorsInGroup)
                        {
                            //     Trace.WriteLine(string.Format("time={0:hh:mm:ss.fff}, JoyButtPressed" + " ms --- CP: " + motor.CP + " --- CV:" + motor.CV, DateTime.Now));

                            // sets direction done in JoyNormalized, used change value trigger 
                            // sets TV value in motor
                            grp._joy.JoyNormalized(motor, motor.MaxV);


                            Trace.WriteLine("JOYSTICK vrednost - prosla vrednost : " + motor.TV + " - " + motor.preiousTV);
                            if (motor.preiousTV == motor.TV) { continue; }



                            if (motor.TV != 0) //1.9.2010. regulator daje error response ako se posalje mv=0  lence 9300/9400
                            {
                                if (motor.TV > 0)
                                {
                                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Set Position", motor.LimPos * motor.LimitCoeficient, motor.MotorIdAsArray); //set position Limit positive
                                }
                                else if (motor.TV < 0)
                                {
                                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Set Position", motor.LimNeg * motor.LimitCoeficient, motor.MotorIdAsArray); //set position Limit Negative
                                }

                                UdpTx.UniqueInstance.ModBusSigleCommandWrite("Temp Velocity", Math.Abs(motor.TV), motor.MotorIdAsArray); //amc sv mv
                                UdpTx.UniqueInstance.ModBusSigleCommandWrite("amc absolute MAX speed", Math.Abs(motor.TV), motor.MotorIdAsArray); //amc sv,mv MAX speed
                                UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.PositionAbs1, motor.MotorIdAsArray); //amc ref select 
                            }
                            else
                            {
                                UdpTx.UniqueInstance.ModBusSigleCommandWrite("amc absolute MAX speed", 0, motor.MotorIdAsArray); //amc sv,mv MAX speed
                                UdpTx.UniqueInstance.ModBusSigleCommandWrite("Temp Velocity", 0, motor.MotorIdAsArray); //amc sv mv
                                UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, motor.MotorIdAsArray); //amc ref select 
                            }

                            motor.preiousTV = motor.TV;

                        }
                        state = states.JoyButtPressed;

                    }
                    else
                    {
                        state = states.JoyButtReleased;
                        AddTimeout(Properties.Settings.Default.TimeoutJoy_ms);
                    }
                    lastState = states.JoyButtPressed;
                    break;

                #endregion

                #region STOP

                case states.Stopping1:

                    grp.SbcLock = MotorBrake.Unlocked; // this is for man or something ???     
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, grp.motorIds.ToArray());
                    state = states.Stopping2;
                    AddTimeout(500);
                    break;


                case states.Stopping2:
                    if (grp.AnyMoving == false || (timeoutTime < DateTime.Now))
                    {
                        state = states.Stopped;
                        UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", (ushort)0, grp.motorIds.ToArray()); //RUN disable
                        AddTimeout(500);
                    }
                    else state = states.Stopping2;
                    break;


                //joystick havarijski stop
                case states.Stopping3:
                    AddTimeout(500);
                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", (ushort)0, grp.motorIds.ToArray()); //RUN disable
                    state = states.Stopped;


                    break;


                case states.Stopped:

                    if (timeoutTime < DateTime.Now)

                    {
                        grp.SbcLock = MotorBrake.Locked;
                        state = states.Idle;
                        AddTimeout(1000);
                    }
                    else
                    {
                        state = states.Stopped;
                    }
                    break;

                #endregion


                #region ERROR

                case states.Error1:

                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Amc Selected Reference", (ushort)AmcRef.stop0, grp.motorIds.ToArray());

                    // foreach (var mot in grp.MotorsInGroup.Where(q => q.Brake == false)) mot.soloSM.state = states.Stopping1;
                    state = states.Error2;
                    AddTimeout(0);
                    break;

                case states.Error2:


                    UdpTx.UniqueInstance.ModBusSigleCommandWrite("Run", (ushort)0, grp.motorIds.ToArray()); //RUN disable
                    state = states.Error3;

                    break;

                case states.Error3:
                    if (grp.SbcLock != MotorBrake.Locked)
                    {
                        grp.SbcLock = MotorBrake.Locked;
                    }

                    //this is for situation where operator moves all tripped motors out of group and want to continue moving 
                    foreach (MotorLogic motor in grp.MotorsInGroup)
                    {
                        //for manual trips in over speed
                        motor.cmdUp = false;
                        motor.cmdDown = false;
                    }
                    // in trip there is no reset of manual movement
                    grp.cmdDown = false;
                    grp.cmdUp = false;

                        if (grp.cmdRstTrip)
                    {
                        foreach (MotorLogic motor in grp.MotorsInGroup) motor.soloSM.tripReset();
                        state = states.Error3;
                    }

                    if (grp.Trip == false) state = states.Idle;
                    else state = states.Error3;
                    break;

                    #endregion



            }

            //if (grp.cmdStop) stopMotors();


            if (grp.Trip == true && !(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Error1;


        }
        public void stopMotors()
        {
            if (!(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Stopping1;
        }

        void AddTimeout(double timemiliSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromMilliseconds(timemiliSeconds); }
    }
}
