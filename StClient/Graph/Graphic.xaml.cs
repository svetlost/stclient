﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Graphic.xaml
    /// </summary>
    public partial class Graphic : UserControl, INotifyPropertyChanged
    {


        MotorLogic motor;

        double _DisplayMax = 1900, _DisplayMin = -273, _Max = 1720, _Min = 350;
        double _BigTick = 100, _BigTicks = double.NaN;
        //_NumberOfBigTicks = 15;
        public double DisplayMax { get { return _DisplayMax; } set { _DisplayMax = value; } }
        public double DisplayMin { get { return _DisplayMin; } set { _DisplayMin = value; } }
        public double Max { get { return _Max; } set { _Max = value; } }
        public double Min { get { return _Min; } set { _Min = value; } }
        public double BigTicks { get { return _BigTicks; } set { _BigTicks = value; } }

        double _CPInPixels, _SPInPixels;
        public double CPInPixels { get { return _CPInPixels; } set { if (_CPInPixels != value) { _CPInPixels = value; OnNotify("CPInPixels"); } } }
        public double SPInPixels { get { return _SPInPixels; } set { if (_SPInPixels != value) { _SPInPixels = value; OnNotify("SPInPixels"); } } }

        double SmallTick { get { return _BigTick / 5; } }
        double FirstBigTickPosition { get { return _BigTick * Math.Floor(Min / _BigTick); } }

        double DisplayMaxCeiling { get { return _BigTick * Math.Ceiling(DisplayMax / _BigTick); } }
        double DisplayMinFloor { get { return _BigTick * Math.Floor(DisplayMin / _BigTick); } }
        double k { get { return gridGraphics.ActualHeight / (DisplayMaxCeiling - DisplayMinFloor); } }
        double n { get { return -DisplayMinFloor * k; } }
        double UnitsToPixels(double ValueInUnits) { return k * ValueInUnits + n; }

        // double hheight, wwidth, eventFiredCount;

        Polygon CP_polygon = new Polygon() { Fill = Brushes.Red, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(35, 0, 0, 0), Points = new PointCollection() { new Point(0, 10), new Point(0, -10), new Point(10, 0) } };
        Polygon SP_polygon = new Polygon() { Stroke = Brushes.Green, StrokeThickness = 2, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(35, 0, 0, 0), Points = new PointCollection() { new Point(20, 10), new Point(20, -10), new Point(10, 0) } };


        #region dep props and notifies
        public double CP { get { return (double)GetValue(CPProperty); } set { SetValue(CPProperty, value); } }
        public static readonly DependencyProperty CPProperty = DependencyProperty.Register("CP", typeof(double), typeof(Graphic), new PropertyMetadata((double)33, new PropertyChangedCallback(OnCpPropertyChanged)));

        public double SP { get { return (double)GetValue(SPProperty); } set { SetValue(SPProperty, value); } }
        public static readonly DependencyProperty SPProperty = DependencyProperty.Register("SP", typeof(double), typeof(Graphic), new PropertyMetadata((double)33, new PropertyChangedCallback(OnSpPropertyChanged)));


        private static void OnCpPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            Graphic s = (Graphic)dependencyObject;
            s.CPInPixels = s.gridGraphics.Height - s.UnitsToPixels(s.CP);

        }
        private static void OnSpPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            Graphic s = (Graphic)dependencyObject;
            s.SPInPixels = s.gridGraphics.Height - s.UnitsToPixels(s.SP);
        }

        #endregion

        TranslateTransform ttCP = new TranslateTransform(), ttSP = new TranslateTransform();

        public Graphic(MotorLogic motor)
        {
            InitializeComponent();
            this.DataContext = motor;
            this.motor = motor;
            //_DisplayMax = motor.GraphMax;
            //_DisplayMin = motor.GraphMin;
            //_Max = motor.LimPos;
            //_Min = motor.LimNeg;

            //BindingOperations.SetBinding(ttCP, TranslateTransform.YProperty, new Binding("CPInPixels") { Source = this });
            //BindingOperations.SetBinding(ttSP, TranslateTransform.YProperty, new Binding("SPInPixels") { Source = this });

            //CP_polygon.RenderTransform = ttCP;
            //SP_polygon.RenderTransform = ttSP;

            //darko 20101201

            // graph with position and axis y
            grph000 g = new grph000(motor.BigTicks) { DisplayMax = motor.GraphMax, DisplayMin = motor.GraphMin, Max = motor.LimPos, Min = motor.LimNeg };

            gridGraphics.Children.Add(g);
            BindingOperations.SetBinding(g, grph000.CPProperty, new Binding("CP") { Source = motor });
            BindingOperations.SetBinding(g, grph000.SPProperty, new Binding("SP") { Source = motor });

        }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion

        #region Commands

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            motor.soloSM.tripReset();
            //commands.resetTrip(motor.toList());
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            motor.soloSM.stopMotor();
        }

        #endregion

        //private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        //{
        //    return;/////////////////////////
        //    //eventFiredCount++;
        //    if (this.gridGraphics.ActualHeight == 0 || this.gridGraphics.ActualWidth == 0) return;
        //    if (this.gridGraphics.ActualHeight == hheight && this.gridGraphics.ActualWidth == wwidth) return;

        //    hheight = this.gridGraphics.ActualHeight;
        //    wwidth = this.gridGraphics.ActualWidth;

        //    double x = Math.Abs(Max - Min), t;
        //    for (int i = 4; i >= -2; i--)
        //    {
        //        t = x / Math.Pow(10, i);
        //        if (t > 2 && t < 25)
        //        {
        //            _BigTick = Math.Pow(10, i);

        //            _NumberOfBigTicks = Math.Ceiling(Max / _BigTick) - Math.Floor(Min / _BigTick);

        //            break;
        //        }
        //    }

        //    gridGraphics.Children.Clear();


        //    gridGraphics.Children.Add(new Line() { X1 = 0, X2 = 30, Y1 = hheight - UnitsToPixels(Min), Y2 = hheight - UnitsToPixels(Min), StrokeThickness = 2, Stroke = Brushes.Purple });
        //    gridGraphics.Children.Add(new Line() { X1 = 0, X2 = 30, Y1 = hheight - UnitsToPixels(Max), Y2 = hheight - UnitsToPixels(Max), StrokeThickness = 2, Stroke = Brushes.Purple });

        //    for (double i = FirstBigTickPosition; i <= FirstBigTickPosition + _BigTick * _NumberOfBigTicks; i += _BigTick)
        //    {
        //        gridGraphics.Children.Add(new TextBlock() { Text = i.ToString(), Margin = new Thickness(10, hheight - UnitsToPixels(i), 0, 0) });
        //        gridGraphics.Children.Add(new Line() { X1 = 30, X2 = 50, Y1 = hheight - UnitsToPixels(i), Y2 = hheight - UnitsToPixels(i), StrokeThickness = 2, Stroke = Brushes.Black });
        //    }
        //    for (double i = FirstBigTickPosition; i <= FirstBigTickPosition + _BigTick * _NumberOfBigTicks; i += SmallTick)
        //        gridGraphics.Children.Add(new Line() { X1 = 40, X2 = 50, Y1 = hheight - UnitsToPixels(i), Y2 = hheight - UnitsToPixels(i), StrokeThickness = 1, Stroke = Brushes.Black });

        //    gridGraphics.Children.Add(CP_polygon);
        //    gridGraphics.Children.Add(SP_polygon);


        //    OnCpPropertyChanged(this, new DependencyPropertyChangedEventArgs());
        //    OnSpPropertyChanged(this, new DependencyPropertyChangedEventArgs());


        //}

    }



}
