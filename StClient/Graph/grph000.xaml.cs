﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for grph000.xaml
    /// </summary>
    public partial class grph000 : UserControl, INotifyPropertyChanged
    {
        double _DisplayMax = 1900, _DisplayMin = -273, _Max = 1720, _Min = 350;
        double _BigTick = double.NaN, _NumberOfBigTicks = 15;
        public double DisplayMax { get { return _DisplayMax; } set { _DisplayMax = value; } }
        public double DisplayMin { get { return _DisplayMin; } set { _DisplayMin = value; } }
        public double Max { get { return _Max; } set { _Max = value; } }
        public double Min { get { return _Min; } set { _Min = value; } }
        public double BigTick { get { return _BigTick; } set { _BigTick = value; } }


        //public double BigTick { get { return _BigTick; } }
        double SmallTick { get { return _BigTick / 5; } }
        double FirstBigTickPosition { get { return _BigTick * Math.Floor(Min / _BigTick); } }
        //public double NumberOfBigTicks { get { return _NumberOfBigTicks; } }

        double DisplayMaxCeiling { get { return _BigTick * Math.Ceiling(DisplayMax / _BigTick); } }
        double DisplayMinFloor { get { return _BigTick * Math.Floor(DisplayMin / _BigTick); } }
        double k { get { return ActualHeight / (DisplayMaxCeiling - DisplayMinFloor); } }
        double n { get { return -DisplayMinFloor * k; } }
        double UnitsToPixels(double ValueInUnits) { return k * ValueInUnits + n; }

        double hheight, wwidth;
        //double eventFiredCount;
        //Rectangle rrr = new Rectangle() { Width = 15, Height = 15, Fill = Brushes.Red, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(40, 0, 0, 0) };
        //Polygon SP_pol = new Polygon() { Fill = Brushes.Green, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(35, 0, 0, 0), Points = new PointCollection() { new Point(0, 10), new Point(0, -10), new Point(10, 0) } };
        //Polygon CueP_pol = new Polygon() { Fill = Brushes.Cyan, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(35, 0, 0, 0), Points = new PointCollection() { new Point(0, 10), new Point(0, -10), new Point(10, 0) } };

        //Polygon CP_polygon = new Polygon() { Fill = Brushes.Red, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(25, 0, 0, 0), Points = new PointCollection() { new Point(0, 10), new Point(0, -10), new Point(10, 0) } };
        //Polygon SP_polygon = new Polygon() { Stroke = Brushes.Green, StrokeThickness = 2, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(35, 0, 0, 0), Points = new PointCollection() { new Point(20, 10), new Point(20, -10), new Point(10, 0) } };
        //Polygon CueP_polygon = new Polygon() { Fill = Brushes.Cyan, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(25, 0, 0, 0), Points = new PointCollection() { new Point(20, 8), new Point(20, -8), new Point(12, 0) } };
        //Polygon CP_polygon = new Polygon() { Fill = Brushes.Red, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(25, 0, 0, 0), Points = new PointCollection() { new Point(0, 10), new Point(0, -10), new Point(10, 0) } };
        Polygon SP_polygon = new Polygon() { Stroke = Brushes.Green, StrokeThickness = 2, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(20, 0, 0, 0), Points = new PointCollection() { new Point(20, 10), new Point(20, -10), new Point(10, 0) } };
        Polygon CP_polygon = new Polygon() { Fill = Brushes.Red, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(20, 0, 0, 0), Points = new PointCollection() { new Point(20, 8), new Point(20, -8), new Point(12, 0) } };

        #region dep props and notifies
        public double CP { get { return (double)GetValue(CPProperty); } set { SetValue(CPProperty, value); } }
        public static readonly DependencyProperty CPProperty = DependencyProperty.Register("CP", typeof(double), typeof(grph000), new PropertyMetadata((double)33, new PropertyChangedCallback(OnCpPropertyChanged)));

        public double SP { get { return (double)GetValue(SPProperty); } set { SetValue(SPProperty, value); } }
        public static readonly DependencyProperty SPProperty = DependencyProperty.Register("SP", typeof(double), typeof(grph000), new PropertyMetadata((double)33, new PropertyChangedCallback(OnSpPropertyChanged)));

        public double CueP { get { return (double)GetValue(CuePProperty); } set { SetValue(CuePProperty, value); } }
        public static readonly DependencyProperty CuePProperty = DependencyProperty.Register("CueP", typeof(double), typeof(grph000), new PropertyMetadata((double)33, new PropertyChangedCallback(OnCuePPropertyChanged)));

        private static void OnCpPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            grph000 s = (grph000)dependencyObject;
            //s.valueInPixels = s.value * s.PixelsPerUnit;
            //s.valueInPixels = .5 * s.hheight - s.UnitsToPixels(s.value);
            s.CPInPixels = s.hheight - s.UnitsToPixels(s.CP);
            //Debug.WriteLine(string.Format("cp={0} cpPIXELS={1}", s.CP, s.CPInPixels));
        }
        private static void OnSpPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            grph000 s = (grph000)dependencyObject;
            s.SPInPixels = s.hheight - s.UnitsToPixels(s.SP);
        }
        private static void OnCuePPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            grph000 s = (grph000)dependencyObject;
            s.CuePInPixels = s.hheight - s.UnitsToPixels(s.CueP);
        }

        double _CPInPixels, _SPInPixels, _CuePInPixels;
        public double CPInPixels { get { return _CPInPixels; } set { if (_CPInPixels != value) { _CPInPixels = value; OnNotify("CPInPixels"); } } }
        public double SPInPixels { get { return _SPInPixels; } set { if (_SPInPixels != value) { _SPInPixels = value; OnNotify("SPInPixels"); } } }
        public double CuePInPixels { get { return _CuePInPixels; } set { if (_CuePInPixels != value) { _CuePInPixels = value; OnNotify("CuePInPixels"); } } }
        #endregion

        //TranslateTransform ttCP = new TranslateTransform(), ttSP = new TranslateTransform(), ttCueP = new TranslateTransform();
        TranslateTransform ttCP = new TranslateTransform(), ttSP = new TranslateTransform(), ttCueP = new TranslateTransform();


        public grph000() : this(double.NaN) { }
        public grph000(double bigTicks)
        {
            BigTick = bigTicks;

            InitializeComponent();

            //double _DisplayMax = 1900, _DisplayMin = -273, _Max = 1720, _Min = 350;

            BindingOperations.SetBinding(ttCP, TranslateTransform.YProperty, new Binding("CPInPixels") { Source = this });
            BindingOperations.SetBinding(ttSP, TranslateTransform.YProperty, new Binding("SPInPixels") { Source = this });
            //BindingOperations.SetBinding(ttCueP, TranslateTransform.YProperty, new Binding("CuePInPixels") { Source = this });

            CP_polygon.RenderTransform = ttCP;
            SP_polygon.RenderTransform = ttSP;
            //CP_polygon.LayoutTransform = ttCP;
            //SP_polygon.LayoutTransform = ttSP;
            //ttCP.X = -5;
            //ttSP.X = -15;
            //CueP_polygon.RenderTransform = ttCueP;
            /*
            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                this.Width =0;
                this.Height = 0 ;
            }
            else
            {
                this.Width = 0;
                this.Height = 0 ;
            }
           */
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //eventFiredCount++;
            if (gridGRPH.ActualHeight == 0 || gridGRPH.ActualWidth == 0) return;
            if (gridGRPH.ActualHeight == hheight && gridGRPH.ActualWidth == wwidth) return;

            //hheight = gridGRPH.ActualHeight;
            //wwidth = gridGRPH.ActualWidth;
            hheight = 750;
            wwidth = gridGRPH.ActualWidth;

            double x = Math.Abs(Max - Min), t;

            if (double.IsNaN(_BigTick))
            {
                for (int i = 4; i >= -2; i--)
                {
                    t = x / Math.Pow(10, i);
                    if (t > 2 && t < 25)
                    {
                        _BigTick = Math.Pow(10, i);
                        break;
                    }
                }
            }
            else
                _BigTick = BigTick;

            _NumberOfBigTicks = Math.Ceiling(Max / _BigTick) - Math.Floor(Min / _BigTick);

            gridGRPH.Children.Clear();

            //for (int i = 0; i < 15; i++)
            //    grid.Children.Add(new Line() { X1 = 0, X2 = 50, Y1 = i * (hheight / 15), Y2 = i * (hheight / 15), StrokeThickness = 2, Stroke = Brushes.Black });

            gridGRPH.Children.Add(new Line() { X1 = 0, X2 = 30, Y1 = hheight - UnitsToPixels(Min), Y2 = hheight - UnitsToPixels(Min), StrokeThickness = 2, Stroke = Brushes.Purple });
            gridGRPH.Children.Add(new Line() { X1 = 0, X2 = 30, Y1 = hheight - UnitsToPixels(Max), Y2 = hheight - UnitsToPixels(Max), StrokeThickness = 2, Stroke = Brushes.Purple });

            for (double i = FirstBigTickPosition; i <= FirstBigTickPosition + _BigTick * _NumberOfBigTicks; i += _BigTick)
            {
                gridGRPH.Children.Add(new TextBlock() { Text = i.ToString(), Margin = new Thickness(2, hheight - UnitsToPixels(i), 0, 0) });
                //grid.Children.Add(new TextBlock() { Text = i.ToString(), FontSize = UnitsToPixels(BigTick) * .5, Margin = new Thickness(10, hheight - UnitsToPixels(i), 0, 0) });
                gridGRPH.Children.Add(new Line() { X1 = 20, X2 = 40, Y1 = hheight - UnitsToPixels(i), Y2 = hheight - UnitsToPixels(i), StrokeThickness = 2, Stroke = Brushes.Black });
            }
            for (double i = FirstBigTickPosition; i <= FirstBigTickPosition + _BigTick * _NumberOfBigTicks; i += SmallTick)
                gridGRPH.Children.Add(new Line() { X1 = 30, X2 = 40, Y1 = hheight - UnitsToPixels(i), Y2 = hheight - UnitsToPixels(i), StrokeThickness = 1, Stroke = Brushes.Black });

            gridGRPH.Children.Add(CP_polygon);
            //grid.Children.Add(CueP_polygon);
            gridGRPH.Children.Add(SP_polygon);


            //valueInPixels = .5 * hheight - UnitsToPixels(value);
            //CPInPixels = hheight - UnitsToPixels(CP);
            //SPInPixels = hheight - UnitsToPixels(SP);
            //CuePInPixels = hheight - UnitsToPixels(CueP);
            OnCpPropertyChanged(this, new DependencyPropertyChangedEventArgs());
            OnSpPropertyChanged(this, new DependencyPropertyChangedEventArgs());
            OnCuePPropertyChanged(this, new DependencyPropertyChangedEventArgs());


            //grid.Children.Add(new TextBlock() { FontSize = 20, TextWrapping = TextWrapping.Wrap, Text = string.Format("count={0} height={1} width={2}", eventFiredCount, hheight, wwidth) });
        }
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion

    }
}
