﻿using System.Data.Linq.Mapping;

namespace StClient
{
    [Table(Name = "rfid")]
    public class RfidItem
    {
        string _rfidtag = "01075915bc", _name = "no name";

        [Column(Name = "rfid_tag")]
        public string RfidTag { get { return _rfidtag; } set { _rfidtag = value; } }

        [Column(Name = "name")]
        public string Name { get { return _name; } set { _name = value; } }
    }
}
