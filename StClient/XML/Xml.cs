﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;


namespace StClient
{

    //public class CueItem : INotifyPropertyChanged
    //{
    //    int _cueNo = 0;
    //    int _motorID = 0;
    //    string _motorTitle = "";
    //    string _cueDescription = "";
    //    double _sp = 0;
    //    double _sv = 0;

    //    [XmlAttribute("MotorID")]
    //    public int MotorID { get { return _motorID; } set { _motorID = value; } }

    //    [XmlAttribute("MotorTitle")]
    //    public string MotorTitle { get { return _motorTitle; } set { _motorTitle = value; } }

    //    [XmlAttribute("CueDescription")]
    //    public string CueDescription { get { return _cueDescription; } set { if (_cueDescription != value) { _cueDescription = value; OnNotify("CueDescription"); } } }

    //    [XmlAttribute("CueNo")]
    //    public int CueNo { get { return _cueNo; } set { if (_cueNo != value) { _cueNo = value; OnNotify("CueNo"); } } }

    //    [XmlAttribute("SP")]
    //    public double SP { get { return _sp; } 
    //        set { if (_sp != value) { _sp = value; OnNotify("SP"); } } }

    //    [XmlAttribute("SV")]
    //    public double SV { get { return _sv; } set { if (_sv != value) { _sv = value; OnNotify("SV"); } } }

    //    [XmlIgnore]
    //    public MotorLogic Motor;

    //    [XmlIgnore]
    //    public double LimitMin;
    //    [XmlIgnore]
    //    public double LimitMax;

    //    //public double treshold(double value, double min, double max)
    //    //{
    //    //    if (value < min) return min;
    //    //    else if (value > max) return max;

    //    //    return value;
    //    //}

    //    #region INotifyPropertyChanged Members
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    private void OnNotify(String parameter)
    //    {
    //        if (PropertyChanged != null)
    //            PropertyChanged(this, new PropertyChangedEventArgs(parameter));
    //    }
    //    #endregion
    //}


    public class XmlUtil<T> : IDisposable where T : new()
    {
        XmlSerializer xmlSerializer;
        TextWriter writer;
        XmlReader reader;





        public XmlUtil()
        {
            xmlSerializer = new XmlSerializer(typeof(List<T>));
        }

        //public List<T> ReadXml(string fileName, bool createDefaultRow)
        public List<T> ReadXml(string fileName, CreateDefaultRow createDefaultRow)
        {
            List<T> _Xml = null;

            try
            {
                if (File.Exists(fileName))
                {
                    reader = XmlReader.Create(new StreamReader(fileName));
                    _Xml = (List<T>)xmlSerializer.Deserialize(reader);

                }
                else if (createDefaultRow == CreateDefaultRow.Create)
                {
                    writer = new StreamWriter(fileName);
                    _Xml = new List<T>();
                    _Xml.Add(new T());
                    xmlSerializer.Serialize(writer, _Xml);
                }
            }
            catch (Exception ex)
            {
                Log.Write("ReadXml error.\n" + ex.Message, EventLogEntryType.Error);
                return null;
            }


            return _Xml;
        }

        public bool WriteXml(string fileName, List<T> listToWrite)
        {
            if (listToWrite.Count == 0) return false;

            try
            {
                File.Delete(fileName);
                writer = new StreamWriter(fileName);
                xmlSerializer.Serialize(writer, listToWrite);
            }
            catch (Exception ex)
            {
                //MainWindow.AlertsLB.Add("WriteXml error.");
                Log.Write("WriteXml error.\n" + ex.Message, EventLogEntryType.Error);
                return false;
            }

            writer.Close();

            return true;
        }

        #region IDisposable Members

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                reader.Close();
                writer.Close();
            }
            // free native resources
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }

        #endregion
    }





}