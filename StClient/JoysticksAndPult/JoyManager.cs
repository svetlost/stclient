﻿using LogAlertHB;
using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace StClient
{
    public class JoyManager
    {
        public List<Joystick> joyDxList = new List<Joystick>();
        List<StJoystick> joyStList;
        DirectInput directInput = new DirectInput();
        JoystickState _state = new JoystickState();
        //DispatcherTimer _timer = new DispatcherTimer();


        public JoyManager(List<StJoystick> stJoyList, String joystickType)
        {
            joyStList = stJoyList;
            string s = "loading joysticks";



            if (joystickType == "DirectX")
            {
                var detectedJoysticks = directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices);
                foreach (var j in detectedJoysticks)
                {
                    try
                    {
                        var tmpStJoy = stJoyList.First(stj => stj.JGUID == j.InstanceGuid);
                        if (tmpStJoy != null)
                        {
                            Joystick joy = new Joystick(directInput, j.InstanceGuid);
                            joy.Properties.BufferSize = 128;
                            joy.Acquire();
                            tmpStJoy.SetDxJoystick(joy);

                            Sys.StaticGroupList[tmpStJoy.JIndex]._joy = tmpStJoy;
                            tmpStJoy._groupLogic = Sys.StaticGroupList[tmpStJoy.JIndex];
                        }
                        s += string.Format("\n loaded joystick id={0} guid={1}", tmpStJoy.JIndex, j.InstanceGuid);
                    }
                    catch { Log.Write(string.Format("Unable to load joystick."), EventLogEntryType.Error); }
                }

                Log.Write(s, EventLogEntryType.Information);
            }
            else if (joystickType == "Phidget")
            {
                // read joyst group from xml
                // add joys to groups
                for (int i = 0; i < joyStList.Count; i++)
                {
                    if (joyStList[i].Group == i)
                    {
                        Sys.StaticGroupList[i]._joy = joyStList[i];
                        joyStList[i]._groupLogic = Sys.StaticGroupList[i];
                    }
                }

            }
            else { }
            //  
            //  if (detectedJoysticks.Count == 0) return;



            //directX


            //_timer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval);
            //_timer.Tick += _timer_Tick;
            //_timer.Start();
        }

        //private void _timer_Tick(object sender, EventArgs e)
        //{
        //    if (joyStList.Count == 0 || joyStList.Any(q => q._joystickDx == null)) return;
        //    foreach (var joy in joyStList)
        //    {

        //        try { joy._joystickDx.GetCurrentState(ref _state); }
        //        catch
        //        {
        //            ///status = "err reading joy. stopping joy."; //todo add log
        //            _timer.Stop();
        //        }
        //        joy.JV_Raw = _state.Y;//to do: make both X and Y axis (selectable from config)
        //        joy.Butt1Pressed = _state.Buttons[0];
        //        //if (joy.Butt1Pressed == false) commands.JoyButtonPressed(joy._groupLogic);
        //        //status = "reading joy.";



        //        //debugjoy
        //        //MotorLogic ml = Sys.StaticMotorList[0];
        //        //if (Sys.StaticMotorList.Count > 0) joy.JoyNormalized3(Sys.StaticMotorList[0], 22);
        //    }
        //}

        public static void readJoystick(StJoystick joy)
        {
            JoystickState _readState = new JoystickState();
            try { joy._joystickDx.GetCurrentState(ref _readState); }
            catch
            {
                ///status = "err reading joy. stopping joy."; //todo add log
                //_timer.Stop();
            }
            joy.JV_Raw = _readState.Y;//to do: make both X and Y axis (selectable from config)
            joy.Butt1Pressed = _readState.Buttons[0];
        }

        //public void AddJoy(StJoystick joy) { }
    }
}
