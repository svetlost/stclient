﻿using SharpDX.DirectInput;
using System;
using System.Data.Linq.Mapping;

namespace StClient
{
    [Table(Name = "ph_joystick")]
    public class StJoystick //: IMv
    {
        byte _group = 0, _jindex = 0, posdirection, negdirection;
        bool _isInverted = false;
        double j_measured_min, j_measured_max, j_measured_mid, j_dead_zone, coefficient = 1;
        Guid _jguid;

        Properties.Settings Sett = Properties.Settings.Default;
        

        [Column(Name = "j_group")]
        public byte Group { get { return _group; } set { _group = value; } }

        [Column(Name = "j_guid")]
        public Guid JGUID { get { return _jguid; } set { _jguid = value; } }

        [Column(Name = "j_index")]
        // JIndex is used in phidget 8/8/8 , it is port number for wires 
        public byte JIndex { get { return _jindex; } set { _jindex = value; } }

        [Column(Name = "inverted")]
        public bool IsInverted { get { return _isInverted; } set { if (value) { coefficient = -1; } _isInverted = value; } }

        [Column(Name = "j_measured_min")]
        public double JMeasuredMin { get { return j_measured_min; } set { j_measured_min = value; } }

        [Column(Name = "j_measured_max")]
        public double JMeasuredMax { get { return j_measured_max; } set { j_measured_max = value; } }

        [Column(Name = "j_measured_mid")]
        public double JMeasuredMid { get { return j_measured_mid; } set { j_measured_mid = value; } }

        [Column(Name = "j_dead_zone")]
        public double JDeadZone { get { return j_dead_zone; } set { j_dead_zone = value; } }

        public bool Butt1Pressed { get; set; }

        public Joystick _joystickDx { get; private set; }

        public GroupLogic _groupLogic { get; set; }

        public double JV_Raw { get; set; }

        public byte PosDirection { get { return posdirection; } set { posdirection = value; } }

        public byte NegDirection { get { return negdirection; } set { negdirection = value; } }





        public double JoyNormalized(MotorLogic moto, double _vmax)
        {
            //  linear equation, coefficient for positive joystick
            double k = 0;
            double n = 0;

            // going UP
            if (JV_Raw > (JMeasuredMid + JDeadZone))
            {

                k = (_vmax - 0) / (JMeasuredMax - (JMeasuredMid + JDeadZone));
                n = -k * (JMeasuredMid + JDeadZone);


            }
            else if ((JV_Raw < (JMeasuredMid + JDeadZone)) && (JV_Raw > (JMeasuredMid - JDeadZone)))
            {
                return moto.TV = 0;
            }
            else
            {
                k = (-_vmax - 0) / (JMeasuredMin - (JMeasuredMid - JDeadZone));
                n = -k * (JMeasuredMid - JDeadZone);

            }

            return moto.TV = Math.Floor(k * JV_Raw + n) * moto.MvSvCoeficient * coefficient;
        }


       

        public void SetDxJoystick(Joystick _tempJoy) { _joystickDx = _tempJoy; }
    }

}
