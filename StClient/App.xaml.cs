﻿using LogAlertHB;
using StClient.Properties;
using System;
using System.Diagnostics;
using System.Windows;


namespace StClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {


        //public Heartbeat hb;
        Log logger;

        string configPath = Environment.CurrentDirectory + @"\Config\";
        string backupPath = Environment.CurrentDirectory + @"\Backup\";

        //public WindowCollection mainWindow;


        //IPEndPoint IpEpClientHeartBeat = new IPEndPoint(IPAddress.Parse(Settings.Default.LocalIp), Settings.Default.ClientHeartbeatPort);
        //IPEndPoint IpEpServerHeartBeat = new IPEndPoint(IPAddress.Parse(Settings.Default.ServerIP), Settings.Default.ServerHeartbeatPort);

        protected override void OnLoadCompleted(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnLoadCompleted(e);
            //mainWindow=this.Windows
        }

        protected override void OnStartup(StartupEventArgs e) // promene 16 jun : single app
        {
            // Get Reference to the current Process
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
                // If ther is more than one, than it is already running.
                MessageBox.Show("Application is already running.");
                Application.Current.Shutdown();
                return;
            }

            //logger = new Log("ClientLog", Settings.Default.BackupLogTimerIntervalMinutes, "", Settings.Default.EventLogMaximumKilobytes, Settings.Default.PathLog, backupPath);
            logger = new Log(Settings.Default.BackupLogTimerIntervalMinutes, "ST_CLIENT", backupPath);

            if (!System.IO.Directory.Exists(configPath))
                System.IO.Directory.CreateDirectory(configPath);

            //hb = new Heartbeat(IpEpClientHeartBeat, "Client", IpEpServerHeartBeat, "Server", Stopping);
            //hb.StartServer();



            base.OnStartup(e);

        }

        //public void Stopping()
        //{
        //    foreach (MotorLogic motor in Sys.StaticMotorList)
        //        motor.autoEnabled = motor.Brake = motor.Net = motor.RefOk = motor.Brake = motor.SpSvEnabled = motor.MVEnabled = motor.manEnabled = false;
        //}


        //never executes
        //protected override void OnExit(ExitEventArgs e)
        //{
        //    //Sys.StopAllMotors();

        //    //while (Sys.StaticMotorList.All(q => q.state != states.Idle))
        //    //    Thread.Sleep(300);

        //    base.OnExit(e);
        //}
    }
}
