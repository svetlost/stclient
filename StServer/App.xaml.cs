﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using LogAlertHB;
using Server.Properties;
using System.Threading;

namespace Server
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        Log logger;
        string backupPath = Environment.CurrentDirectory + @"\Backup\";
        string user = Environment.UserName + "Server";
        string logName = Environment.UserName + "SrvLog";
        Mutex mutex = new Mutex(true, "Global\\{E88F694B-C541-41A7-9361-C5BF7D01DFA7}");
        //static List<Alerts> _alerts = new List<Alerts>();


        protected override void OnStartup(StartupEventArgs e) // promene 16 jun : single app
        {
            #region something
            //// Get Reference to the current Process
            //Process thisProc = Process.GetCurrentProcess();
            //// Check how many total processes have the same name as the current one
            //if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            //{
            //    // If ther is more than one, than it is already running.


            //    //MessageBox.Show("Application is already running");                
            //    //Application.Current.Shutdown();

            //    foreach (Process clsProcess in Process.GetProcesses())
            //    {
            //        if (clsProcess.ProcessName.StartsWith(thisProc.ProcessName))
            //            clsProcess.Kill();
            //    }
            //}
            #endregion

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                //Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new Form1());
                mutex.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("Only one instance at a time");
                Application.Current.Shutdown();
            }


            //logger = new Log(logName, 3, user, Settings.Default.EventLogMaximumKilobytes, Settings.Default.PathLog, backupPath);
            //logger = new Log(logName, Settings.Default.BackupLogTimerIntervalMinutes, user, Settings.Default.EventLogMaximumKilobytes, Settings.Default.PathLog, backupPath);
            logger = new Log(Settings.Default.BackupLogTimerIntervalMinutes, "ST_SERVER", backupPath);
            Log.Write("StServer init", EventLogEntryType.Information);
            base.OnStartup(e);
        }
        //public static void AlertAdd(string text)
        //{
        //    try
        //    {
        //        _alerts.Add(new Alerts { AlertTime = DateTime.Now, AlertText = text });
        //        _alv.LV2.ItemsSource = _alerts;
        //        //_alv.setItemsSource(_alerts);
        //        //alertsCountPrevious=_alerts.Count;
        //    }
        //    catch (Exception ex)
        //    {
        //        //MessageBox.Show(ex.Message); //20150813 sklonio, mislim da ponekad izbacuje exception. staviti ove operacija da idu kroz dispatcher
        //    }
        //}

        //public class Alerts
        //{
        //    public DateTime AlertTime { get; set; }
        //    public string AlertText { get; set; }
        //}


    }
}
