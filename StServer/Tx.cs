﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using vxlapi_NET20;
using System.Windows.Threading;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
//using ML;
using LogAlertHB;
//using StClient;
namespace Server
{

    

    public abstract class Tx
    {       
       
        static IPEndPoint ipepServer = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerIP), Properties.Settings.Default.ServerPort); //ovo je serverova ip adresa i port za cekanje
        public Socket _server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        public EndPoint Remote = (EndPoint)ipepServer;
        public byte[] dataToSendToCAN = new byte[Properties.Settings.Default.CanOverUdpMessageLength];
        public uint SyncCanMsgID = (uint)Properties.Settings.Default.SyncID;      
        //public List<MotorLogic> _motorsList;
        public Counters counters;
        public static DispatcherTimer SyncTimer = new DispatcherTimer(), SenderTimer = new DispatcherTimer();



        //public CanTx(List<MotorLogic> motorsList, Counters c)
        public Tx( Counters c)
        {

            //_motorsList = motorsList;
            counters = c;

            try
            {
                _server.Bind(ipepServer);               
              
            }
            catch (SocketException sockEx)
            {
                AlertLogic.Add("Message send failed :" + sockEx.ErrorCode.ToString()
                                    + "\nMessage send failed :" + sockEx.Message
                                    + "\nMessage send failed :" + sockEx.StackTrace);
            }

            catch (Exception ex)
            {
                AlertLogic.Add("Message send failed" + ex.Message);
            }

           
        }

       

        public void CloseServerSocket()
        {
            AlertLogic.Add("Server socket closed.");
            _server.Close();
        }

        //public virtual void canStopAll() { }
     

       



    }
}

