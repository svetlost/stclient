﻿using System;
using LogAlertHB;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics;

namespace Server.modbus
{
    [Serializable]
    class dataToSend
    {
        public string id { get; set; }
        public double value { get; set; }
        public byte type { get; set; }
    }
    public class ModbusTx : Tx
    {
        ModbusIpMaster master;
        byte[] recivedMessage = new byte[50];
        public ModbusTx(Counters c, ModbusIpMaster m) : base(c)
        {

            master = m;
            try
            {

                SenderTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.CanTxInterval);
                SenderTimer.Tick += new EventHandler(CanTxLoop);
                SenderTimer.Start();
            }


            catch (Exception ex)
            {
                AlertLogic.Add("Message send failed" + ex.Message);
            }
        }

        public void CanTxLoop(object sender, EventArgs e)
        {   

            ushort emersonMemAddress;
            ushort[] valuesTosend = new ushort[] { };

            while (_server.Available > 0)
            {
                if (_server.Available > Properties.Settings.Default.UdpBufferOverflowSize)
                    AlertLogic.Add("Receive socket buffer full.");

                try
                {
                    // we receive array of bytes from client 
                    _server.ReceiveFrom(recivedMessage, ref Remote);

                    counters.UdpRxOk++;

                    // we have to know size of all parts of our udp package, so we read from the start
                    int packetOffset = 0;

                    double value = BitConverter.ToDouble(recivedMessage, packetOffset); // sent as double !!
                    packetOffset += 8;
                    // read the length (in bytes) of string parameter
                    int parametarLength = BitConverter.ToInt32(recivedMessage, packetOffset);
                    packetOffset += 4;
                    string parametar = Encoding.UTF8.GetString(recivedMessage, packetOffset, parametarLength);
                    packetOffset += parametarLength;
                    double regulatorID = BitConverter.ToInt16(recivedMessage, packetOffset); // sent as short !!
                    packetOffset += 2;
                    // 0 for 16 bit 1 for 32 bit
                    bool is16or32 = BitConverter.ToBoolean(recivedMessage, packetOffset);

                    //1= 32bit
                    if (is16or32)
                    {
                        emersonMemAddress = Utilz.ind(parametar, Utilz.len.b32);
                        valuesTosend = new ushort[] { (ushort)((int)value >> 16), (ushort)((int)value & 0x0000FFFF) };
                        master.WriteMultipleRegistersAsync(emersonMemAddress, valuesTosend); // we need to write to multiple registers , emerson has only 16bit registers
                    }
                    //0=16bit
                    else
                    {
                        emersonMemAddress = Utilz.ind(parametar, Utilz.len.b16);
                        master.WriteSingleRegisterAsync(emersonMemAddress, (ushort)value);
                    }                   

                }
                catch (Exception ex)
                {
                   Trace.WriteLine(ex.Message);
                    counters.UdpRxError++;
                }
            }
        }
    }
}
