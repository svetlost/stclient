﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Server.modbus
{
    public class modbusParams
    {
        //i za auto mora da se da i drzi pravac (bilo koji) da bi se izvrsilo pozicionijranje
        ushort pPresetRef02_01_022 = Utilz.ind("01.022", Utilz.len.b16); //treba da bude 1000 bin za amc, ili 001 za normalno NIJE32bit

        ushort pSto_Input01_State_08_009 = Utilz.ind("08.009", Utilz.len.b16); //inh sto ulaz, on=moze da se vozi
        ushort pTimingOptionsSelect_02_050 = Utilz.ind("02.050", Utilz.len.b16); //treba da bude 1000 bin za amc, ili 001 za normalno
        ushort pFinalSpeedRef_03_001 = Utilz.ind("03.001", Utilz.len.b16);
        ushort pCurrentMagnitude_04_001 = Utilz.ind("04.001", Utilz.len.b16);
        ushort pOutputFreq_05_001 = Utilz.ind("05.001", Utilz.len.b16);
        ushort pDriveEnable_06_015 = Utilz.ind("06.015", Utilz.len.b16);
        ushort pRunFw_06_030 = Utilz.ind("06.030", Utilz.len.b16);
        ushort pJog_06_031 = Utilz.ind("06.031", Utilz.len.b16);
        ushort pRunRev_06_032 = Utilz.ind("06.032", Utilz.len.b16);
        ushort pRun_06_034 = Utilz.ind("06.034", Utilz.len.b16);
        ushort pControlWord_06_042 = Utilz.ind("06.042", Utilz.len.b16);
        ushort pControlWordEnable_06_043 = Utilz.ind("06.043", Utilz.len.b16);
        ushort pTripSet_10_032 = Utilz.ind("10.032", Utilz.len.b16);
        ushort pDriveReset_10_033 = Utilz.ind("10.033", Utilz.len.b16);
        ushort pStatusWord_10_040 = Utilz.ind("10.040", Utilz.len.b16);
        ushort pAmcSelect_31_001 = Utilz.ind("31.001", Utilz.len.b16);
        ushort pAmcPos_33_004 = Utilz.ind("33.004", Utilz.len.b32);
        ushort pAmcSpeed_33_005 = Utilz.ind("33.005", Utilz.len.b32);
        ushort pAmcPosRef_34_003 = Utilz.ind("34.003", Utilz.len.b32);
        // ushort pAmcSpeedRef_34_006 =Utilz.ind("34.006", Utilz.len.b16);
        ushort pAmcSpeedRef_34_006 = Utilz.ind("34.006", Utilz.len.b32); //pazi da ne predje max speed ref - bice exception
        ushort pAmcRefSelect_34_007 = Utilz.ind("34.007", Utilz.len.b16);
        ushort pAmcProfileMaxSpeed_38_003 = Utilz.ind("38.003", Utilz.len.b32);
        ushort pAmcHomePos_40_004 = Utilz.ind("40.004", Utilz.len.b32);
        ushort pAmcHomeSwitch_40_012 = Utilz.ind("40.012", Utilz.len.b16);
        ushort pAmcHomePositiveLimit_40_013 = Utilz.ind("40.013", Utilz.len.b16);
        ushort pAmcEnable_41_001 = Utilz.ind("41.001", Utilz.len.b16);
        ushort pAmcStatus_41_002 = Utilz.ind("41.002", Utilz.len.b16);
        ushort pAmcSoftlimEnable_41_025 = Utilz.ind("41.026", Utilz.len.b16);
        ushort pAmcSoftlimPos_41_026 = Utilz.ind("41.026", Utilz.len.b32);
        ushort pAmcSoftlimNeg_41_027 = Utilz.ind("41.027", Utilz.len.b32);

    }
}
