﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Utilz
    {
        public static byte[] SubArray(byte[] data, int index, int length)
        {
            byte[] result = new byte[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        public static byte[] appendBytes(byte[] source, byte[] destination)
        {
            int i = destination.Length;
            Array.Resize<byte>(ref destination, i + source.Length);
            source.CopyTo(destination, i);

            return destination;
        }
        public static ushort ind(string s, len bits)
        {
            string[] split = s.Split('.');
            int menu = Convert.ToInt32(split[0]);
            int item = Convert.ToInt32(split[1]);
            return (ushort)((menu * 100) + (item - 1) + (bits == len.b32 ? 16384 : 0));
        }
        public enum len { b16, b32 }

        public static void SetBit(ref ushort b, int bitNumber, bool state)
        {
            //if (bitNumber < 0 || bitNumber > 15)//throw an Exception or return
            if (state) b |= (ushort)(1 << bitNumber);
            else { int i = b; i &= ~(1 << bitNumber); b = (ushort)i; }
        }

        public static bool GetBit(ushort b, int bitNumber)
        {
            //if (bitNumber < 0 || bitNumber > 15)   //throw an Exception or just return false
            return (b & (1 << bitNumber)) > 0;
        }
    }
}
