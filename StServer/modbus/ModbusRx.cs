﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAlertHB;
using System.Windows;
using System.Net.Sockets;
using System.Windows.Threading;

namespace Server
{
    class ModbusRx : Rx
    {

       // private ModBusClient modbusClient;
        ModbusIpMaster master;
        DispatcherTimer dt2 = new DispatcherTimer();
        String _masterAdsdress;

       
        ushort test = Utilz.ind("00.060", Utilz.len.b32);      

        public ModbusRx(Counters c, ModbusIpMaster m,string iPAddress) : base(c) {
            // modbusClient = m;
            master = m;
            _masterAdsdress = iPAddress.Substring(iPAddress.LastIndexOf(".")+1, iPAddress.Length -1 - iPAddress.LastIndexOf(".") );
        }

      

        public override void CanRxLoop()
        {
            dt2.Interval = TimeSpan.FromMilliseconds(300);
            dt2.Tick += Dt_Tick2;
            dt2.Start();
        }

        private async void Dt_Tick2(object sender, EventArgs e)
        {
           
            // read from regulator
            byte[] statuses = new byte[40];

            // number of points 
            // we are asking for 10 memory locations, since we are requsting 32bit read, because test is converted to 32bit ushort,
            // we have to read two mem locations for each parametar , so 10 params --> 20 mem locations --> 40 bytes            
            statuses = await master.ReadHoldingRegistersAsyncBytes(test, 20);

            /// adding motor tcp address
            byte[] address = BitConverter.GetBytes(Convert.ToUInt16(_masterAdsdress));
           statuses = Utilz.appendBytes(address, statuses);
           
            //send to client
            _client.SendTo(statuses, statuses.Length, SocketFlags.None, _ipepClient);      // darko:proveriti sta treba da se filtrira




           
        }
    }
}
