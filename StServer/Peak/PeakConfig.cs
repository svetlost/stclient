﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPCANHandle = System.Byte;
using System.Windows;
using LogAlertHB;
using System.Diagnostics;
namespace Server
{
    public class PeakConfig : Configs
    {

        TPCANStatus stsResult;
        public static List<byte> hanldeList = new List<byte>();
        public static List<byte> syncList = new List<byte>();

        string s;

        #region Members
        /// <summary>
        /// Saves the handle of a PCAN hardware
        /// </summary>
        public static TPCANHandle PcanHandle(int channel)
        {
            switch (channel)
            {
                //case 1: return Convert.ToByte("41", 16);
                //case 2: return Convert.ToByte("42", 16);
                case 0: return Convert.ToByte("41", 16);//public const TPCANHandle PCAN_PCIBUS1 = 0x41;201708071100
                case 1: return Convert.ToByte("42", 16);
                case 2: return Convert.ToByte("43", 16);
                case 3: return Convert.ToByte("44", 16);
                default: return 0;


            }

        }
        /// <summary>
        /// Saves the baudrate register for a conenction
        /// </summary>
        private TPCANBaudrate Baudrate(uint baud)
        {
            switch (baud)
            {
                case 1000000: return TPCANBaudrate.PCAN_BAUD_1M;
                case 500000: return TPCANBaudrate.PCAN_BAUD_500K;
                case 250000: return TPCANBaudrate.PCAN_BAUD_250K;
                case 125000: return TPCANBaudrate.PCAN_BAUD_125K;
                case 100000: return TPCANBaudrate.PCAN_BAUD_100K;
                case 50000: return TPCANBaudrate.PCAN_BAUD_50K;
                case 20000: return TPCANBaudrate.PCAN_BAUD_20K;
                case 10000: return TPCANBaudrate.PCAN_BAUD_10K;
                case 5000: return TPCANBaudrate.PCAN_BAUD_5K;
                default: return TPCANBaudrate.PCAN_BAUD_500K;
            }

        }
        #endregion

        #region Methods

        #endregion

        public PeakConfig()
        {
            s = "PeakConfig init: channels=" + CanChannelConfigurationList.Count + "; ";
            try
            {
                foreach (CanChannelConfiguration ch in CanChannelConfigurationList)
                {
                    if (ch.Enabled == true)
                    {
                        stsResult = PCANBasic.Initialize(PcanHandle(ch.Channel), Baudrate(ch.BitRate));
                        s += string.Format("\n channel={0} baudrate={1} initResult={2}", ch.Channel, ch.BitRate, stsResult);
                        // stsResult = PCANBasic.Initialize(PcanHandle(ch.Channel), Baudrate(ch.BitRate), TPCANType.PCAN_TYPE_ISA, 0100, 3);
                        hanldeList.Add(PcanHandle(ch.Channel));
                    }
                    if (ch.SendSync == true) syncList.Add(PcanHandle(ch.Channel));
                }

            }
            catch (Exception e1)
            {
                Log.Write(" FAILED PEAKCAN INIT! " + e1.Message, EventLogEntryType.Error);
                AlertLogic.Add(e1.Message);
                //Log.Write(e1.Message);
            }
            CanDriverInitialized = true;
            Log.Write(s, EventLogEntryType.Information);
        }

        public override void CloseCanDriver()
        {
            AlertLogic.Add("CAN driver closed.");
            s = "PeakConfig close driver: channels=" + CanChannelConfigurationList.Count + "; ";

            foreach (CanChannelConfiguration ch in CanChannelConfigurationList)
            {
                if (ch.Enabled == true)
                {
                    PCANBasic.Uninitialize(PcanHandle(ch.Channel));
                    s += string.Format("\n channel={0} baudrate={1} closeResult={2}", ch.Channel, ch.BitRate, stsResult);
                }
                Log.Write(s, EventLogEntryType.Information);
            }
        }



    }
}

