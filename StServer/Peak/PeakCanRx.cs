﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using LogAlertHB;

namespace Server
{


    public class PeakCanRx : Rx
    {

        private delegate void ReadDelegateHandler();

        TPCANStatus stsResult;
        private System.Threading.AutoResetEvent m_ReceiveEvent = new System.Threading.AutoResetEvent(false);
        private ReadDelegateHandler m_ReadDelegate;
        UInt32 iBuffer;


        private string GetFormatedError(TPCANStatus error)
        {
            StringBuilder strTemp;

            // Creates a buffer big enough for a error-text
            //
            strTemp = new StringBuilder(256);
            // Gets the text using the GetErrorText API function
            // If the function success, the translated error is returned. If it fails,
            // a text describing the current error is returned.
            //
            if (PCANBasic.GetErrorText(error, 0, strTemp) != TPCANStatus.PCAN_ERROR_OK)
                return string.Format("An error occurred. Error-code's text ({0:X}) couldn't be retrieved", error);
            else
                return strTemp.ToString();
        }


        public PeakCanRx(Counters c) : base(c) { m_ReadDelegate = new ReadDelegateHandler(Receive); }

        public override void CanRxLoop()
        {

            iBuffer = Convert.ToUInt32(m_ReceiveEvent.SafeWaitHandle.DangerousGetHandle().ToInt32());
            // Sets the handle of the Receive-Event.
            //

            for (byte i = 0; i < PeakConfig.hanldeList.Count; i++)
            {
                stsResult = PCANBasic.SetValue(PeakConfig.hanldeList[i], TPCANParameter.PCAN_RECEIVE_EVENT, ref iBuffer, sizeof(UInt32));

                if (stsResult != TPCANStatus.PCAN_ERROR_OK)
                {
                    AlertLogic.Add("Error" + GetFormatedError(stsResult));
                    //Log.Write("Error" + GetFormatedError(stsResult));
                    return;
                }
            }

            while (true)
            {
               
                Receive();

            }

        }

        private void Receive()
        {
            // note: this thread is destroyed by MAIN

            for (byte i = 0; i < PeakConfig.hanldeList.Count; i++)
            {
                TPCANMsg CANMsg;

                // wait for hardware events
                waitResult = (WaitResults)WaitForSingleObject((int)iBuffer, Properties.Settings.Default.CanRxWaitForSingleObjectInterval);

                // if event occured...
                if (waitResult != WaitResults.WAIT_TIMEOUT)
                {
                    stsResult = TPCANStatus.PCAN_ERROR_OK;

                    while (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                    {
                        stsResult = PCANBasic.Read(PeakConfig.hanldeList[i], out CANMsg);

                        if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                        {
                            counters.CanRx_xlStatusSuccess++;

                            if (CANMsg.ID == 0) // skip sync and NMT message
                            {
                                break;
                            }

                            if (CANMsg.MSGTYPE == TPCANMessageType.PCAN_MESSAGE_STANDARD)
                            {

                                byte[] can_Msg_ID = BitConverter.GetBytes(CANMsg.ID);
                                CANMsg.DATA.CopyTo(dataReceived, 0);
                                dataReceived[8] = (byte)(i + 1);//..chanIndex;
                                dataReceived[9] = can_Msg_ID[0];
                                dataReceived[10] = can_Msg_ID[1];


                                try
                                {
                                    _client.SendTo(dataReceived, dataReceived.Length, SocketFlags.None, _ipepClient);      // darko:proveriti sta treba da se filtrira
                                    counters.UdpTxOk++;
                                    timer = 0;
                                }
                                catch (SocketException sockEx)
                                {
                                    counters.UdpTxError++;
                                    if (timer < 1)
                                    {
                                        AlertLogic.Add("Message Send Failed :" + sockEx.ErrorCode.ToString()
                                                           + "\nMessage Send Failed :" + sockEx.Message
                                                           + "\nMessage Send Failed :" + sockEx.StackTrace);
                                        timer++;
                                    }
                                    else
                                    {
                                        AlertLogic.Add("Message Sending Stopped");
                                        return;
                                    }
                                }

                                catch (Exception ex)
                                {
                                    counters.UdpTxError++;
                                    AlertLogic.Add("Message Send Failed :" + ex.Message);
                                }


                            }
                        }
                        else//can rx error
                        {
                            if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                                counters.CanRx_xlStatusError++;
                        }
                    }

                }
            }
        }
    }
}