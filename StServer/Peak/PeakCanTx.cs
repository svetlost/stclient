﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using ML;
using LogAlertHB;
using System.Collections;
using System.Threading;
using System.Diagnostics;
//using StClient;

namespace Server
{
    class PeakCanTx : Tx
    {
        TPCANStatus stsResult;
        //IEnumerable<byte> listofChannels;

        

        //public PeakCanTx(List<MotorLogic> motorsList, Counters c) : base(motorsList, c)
        public PeakCanTx(Counters c) : base(c)
        {
            //listofChannels = motorsList.Select(qq => qq.CanChannel).Distinct();

            try
            {

                SenderTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.CanTxInterval);
                SenderTimer.Tick += new EventHandler(CanTxLoop);
                SenderTimer.Start();
            }


            catch (Exception ex)
            {
                AlertLogic.Add("Message send failed" + ex.Message);
            }

            AlertLogic.Add("Sync started.");
            SyncTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval);
            SyncTimer.Tick += new EventHandler(SyncTx);
            SyncTimer.Start();
        }



        public void CanTxLoop(object sender, EventArgs e)
        {
            TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];

            while (_server.Available > 0)
            {
                if (_server.Available > Properties.Settings.Default.UdpBufferOverflowSize)
                    AlertLogic.Add("Receive socket buffer full.");

                try
                {
                    _server.ReceiveFrom(dataToSendToCAN, ref Remote);
                    counters.UdpRxOk++;
                }
                catch
                {
                    counters.UdpRxError++;
                }


                CANMsg.ID = BitConverter.ToUInt16(dataToSendToCAN, 10);
                CANMsg.LEN = dataToSendToCAN[9];
                CANMsg.DATA[0] = dataToSendToCAN[0];
                CANMsg.DATA[1] = dataToSendToCAN[1];
                CANMsg.DATA[2] = dataToSendToCAN[2];
                CANMsg.DATA[3] = dataToSendToCAN[3];
                CANMsg.DATA[4] = dataToSendToCAN[4];
                CANMsg.DATA[5] = dataToSendToCAN[5];
                CANMsg.DATA[6] = dataToSendToCAN[6];
                CANMsg.DATA[7] = dataToSendToCAN[7];
                CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;
                stsResult = PCANBasic.Write(PeakConfig.PcanHandle(dataToSendToCAN[8]), ref CANMsg);

                if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                    counters.CanTx_xlStatusSuccess++;
                else
                    counters.CanTx_xlStatusError++;

            }
        }

        //public override void canStopAll()
        //{

        //    //try
        //    //{


        //    //ovde mora da se salje svih osam zbog marsaling Exceptiona na dati .. ako se stavio manje od 8 javlja gresku na heartbeat klasi ali je excepton odavde 
        //    foreach (MotorLogic motorLogic in _motorsList)
        //    {
        //        TPCANMsg CANMsg = new TPCANMsg();
        //        CANMsg.ID = Convert.ToUInt16(CalculateAddress(motorLogic.CanChannel) + motorLogic.CanIpAddress);
        //        CANMsg.DATA = new byte[8];
        //        CANMsg.LEN = 8;
        //        CANMsg.DATA[0] = 0;
        //        CANMsg.DATA[1] = 0;
        //        CANMsg.DATA[2] = 0;
        //        CANMsg.DATA[3] = 0;
        //        CANMsg.DATA[4] = 0;
        //        CANMsg.DATA[5] = 0;
        //        CANMsg.DATA[6] = 0;
        //        CANMsg.DATA[7] = 0;
        //        CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;
        //        stsResult = PCANBasic.Write(PeakConfig.PcanHandle(motorLogic.CanChannel), ref CANMsg);
        //    }

        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    Debug.WriteLine(ex.Message);
        //    //}

        //    foreach (MotorLogic motorLogic in _motorsList)
        //    {
        //        TPCANMsg CANMsg = new TPCANMsg();
        //        CANMsg.DATA = new byte[8];
        //        CANMsg.ID = Convert.ToUInt16(CalculateAddress(motorLogic.CanChannel) + motorLogic.SbcAddress);
        //        CANMsg.LEN = 8;
        //        CANMsg.DATA[0] = 0;
        //        CANMsg.DATA[1] = 0;
        //        CANMsg.DATA[2] = 0;
        //        CANMsg.DATA[3] = 0;
        //        CANMsg.DATA[4] = 0;
        //        CANMsg.DATA[5] = 0;
        //        CANMsg.DATA[6] = 0;
        //        CANMsg.DATA[7] = 0;
        //        CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

        //        stsResult = PCANBasic.Write(PeakConfig.PcanHandle(motorLogic.CanChannel), ref CANMsg);

        //    }
        //}

        int CalculateAddress(int CanChannel)
        {
            switch (CanChannel)
            {
                case 1:
                    return Properties.Settings.Default.Pdo1;
                case 2:
                    return Properties.Settings.Default.Pdo2;
                case 3:
                    return Properties.Settings.Default.Pdo3;
                default:
                    return 0;
            }
        }

        public void SyncTx(object sender, EventArgs e)
        {
            if (PeakConfig.syncList.Count == 0) return;

            TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];


            CANMsg.ID = SyncCanMsgID; // ID
            CANMsg.LEN = 0; //dlc
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

            foreach (byte channel in PeakConfig.syncList)
                stsResult = PCANBasic.Write(channel, ref CANMsg);

            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                counters.CanTx_xlStatusSuccess++;
            else
                counters.CanTx_xlStatusError++;
            //TODO whatif not success
        }

        public void SbcInit()
        {
            TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[2];

            CANMsg.ID = 00; // ID
            CANMsg.LEN = 2; //dlc
            CANMsg.DATA[0] = 1;
            CANMsg.DATA[1] = 0;
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

            foreach (byte channel in PeakConfig.hanldeList)
                stsResult = PCANBasic.Write(channel, ref CANMsg);

            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                counters.CanTx_xlStatusSuccess++;
            else
                counters.CanTx_xlStatusError++;
            //TODO whatif not success
        }
    }
}

