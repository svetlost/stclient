﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using LogAlertHB;



namespace Server
{
    


    public class Rx
    {
        #region DLL Import
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern int WaitForSingleObject(int handle, int timeOut);
        #endregion

        public enum WaitResults : int
        {
            WAIT_OBJECT_0 = 0x0,
            WAIT_ABANDONED = 0x80,
            WAIT_TIMEOUT = 0x102,
            INFINITE = 0xFFFF,
            WAIT_FAILED = 0xFFFFFFF
        }

        public Socket _client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        public IPEndPoint _ipepClient;
        //AlertsLB _lbAlerts;
        public static byte[] dataReceived = new byte[Properties.Settings.Default.CanOverUdpMessageLength];
        public int timer = 0;
        public WaitResults waitResult = new WaitResults();                 // result values of WaitForSingleObject       

      


        public Counters counters;

        public void CloseClientSocket()
        {
            AlertLogic.Add("Client socket closed.");
            _client.Close();
        }

        public Rx(Counters c , Object o = null)
        {

            _ipepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP), Properties.Settings.Default.ClientPort); //ovo je klijentova ip adresa i port na kome ceka!!!!!      
            counters = c;
        }


        // message receiving from can overrided by vector or peak
        public virtual void CanRxLoop() {  }


    }
}

