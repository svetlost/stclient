﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogAlertHB;
using vxlapi_NET20;
//using StClient;
using System.Windows.Threading;
using System.Diagnostics;



namespace Server
{
    public class VectorCanTx : Tx
    {
        XLDriver drive = VectorConfig.App;
        XLClass.XLstatus xlStatus = XLClass.XLstatus.XL_SUCCESS;

       

        //public VectorCanTx(List<StClient.MotorLogic> motorsList, Counters c) : base(motorsList, c)
        public VectorCanTx(Counters c) : base(c)
        {
            try
            {

                SenderTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.CanTxInterval);
                SenderTimer.Tick += new EventHandler(CanTxLoop);
                SenderTimer.Start();
            }


            catch (Exception ex)
            {
                AlertLogic.Add("Message send failed" + ex.Message);
            }

            AlertLogic.Add("Sync started.");
            SyncTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval);
            SyncTimer.Tick += new EventHandler(SyncTx);
            SyncTimer.Start();
            //SyncTimer.Start();//darko sklonio 20101211, plc salje sync. sync moze da se ukljuci pritiskom na CanSync
        }



        public void CanTxLoop(object sender, EventArgs e)
        {
            while (_server.Available > 0)
            {
                if (_server.Available > Properties.Settings.Default.UdpBufferOverflowSize)
                    AlertLogic.Add("Receive socket buffer full.");

                try
                {
                    _server.ReceiveFrom(dataToSendToCAN, ref Remote);
                    counters.UdpRxOk++;
                }
                catch
                {
                    counters.UdpRxError++;
                }

                XLClass.xl_event xlEvent = new XLClass.xl_event();
                xlEvent.tagData.can_Msg.id = BitConverter.ToUInt16(dataToSendToCAN, 10);
                //xlEvent.tagData.can_Msg.id = (uint) dataToSendToCAN[10]*256+dataToSendToCAN[11];

                //int iii = BitConverter.ToUInt16(dataToSendToCAN, 10);
                //if (iii < 10) Debug.WriteLine(string.Format("{0} {1}", DateTime.Now.ToLongTimeString(), iii));


                xlEvent.tagData.can_Msg.dlc = dataToSendToCAN[9];
                xlEvent.tagData.can_Msg.data[0] = dataToSendToCAN[0];
                xlEvent.tagData.can_Msg.data[1] = dataToSendToCAN[1];
                xlEvent.tagData.can_Msg.data[2] = dataToSendToCAN[2];
                xlEvent.tagData.can_Msg.data[3] = dataToSendToCAN[3];
                xlEvent.tagData.can_Msg.data[4] = dataToSendToCAN[4];
                xlEvent.tagData.can_Msg.data[5] = dataToSendToCAN[5];
                xlEvent.tagData.can_Msg.data[6] = dataToSendToCAN[6];
                xlEvent.tagData.can_Msg.data[7] = dataToSendToCAN[7];
                xlEvent.tag = (byte)XLClass.XLeventType.XL_TRANSMIT_MSG;
                xlStatus = drive.XL_CanTransmit(VectorConfig.portHandle, Convert.ToUInt32(1 << dataToSendToCAN[8]), xlEvent);
                //xlStatus = drive.XL_CanTransmit(Configs.portHandle, Convert.ToUInt32(1 << dataToSendToCAN[8]-1), xlEventCollection);
                if (xlStatus == XLClass.XLstatus.XL_SUCCESS)
                    counters.CanTx_xlStatusSuccess++;
                else
                    counters.CanTx_xlStatusError++;
                //TODO
            }
        }

        //public override void canStopAll()
        //{
        //    foreach (MotorLogic motorLogic in _motorsList)
        //    {
        //        XLClass.xl_event xlEvent = new XLClass.xl_event();

        //        xlEvent.tagData.can_Msg.id = Convert.ToUInt16(CalculateAddress(motorLogic.CanChannel) + motorLogic.CanIpAddress);
        //        xlEvent.tagData.can_Msg.dlc = 8;
        //        xlEvent.tagData.can_Msg.data[0] = 4;
        //        xlEvent.tagData.can_Msg.data[1] = 0;
        //        xlEvent.tagData.can_Msg.data[2] = 0;
        //        xlEvent.tagData.can_Msg.data[3] = 0;
        //        xlEvent.tagData.can_Msg.data[4] = 0;
        //        xlEvent.tagData.can_Msg.data[5] = 0;
        //        xlEvent.tagData.can_Msg.data[6] = 0;
        //        xlEvent.tagData.can_Msg.data[7] = 0;
        //        xlEvent.tag = (byte)XLClass.XLeventType.XL_TRANSMIT_MSG;
        //        xlStatus = drive.XL_CanTransmit(VectorConfig.portHandle, Convert.ToUInt32(1 << motorLogic.CanChannel), xlEvent);

        //    }
        //    foreach (MotorLogic motorLogic in _motorsList)
        //    {
        //        XLClass.xl_event xlEvent = new XLClass.xl_event();
        //        xlEvent.tagData.can_Msg.id = Convert.ToUInt16(CalculateAddress(motorLogic.CanChannel) + motorLogic.CanIpAddress);
        //        // xlEvent.tagData.can_Msg.id = Convert.ToUInt16(CalculateAddress(motorLogic.CanChannel) + motorLogic.SecBrkControlAddress);
        //        xlEvent.tagData.can_Msg.dlc = 8;
        //        xlEvent.tagData.can_Msg.data[0] = 0;
        //        xlEvent.tagData.can_Msg.data[1] = 0;
        //        xlEvent.tagData.can_Msg.data[2] = 0;
        //        xlEvent.tagData.can_Msg.data[3] = 0;
        //        xlEvent.tagData.can_Msg.data[4] = 0;
        //        xlEvent.tagData.can_Msg.data[5] = 0;
        //        xlEvent.tagData.can_Msg.data[6] = 0;
        //        xlEvent.tagData.can_Msg.data[7] = 0;
        //        xlEvent.tag = (byte)XLClass.XLeventType.XL_TRANSMIT_MSG;
        //        xlStatus = drive.XL_CanTransmit(VectorConfig.portHandle, Convert.ToUInt32(1 << motorLogic.CanChannel), xlEvent);

        //    }
        //}

        int CalculateAddress(int CanChannel)
        {
            switch (CanChannel)
            {
                case 1:
                    return Properties.Settings.Default.Pdo3;
                case 2:
                    return Properties.Settings.Default.Pdo2;
                case 3:
                    return Properties.Settings.Default.Pdo3;
                default:
                    return 0;
            }
        }

        public void SyncTx(object sender, EventArgs e)
        {
            if (VectorConfig.sendSyncAccessMask == 0) return;

            XLClass.xl_event xlEvent = new XLClass.xl_event();

            xlEvent.tagData.can_Msg.id = SyncCanMsgID; // ID
            xlEvent.tagData.can_Msg.dlc = 0; //dlc
            xlEvent.tag = (byte)XLClass.XLeventType.XL_TRANSMIT_MSG;

            xlStatus = drive.XL_CanTransmit(VectorConfig.portHandle, VectorConfig.sendSyncAccessMask, xlEvent);
            //Console.WriteLine("sync "+xlStatus.ToString());
            if (xlStatus == XLClass.XLstatus.XL_SUCCESS)
                counters.CanTx_xlStatusSuccess++;
            else
            {
                counters.CanTx_xlStatusError++;
                //Console.WriteLine(string.Format("{0} sync tx error {1}", DateTime.Now.ToLongTimeString(), xlStatus.ToString()));

            }//TODO whatif not success
        }

        public void SbcInit()
        {
            XLClass.xl_event xlEvent = new XLClass.xl_event();

            xlEvent.tagData.can_Msg.id = 00; // ID
            xlEvent.tagData.can_Msg.dlc = 2; //dlc
            xlEvent.tagData.can_Msg.data[0] = 1;
            xlEvent.tagData.can_Msg.data[1] = 0;
            xlEvent.tag = (byte)XLClass.XLeventType.XL_TRANSMIT_MSG;

            xlStatus = drive.XL_CanTransmit(VectorConfig.portHandle, VectorConfig.sendSyncAccessMask, xlEvent);
            if (xlStatus == XLClass.XLstatus.XL_SUCCESS)
                counters.CanTx_xlStatusSuccess++;
            else
                counters.CanTx_xlStatusError++;
            //TODO whatif not success
        }
    }
}
