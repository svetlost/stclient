﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using vxlapi_NET20;
using LogAlertHB;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;

namespace Server
{



    public class VectorCanRx : Rx
    {


        public VectorCanRx(Counters c) : base(c) { }

        public override void CanRxLoop()
        {
            XLClass.xl_event receivedEvent = new XLClass.xl_event();            // create new object containing received data       
            XLClass.XLstatus xlStatus = XLClass.XLstatus.XL_SUCCESS;       // result of XL driver func. calls


            // note: this thread is destroyed by MAIN
            while (true)
            {
                // wait for hardware events
                waitResult = (WaitResults)WaitForSingleObject(VectorConfig.eventHandle, Properties.Settings.Default.CanRxWaitForSingleObjectInterval);


                // if event occured...
                if (waitResult != WaitResults.WAIT_TIMEOUT)
                {
                    // ...init xlStatus first
                    xlStatus = XLClass.XLstatus.XL_SUCCESS;
                    counters.WaitForSingleObject++;

                    // afterwards: while hw queue is not empty...
                    while (xlStatus != XLClass.XLstatus.XL_ERR_QUEUE_IS_EMPTY)
                    {
                        // ...reveice data from hardware...
                        xlStatus = VectorConfig.App.XL_Receive(VectorConfig.portHandle, ref receivedEvent);

                        //  if receiving succeed....
                        if (xlStatus == XLClass.XLstatus.XL_SUCCESS)
                        {
                            counters.CanRx_xlStatusSuccess++;


                            if (receivedEvent.tagData.can_Msg.id == 0) // skip sync and NMT message
                            {
                                break;
                            }
                            // ...and data was Rx msg...
                            if (receivedEvent.tag == (byte)XLClass.XLeventType.XL_RECEIVE_MSG)
                            {
                                // ...check various flags
                                if ((receivedEvent.tagData.can_Msg.flags & (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME) == (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME)
                                {
                                    //TODO: how to handle
                                    counters.ErrorFrame++;
                                    counters.RxError = receivedEvent.tagData.chipState.rxErrorCounter;
                                    counters.TxError = receivedEvent.tagData.chipState.txErrorCounter;
                                    
                                }

                                else if ((receivedEvent.tagData.can_Msg.flags & (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME) == (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME)
                                {
                                    //TODO: how to handle
                                }

                                else if ( // filter various messages
                                    ((receivedEvent.tagData.can_Msg.flags & (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_OVERRUN) == 0) &&
                                    ((receivedEvent.tagData.can_Msg.flags & (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_NERR) == 0) &&
                                    ((receivedEvent.tagData.can_Msg.flags & (ushort)XLClass.XLmessageFlags.XL_CAN_MSG_FLAG_TX_REQUEST) == 0))
                                {
                                    //Convert recived data to byte Array sutable for sending to client
                                    byte[] can_Msg_ID = BitConverter.GetBytes(receivedEvent.tagData.can_Msg.id);
                                    receivedEvent.tagData.can_Msg.data.CopyTo(dataReceived, 0);
                                    dataReceived[8] = receivedEvent.chanIndex;
                                    dataReceived[9] = can_Msg_ID[0];
                                    dataReceived[10] = can_Msg_ID[1];

                                    // send to client
                                    try
                                    {
                                        _client.SendTo(dataReceived, dataReceived.Length, SocketFlags.None, _ipepClient);      // darko:proveriti sta treba da se filtrira
                                        counters.UdpTxOk++;
                                        timer = 0;
                                    }
                                    catch (SocketException sockEx)
                                    {
                                        counters.UdpTxError++;
                                        if (timer < 1)
                                        {
                                            AlertLogic.Add("Message Send Failed :" + sockEx.ErrorCode.ToString()
                                                               + "\nMessage Send Failed :" + sockEx.Message
                                                               + "\nMessage Send Failed :" + sockEx.StackTrace);
                                            timer++;
                                        }
                                        else
                                        {
                                            AlertLogic.Add("Message Sending Stopped");
                                            return;
                                        }
                                    }

                                    catch (Exception ex)
                                    {
                                        counters.UdpTxError++;
                                        AlertLogic.Add("Message Send Failed :" + ex.Message);
                                    }

                                }
                            }
                        }
                        else//can rx error
                        {
                            if (xlStatus != XLClass.XLstatus.XL_ERR_QUEUE_IS_EMPTY)
                            {
                               Trace.WriteLine(string.Format("{0} rx error {1}", DateTime.Now.ToLongTimeString(), xlStatus.ToString()));
                                counters.CanRx_xlStatusError++;
                            }
                        }
                    }
                }
                else
                    counters.WaitForSingleObjectTimeout++;
                // no event occured
            }
        }
    }
}
