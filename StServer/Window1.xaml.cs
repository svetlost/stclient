﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Diagnostics;
using System.Windows.Threading;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Reflection;
using LogAlertHB;
using Server.modbus;

namespace Server
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private System.Windows.Forms.NotifyIcon TippuTrayNotify;
        private System.Windows.Forms.ContextMenuStrip ctxTrayMenu;
        //AlertsListView _alv = new AlertsListView(1000);
        Rx _toClient;
        Configs _config;
        Tx _toServer;
        private Thread _rxThread;
        public Counters counters = new Counters();
        DispatcherTimer _updateAlertView = new DispatcherTimer();
        //IPEndPoint IpEpClientHeartBeat = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP), Properties.Settings.Default.ClientHeartbeatPort);
        //IPEndPoint IpEpServerHeartBeat = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerIP), Properties.Settings.Default.ServerHeartbeatPort);
        //IPEndPoint IpEpClientHeartBeat1 = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP1), Properties.Settings.Default.ClientHeartbeatPort1);
        //IPEndPoint IpEpServerHeartBeat1 = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerIP), Properties.Settings.Default.ServerHeartbeatPort1);
        //Heartbeat hb, hb1;
        static Alarm_LV _alv = new Alarm_LV();
        private ModBusClient modbusClient;

        public Window1()
        {
            InitializeComponent();

            modbusClient = new ModBusClient();
            modbusClient.ConnectedChanged += new ModBusClient.ConnectedChangedHandler(UpdateConnectedChanged);

            try
            {
                if (modbusClient.Connected)
                    modbusClient.Disconnect();

                modbusClient.IPAddress = "192.168.40.78";
                modbusClient.Port = 502;
                modbusClient.Connect();

            }
            catch (Exception exc)
            {
                AlertLogic.Add(exc.Message + "Unable to connect to Modbus Server");
            }


            DateTime now1;
            int deltaT_ms = 0, a = 0;
            int temp=0,m=0,max=0,avgP=0,avg=0,prekosto=0,brojanje=0;
            byte[] statuses = new byte[40];



            #region MINIMIZE TO TRAY
            //Create an instance of the NotifyIcon Class
            TippuTrayNotify = new System.Windows.Forms.NotifyIcon();

            // This icon file needs to be in the bin folder of the application
            TippuTrayNotify.Icon = Properties.Resources.ikona_server_000;
            //new System.Drawing.Icon(Environment.CurrentDirectory + @"\Config\" + "App.ico");

            //show the Tray Notify Icon
            TippuTrayNotify.Visible = true;

            //Create a object for the context menu
            ctxTrayMenu = new System.Windows.Forms.ContextMenuStrip();

            //Add the Menu Item to the context menu

            System.Windows.Forms.ToolStripMenuItem mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            mnuExit.Text = "Exit";
            mnuExit.Click += new EventHandler(mnuExit_Click);

            System.Windows.Forms.ToolStripMenuItem mnuRestore = new System.Windows.Forms.ToolStripMenuItem();
            mnuRestore.Text = "Restore";
            mnuRestore.Click += new EventHandler(mnuRestore_Click);

            ctxTrayMenu.Items.Add(mnuRestore);
            ctxTrayMenu.Items.Add(mnuExit);

            //Add the Context menu to the Notify Icon Object
            TippuTrayNotify.ContextMenuStrip = ctxTrayMenu;
            #endregion

            //canvasXaml.Children.Add(_alv);
            canvasXaml.Children.Add(_alv);

            _updateAlertView.Tick += _updateAlertView_Tick;
            _updateAlertView.Interval = TimeSpan.FromMilliseconds(500);
            _updateAlertView.Start();



            AlertLogic.Add("Server Log started.");

            ServerLeft.DataContext = counters;

            if (modbusClient.Connected)
            {

                ModbusIpMaster master = ModbusIpMaster.CreateIp(modbusClient.tcpClient);
                _toClient = new ModbusRx(counters, master, modbusClient.IPAddress);
                _toServer = new ModbusTx(counters, master);

            }
            /*        while (m < 1000)
                    {
                        {
                            now1 = DateTime.Now;
                            //for (int a = 1; a < 100; a++) master.WriteMultipleRegisters(41, new ushort[] { 0, (ushort)(1000 + a) });
                            //for (a = 1; a < 1000; a++) send(a);
                            for (a = 1; a < 50; a++)
                            {
                                _toServer.sendS(a, temp);
                                deltaT_ms = (int)(DateTime.Now - now1).TotalMilliseconds;
                                AlertLogic.Add(brojanje + ". -- " + deltaT_ms.ToString() + "  -- ms");
                                if (deltaT_ms > 100) { prekosto++; }
                                if (max < deltaT_ms) { max = deltaT_ms; }
                                avg += deltaT_ms;
                                brojanje++;
                            }
                            m++;
                        }


                    }
                    AlertLogic.Add("PREKO 100 ms: " + prekosto.ToString());
                    AlertLogic.Add("MAX: " + max.ToString() );
                    AlertLogic.Add("AVG: " + (avg/(m*50)).ToString());*/
            else
            {

                if (Properties.Settings.Default.IsVectorCardWorking)
                {
                    _config = new VectorConfig();
                    //_toServer = new VectorCanTx(_config.MotorsConfigurationList, counters);
                    _toServer = new VectorCanTx(counters);

                }
                else
                {
                    _config = new PeakConfig();
                    //_toServer = new PeakCanTx(_config.MotorsConfigurationList, counters);
                    _toServer = new PeakCanTx(counters);
                }
            }
            if (modbusClient.Connected)  // if (_config.CanDriverInitialized)
            {
                //hb = new Heartbeat(IpEpServerHeartBeat, "Server", IpEpClientHeartBeat, "Client", stopingAll);
                //hb1 = new Heartbeat(IpEpServerHeartBeat1, "Server", IpEpClientHeartBeat1, "Client1", stopingAll);
                //if (hb.UDPInitialized && hb1.UDPInitialized)
                {
                    //hb.StartServer();
                    //hb1.StartServer();

               /*     if (Properties.Settings.Default.IsVectorCardWorking)

                        _toClient = new VectorCanRx(counters);
                    else
                        _toClient = new PeakCanRx(counters);
*/

                    ThreadStart threadDelegate = new System.Threading.ThreadStart(_toClient.CanRxLoop);
                    _rxThread = new System.Threading.Thread(threadDelegate);
                    _rxThread.IsBackground = true;
                    _rxThread.Name = "stRxThread";
                    _rxThread.Start();

                    //_rxThread = new Thread(new ThreadStart(_toClient.CanRxLoop));
                    //_rxThread.Start();

                    // _alv.Add("Secundary Break Control initialization started.");
                    // _toServer.SbcInit();

                }
                //else
                //    AlertLogic.Add("UDP initialization failed.");
            }
            else
            {
                AlertLogic.Add("Can Driver initialization failed.");
                Log.Write("Can Driver initialization failed. Closing driver.", EventLogEntryType.Error);
                _config.CloseCanDriver();
            }

        }

       

        private void _updateAlertView_Tick(object sender, EventArgs e)
        {
            if (AlertLogic.AlertsGet.Count == alertsCountPrevious) return;

            alertsCountPrevious = AlertLogic.AlertsGet.Count;
            _alv.LV2.ItemsSource = AlertLogic.AlertsGet;
        }

        //public void stopingAll()
        //{
        //    AlertLogic.Add("Stop all motors clicked");
        //    Log.Write("Stop all motors clicked.", EventLogEntryType.Information);

        //    _toServer.canStopAll();
        //}
        void mnuRestore_Click(object sender, EventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        void mnuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //mnuExit zove ovo 
        protected override void OnClosed(EventArgs e)
        {
            TippuTrayNotify.Visible = false;
            base.OnClosed(e);
        }

        //static ObservableCollection<Alerts> _alerts = new ObservableCollection<Alerts>();
        static int alertsCountPrevious;


        private void UpdateConnectedChanged(object sender)
        {
            if (modbusClient.Connected)
            {
                AlertLogic.Add("Connected to Server");
            }
            else
            {
                AlertLogic.Add("Not Connected to Server");

            }
        }



        //private void StopAll_Click(object sender, RoutedEventArgs e)
        //{
        //    _toServer.canStopAll();
        //}

        //private void SyncSend_Click(object sender, RoutedEventArgs e)
        //{
        //    string s = "CanSync ";
        //    if (CanTx.SyncTimer.IsEnabled == false)
        //    {
        //        CanTx.SyncTimer.Start();
        //        SyncSend.Content = "Stop Can Sync";
        //        s += "stopped.";
        //    }
        //    else
        //    {
        //        CanTx.SyncTimer.Stop();
        //        SyncSend.Content = "Start Can Sync";
        //        s += "started.";
        //    }
        //    AlertLogic.Add(s);
        //    Log.Write(s, EventLogEntryType.Information);
        //}



    }







}
