﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Server
{
    public class Counters : INotifyPropertyChanged
    {
        int udpRxOk = 0;
        public int UdpRxOk { get { return udpRxOk; } set { if (udpRxOk != value) { udpRxOk = value; OnNotify("UdpRxOk"); } } }

        int udpTxOk = 0;
        public int UdpTxOk { get { return udpTxOk; } set { if (udpTxOk != value) { udpTxOk = value; OnNotify("UdpTxOk"); } } }

        int udpTxError = 0;
        public int UdpTxError { get { return udpTxError; } set { if (udpTxError != value) { udpTxError = value; OnNotify("UdpTxError"); } } }

        int udpRxError = 0;
        public int UdpRxError { get { return udpRxError; } set { if (udpRxError != value) { udpRxError = value; OnNotify("UdpRxError"); } } }

        int canRx_xlStatusError = 0;
        public int CanRx_xlStatusError { get { return canRx_xlStatusError; } set { if (canRx_xlStatusError != value) { canRx_xlStatusError = value; OnNotify("CanRx_xlStatusError"); } } }

        int canTx_xlStatusError = 0;
        public int CanTx_xlStatusError { get { return canTx_xlStatusError; } set { if (canTx_xlStatusError != value) { canTx_xlStatusError = value; OnNotify("CanTx_xlStatusError"); } } }

        int canRx_xlStatusSuccess = 0;
        public int CanRx_xlStatusSuccess { get { return canRx_xlStatusSuccess; } set { if (canRx_xlStatusSuccess != value) { canRx_xlStatusSuccess = value; OnNotify("CanRx_xlStatusSuccess"); } } }

        int canTx_xlStatusSuccess = 0;
        public int CanTx_xlStatusSuccess { get { return canTx_xlStatusSuccess; } set { if (canTx_xlStatusSuccess != value) { canTx_xlStatusSuccess = value; OnNotify("CanTx_xlStatusSuccess"); } } }

        int waitForSingleObject = 0;
        public int WaitForSingleObject { get { return waitForSingleObject; } set { if (waitForSingleObject != value) { waitForSingleObject = value; OnNotify("WaitForSingleObject"); } } }
        int waitForSingleObjectTimeout = 0;
        public int WaitForSingleObjectTimeout { get { return waitForSingleObjectTimeout; } set { if (waitForSingleObjectTimeout != value) { waitForSingleObjectTimeout = value; OnNotify("WaitForSingleObjectTimeout"); } } }
        int errorFrame = 0;
        public int ErrorFrame { get { return errorFrame; } set { if (errorFrame != value) { errorFrame = value; OnNotify("ErrorFrame"); } } }
        int rxError = 0;
        public int RxError { get { return rxError; } set { if (rxError != value) { rxError = value; OnNotify("RxError"); } } }
        int txError = 0;
        public int TxError { get { return txError; } set { if (txError != value) { txError = value; OnNotify("TxError"); } } }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
